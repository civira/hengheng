<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(
    [
		'namespace' => 'Front\Heng\API',
		'as' 		=> 'f.heng.api.',
        'prefix'    => 'v1',
    ], function()
    {
        Route::get( 'search/lucky-draws', 'SearchController@indexLuckyDraws' );
        Route::get( 'search/redemptions', 'SearchController@indexRedemptions' );
        Route::get( 'search/free-gifts', 'SearchController@indexFreeGifts' );
        Route::get( 'search/brands', 'SearchController@indexBrands' );
        Route::get( 'search/retailers', 'SearchController@indexRetailers' );
        Route::get( 'search/free-samples', 'SearchController@indexFreeSamples' );

        Route::post( 'home/subscribe', 'EmailController@subscribeNewsletter' )->name( 'email.subscribe_newsletter' );

        Route::group(
            [
                'middleware' => [ 'cookie', 'start_session' ]
            ], function()
            {
                Route::post( 'self/favorite/update', 'SelfController@toggleFavorite' )->name( 'self.toggle_favorite' );
            });
    });

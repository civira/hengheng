<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
	[
		'namespace' => 'Front\Heng',
		'as' 		=> 'f.heng.',
	],
	function() {
		Route::get('/', 'HomeController@index' )->name('home');

		/*
			email verification link url
		 */
		Route::get( 'verify/member/{token}', [ 'as' => 'user.verify_member', 'uses' => 'UserController@verify_member' ]);
		Route::get( 'verify/newsletter/{token}', [ 'as' => 'user.verify_newsletter', 'uses' => 'UserController@verify_subscriber' ]);
		Route::get( 'verify/forgot-password/{token}', [ 'as' => 'user.verify_forgot_password', 'uses' => 'AuthController@verify_forgot_password' ]);

		Route::group(
			[
				'middleware' => [ 'auth.frontend.guest' ]
			],
		function()
		{
			/**
			 * AUTH
			 */
			Route::get( 'register', 'AuthController@register' )->name( 'auth.register' );
			Route::post( 'register', 'AuthController@store_register' )->name( 'auth.store_register' );

			/**
			 * request new verification
			 */
			Route::get( 'request/verification/{email?}', 'AuthController@request_verification' )->name( 'auth.request_verification' );


			Route::get( 'login', 'AuthController@login' )->name( 'auth.login' );
			Route::post( 'login', 'AuthController@auth' )->name( 'auth.auth' );

			Route::get( 'login/facebook', 'AuthController@login_facebook' )->name( 'auth.login_facebook' );

		});

		Route::group(
			[
				'middleware' => [ 'auth.frontend' ]
			],
		function()
		{
			/**
			 * AUTH
			 */
			Route::get( 'logout', 'AuthController@logout' )->name( 'auth.logout' );

			Route::get( 'update-password', 'AuthController@update_password' )->name( 'auth.update_password' );
			Route::get( 'success-password', 'AuthController@success_password' )->name( 'auth.success_password' );

			/**
			 * SELF - USER
			 */
			Route::get( 'self/dashboard', 'UserController@dashboard' )->name( 'users.dashboard' );
			Route::get( 'self/list/redemptions', 'UserController@redemption_list' )->name( 'users.redemption_list' );

			Route::get( 'self/delete/contest/{id}', 'UserController@delete_contest' )->name( 'users.delete_contest' );
			Route::get( 'self/contest/{status}', 'UserController@list_contests' )->name( 'users.list_contests' );

			Route::post( 'self/delete/contest', 'UserController@multi_delete_contest' )->name( 'users.multi_delete_contest' );

			Route::get( 'self/contest-summary', 'UserController@contest_summary' )->name( 'users.contest_summary' );

			Route::get( 'self/my-account', 'UserController@my_account' )->name( 'users.my_account' );
			Route::post( 'self/my-account', 'UserController@store_profile' )->name( 'users.my_account' );

			Route::get( 'self/resend/verification', 'UserController@resend_verification' )->name( 'users.resend_verification' );

			Route::get( 'self/receipt/upload/{id?}', 'UserController@multi_upload' )->name( 'users.multi_upload' );
			Route::post( 'self/receipt/upload', 'UserController@store_multi_upload' )->name( 'users.store_multi_upload' );
 
			Route::get( 'self/change-password', 'UserController@change_password' )->name( 'users.change_password' );
			Route::post( 'self/change-password', 'UserController@store_password' )->name( 'users.store_password' );

			Route::get( 'self/upload/success', 'UserController@page_success_upload' )->name( 'users.page_success_upload' );

			/**
			 * all address member
			 */
			Route::get( 'self/address', 'UserController@address_page' )->name( 'users.address_page' );
			Route::get( 'self/address/default/{id}', 'UserController@address_default_store' )->name( 'users.address_default_store' );

			Route::get( 'self/address/edit/{id?}', 'UserController@address_edit_page' )->name( 'users.address_edit' );
			Route::post( 'self/address/edit/{id?}', 'UserController@address_store' )->name( 'users.address_store' );

			Route::get( 'self/address/delete/{id?}', 'UserController@address_delete' )->name( 'users.address_delete' );

			/*
				upload data
			 */
			Route::post( 'members/upload/photo', 'UserController@photo_upload' )->name( 'users.photo_upload' );
			Route::post( 'members/upload/receipt/{id}', 'UserController@receipt_upload' )->name( 'users.receipt_upload' );


            /**
             * promo codes - after login
             */
            Route::get( 'promo-codes/participate/{id}', 'PromoCodeController@participate' )->name( 'promo_codes.participate' );
            Route::post( 'promo-codes/participate/{id}', 'PromoCodeController@participate' )->name( 'promo_codes.participate' );

			Route::get( 'promo-codes/downloads/{id}', 'PromoCodeController@download_file' )->name( 'promo_codes.download' );

            /**
             * promo codes - for type B with form and receipt upload
             */
			Route::get( 'self/promo-codes/list', 'UserController@promo_code_list' )->name( 'users.promo_code_list' );


				/**
				 * success page
				 */
				Route::get( 'self/promo-codes/redeem/success', 'PromoCodeController@success_redeem' )->name( 'promo_codes.success_redeem' );
	            Route::get( 'self/promo-codes/upload/success', 'PromoCodeController@success_upload' )->name( 'promo_codes.success_upload' );

	            /**
	             * store redeem
	             */
				Route::get( 'self/promo-codes/redeem/{id}', 'PromoCodeController@store_redeem' )->name( 'promo_codes.store_redeem' );

	            /**
	             * upload page
	             */
	            Route::get( 'self/promo-codes/upload/{id?}', 'PromoCodeController@multi_upload' )->name( 'promo_codes.multi_upload' );
	            Route::post( 'self/promo-codes/upload', 'PromoCodeController@store_multi_upload' )->name( 'promo_codes.store_multi_upload' );


	            Route::get( 'self/promo-codes/receipts/{id}', 'UserController@promo_codes_receipt_list' )->name( 'users.promo_codes_receipt_list' );
				Route::post( 'self/promo-codes/delete', 'UserController@promo_code_delete_participation' )->name( 'users.promo_code_delete_participation' );

			/**
			 * lucky draw subscribe & participation
			 */
			Route::get( 'lucky-draws/participate/{id}', 'LuckyDrawController@participation' )->name( 'lucky.participation' );
			Route::post( 'lucky-draws/subscribe/{id}', 'LuckyDrawController@subscribe_member' )->name( 'lucky.subscribe' );
            Route::get( 'lucky-draws/receipts/{lucky_draw_id}', 'LuckyDrawController@receipt_list' )->name( 'lucky.receipts' );

            /**
             * redemption participation
             */

			Route::get( 'redemptions/participate/{id}', 'RedemptionController@participate' )->name( 'redemptions.participate' );
			Route::post( 'redemptions/participate/{id}', 'RedemptionController@participate' )->name( 'redemptions.participate' );


			Route::get( 'self/redemptions/summary', 'UserController@redemptions_summary' )->name( 'users.redemptions_summary' );
			Route::get( 'self/promo-codes/summary', 'UserController@promo_codes_summary' )->name( 'users.promo_codes_summary' );
			Route::get( 'self/free-samples/summary', 'UserController@free_samples_summary' )->name( 'users.free_samples_summary' );

			/**
			 * redemption - success
			 */
			Route::get( 'self/redemptions/redeem/success', 'RedemptionController@success_redeem' )->name( 'redemptions.success_redeem' );
			Route::get( 'self/redemptions/redeem/{id}', 'RedemptionController@store_redeem' )->name( 'redemptions.store_redeem' );

			Route::get( 'self/redemptions/upload/success', 'RedemptionController@success_upload' )->name( 'redemptions.success_upload' );


			Route::get( 'self/redemptions/upload/{id?}', 'RedemptionController@multi_upload' )->name( 'redemptions.multi_upload' );
			Route::post( 'self/redemptions/upload', 'RedemptionController@store_multi_upload' )->name( 'redemptions.store_multi_upload' );

            Route::get( 'self/redemptions/receipts/{id}', 'RedemptionController@receipt_list' )->name( 'users.redemption_receipt_list' );


			Route::post( 'self/redemptions/delete', 'UserController@redemption_delete_participation' )->name( 'users.redemption_delete_participation' );


			/**
			 * free sample participation
			 */
			// Route::get( 'redemptions/participate/{id}', 'RedemptionController@participate' )->name( 'redemptions.participate' );
			Route::post( 'free-samples/participate/{id}', 'FreeSampleController@participate' )->name( 'free_samples.participate' );

			Route::post( 'free-samples/request/select-store/{id}', 'FreeSampleController@request_select_store' )->name( 'free_samples.request_select_store' );
			Route::post( 'free-samples/request/new-store/{id}', 'FreeSampleController@request_new_store' )->name( 'free_samples.request_new_store' );

			Route::get( 'free-samples/request/success', 'FreeSampleController@request_success_page' )->name( 'free_samples.request_success' );

			Route::get( 'free-samples/request/{id}', 'FreeSampleController@request_page' )->name( 'free_samples.request' );


            Route::get( 'self/free-samples/request/{id}', 'UserController@free_sample_request_list' )->name( 'users.free_samples_request_list' );


		});

		Route::get( 'forget-password', 'AuthController@forget_password' )->name( 'auth.forget_password' );
		Route::post( 'forget-password', 'AuthController@mail_forget_password' )->name( 'auth.mail_forget_password' );
		Route::post( 'reset-password', 'AuthController@reset_password' )->name( 'auth.reset_password' );

		Route::get( 'success-password', 'AuthController@success_password' )->name( 'auth.success_password' );

		Route::get( 'qr', 'ContestController@qr' )->name( 'contests.qr' );

		/**
		 * CONTESTS
		 */
		Route::get( 'contests', 'ContestController@index' )->name( 'contests.index' );

		/**
		 * CATEGORIES
		 */
		Route::get( 'categories', 'CategoryController@index' )->name( 'categories.index' );

		/**
		 * PROMO CODES
		 */
		Route::get( 'promo-codes', 'PromoCodeController@index' )->name( 'promo_codes.index' );
        Route::get( 'promo-codes/detail/{id}', 'PromoCodeController@detailOld' )->name( 'promo_codes.detail.old' );
		Route::get( 'promo-codes/{id}/{slug}', 'PromoCodeController@detail' )->name( 'promo_codes.detail' );


		/**
		 * BRANDS
		 */
		Route::get( 'brands', 'BrandController@index' )->name( 'brands.index' );
		Route::get( 'brand/product/{brand_slug}/{id}/{slug}', 'BrandController@product_detail' )->name( 'brands.product_detail' );
        Route::get( 'brand/detail/{id}/{slug?}', 'BrandController@detailOld' )->name( 'brands.detail.old' );
		Route::get( 'brand/{id}/{slug}', 'BrandController@list' )->name( 'brands.list' );

		/**
		 * RETAILERS
		 */
		Route::get( 'retailers', 'RetailerController@index' )->name( 'retailers.index' );

		Route::get( 'retailer/lucky-draw/list/{id}', 'RetailerController@lucky_list' )->name( 'retailers.lucky_list' );
		Route::get( 'retailer/redemption/list/{id}', 'RetailerController@redemption_list' )->name( 'retailers.redemption_list' );
		Route::get( 'retailer/free-gift/list/{id}', 'RetailerController@gift_list' )->name( 'retailers.gift_list' );

        Route::get( 'retailer/detail/{id}', 'RetailerController@detailOld' )->name( 'retailers.detail.old' );
		Route::get( 'retailer/{id}/{slug}', 'RetailerController@detail' )->name( 'retailers.detail' );

		/**
		 * LUCKY DRAWS
		 */
		Route::get( 'lucky-draws', 'LuckyDrawController@index' )->name( 'lucky.index' );

		Route::get( 'lucky-draws/winners/{slug}', 'LuckyDrawController@winner_list' )->name( 'lucky.winner_list' );
        Route::get( 'lucky-draws/detail/{id}/{slug?}', 'LuckyDrawController@detailOld')->name( 'lucky.detail.old' );
		Route::get( 'lucky-draws/{brand}/{slug}', 'LuckyDrawController@detail' )->name( 'lucky.detail' );


		/**
		 * LUCKY DRAWS
		 */
		Route::get( 'free-samples', 'FreeSampleController@index' )->name( 'free_samples.index' );
		Route::get( 'free-samples/{brand}/{slug}', 'FreeSampleController@detail' )->name( 'free_samples.detail' );

		/**
		 * REDEMPTIONS
		 */
		Route::get( 'redemptions', 'RedemptionController@index' )->name( 'redemptions.index' );
        Route::get( 'redemptions/detail/{id}/{slug?}', 'RedemptionController@detailOld' )->name( 'redemptions.detail.old' );
		Route::get( 'redemptions/{brand}/{slug}', 'RedemptionController@detail' )->name( 'redemptions.detail' );

		/**
		 * FREE GIFTS
		 */
		Route::get( 'free-gifts', 'GiftController@index' )->name( 'gifts.index' );
        Route::get( 'free-gifts/detail/{id}/{slug?}', 'GiftController@detailOld' )->name( 'gifts.detail.old' );
		Route::get( 'free-gifts/{brand}/{slug}', 'GiftController@detail' )->name( 'gifts.detail' );

		/**
		 * information
		 */
		Route::get( 'article/{slug?}', 'InformationController@index' )->name( 'information.index' );

		/**
		 * CONTESTS
		 */
		Route::get( 'contact-us', 'ContactController@index' )->name( 'contact.index' );
		Route::post( 'contact-us', 'ContactController@store' )->name( 'contact.store' );

		/**
		 * SCAN QR
		 */
		Route::get( 'scan-qr', 'QRController@index' )->name( 'qr.index' );
	});

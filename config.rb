# Require any additional compass plugins here.
require "rgbapng"

# Set this to the root of your project when deployed:
http_path = "/"
sass_dir = "./resources/assets/sass"
css_dir = "./public/css"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :expanded

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

# Enable source map
sourcemap = true
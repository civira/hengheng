function initOwlCarousel( $carousel )
{
	var data  = $carousel.data();

	data.items    = typeof data.items === "undefined"? 1 : data.items;
	data.autoplay = typeof data.autoplay === "undefined"? false : data.autoplay;

    if( typeof data.evtInitialized !== "undefined" &&
        typeof window[ data.evtInitialized ] === "function" )
    {
        data.onInitialized = window[ data.evtInitialized ];
    }

    if( typeof data.evtInitialized !== "undefined" &&
        typeof window[ data.evtInitialized ] === "function" )
    {
        data.onInitialized = window[ data.evtInitialized ];
    }

    if( typeof data.evtTranslated !== "undefined" &&
        typeof window[ data.evtTranslated ] === "function" )
    {
        data.onTranslated = window[ data.evtTranslated ];
    }

	if( data.items > 1 )
	{
		if( data.sizes )
		{
			data.responsive = {};

			for( var i = 0, len = data.sizes.length; i < len; i++ )
			{
				var s = data.sizes[ i ];
				data.responsive[ s[ 0 ] ] =
				{
					items : s[ 1 ]
				};
			}
		}
	}


	if( data.nav )
	{
		data.navText =
		[
			'<span class="ico arrow left">‹</span>',
			'<span class="ico arrow right">›</span>'
		];
	}

	$carousel.owlCarousel( data );
}

$(function()
{
	var $carousels = $( ".owl-carousel:not(.customized)" ),
		i, len;
	for( i = 0, len = $carousels.length; i < len; i++ )
	{
		var $carousel = $carousels.eq( i );

		initOwlCarousel( $carousel );
	}

	var $carousels2 = $( ".owl-carousel.customized" ),
		i2, len2;
	$carousels2.owlCarousel({
		nav : true,
		dots : false,
		loop : false,
		autoplay : false,
		items : 3,
    	kresponsiveClass:true,
		responsive : 
		{
			0 : { items : 1 },
			320 : { items : 1 },
			720 : { items : 2 },
			960 : { items : 3 }
		}
	})

	$('.hgi-arrow').click(function() {
	    $carousels2.trigger('next.owl.carousel');
	})

	/**
	 * G.C
	 */
	i = len = $carousels = null;

});

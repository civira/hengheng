/*FB.logout(function(response) {
});*/

var permissions_needed = [ "email", "public_profile" ];
var login_dialog_opened = false;

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    login_dialog_opened = false;
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {

        // Logged into your app and Facebook.
        testAPI();
    } else {
        loginFB();

    }
}

function loginFB() {
    FB.login(function(response)
    {
        login_dialog_opened = true;
        if (response.status === 'connected')
        {
            // Logged into your app and Facebook.
                testAPI();
        } else {
            // The person is not logged into this app or we are unable to tell.
            document.getElementById('status_error').innerHTML = 'Please log ' +
            'into this app.';
        }
    },
    {
        scope: permissions_needed.join( "," ),
        auth_type: "rerequest"
    });
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

window.fbAsyncInit = function()
{
    FB.init(
    {
        appId      : '396645080868248',
        cookie     : false,  // enable cookies to allow the server to access
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v3.1' // use graph api version 2.8
    });

  // Now that we've initialized the JavaScript SDK, we call
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.
};

$(".ct-facebook-button" ).on( "click", function()
{
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
});

// Load the SDK asynchronously
(function(d, s, id)
{
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);

}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');

    FB.api('/me?fields=name,email', function(response)
    {
        if( typeof response.email === "undefined" )
        {
            /* https://stackoverflow.com/questions/27642037/facebook-javascript-sdk-rerequest-permissions-doesnt-prompt */
            permissions_needed = [ "email" ];
            alert( "Email information is mandatory for our lucky draw contest. Please allow the email permission as intended.");

            if( login_dialog_opened == false )
            {
                loginFB();
            }
        }
        else
        {
            window.location.replace('https://www.shopnwin.sg/login/facebook?email='+response.email+'&name='+response.name);
        }
    });
}

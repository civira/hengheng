
function openMobileNav() {
    $( "#st_mobile_menu" ).css(
    {
        "-webkit-transform" : "translateX(0)",
        "transform" : "translateX(0)"
    });
    // document.getElementById("st_mobile_menu").style.width = "275px";
}

function closeMobileNav() {
    $( "#st_mobile_menu" ).css(
    {
        "-webkit-transform" : "",
        "transform" : ""
    });
    // document.getElementById("st_mobile_menu").style.width = "0";
}


function submitCategoryForm()
{
	var $form = $( "#ct_filter_form" );
	$form.submit();
	if( hasLoadingOverlay() )
	{
		showLoadingOverlay();
	}
}

function showLoadingOverlay()
{
	$.LoadingOverlay( "show" );
}

function hideLoadingOverlay()
{
	$.LoadingOverlay( "hide" );
}

function hasLoadingOverlay()
{
	return ( typeof $.LoadingOverlaySetup !== "undefined" );
}

function isAPISuccess( resp )
{
    if( resp.status === "SUCCESS" )
    {
        return true;
    }

    return false;
}

$(function()
{
    $(".prevent_chars").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

	$("#tabs_terms_and_conditions").hide();
     $(".ct-tabs-menu").on('click', 'a', function(e){
         e.preventDefault();

	    $('.active-tab').removeClass('active-tab');
	    $(this).closest('li a').addClass('active-tab');

         $('#ct_tabs_container .panel-article').hide();
         $($(this).attr("href")).show();
     });


    $(".cpm-tabs").click(function(){
        $(".cpm-routes").toggle();
    });

    $( "#smmba_trigger" ).on( "click", function()
    {
        var $self    = $( this ),
            $wrapper = $( ".smmba-wrapper" );

        if( $wrapper.is(":visible") )
        {
            $self.removeClass( "shown" );
            $wrapper.slideUp( 300 );
        }
        else
        {
            $self.addClass( "shown" );
            $wrapper.slideDown( 300 );
        }
    });

	/**
	 * FILTER
	 */
	var $control = $( ".controls" );
	if( $control.length > 0 )
	{
		$( ".control-sort" ).on( "change", submitCategoryForm );
	}

	/**
	 * jQuery Type Ahead
	 * @see http://www.runningcoder.org/jquerytypeahead/demo/
	 */
	$.typeahead({
	    input: ".js-typeahead",
	    order: "asc",
        dynamic: true,
        delay : 500,
		maxItemPerGroup : 5,
		group : {
				template : "{{group}}"
		},
		backdrop : {
			backgroundColor : "#000",
			opacity : "0.4",
			filter : "alpha(opacity=40)"
		},
        emptyTemplate : "no result for {{query}}",
	    source: {
	        "Lucky Draw": {
	            display : "label",
	            ajax: {
					type: "GET",
	                url: st_base_url + "api/v1/search/lucky-draws",
					path: "data",
                    data : {
                        q: "{{query}}"
                    }
	            }
	        },
			"Redemption": {
				display : "label",
				ajax: {
					type: "GET",
	                url: st_base_url + "api/v1/search/redemptions",
					path: "data",
                    data : {
                        q: "{{query}}"
                    }
	            }
			},
			"Free Gift": {
				display : "label",
				ajax: {
					type: "GET",
	                url: st_base_url + "api/v1/search/free-gifts",
					path: "data",
                    data : {
                        q: "{{query}}"
                    }
	            }
			},
			"Brand": {
				display : "label",
				ajax: {
					type: "GET",
	                url: st_base_url + "api/v1/search/brands",
					path: "data",
                    data : {
                        q: "{{query}}"
                    }
	            }
			},
			"Retailer": {
				display : "label",
				ajax: {
					type: "GET",
	                url: st_base_url + "api/v1/search/retailers",
					path: "data",
                    data : {
                        q: "{{query}}"
                    }
	            }
			},
            "Free Sample": {
                display : "label",
                ajax: {
                    type: "GET",
                    url: st_base_url + "api/v1/search/free-samples",
                    path: "data",
                    data : {
                        q: "{{query}}"
                    }
                }
            }
	    },
	    callback: {
			onClickAfter : function( node, a, item, evt )
			{
				evt.preventDefault();

				var redirect = item.url;

				switch( item.group ) {
					case "Lucky Draw":
						self.document.location = redirect;
						break;
					case "Redemption":
						self.document.location = redirect;
						break;
					case "Free Gift":
						self.document.location = redirect;
						break;
					case "Brand":
						self.document.location = redirect;
						break;
					case "Retailer":
						self.document.location = redirect;
						break;
                    case "Free Sample":
                        self.document.location = redirect;
                        break;    
				}
			}
	    }
	});

	/**
	 * Ripple Effect
	 */
    const showEvent = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 'touchstart' : 'mousedown';
    const hideEvent = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 'touchend' : 'mouseup';
    $(document).on(showEvent, '[ripple]', function(e){
        if (e.button == 2){
            return false
        }
    	$ripple = $('<span class="ripple-effect" />'),
    	$button = $(this),
    	$offset = $button.offset(),
    	xPos = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) && 'touches' in e ? ( e.touches[0].pageX - $offset.left ) : (e.pageX - $offset.left),
    	yPos = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) && 'touches' in e ? ( e.touches[0].pageY - $offset.top ) : (e.pageY - $offset.top),
    	$color = $button.data('ripple-color') || $button.css('color'),
    	scaledSize = Math.max( $button.width() , $button.height()) * Math.PI * 1.5;
    	$ripple.css({
    		'top': yPos,
    		'left': xPos,
    		'background-color': $color,
            opacity: .175
    	}).appendTo( $button ).animate({
    		'height': scaledSize,
    		'width': scaledSize,
    	}, 700/3*2)
        $(document).on(hideEvent, $button, function (e) {
            $ripple.animate({
                'opacity': 0
            }, 700/3, function () {
                $(this).remove()
            })
        })
    })

	/**
	 * Double Column Same Height
	 */
	// $( window ).resize(function()
	// {
	// 	var $first_column = $( ".first-twin-column" );
	// 	$first_column.siblings( ".second-twin-column" ).each( function( i, o )
	// 	{
	// 		$(o).height( $first_column.height());
	// 	});

	// 	var $st_calc_content = $( ".st-calc-content" );
	// 	if( $st_calc_content.length > 0 )
	// 	{
	// 		var h_calc_outer  = $( ".st-calc-outer" ).outerHeight( true );
	// 		var h_sum = 0;
	// 		$( "div[class*='st-calc-']" ).each(function( i, o )
	// 		{
	// 			if( $(o).hasClass( "st-calc-content" ) || $(o).hasClass( "st-calc-outer" ) )
	// 			{
	// 				return;
	// 			}

	// 			h_sum += $(o).outerHeight( true );
	// 		});

	// 		$( ".st-calc-content" ).height( h_calc_outer - h_sum );
	// 	}
	// }).resize();

	/**
	 * Select 2
	 */
	var i, len = 0;

	var $select2s = $( ".site-select2" );
	for( i = 0, len = $select2s.length; i < len; i++ )
	{
		var $select2 = $select2s.eq( i ),
			data 	 = $select2.data();

		data.containerCssClass = data.class;
		data.dropdownCssClass = data.openClass;

		if( data.noSearch ) {
			data.minimumResultsForSearch = -1
		}

		$select2.select2( data );
	}

	/**
	 * Loading Overlay
	 */

	if( hasLoadingOverlay() )
	{
		$.LoadingOverlaySetup({
			imageColor : "#555555",
			textResizeFactor : 0.2,
			textColor : "#555555",
			imageResizeFactor : 2.5
		});
	}

    /**
     * Simple AJAX
     */
    var $ajax_forms = $( ".st-ajax-form" );
    for( i = 0, len = $ajax_forms.length; i < len; i++ )
    {
        var $form = $ajax_forms.eq( i ),
            data  = $form.data(),

        	tag_name = $form.prop( "tagName" ).toLowerCase();

         function simpleAjaxEvent(e)
         {
            showLoadingOverlay();

            if( typeof window[ data.beforeSent ] === "function" )
            {
                window[ data.beforeSent ]();
            }

            var url = "",
                ajax_data = null;

            if( tag_name == "form" )
            {
            	url  = $form.attr( "action" );
                ajax_data = $form.serialize();
            }
            else
            {
            	url  = $form.attr( "href" );
                ajax_data = {};

                var $datas = $form.find( ".staf-data" );
                for( var j = 0, len_j = $datas.length; j < len_j; j++ )
                {
                    var $data = $datas.eq( j );
                    $.each( $data.data(), function( key, val )
                    {
                        ajax_data[ key ] = val;
                    });
                }
            }

            $.ajax(
            {
                url    : url,
                method : "POST",
                data   : ajax_data
            })

            .done(function( resp )
            {
                if( resp.status === "FAILED" )
                {
                    if( typeof window[data.onFailed] === "function" )
                    {
                        window[data.onFailed]( resp );
                    }
                }
                else
                {
                    if( typeof window[data.onSuccess] === "function" )
                    {
                        window[data.onSuccess]( resp );
                    }
                }
            })

            .fail(function( resp )
            {
                if( typeof window[data.onFailed] === "function" )
                {
                    window[data.onFailed]( resp );
                }
            })

            .always(function()
            {
                hideLoadingOverlay();
            });

            e.preventDefault();
            return false;
         }


        if( tag_name == "form" )
        {
        	$form.on( "submit", simpleAjaxEvent );
        }
        else
        {
        	$form.on( "click", simpleAjaxEvent );
        }
    }


	/**
	 * G.C
	 */
	i = len = $select2s = $ajax_forms = null;
});

<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'heng_db_members';

    const PROMO_SMS_ENABLE = 1;
    const PROMO_SMS_DISABLE = 0;

    const PROMO_EMAIL_ENABLE = 1;
    const PROMO_EMAIL_DISABLE = 0;

    use Notifiable;

    use Traits\StandardTrait,
        Traits\FileTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * route
     */
    public function routeToContestUpload()
    {
        return route( 'f.heng.users.multi_upload' );
    }

    public function routeToRedemptionUpload()
    {
        return route( 'f.heng.redemptions.multi_upload' );
    }

    public function routeToPromoCodeUpload()
    {
        return route( 'f.heng.promo_codes.multi_upload' );
    }


    public function isFrontendAllowed()
    {
        return true;
    }

    public function isVerified()
    {
        if( $this->is_verified )
        {
            return true;
        }

        return false;

    }

    public function isBanned()
    {
        return false;
    }


    /**
     * Function
     */
   public function profile_img()
    {
        $img = $this->file( 'user_photo' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

    public function isProfilePartialFilled()
    {
        if( empty($this->full_name) ||
            empty($this->phone_number) )
        {
            return false;
        }

        return true;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Information extends BaseModel
{
	protected $table = 'heng_db_informations';

	static $STATUS_PUBLISHED = 'published';
    static $STATUS_YES        = 'YES';

    /**
     * SCOPES
     */
    public function scopePublished( Builder $q )
    {
    	return $q->whereIsStatus( self::$STATUS_PUBLISHED );
    } 

    public function scopeShowArticle( Builder $q )
    {
        return $q->whereIsArticle( self::$STATUS_YES );
    } 

    public function scopeShowFooter( Builder $q )
    {
    	return $q->whereIsFooter( self::$STATUS_YES );
    } 

}


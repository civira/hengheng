<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

use App\Models\Brand;
use App\Models\Retailer;

use Carbon\Carbon;

class LuckyDraw extends BaseModel
{
	protected $table = 'heng_db_lucky_draws';

	static $STATUS_PUBLISHED        = 'YES';
    static $STATUS_FEATURED         = 'YES';
    static $STATUS_FEATURED_NO      = 'NO';

    static $STATUS_ONGOING        = 'ongoing';
    static $STATUS_AWAITING       = 'awaiting';
    static $STATUS_WINNER         = 'winner';


	use Traits\StandardTrait,
        Traits\SEOTrait,
		Traits\FileTrait;

    /**
     * RELATIONSHIP
     */
    public function lucky_draw_retailer()
    {
        return $this->hasMany( LuckyDrawRetailer::class );
    }

    /**
     * ROUTE URL
     */

    public function detailUrl()
    {
        $db_brand = Brand::find( $this->brand_id );

        return route( 'f.heng.lucky.detail', [  $db_brand->slug, $this->slug ] );
    }

    public function winnerUrl()
    {
        return route( 'f.heng.lucky.winner_list',  $this->slug );
    }

    public function receiptListUrl()
    {
        return route( 'f.heng.lucky.receipts', $this->id );
    }

    /**
     * SCOPES
     */

    public function scopeStatusOngoing( Builder $q )
    {
        return $q->whereStatus( self::$STATUS_ONGOING );
    }

    public function scopePublished( Builder $q )
    {
        return $q->whereIsPublished( self::$STATUS_PUBLISHED );
    }

    public function scopeFeatured( Builder $q )
    {
        return $q->whereIsFeatured( self::$STATUS_FEATURED );
    }

    public function scopeNotFeatured( Builder $q )
    {
        return $q->whereIsFeatured( self::$STATUS_FEATURED_NO );
    }

    public function scopeValidContest( Builder $q )
    {
        $now_time  = new Carbon;
        
        return $q->where('end_date', '>', $now_time )->published()->statusOngoing();
    }

    /**
     * FUNCTIONS
     */
    public function isOngoingContest()
    {
        return $this->status == 'ongoing';
    }

    public function statusAwaitingContest()
    {
        return $this->status == self::$STATUS_AWAITING ;
    }

    public function statusWinnerContest()
    {
        return $this->status == self::$STATUS_WINNER ;
    }

    public function isWinnerAnnounced()
    {
        return $this->status != 'ongoing';
    }

    public function detailRetailer()
    {
        return Retailer::find( $this->retailer_id );
    }

    public function detailBrand()
    {
        return Brand::find( $this->brand_id );
    }

    public function file_img()
    {
        $img = $this->file( 'lucky_draw_image' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

    public function isDateExpired()
    {
        $today      = new Carbon;
        $endDate    = Carbon::createFromFormat('Y-m-d H:i:s', $this->end_date );

        if( $today->greaterThan($endDate))
        {
            return true;
        }

        return false;
    }

}


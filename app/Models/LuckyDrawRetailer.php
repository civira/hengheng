<?php

namespace App\Models;

use App\Models\LuckyDraw;
use App\Models\Retailer;

class LuckyDrawRetailer extends BaseModel
{
	protected $table = 'heng_db_lucky_draw_retailers';

    /*
        information
     */
    public function detailRetailer()
    {
        return $this->belongsTo( Retailer::class, 'retailer_id' );
    }

    public function detailLuckyDraw()
    {
    	return LuckyDraw::whereId( $this->lucky_draw_id )->published()->first();
    }

}


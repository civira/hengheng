<?php

namespace App\Models;

use App\Models\Promotion;

use App\Jobs\SendRedemptionAdminNotify;

use Illuminate\Database\Eloquent\Builder;

class MemberPromotionMap extends BaseModel
{
	protected $table = 'heng_db_member_promotion_maps';

    /**
     * routes
     */
    public function receiptListUrl()
    {
        return route( 'f.heng.users.redemption_receipt_list', $this->id );
    }
	/**
	 * STATIC
	 */
	static $STATUS_YES = 'YES';
    static $STATUS_NO  = 'NO';

    /**
     * status on map 
     1. awaiting
     2. verified
     3. rejected
     */

	/**
	 * 	FUNCTION
	 */
    public function detailPromotion()
    {
    	return Promotion::find( $this->promotion_id );
    }

    public function promotion()
    {
    	return $this->belongsTo( Promotion::class );
    }

    public function isParticipateNo()
    {
        if( $this->is_participate == 'NO' )
        {
            return true;
        }

        return false;
    }

    public function isParticipateYES()
    {
        if( $this->is_participate == 'YES' )
        {
            return true;
        }

        return false;
    }

    /*
        SCOPES
     */
    public function scopeParticipated( Builder $q )
    {
        return $q->whereIsParticipate( self::$STATUS_YES );
    }

    public function scopeRequestRedeem( Builder $q )
    {
        return $q->whereIsParticipate( self::$STATUS_NO );
    }

    public function sendRedemptionAdminNotify( $data )
    {
        $job = new SendRedemptionAdminNotify( $data );
        $job->handle();
    }

    /**
     * drop down
     */
    
    public static function getFilterSelection()
    {
        $selection = 
        [
            'awaiting'  => 'Awaiting Verification',
            'activate' => 'Ongoing Activation',
            'rejected'  => 'Rejected'
        ];

        return $selection;
    }

}


<?php

namespace App\Models\Traits;

use Carbon\Carbon;

use DB;

trait TokenTrait
{
    /**
     * @var integer
     */
    protected $allowed_days = 30; //1 month

	public function deleteToken()
	{
		return DB::table( $this->table )
			->whereUserId( $this->user_id )
			->whereToken( $this->token )
			->delete();
	}

	public function getDifference()
	{
		$token_time = new Carbon( $this->created_at );
		$current    = Carbon::now();

		return $token_time->diffInDays( $current, false );
	}

	public function isExpired()
	{
		if( $this->getDifference() > $this->allowed_days )
		{
			return true;
		}

		return false;
	}

	protected static function getToken()
	{
		return hash_hmac('sha256', str_random(40), config('app.key'));
	}
}
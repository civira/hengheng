<?php

namespace App\Models;

use App\Jobs\SendInquiryJob;

class Inquiry extends BaseModel
{
	protected $table = 'heng_db_inquiries';

    public function sendInquiryNotification( $data )
    {
        $job = new SendInquiryJob( $data );
        $job->handle();
    }

}




<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

use Carbon\Carbon;

class Counterable extends BaseModel
{
    protected $table = 'heng_db_counterables';

	public static function counterable_views( $c_category, $c_id=null, $c_model_type=null, $c_name=null, $c_info=null )
	{
		/*
			today date
		 */
		$today = new Carbon;

		/*
			format date to y-m-d
		 */
	    $format_date = $today->format( 'Y-m-d' );

	    /*
	    	check if any current view counts in database 
	     */
		$db_count = Counterable::whereCategory( $c_category )
								->whereName( $c_name )
								->whereModelTypes( $c_model_type )
								->whereModelId( $c_id )
								->whereOptionalRoot( $c_info )
								->where( 'date', '=', $format_date )
								->orderBy( 'id', 'desc' )
								->first();

		/*
			if there is no view count
		 */
		if( !$db_count )
		{
			$db_count 				= new Counterable;
			$db_count->date 		= $format_date;
		}

		/*
			optional info record
		 */
		if( $c_id )
		{
			$db_count->model_id 	= $c_id;
		}

		if( $c_model_type )
		{
			$db_count->model_types 	= $c_model_type;
		}

		if( $c_info )
		{
			$db_count->optional_root 	= $c_info;
		}

		if( $c_name )
		{
			$db_count->name 	= $c_name;
		}

		/*
			fixed variable
		 */
		$db_count->category 	= $c_category;
		$db_count->counts 		+= 1;
		$db_count->save();

	}

}


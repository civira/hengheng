<?php

namespace App\Models;

use App\Jobs\SendRequestMail;

class MemberFreeSampleRequest extends BaseModel
{
	protected $table = 'heng_db_member_free_sample_requests';

    use Traits\RouteTrait;

    /**
     * status 3
        1. awaiting
        2. rejected
        3. delivered
     */

    public function free_sample()
    {
        return $this->belongsTo( FreeSample::class );
    }

    public function member_address()
    {
        return $this->belongsTo( MemberAddress::class );
    }

    /**
     *  route
     */
    public function sendMailRequest( $data )
    {
        $job = new SendRequestMail( $data );
        $job->handle();
    }


    /**
     * drop down
     */
    
    public static function getFilterSelection()
    {
        $selection = 
        [
            'awaiting'  => 'Awaiting Delivery',
            'rejected'  => 'Rejected',
            'delivered' => 'Delivered'
        ];

        return $selection;
    }

}


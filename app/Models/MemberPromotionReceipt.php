<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class MemberPromotionReceipt extends BaseModel
{
	protected $table = 'heng_db_member_promotion_receipts';

	use Traits\StandardTrait,
		Traits\FileTrait;

    static $STATUS_YES      = 'YES';
    static $STATUS_PENDING  = 'PENDING';

    public function promotion()
    {
        return $this->belongsTo( Promotion::class );
    }

    /**
     * ROUTE
     */
    public function routeToUploadedPage()
    {
        return route( 'f.heng.users.redemption_receipt_list', $this->promotion_id );
    }

    /**
     *  function
     */
    public function receipt_img()
    {
        $img = $this->file( 'user_promotion_receipt' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

    public function all_file_receipts()
    {
        $img = $this->file( 'user_promotion_receipt' )->get();

        if( $img )
        {
            return $img;
        }

        return null;
    }

    public function file_rename( $with_ext = false, $number = null )
    {
        $new_date = date("Ymd_His", strtotime($this->created_at ));

        $str = 'receipt_'.$new_date;

        if( isset( $number ))
        {
            $str .= "_$number";
        }

        if( $with_ext )
        {
            $image = $this->receipt_img();
            if( $image )
            {
                $ext = pathinfo( $image, PATHINFO_EXTENSION );
                $str .= ".$ext";
            }
        }

        return $str;
    }

}


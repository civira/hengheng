<?php

namespace App\Models;

class PromotionRetailer extends BaseModel
{
	protected $table = 'heng_db_promotion_retailers';

    /*
        information
     */
    public function detailRetailer()
    {
    	return $this->belongsTo( Retailer::class, 'retailer_id' );
    }
}


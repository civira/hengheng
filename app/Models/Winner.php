<?php

namespace App\Models;

use App\Models\User;

class Winner extends BaseModel
{
    protected $table = 'heng_db_winners';

    public function memberDetail()
    {
    	return User::find( $this->member_id );
    }
}


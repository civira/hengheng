<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Receipt extends BaseModel
{
	protected $table = 'heng_db_receipts';

    protected $morphClass = 'Heng\Db\Models\Receipt';

    static $STATUS_PENDING  = 'PENDING';

	use Traits\StandardTrait,
		Traits\FileTrait;

    /**
     * RELATIONSHIP
     */
    public function lucky_draw()
    {
        return $this->belongsTo( LuckyDraw::class );
    }

    /**
     * FUNCTIONS
     */
    public function receipt_img()
    {
        $img = $this->file( 'user_receipt' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

    public function all_file_receipts()
    {
        $img = $this->file( 'user_receipt' )->get();

        if( $img )
        {
            return $img;
        }

        return null;
    }

    public function detailLuckyDraw()
    {
        return LuckyDraw::find( $this->lucky_draw_id );
    }

    public function file_rename( $with_ext = false, $number = null )
    {
        $new_date = date("Ymd_His", strtotime($this->created_at ));

        $str = 'receipt_'.$new_date;

        if( isset( $number ))
        {
            $str .= "_$number";
        }

        if( $with_ext )
        {
            $image = $this->receipt_img();
            if( $image )
            {
                $ext = pathinfo( $image, PATHINFO_EXTENSION );
                $str .= ".$ext";
            }
        }

        return $str;
    }
}


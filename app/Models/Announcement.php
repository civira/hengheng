<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Announcement extends BaseModel
{
	protected $table = 'heng_db_announcements';

    static $STATUS_PUBLISHED        = 'YES';

    public function scopePublished( Builder $q )
    {
        return $q->whereIsPublished( self::$STATUS_PUBLISHED );
    }

}


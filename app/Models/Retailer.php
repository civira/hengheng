<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Retailer extends BaseModel
{
	protected $table = 'heng_db_retailers';

	static $STATUS_PUBLISHED = 'YES';

	use Traits\StandardTrait,
		Traits\FileTrait,
        Traits\SEOTrait;

    public function detailUrl()
    {
        return route( 'f.heng.retailers.detail', [ $this->id, $this->slug ]);
    }

    public function searchUrl()
    {
        return route( 'f.heng.retailers.detail', [ $this->id, $this->slug ] );
    }

    public function scopePublished( Builder $q )
    {
    	return $q->whereIsPublished( self::$STATUS_PUBLISHED );
    }

    public function file_img()
    {
        $img = $this->file( 'retailerImage' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

}


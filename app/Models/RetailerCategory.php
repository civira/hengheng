<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class RetailerCategory extends BaseModel
{
	protected $table = 'heng_db_retailer_categories';

	public function retailers()
	{
		return $this->hasMany( Retailer::class );
	}
}


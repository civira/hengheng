<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Favorite extends BaseModel
{
    protected $table = 'heng_db_member_favorites';

    private $join_brand = false;

    public function brand()
    {
        return $this->belongsTo( Brand::class, 'model_id', 'id'  );
    }

    public function scopeForBrand(Builder $q)
    {
        return $q->whereModelType( Brand::class );
    }

    public function scopeOrderByBrandName( Builder $q, $order = 'asc' )
    {
        if( !$this->join_brand )
        {
            $q->innerJoinBrand();
        }

        return $q->orderBy( Brand::getTableName() . '.name', $order );
    }

    public function scopeInnerJoinBrand( Builder $q )
    {
        $this->join_brand = true;

        $brand = Brand::getTableName();
        return $q->join( $brand, function( $join ) use ( $brand )
            {
                $brand_class = addslashes(Brand::class);

                $join->on( 'model_id', '=', "$brand.id" )
                     ->on( 'model_type', '=',  \DB::raw( "'$brand_class'" ) );
            });
    }

    public function scopeWhereBrandCategoryId( Builder $q, $id )
    {
        if( $this->join_brand )
        {
            $brand = Brand::getTableName();
            return $q->where( "$brand.brand_category_id", '=', $id );
        }
        else
        {
            return $q->whereHas( 'brand', function( $qq ) use ($id)
            {
                $qq->whereBrandCategoryId( $id );
            });
        }
    }
}


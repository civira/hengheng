<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class MemberPromoCodeReceipt extends BaseModel
{
    protected $table = 'heng_db_member_promo_code_receipts';

    use Traits\StandardTrait,
        Traits\FileTrait;

    static $STATUS_PENDING  = 'PENDING';

    /**
     * RELATIONSHIP
     */
    public function promo_code()
    {
        return $this->belongsTo( PromoCode::class );
    }

    /**
     * FUNCTIONS
     */
    public function receipt_img()
    {
        $img = $this->file( 'user_promo_code_receipt' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

    public function all_file_receipts()
    {
        $img = $this->file( 'user_promo_code_receipt' )->get();

        if( $img )
        {
            return $img;
        }

        return null;
    }

    public function detailLuckyDraw()
    {
        return LuckyDraw::find( $this->lucky_draw_id );
    }

    public function file_rename( $with_ext = false, $number = null )
    {
        $new_date = date("Ymd_His", strtotime($this->created_at ));

        $str = 'receipt_'.$new_date;

        if( isset( $number ))
        {
            $str .= "_$number";
        }

        if( $with_ext )
        {
            $image = $this->receipt_img();
            if( $image )
            {
                $ext = pathinfo( $image, PATHINFO_EXTENSION );
                $str .= ".$ext";
            }
        }

        return $str;
    }

}


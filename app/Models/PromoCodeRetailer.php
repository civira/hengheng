<?php

namespace App\Models;

class PromoCodeRetailer extends BaseModel
{
	protected $table = 'heng_db_promo_code_retailers';

	/**
	 * Relationship
	 */
    public function detailRetailer()
    {
    	return $this->belongsTo( Retailer::class, 'retailer_id' );
    }

}


<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Brand;


class FreeSample extends BaseModel
{
	protected $table = 'heng_db_free_samples';

	static $STATUS_PUBLISHED        = 'YES';

	use Traits\StandardTrait,
        Traits\SEOTrait,
		Traits\FileTrait;

    /**
     * SCOPE
     */
    public function scopePublished( Builder $q )
    {
        return $q->whereIsPublished( self::$STATUS_PUBLISHED );
    }

    /**
     * URL
     */
    public function detailUrl()
    {
        $db_brand = Brand::find( $this->brand_id );

        return route( 'f.heng.free_samples.detail', [  $db_brand->slug, $this->slug ] );
    }

    public function routeToRequestPage()
    {
        return route( 'f.heng.users.free_samples_request_list', $this->id );
    }

    /**
     * FUNCTION
     */
    public function detailBrand()
    {
        return Brand::find( $this->brand_id );
    }

    public function file_img()
    {
        $img = $this->file( 'free_sample_image' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

    public function isDateExpired()
    {
        $today      = new Carbon;
        $endDate    = Carbon::createFromFormat('Y-m-d H:i:s', $this->end_date );

        if( $today->greaterThan($endDate))
        {
            return true;
        }

        return false;
    }

}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Brand extends BaseModel
{
	protected $table = 'heng_db_brands';

	static $STATUS_YES = 'YES';

	use Traits\StandardTrait,
		Traits\FileTrait,
        Traits\SEOTrait;

    public function detailUrl()
    {
        return route( 'f.heng.brands.detail', $this->id );
    }

    public function searchUrl()
    {
        return route( 'f.heng.brands.list', [ $this->id, $this->slug ]);
    }

    public function scopePublished( Builder $q )
    {
    	return $q->whereIsPublished( self::$STATUS_YES );
    }

    public function scopeFeatured( Builder $q )
    {
        return $q->whereIsFeatured( self::$STATUS_YES );
    }

    public function file_img()
    {
        $img = $this->file( 'brandImage' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

}


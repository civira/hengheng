<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Retailer;

class Promotion extends BaseModel
{
	protected $table = 'heng_db_promotions';

	static $STATUS_PUBLISHED = 'YES';
    static $CATEGORY_REDEMPTION = 'redemption';
    static $CATEGORY_GIFT       = 'free-gift';

	use Traits\StandardTrait,
        Traits\SEOTrait,
		Traits\FileTrait;

    /**
     * RELATIONSHIP
     */
    public function promotion_retailer()
    {
        return $this->hasMany( PromotionRetailer::class );
    }

    public function brand()
    {
        return $this->belongsTo( Brand::class );
    }

    /*
        url route
     */
    public function urlRedemption()
    {
        $this->load( 'brand' );
        return route( 'f.heng.redemptions.detail', [ $this->brand->slug, $this->slug ]);
    }

    public function urlGift()
    {
        $this->load( 'brand' );
        return route( 'f.heng.gifts.detail', [ $this->brand->slug, $this->slug ]);
    }

    public function receiptListUrl()
    {
        return route( 'f.heng.users.redemption_receipt_list', $this->id );
    }

    /*
        function
     */
    public function detailBrand()
    {
        return Brand::find( $this->brand_id );
    }

    public function isStatusFinish()
    {
        if( $this->is_status == 'finish' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function isTypeArticle()
    {
        if( $this->is_type == 'type-a' )
        {
            return true;
        }

        return false;
    }

    public function isDateExpired()
    {
        $today      = new Carbon;
        $endDate    = Carbon::createFromFormat('Y-m-d H:i:s', $this->end_date );

        if( $today->greaterThan($endDate))
        {
            return true;
        }

        return false;
    }

    /*
        SCOPES
     */
    public function scopePublished( Builder $q )
    {
        return $q->whereIsPublished( self::$STATUS_PUBLISHED );
    }

    public function scopeCategoryRedemption( Builder $q )
    {
        return $q->whereCategory( self::$CATEGORY_REDEMPTION );
    }

    public function scopeCategoryGift( Builder $q )
    {
        return $q->whereCategory( self::$CATEGORY_GIFT );
    }

    public function scopeValidContest( Builder $q )
    {
        $now_time       = new Carbon;
        
        return $q->where('end_date', '>', $now_time )->published();
    }

    /*
        FILE
     */
    public function file_img()
    {
        $img = $this->file( 'promotion_image' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

}


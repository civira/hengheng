<?php

namespace App\Models;

class Setting extends BaseModel
{
	protected $table = 'heng_db_settings';

	use Traits\StandardTrait,
		Traits\FileTrait;

    public function logoHeader()
    {
        $img = $this->file( 'logoHeaderImg' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

    public function logoFooter()
    {
        $img = $this->file( 'logoFooterImg' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

}


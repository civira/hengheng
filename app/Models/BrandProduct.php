<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class BrandProduct extends BaseModel
{
	protected $table = 'heng_db_brand_products';

	static $STATUS_PUBLISHED = 'YES';

	use Traits\StandardTrait,
		Traits\FileTrait,
        Traits\SEOTrait;

    public function brand()
    {
        return $this->belongsTo( Brand::class );
    }

    public function detailUrl()
    {
        $db_brand = $this->brand;
        return route( 'f.heng.brands.product_detail', [ $db_brand->slug, $this->id, $this->slug ]);
    }

    public function scopePublished( Builder $q )
    {
    	return $q->whereIsPublished( self::$STATUS_PUBLISHED );
    }

    public function file_img()
    {
        $img = $this->file( 'brandProductImage' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

}


<?php

namespace App\Models;

class MemberFreeSampleMap extends BaseModel
{
	protected $table = 'heng_db_member_free_sample_maps';

    use Traits\RouteTrait;

	/**
	 * STATIC
	 */
	static $STATUS_YES = 'YES';

    public function isParticipateYES()
    {
        if( $this->is_participate == 'YES' )
        {
            return true;
        }

        return false;
    }

}


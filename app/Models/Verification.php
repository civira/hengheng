<?php

namespace App\Models;

use App\Jobs\SendMailSubscribeJob;
use App\Jobs\SendForgotPasswordJob;
use App\Jobs\SendUserVerificationJob;
use App\Jobs\ResendVerificationEmail;

class Verification extends BaseModel
{
    protected $table = 'heng_db_verifications';

    use Traits\RouteTrait,
        Traits\TokenTrait;

    /**
     * @var array
     */
    protected $url_pattern = [
        'f.user_verification'    	  => 'f.heng.user.verify_member',
        'f.verify_newsletter'         => 'f.heng.user.verify_newsletter',
        'f.verify_forgot_password'    => 'f.heng.user.verify_forgot_password'
    ];

    /**
     * @var array
     */
    protected $url_pattern_params = [
        'f.user_verification'           => [ 'token' ],
        'f.verify_newsletter'           => [ 'token' ],
        'f.verify_forgot_password'      => [ 'token' ]
    ];

    public static function createRecord( $packet, $user_id = null, $type=null )
    {
        $v              = new Verification;
        $v->user_id 	= $user_id;
        $v->packet      = $packet;
        $v->token       = self::getToken();
        $v->type 		= $type;
        $v->save();

        return $v;
    }

    /*
    	url verification
     */
    public function verifyUserUrl()
    {
        return $this->getUrlFor( 'f.user_verification' );
    }

	public function verifyNewsletterUrl()
	{
		return $this->getUrlFor( 'f.verify_newsletter' );
	}

	public function verifyForgotPasswordUrl()
	{
		return $this->getUrlFor( 'f.verify_forgot_password' );
	}
	/*
		jobs
	 */
    public function resendVerificationEmail( $data )
    {
        $job = new ResendVerificationEmail( $data );
        $job->handle();
    }

    public function sendUserVerificationEmail( $data )
    {
        $job = new SendUserVerificationJob( $data );
        $job->handle();
    }

    public function sendNewsletterEmail( $data )
    {
        $job = new SendMailSubscribeJob( $data );
        $job->handle();
    }

    public function sendForgotPasswordEmail( $data )
    {
        $job = new SendForgotPasswordJob( $data );
        $job->handle();
    }

}


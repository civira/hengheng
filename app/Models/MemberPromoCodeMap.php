<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Builder;

use App\Jobs\SendPromoCodeAdminNotify;

use App\Models\PromoCode;


class MemberPromoCodeMap extends BaseModel
{
    protected $table = 'heng_db_member_promo_code_maps';

    /**
     * STATIC
     */
    static $STATUS_YES  = 'YES';
    static $STATUS_NO   = 'NO';

    /**
     * status on map 
     1. awaiting
     2. verified
     3. rejected
     */

    /**
     * routes
     */
    public function receiptListUrl()
    {
        return route( 'f.heng.users.promo_codes_receipt_list', $this->id );
    }

    /**
     *  FUNCTION
     */
    public function detailPromoCode()
    {
        return PromoCode::find( $this->promo_code_id );
    }

    public function promo_code()
    {
        return $this->belongsTo( PromoCode::class );
    }

    public function isParticipateNo()
    {
        if( $this->is_participate == SELF::$STATUS_NO )
        {
            return true;
        }

        return false;
    }

    public function isParticipateYES()
    {
        if( $this->is_participate == SELF::$STATUS_YES )
        {
            return true;
        }

        return false;
    }

    /*
        SCOPES
     */
    public function scopeParticipated( Builder $q )
    {
        return $q->whereIsParticipate( self::$STATUS_YES );
    }

    public function scopeRequestRedeem( Builder $q )
    {
        return $q->whereIsParticipate( self::$STATUS_NO );
    }

    /**
     * JOBS
     */
    public function sendPromoCodeAdminNotify( $data )
    {
        $job = new SendPromoCodeAdminNotify( $data );
        $job->handle();
    }

    /**
     * drop down
     */
    
    public static function getFilterSelection()
    {
        $selection = 
        [
            'awaiting'  => 'Awaiting Verification',
            'activate'  => 'Ongoing Activation',
            'rejected'  => 'Rejected'
        ];

        return $selection;
    }

}


<?php

namespace App\Models;

use App\Models\LuckyDraw;

class MemberLuckyDrawMap extends BaseModel
{
	protected $table = 'heng_db_member_lucky_draw_maps';

    public function detailLuckyDraw()
    {
    	return LuckyDraw::find( $this->lucky_draw_id );
    }

    public function lucky_draw()
    {
    	return $this->belongsTo( LuckyDraw::class );
    }

}


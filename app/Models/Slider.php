<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Slider extends BaseModel
{
	protected $table = 'heng_db_sliders';

	static $STATUS_SHOW = 'YES';

	use Traits\StandardTrait,
		Traits\FileTrait;

    public function scopeShowed( Builder $q )
    {
    	return $q->whereIsShow( self::$STATUS_SHOW );
    } 

    public function slider_img()
    {
        $img = $this->file( 'sliderImage' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

}


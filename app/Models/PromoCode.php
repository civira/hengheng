<?php

namespace App\Models;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Builder;

class PromoCode extends BaseModel
{
	protected $table = 'heng_db_promo_codes';

	static $STATUS_PUBLISHED = 'YES';

	use Traits\StandardTrait,
        Traits\SEOTrait,
		Traits\FileTrait;

    /**
     * RELATIONSHIP
     */
    public function promocode_retailer()
    {
        return $this->hasMany( PromoCodeRetailer::class );
    }

    /*
        Link Url
     */
    public function detailUrl()
    {
        return route( 'f.heng.promo_codes.detail', [ $this->id, $this->slug ]);
    }

    public function receiptListUrl()
    {
        return route( 'f.heng.users.promo_codes_receipt_list', $this->id );
    }

    /*
        scope
     */

    public function scopePublished( Builder $q )
    {
    	return $q->whereIsPublished( self::$STATUS_PUBLISHED );
    }

    public function scopeValidContest( Builder $q )
    {
        $now_time       = new Carbon;
        
        return $q->where('end_date', '>', $now_time )->published();
    }

    /*
        function
     */
    public function isDateExpired()
    {
        $today      = new Carbon;
        $endDate    = Carbon::createFromFormat('Y-m-d H:i:s', $this->end_date );

        if( $today->greaterThan($endDate))
        {
            return true;
        }

        return false;
    }

    public function isTypeDownload()
    {
        if( $this->is_type == 'type-a' )
        {
            return true;
        }

        return false;
    }

    public function file_img()
    {
        $img = $this->file( 'promo_code_image' )->first();

        if( $img )
        {
            return $img->original();
        }

        return null;
    }

    public function file_data()
    {
        $img = $this->file( 'promo_code_file' )->first();

        if( $img )
        {
            return $img->getFullPath( $img->disk_name, false, true );
        }

        return null;
    }

}


<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Builder;

class MemberAddress extends BaseModel
{
	protected $table = 'heng_db_member_addresses';

	static $STATUS_YES = 'YES';


	/**
	 * FUNCTION
	 */
    public function isDefaultAddress()
    {
        if( $this->is_default == self::$STATUS_YES )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getJSON()
    {
        $temp = 
        [
            'id'      => $this->id,
            'address' => $this->address,
            'unit_no' => $this->unit_no,
            'postal_code' => $this->postal_code
        ];

        return json_encode( $temp );
    }
}


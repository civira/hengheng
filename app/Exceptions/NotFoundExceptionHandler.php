<?php

namespace App\Exceptions;

class NotFoundExceptionHandler extends Handler {

	public function handle()
	{
		return response()->view( 'errors.404' );
	}

}
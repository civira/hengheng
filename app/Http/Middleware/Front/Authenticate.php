<?php

namespace App\Http\Middleware\Front;

use Auth;
use Closure;

class Authenticate
{/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if( !$user || !$user->isFrontendAllowed() )
        {
            $referer = url()->current();

            /**
             * ENCODE
             */
            $referer = encode_authenticate_redirect( $referer );

            return redirect()
                    ->route( 'f.heng.auth.login', [ 'l' => $referer ]);
        }

        return $next( $request );
    }
}

<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

class APIController extends Controller
{
    /**
     * @var Illuminate\Http\Request;
     */
    protected $r;

    public function __construct( Request $r )
    {
        $this->r = $r;
    }

    public function output( $data, $status = true, $error_code = '00500' )
    {
        switch( $status )
        {
            case true :
                $error_code = '00000';
                $status = 'SUCCESS';
                break;
            default :
                $error_code = $error_code;
                $status = 'FAILED';
                break;
        }

        return
        [
            'status'     => $status,
            'error_code' => $error_code,
            'data'       => $data
        ];
    }

    public function validate( Request $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $inputs = $request->all();
        $validator = Validator::make( $inputs, $rules, $messages, $customAttributes );

        if( $validator->fails() )
        {
            return $validator->errors()->first();
        }

        return true;
    }
}

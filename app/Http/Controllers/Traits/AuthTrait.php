<?php

namespace App\Http\Controllers\Traits;

use Auth;
use Illuminate\Support\Facades\Lang;

/**
 * Combined with below traits:
 * Illuminate\Foundation\Auth\ThrottlesLogins;
 */
trait AuthTrait
{
	protected $lockedOut = null;

	/**
	 * @return array
	 */
	protected function loginFields()
	{
		return [ 'email', 'password' ];
	}

	/**
	 * @return string
	 */
	protected function redirectTo()
	{
		return 'f.home';
	}

    /**
     * @return string
     */
    protected function loginUsername()
    {
    	return 'email';
    }

    /**
     * @return string
     */
	protected function getLockoutMessage()
	{
		if( $this->lockedOut = $this->hasTooManyLoginAttempts($this->r) )
		{
            $this->fireLockoutEvent($this->r);

            $seconds = $this->limiter()->availableIn(
                    $this->throttleKey($this->r)
                );

            return Lang::get('auth.throttle', ['seconds' => $seconds]);
        }

        return '';
	}

	/**
	 * @param  string 	$msg
	 * @return redirect
	 */
	protected function redirectBackWithErrors( $msg )
	{
		if( !empty( $this->is_api) && $this->is_api )
		{
			return $this->output( $msg, false );
		}
		else
		{
			return $this->redirectBack( $msg, false );
		}
	}

	/**
	 * @param  Request $request
	 * @return void
	 */
	protected function doIncrementLoginAttempts( $request )
	{
		if( !$this->lockedOut )
		{
			$this->incrementLoginAttempts( $request );
		}
	}

	/**
	 * @param  function $afterAuth
	 * @param  App\Models\User $manual_user
	 * @param  boolean  $remember_me
	 * @return redirect
	 */
	protected function doAuthentication( $afterAuth = null, $manual_user = null, $remember_me = false )
	{
		/**
		 * Throttles Login
		 */
		$e_msg = $this->getLockoutMessage();
		if( $e_msg )
		{
			return $this->redirectBackWithErrors( $e_msg );
		}

        /**
         * Authentication
         */
        if( isset( $manual_user ))
        {
        	$authed = Auth::login( $manual_user );
        }
        else
        {
			$authed = Auth::attempt( $this->r->only($this->loginFields()), $remember_me );
        }

		if( $authed )
		{
			$user = Auth::user();

			if( is_callable( $afterAuth ) )
			{
				$resp = $afterAuth( $user );

				if( is_string( $resp ) )
				{
					$e_msg = $resp;
				}
				else
				{
					return $resp;
				}
			}
			else
			{
				return $this->redirectToRoute( $this->redirectTo() );
			}
		}
		else
		{
			$e_msg = trans( 'messages.errors.auth.invalid_credential', $this->loginFields() );
		}

		$this->doIncrementLoginAttempts( $this->r );

		return $this->redirectBack( $e_msg, false );
	}
}

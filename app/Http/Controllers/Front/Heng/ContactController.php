<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Inquiry;

use DB;

class ContactController extends BaseController
{
	public function index()
	{
		$this->seo()->setTitle('Contact Us');
        $this->seo()->setDescription('Shop&Win Contact Us. Feel free to contact us at this page or simply email us at help@shopnwin.sg');

		return $this->output( 'f.heng.contacts.index' );
	}

	public function store()
	{

		$rules = 
		[
			'subject'			=> 'required',
			'name'   			=> 'required|min:4',
			'mobile_number'  	=> 'required|digits:8',
			'email'  			=> 'required|email',
			'message' 	    	=> 'required|min:10',
			
			// 'g-recaptcha-response' => 'required|recaptcha'
		];

		$this->validate( $this->r, $rules );

		/**
		 * security measure for prevent spam
		 */
		if( $this->r->get( config('constants.security.md5' ) ))
		{
			$msg = 'Please try again.';

			return $this->redirectBack( $msg, false, false );
		}

		/**
		 * start process
		 */
		extract( $this->r->all() );

		DB::beginTransaction();

		$r = new Inquiry;
		$r->subject	     = $subject;
		$r->name	     = $name;
		$r->email 		 = $email;
		$r->phone_number = $mobile_number;
		// $r->comment 	 = nl2br(htmlentities($message));
		$r->comment 	 = $message;
		$r->save();

		DB::commit();

		/**
		 * send admin email information
		 */
		$r->sendInquiryNotification( $r );

		$msg = 'Your message has been sent. We will get back to you as soon as possible.';

		return $this->redirectBack( $msg, true, false );
	}

}

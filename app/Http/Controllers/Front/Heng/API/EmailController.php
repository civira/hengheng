<?php

namespace App\Http\Controllers\Front\Heng\API;

use App\Http\Controllers\APIController;
use App\Models\MailSubscriber;
use App\Models\Verification;

class EmailController extends APIController
{
    public function subscribeNewsletter()
    {
        $authed_user    = \Auth::user();

        $rules =
        [
            'email'      => 'required|email|unique:heng_db_mail_subscribers,email'
        ];

        $validate = $this->validate( $this->r, $rules );

        if( $validate !== true )
        {
            return $this->output( $validate, false);
        }

        extract( $this->r->all() );

        // $r                      = new MailSubscriber;
        // $r->email               = $email;
        // $r->save();

        $db_verification     = Verification::createRecord( $email, null, 'newsletter' );

        $db_verification->sendNewsletterEmail( $db_verification );

        $msg = 'Thank you for susbcribing. A verification email has been sent to your email.';

        return $this->output( $msg, true );
    }
}

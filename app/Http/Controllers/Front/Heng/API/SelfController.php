<?php

namespace App\Http\Controllers\Front\Heng\API;

use App\Models\Favorite;
use App\Models\Brand;
use App\Http\Controllers\APIController;

class SelfController extends APIController
{
    public function toggleFavorite()
    {
        $user = \Auth::user();
        $id   = $this->r->get( 'id' );

        $current = Favorite::forBrand()
                            ->whereModelId( $id )
                            ->whereMemberId( $user->id )
                            ->first();

        $subscribed = true;


        /**
         * get brand data for favorite count
         */
        $db_brand = Brand::find( $id );

        if( $current )
        {
            $current->delete();
            $subscribed = false;

            /**
             * decrease favorite
             */
            $db_brand->total_favorite -= 1;
            $db_brand->save();
        }
        else
        {
            $new = new Favorite;
            $new->member_id  = $user->id;
            $new->model_id   = $id;
            $new->model_type = Brand::class;
            $new->save();

            /**
             * increase favorite
             */
            $db_brand->total_favorite += 1;
            $db_brand->save();
        }

        $data = [
            'subscribed' => $subscribed
        ];

        return $this->output( $data );
    }
}

<?php

namespace App\Http\Controllers\Front\Heng\API;

use App\Http\Controllers\APIController;

use App\Models\LuckyDraw;
use App\Models\Promotion;
use App\Models\Brand;
use App\Models\Retailer;
use App\Models\FreeSample;

class SearchController extends APIController
{
    public function indexLuckyDraws()
    {
        $query = $this->r->get( 'q' );

        $db_contest = LuckyDraw::published()->where( 'name', 'LIKE', '%'.$query.'%' )->get();

        $data = [];

        foreach( $db_contest as $dc )
        {
            $temp = [
                'id' => $dc->id,
                'label' => $dc->name,
                'url' => $dc->detailUrl()
            ];

            $data[] = $temp;
        } 

        return $this->output( $data );
    }

    public function indexFreeSamples()
    {
        $query = $this->r->get( 'q' );

        $db_contest = FreeSample::published()->where( 'name', 'LIKE', '%'.$query.'%' )->get();

        $data = [];

        foreach( $db_contest as $dc )
        {
            $temp = [
                'id' => $dc->id,
                'label' => $dc->name,
                'url' => $dc->detailUrl()
            ];

            $data[] = $temp;
        } 

        return $this->output( $data );
    }

    public function indexRedemptions()
    {
        $query = $this->r->get( 'q' );

        $db_contest = Promotion::published()->categoryRedemption()->where( 'name', 'LIKE', '%'.$query.'%' )->get();

        $data = [];

        foreach( $db_contest as $dc )
        {
            $temp = [
                'id' => $dc->id,
                'label' => $dc->name,
                'url' => $dc->urlRedemption()
            ];

            $data[] = $temp;
        } 

        return $this->output( $data );
    }

    public function indexFreeGifts()
    {
        $query = $this->r->get( 'q' );

        $db_contest = Promotion::published()->categoryGift()->where( 'name', 'LIKE', '%'.$query.'%' )->get();

        $data = [];

        foreach( $db_contest as $dc )
        {
            $temp = [
                'id' => $dc->id,
                'label' => $dc->name,
                'url' => $dc->urlGift()
            ];

            $data[] = $temp;
        } 

        return $this->output( $data );
    }

    public function indexBrands()
    {
        $query = $this->r->get( 'q' );

        $db_contest = Brand::published()->featured()->where( 'name', 'LIKE', '%'.$query.'%' )->get();

        $data = [];

        foreach( $db_contest as $dc )
        {
            $temp = [
                'id' => $dc->id,
                'label' => $dc->name,
                'url' => $dc->searchUrl()
            ];

            $data[] = $temp;
        } 

        return $this->output( $data );
    }

    public function indexRetailers()
    {
        $query = $this->r->get( 'q' );

        $db_contest = Retailer::published()->where( 'name', 'LIKE', '%'.$query.'%' )->get();

        $data = [];

        foreach( $db_contest as $dc )
        {
            $temp = [
                'id' => $dc->id,
                'label' => $dc->name,
                'url' => $dc->searchUrl()
            ];

            $data[] = $temp;
        } 

        return $this->output( $data );
    }

}

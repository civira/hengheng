<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Retailer;
use App\Models\LuckyDraw;
use App\Models\Promotion;
use App\Models\RetailerCategory;
use App\Models\Counterable;

use App\Models\PromotionRetailer;
use App\Models\LuckyDrawRetailer;

use Illuminate\Http\Request;

class RetailerController extends BaseController
{
	public function index( Request $r )
	{
        $this->seo()->setTitle('Your Favorite Retailers In One Place');
        $this->seo()->setDescription('Shop&Win EXCLUSIVE RETAILERS. All your favorite retailers are here!');

		/*
			retailer categories
		 */

		$db_categories = RetailerCategory::All();

		$retailer_data = [];

		/*
			grouping by retailer category
		 */
		foreach( $db_categories as $dc )
		{
			$retailer_data[ $dc->id ] = Retailer::whereRetailerCategoryId( $dc->id )->published()->get();
		}

		$data = compact(
			'db_categories',
			'retailer_data'
		 );

		return $this->output( 'f.heng.retailer.index', $data );
	}

    public function detailOld( $id )
    {
        $db = Retailer::find( $id );

        if( !$db )
        {
            return abort( 404 );
        }

        return redirect( $db->detailUrl(), 301 );
    }

	public function detail( Request $r, $id, $slug )
	{
		/**
		 * 	parameter value
		 */
		$r_show = $r->get( 'show', null );

		/*
			get db brand
		 */
		$db_retailer = Retailer::find( $id );

		if( !$db_retailer )
		{
			return abort(404);
		}


		/*
			SEO
		 */
		$this->seo()->setTitle( seo_meta_helper( 'title', $db_retailer->seo, sprintf( '%s Retailer', $db_retailer->name ) ));
        $this->seo()->setDescription( 
        	seo_meta_helper(
        		'description',
        		$db_retailer->seo,
	        	sprintf(
				'%s RETAILER at Shop&Win. See through all available %s lucky draws, redemptions, free gifts, and products.'
				        , strtoupper($db_retailer->name), $db_retailer->name))
        );



		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'retailer', $id, Retailer::class );
		/*
			increase total view
		 */
		$db_retailer->total_view += 1;
		$db_retailer->save();

		$retailer_logo = $db_retailer->file_img();

		/**
		 *  ALL RETAILERS
		 */
		$all_retailers = Retailer::All();

		/*
			Redemption
		 */
		$db_attach_promotions =  PromotionRetailer::whereRetailerId( $id )->pluck('promotion_id');

		$query_redemptions 	  = Promotion::published()
									->categoryRedemption()
									->whereIn( 'id', $db_attach_promotions )
									->orderBy( 'created_at', 'desc' );

		/**
		 * free gifts category
		 */
		$query_gifts 	= Promotion::published()
						->categoryGift()
						->whereIn( 'id', $db_attach_promotions )
						->orderBy( 'created_at', 'desc' );

		/*
			LUCKY DRAWS
		 */
		$db_attached_lucky = LuckyDrawRetailer::whereRetailerId( $id )->pluck( 'lucky_draw_id' );

		$query_lucky 	= LuckyDraw::published()
								->whereIn( 'id', $db_attached_lucky )
								->orderBy( 'created_at', 'desc' );


		/**
		 * get total data in Database
		 */
		$total_lucky 		= $query_lucky->count();
		$total_redemption 	= $query_redemptions->count();
		$total_gift 		= $query_gifts->count();
			
		if( !$r_show )
		{
			$query_redemptions  = $query_redemptions->take( 3 );
			$query_gifts  		= $query_gifts->take( 3 );
			$query_lucky  		= $query_lucky->take( 3 );
		}

		$db_promo_redemptions 	= $query_redemptions->get();
		$db_promo_gifts 	  	= $query_gifts->get();
		$db_lucky 	  			= $query_lucky->get();

		/*
			data image for promotion and free gifts
		 */
		foreach( $db_promo_redemptions as $pr )
		{
			$pr[ 'image_location' ] = $pr->file_img();
			$pr[ 'brand_data' ]		= $pr->detailBrand();
		}

		foreach( $db_promo_gifts as $pg )
		{
			$pg[ 'image_location' ] = $pg->file_img();
			$pg[ 'brand_data' ]		= $pg->detailBrand();
		}

		foreach( $db_lucky as $db )
		{
			$db['image_location'] = $db->file_img();
			$db['brand_data']	  = $db->detailBrand();
		}

		/**
		 * Select2 Data
		 */
		$retailer_categories = RetailerCategory::orderBy( 'name' )->get();
		foreach( $retailer_categories as $rc )
		{
			$rc->load( 'retailers' );

			foreach( $rc->retailers as $r )
			{
				$r->detailUrl = $r->detailUrl();
			}
		}

		/*
			compact data
		 */
		$data = compact(
			'all_retailers',
			'db_retailer',
			'retailer_logo',
			'db_promo_gifts',
			'db_promo_redemptions',
			'db_lucky',
			'retailer_categories',
			'r_show',
			'total_gift',
			'total_redemption',
			'total_lucky'
		);
		return $this->output( 'f.heng.retailer.detail', $data );
	}

	public function lucky_list( $id )
	{
		$db_retailer	= Retailer::find( $id );

        if( !$db_retailer )
        {
            return abort( 404 );
        }

        $this->seo()->setTitle( sprintf( '%s Lucky Draws', $db_retailer->name ));
        $this->seo()->setDescription( sprintf(
'ALL %s LUCKY DRAWS at Shop&Win. Get a chance to win various exclusive %s lucky draw reward.'
        , strtoupper($db_retailer->name), $db_retailer->name ));

		$retailer_logo 	= $db_retailer->file_img();

		/*
			list
		 */
		$attachment_lucky = LuckyDrawRetailer::whereRetailerId( $id )->pluck( 'lucky_draw_id' );

		$db_list = LuckyDraw::published()
							->whereIn( 'id', $attachment_lucky )
							->orderBy( 'created_at', 'desc' )
							->paginate(6);

		foreach( $db_list as $db )
		{
			$db[ 'image_location' ] = $db->file_img();
			$db[ 'brand_data' ]		= $db->detailBrand();
		}


		$data = compact(
			'db_retailer',
			'retailer_logo',
			'db_list'
		 );

		return $this->output( 'f.heng.retailer.lucky-list', $data );
	}

	public function gift_list( $id )
	{
		$db_retailer	= Retailer::find( $id );

        if( !$db_retailer )
        {
            return abort( 404 );
        }

        $this->seo()->setTitle( sprintf( '%s Free Gifts', $db_retailer->name ));
        $this->seo()->setDescription( sprintf(
'ALL %s FREE GIFTS at Shop&Win. Get for free various %s exclusive gift from your every purchases.'
        , strtoupper($db_retailer->name), $db_retailer->name ));

		$retailer_logo 	= $db_retailer->file_img();
		/*
			list
		 */
		$attachment_promo = PromotionRetailer::whereRetailerId( $id )->pluck( 'promotion_id' );

		$db_list = Promotion::published()
							->categoryGift()
							->whereIn( 'id', $attachment_promo )
							->orderBy( 'created_at', 'desc' )
							->paginate(6);

		foreach( $db_list as $db )
		{
			$db[ 'image_location' ] = $db->file_img();
			$db[ 'brand_data' ]		= $db->detailBrand();
		}

		$data = compact(
			'db_retailer',
			'retailer_logo',
			'db_list'
		 );

		return $this->output( 'f.heng.retailer.gift-list', $data );
	}

	public function redemption_list( $id )
	{
		$db_retailer	= Retailer::find( $id );

        if( !$db_retailer )
        {
            return abort( 404 );
        }

        $this->seo()->setTitle( sprintf( '%s Redemption', $db_retailer->name ));
        $this->seo()->setDescription( sprintf(
'ALL %s REDEMPTIONS at Shop&Win. Redeem various exclusive %s redemption for your every purchases.'
        , strtoupper($db_retailer->name), $db_retailer->name ));

		$retailer_logo 	= $db_retailer->file_img();
		/*
			list
		 */
		$attachment_promo = PromotionRetailer::whereRetailerId( $id )->pluck( 'promotion_id' );

		$db_list = Promotion::published()
							->categoryRedemption()
							->whereIn( 'id', $attachment_promo )
							->orderBy( 'created_at', 'desc' )
							->paginate(6);

		foreach( $db_list as $db )
		{
			$db[ 'image_location' ] = $db->file_img();
			$db[ 'brand_data' ]		= $db->detailBrand();
		}


		$data = compact(
			'db_retailer',
			'retailer_logo',
			'db_list'
		 );

		return $this->output( 'f.heng.retailer.redemption-list', $data );
	}

}

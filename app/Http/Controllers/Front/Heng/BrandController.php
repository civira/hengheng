<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Brand;
use App\Models\BrandCategory;
use App\Models\BrandPromotion;
use App\Models\BrandProduct;

use App\Models\LuckyDraw;
use App\Models\Promotion;
use App\Models\Favorite;

use App\Models\Counterable;

use Illuminate\Http\Request;

class BrandController extends BaseController
{
	public function index( Request $r )
	{
        $this->seo()->setTitle('Your Favorite Brands In One Place');
        $this->seo()->setDescription('Shop&Win EXCLUSIVE BRANDS. All your favorite brands are here!');

		$f_sort = $r->get( 'filter_sort', 0 );

		/*
			quuery data for BRAND
		 */
		$query = Brand::published();

		if( $f_sort != 0 )
		{
			$query = $query->whereBrandCategoryId( $f_sort );
		}

		$db_brand = $query->orderBy( 'is_featured', 'desc' )
						->orderBy( 'sort_order', 'asc' )
						->paginate(36);

		/*
			 setup brand image
		 */
		$brand_img 	= [];

		foreach( $db_brand as $dc )
		{
			$brand_img[ $dc->id ] = $dc->file_img();

			if( $dc->is_featured == 'YES' )
			{
				$dc['highlighted'] = 'YES';
			}
			else
			{
				$dc['highlighted'] = 'NO';
			}
		}

		$total_brand = $query->count();

		/*
			page filter
		 */
		$db_brand_categories = BrandCategory::orderBy('sort_order', 'asc')->get();
		;

        /*
            Faves
         */
        $user = \Auth::user();
        if( $user )
        {
            $favs = Favorite::forBrand()
                            ->whereMemberId( $user->id )
                            ->orderByBrandName( 'asc');

                if( !empty( $f_sort ))
                {
                    $favs = $favs->whereBrandCategoryId( $f_sort );
                }

                $favs = $favs->get();

            $fav_img = [];
                foreach( $favs as $f )
                {
                    $fav_img[ $f->brand->id ] = $f->brand->file_img();

                    if( $f->brand->is_featured == 'YES' )
                    {
                        $f->brand[ 'highlighted' ] = 'YES';
                    }
                    else
                    {
                        $f->brand[ 'highlighted' ] = 'NO';
                    }
                }
        }

		$data = compact(
			'db_brand',
			'brand_img',
			'total_brand',
			'f_sort',
			'db_brand_categories',
            'favs',
            'fav_img'
		 );

		return $this->output( 'f.heng.brand.index', $data );
	}

    public function detailOld( $id )
    {
        $db = Brand::find( $id );

        if( !$db )
        {
            return abort( 404 );
        }

        return redirect( $db->searchUrl(), 301 );
    }

	public function detail( $id )
	{
		/*
			get db brand
		 */
		$db_brand = Brand::find( $id );

		if( !$db_brand || $db_brand->is_featured == 'NO' )
		{
			return abort(404);
		}

        $this->seo()->setTitle( $db_brand->seo->meta_title );
        $this->seo()->setDescription('Shop&Win EXCLUSIVE BRANDS. All your favorite brands are here!');

		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'brand', $id, Brand::class, 'detail', null );

		$data = compact( 'curr_page_title' );
		return $this->output( 'f.heng.brand.detail', $data );
	}

	public function product_detail( $brand_slug, $id, $slug )
	{
		/*
			current contest
		 */
		$db = BrandProduct::find( $id );

		if( !$id || !$db )
		{
			return abort(404);
		}

		$db_brand 		= $db->brand;

		/*
			SEO
		 */
		$this->seo()->setTitle( seo_meta_helper( 'title', $db->seo, sprintf( '%s by %s', $db->name, $db_brand->name ) ));
        $this->seo()->setDescription( 
        	seo_meta_helper(
        		'description',
        		$db->seo,
	        	sprintf(
				'Check out detailed information about %s product by %s at Shop&Win.'
        		, $db->name, $db_brand->name)
        	)	
        );


		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'brand-product', $id, BrandProduct::class );

		/*
			increase total view
		 */
		$db->total_view += 1;
		$db->save();

		$db->image 	=  $db->file_img();

		/*
			brand data
		 */
		$brand_image 	= $db_brand->file_img();

		/*
			other contest
		 */
		$db_other = BrandProduct::where( 'id', '!=', $id )
					->whereBrandId( $db_brand->id )
					->published()
					->orderBy( 'created_at', 'desc' )
					->get();

		foreach( $db_other as $each_other )
		{
			$each_other->image = $each_other->file_img();
			$each_other->brand_data = $each_other->brand;
		}

		/*
			array data
		 */
		$data = compact(
			'db',
			'db_brand',
			'brand_image',
			'db_other'
		);

		return $this->output( 'f.heng.brand.product-detail', $data );
	}

	public function list( $id, $slug )
	{
		/*
			get db brand
		 */
		$db_brand = Brand::find( $id );

		if( !$db_brand || $db_brand->is_featured == 'NO' )
		{
			return abort(404);
		}

		/*
			SEO
		 */
		$this->seo()->setTitle( seo_meta_helper( 'title', $db_brand->seo, sprintf( '%s Brand', $db_brand->name ) ));
        $this->seo()->setDescription( 
        	seo_meta_helper(
        		'description',
        		$db_brand->seo,
	        	sprintf(
				'%s BRAND at Shop&Win. See through all %s lucky draws, redemptions, free gifts, and products.'
		        , strtoupper($db_brand->name), $db_brand->name)
        	)	
        );

		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'brand', $id, Brand::class );

		/*
			increase total view
		 */
		$db_brand->total_view += 1;
		$db_brand->save();


		/*
			BRAND PRODUCT
		 */
		$db_products 	= BrandProduct::whereBrandId( $id )
										->published()
										->orderBy( 'created_at', 'desc')
										->take(3)
										->get();

		$product_img = [];

		foreach( $db_products as $db )
		{
			$product_img[ $db->id ] = $db->file_img();
		}

		/*
			Promotions
		 */
		$db_promotions = Promotion::whereBrandId( $id )
							->published()
							->orderBy( 'created_at', 'desc' )
							->take(6)
							->get();

		$promotion_img = [];

		foreach( $db_promotions as $db )
		{
			$promotion_img[ $db->id ] = $db->file_img();
		}

		/*
			lucky draws
		 */
		$db_lucky 	= LuckyDraw::whereBrandId( $id )->published()->orderBy( 'created_at', 'desc' )->take(3)->get();

		$lucky_img = [];

		foreach( $db_lucky as $db )
		{
			$lucky_img[ $db->id ] = $db->file_img();
		}

        /**
         * Favorite
         */
        $user = \Auth::user();
        if( $user )
        {
            $faved = Favorite::forBrand()
                             ->whereModelId( $db_brand->id )
                             ->whereMemberId( $user->id )
                             ->first();
        }

		/*
			compact data
		 */
		$data = compact(
			'db_brand',
			'db_products',
			'db_lucky',
			'db_promotions',
			'product_img',
			'promotion_img',
			'lucky_img',
            'faved'
		 );

		return $this->output( 'f.heng.brand.item-list', $data );
	}

}

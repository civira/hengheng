<?php

namespace App\Http\Controllers\Front\Heng;

class QRController extends BaseController
{
	public function index()
	{
		$data = [];
		return $this->output( 'f.heng.scan-qr.index', $data );
	}
}
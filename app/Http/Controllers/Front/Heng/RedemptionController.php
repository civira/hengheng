<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Brand;
use App\Models\Retailer;
use App\Models\Promotion;
use App\Models\MemberPromotionMap;
use App\Models\MemberPromotionReceipt;
use App\Models\Counterable;

use Auth;
use DB;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Models\File;
use App\Libraries\Simplifies\ImageUpload;

class RedemptionController extends BaseController
{
	public function index( Request $r )
	{
        $this->seo()->setTitle('Get Exclusive Redemption For Your Purchases');
        $this->seo()->setDescription('Shop&Win REDEMPTION. Dont miss out our exclusive redemption for your every purchases.');

        /**
         * filter
         */
		$f_brand 	= $r->get( 'filter_brand', 0 );
		$f_retailer = $r->get( 'filter_retailer', 0 );

			/*
				LUCKY DRAWS
			 */
			$db_query = Promotion::published()->categoryRedemption()->orderBy( 'created_at', 'desc' );

				/*
					query filter
				 */
				if( $f_brand != 0 )
				{
					$db_query = $db_query->where( 'brand_id', '=', $f_brand );
				}

				if( $f_retailer != 0 )
				{
					$db_query = $db_query->whereHas( 'promotion_retailer', function($query) use ( $f_retailer ) {
				        $query->where( 'retailer_id', '=', $f_retailer);
				    });
				}
				/*
					end filter
				 */

			$db_redemptions = $db_query->paginate(12);

			foreach( $db_redemptions as $dr )
			{
				$dr['brand_data'] = $dr->detailBrand();
			}

			/*
				total result
			 */
			$total_redemptions = $db_query->count();

		/**
		 * vars
		 */
		$db_brands 		= Brand::orderBy( 'name', 'asc' )->get();
		$db_retailers 	= Retailer::orderBy( 'name', 'asc' )->get();

		$data = compact(
				'f_brand',
				'f_retailer',
				'db_redemptions',
				'total_redemptions',
				'db_brands',
				'db_retailers'
				 );

		return $this->output( 'f.heng.redemptions.index', $data );
	}

	public function detail( $brand_slug, $slug )
	{
		/*
			current promotion
		 */
		$db = Promotion::whereSlug( $slug )->first();

		if( !$db )
		{
			return abort(404);
		}

		/**
		 * 	SEO
		 */
        $this->seo()->setTitle( seo_meta_helper( 'title', $db->seo, sprintf( '%s Redemption', $db->name )) );
        $this->seo()->setDescription(
        	seo_meta_helper(
        		'description',
        		$db->seo,
	        	sprintf( 'GET %s REDEMPTION at Shop&Win. Redeem various exclusive redemption for your every purchases.', strtoupper($db->name))
        	)
        );

		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'redemption', $db->id, Promotion::class );

		/*
			increase total view
		 */
		$db->total_view += 1;
		$db->save();

		/**
		 * 	data parameter
		 */
		$db->image 	= $db->file_img();
		$db_brand 	= $db->detailBrand();

			/*
				other contest
			 */
			$db_other = Promotion::where( 'id', '!=', $db->id )
						->published()
						->categoryRedemption()
						->orderBy( 'created_at', 'desc' )
						->get();

			foreach( $db_other as $do )
			{
				$do->brand_data = $do->detailBrand();
				$do->image   	= $do->file_img();
			}

        /**
         * RETAILER LOGO
         */
		$db_associate_retailer = $db->promotion_retailer;

		if( !$db_associate_retailer->isEmpty() )
		{
			foreach( $db_associate_retailer as $x )
			{
				$x['retailer_data']	= $x->detailRetailer;
				$x['retailer_logo'] = $x->detailRetailer->file_img();
			}
		}

		/**
		 * set page view for type A or B
		 	type A = article information page
		 */
		if( $db->isTypeArticle() )
		{
			$data_compact = compact(
				'db',
				'db_brand',
				'db_other',
				'db_associate_retailer'
			);

			$output_file = 'f.heng.redemptions.article_detail';
		}
		else
		{
			$db_exist_participation 	= null;
			$db_active_participation	= null;
			$receipt_count 				= 0;

			/**
			 * if logged user
			 */

			$log_user = Auth::user();


			if( $log_user )
			{
				/**
				 * exist previous participation
				 */
				$db_exist_participation = MemberPromotionMap::whereMemberId( $log_user->id )
													->wherePromotionId( $db->id )
													->first();

				/**
				 * current active participation
				 */
				$db_active_participation = MemberPromotionMap::whereMemberId( $log_user->id )
													->wherePromotionId( $db->id )
													->participated()
													->first();

				if( $db_active_participation )
				{
					$receipt_count = MemberPromotionReceipt::whereMemberPromotionMapId( $db_active_participation->id )
													->count();
				}
			}

			/**
			 * TEXT INFORMATION DISPLAY
			 */
			$txt_main_title = 'Participate to Win!';

			if( $db->isDateExpired() )
			{
				$txt_main_title = 'Redemption period has ended.';
			}
			else if( !$log_user )
			{
				$txt_main_title = 'Click to Redeem!';
			}
			else if( $db_exist_participation )
			{
				$txt_main_title = 'Upload Receipt To Redeem!';
			}
			else
			{
				$txt_main_title = 'Click to Redeem!';
			}

			$display_info['main_title'] = $txt_main_title;

			/*
				array data
			 */
			$data_compact = compact(
				'db',
				'db_brand',
				'db_exist_participation',
				'db_active_participation',
				'receipt_count',
				'db_other',
				'display_info',
				'log_user',
				'db_associate_retailer'
			);

			$output_file = 'f.heng.redemptions.participate_detail';

		}

		return $this->output( $output_file, $data_compact );
	}

    public function detailOld( $id )
    {
        $db = Promotion::find( $id );

        if( !$db )
        {
            return abort(404);
        }

        return redirect( $db->urlRedemption(), 301 );
    }

    /**
     * 	participation for redemption
     */
	public function participate( Request $r, $id )
	{
		$log_user = Auth::user();

		/*
			get agreement
		 */
		$is_status = false;
		$agreement_check = $r->input( 'check_agreement' );

		/**
		 * check if member already participate before or not
		 */

		$db_participate = MemberPromotionMap::whereMemberId( $log_user->id )
											->wherePromotionId( $id )
											->first();

		if( !$agreement_check && !$db_participate )
		{
			$msg = __( 'messages.redemption.no_agreement' );
		}
		else
		{

			if( empty( $log_user->full_name) || empty( $log_user->phone_number) )
			{

				$rules =
				[
					'name'   			=> 'required|min:6',
					'mobile_number' 	=> 'required|digits:8|unique:heng_db_members,phone_number,'.$log_user->id,
				];

				$this->validate( $this->r, $rules );

				extract( $this->r->all() );

				// $db_subscribed = MemberLuckyDrawMap::whereMemberId( $log_user->id )->whereLuckyDrawId( $id )->first();
				/*
					update member data
				 */
				$log_user->full_name 	= $name;
				$log_user->phone_number = $mobile_number;
				$log_user->save();
			}


			/*
			subscribe member data
			 */
			$db_participate 					= new MemberPromotionMap;
			$db_participate->member_id  		= $log_user->id;
			$db_participate->promotion_id  		= $id;
			$db_participate->is_participate 	= 'YES';
			$db_participate->status 			= 'awaiting';
			$db_participate->save();

			$is_status = true;
			$msg = __('messages.redemption.subscribed_success');
		}

		return $this->redirectBack( $msg, $is_status );

	}

	/**
	 * multi upload page
	 */
	public function multi_upload( $checked_id=null )
	{
		$self_active_menu = 'receipt-upload';

        $this->seo()->setTitle( 'Upload Receipt To Redeem!' );
        $this->seo()->setDescription(
		'Upload your purchase receipt to win exclusive prizes from your selected redemption\'s.' );

		/*
			get member data
		 */
		$log_user = Auth::user();

		/**
		 * check participation
		 */
		$db_participation = MemberPromotionMap::whereMemberId( $log_user->id )
											->participated()
                                            ->whereHas('promotion', function ($query){
                                                $query->validContest();
                                            })
											->get();

        /**
         * Default Selection
         */
        $selected_str = $this->r->get( 's' );
            $selecteds = explode( ",", $selected_str );

		foreach( $db_participation as $ac )
		{
			$ac[ 'promotion' ] 	= Promotion::find( $ac->promotion_id );
			$ac[ 'brand' ] 		= Brand::find( $ac->promotion->brand_id );
            $ac[ 'checked' ]    = false;

            foreach( $selecteds as $s )
            {
                if( $s == $ac->id )
                {
                    $ac[ 'checked' ] = true;
                    break;
                }
            }
		}

		$data = compact(
			'checked_id',
			'self_active_menu',
			'log_user',
			'db_participation',
            'selecteds'
		 );
		return $this->output( 'f.heng.redemptions.receipt_upload', $data );
	}

	/**
	 * store multi upload
	 */
	public function store_multi_upload( Request $r )
	{
		/*
			member data
		 */
		$log_user  		= Auth::user();

		/*
			get input data
		 */
		$msg 		= null;
		$is_status 	= false;
		$is_error 	= false;
		$old_uploader = null;
		/**
		 * variables
		 */
		$array_checked  = $r->input( 'checked_box' );
		$image 			= $this->r->file( 'receipt_upload' );

		if( empty( $array_checked ))
		{
			$msg = 'At least 1 redemption must be chosen.';
		}
		else if( empty( $image ) )
		{
			$msg = 'At least 1 photo receipt must be uploaded';
		}
		else
		{
			/**
			 * check each file in upload must be image
			 */
			$approved_file = [];

			DB::beginTransaction();

			/**
			 * 	save into db each file in filesystem
			 * 	for no duplicate image in storage we are only once uploaded the file and get the name
			 * 	file only can be uploaded once after that file is gone need to be uploaded, ERROR output.
			 */
			foreach( $image as $each_file )
			{
				$new_uploader = new ImageUpload(
				[
					'upload' => $each_file,
					'encode' => true
				]);


				if( $new_uploader->hasFile() )
				{
					/**
					 * Upload
					 */
					if( $fileinfo = $new_uploader->upload() ) {

						$approved_file[] = $fileinfo;

					}
					else
					{
						DB::rollback();
						$msg    = $new_uploader->error();
						break;
					}
				}
				else
				{
					DB::rollback();
					$msg  		= 'A receipt must be uploaded.';
					break;
				}
			}

			/**
			 *  if no message error then save into database current file uploaded
			 */
			if( empty($msg) )
			{
				/*
					set to data for each contest
				 */
				foreach( $array_checked as $ac )
				{
					/**
					 * data explode
					 * first array for member promotion id
					 * second array for promotion id
					 */
					$data_id = explode( ',', $ac );

					/*
						save into db receipt current user upload
					 */
					$db_receipt  							= new MemberPromotionReceipt;
					$db_receipt->member_id 					= $log_user->id;
					$db_receipt->member_promotion_map_id 	= $data_id[0];
					$db_receipt->promotion_id 				= $data_id[1];
					$db_receipt->is_valid 					= MemberPromotionReceipt::$STATUS_PENDING;
					$db_receipt->save();

					/**
					 * save each file name into system files
					 */
					foreach( $approved_file as $ap )
					{
						$file = new File;
						$file->make( $db_receipt->id, (new MemberPromotionReceipt())->getMorphClass(), $ap, 'user_promotion_receipt' );
						$file->save();
					}

				}

				DB::commit();

				$is_status = true;
				$msg = 'Upload receipt success.';

				/**
				 * send mail to administrator for redemption attachment
				 */
	    	    return $this->redirectToRoute( 'f.heng.redemptions.success_upload' );
			}
		}


		return $this->redirectBack( $msg, false );
	}

	/**
	 * member request redeem data
	 */
	public function store_redeem( $id )
	{
		$db_member_promotion = MemberPromotionMap::find( $id );

		if( !$db_member_promotion )
		{
			$msg = 'Member redemption data not found.';

			return $this->redirectBack( $msg, false );
		}
		else
		{
			$now = new Carbon;

			DB::beginTransaction();

			/**
			 * data member promotion
			 */
			$db_member_promotion->is_participate = 'NO';
			$db_member_promotion->status 		 = 'awaiting';
			$db_member_promotion->request_date 	 = $now;
			$db_member_promotion->save();

			/**
			 * change receipt status to requested 
			 */
			$db_receipt = MemberPromotionReceipt::whereMemberPromotionMapId( $db_member_promotion->id )->get();

			foreach( $db_receipt as $dr )
			{
				$dr->request_date 	 = $now;
				$dr->save();
			}

			/**
			 * send mail to admin
			 */
			DB::commit();

			$db_member_promotion->sendRedemptionAdminNotify( $db_member_promotion );
		}

	    return $this->redirectToRoute( 'f.heng.redemptions.success_redeem' );
	}

	/**
	 * page verify to admin
	 */
	public function success_redeem()
	{
		$data = null;

		return $this->output( 'f.heng.redemptions.page_success_redeem', $data );
	}

	public function success_upload()
	{
		$data = null;

		return $this->output( 'f.heng.redemptions.page_success_upload', $data );
	}

    public function receipt_list( $map_id )
    {
    	/**
    	 * data member
    	 */
        $authed_user = \Auth::user();

        $db = MemberPromotionMap::whereId( $map_id )
        						->whereMemberId( $authed_user->id )
        						->first();

        if( !$db )
        {
            return abort( 404 );
        }

        $db_promotion = Promotion::find( $db->promotion_id );

        /**
         * SEO
         */
        $this->seo()->setTitle( sprintf( '%s Redemption Receipt List', $db_promotion->name ));
        $this->seo()->setDescription( sprintf(
		'See your uploaded receipt for %s REDEMPTION at Shop&Win. Check out your current redemption files.'
        , strtoupper($db_promotion->name)));

        $receipts = MemberPromotionReceipt::whereMemberPromotionMapId( $map_id )
                            ->whereMemberId( $authed_user->id )
                            ->orderBy( 'created_at', 'desc' )
                            ->get();

        $brand = $db_promotion->detailBrand();

        foreach( $receipts as $r )
        {
            $r->photos = $r->all_file_receipts();
        }

        $data = compact(
                    'db',
                    'db_promotion',
                    'receipts',
                    'brand'
            );

        return $this->output( 'f.heng.redemptions.receipt-list', $data );
    }


}

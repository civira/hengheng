<?php

namespace App\Http\Controllers\Front\Heng;

class CategoryController extends BaseController
{
	public function index()
	{

		$data = compact( '' );
		return $this->output( 'f.heng.categories.index', $data );
	}
}

<?php

namespace App\Http\Controllers\Front\Heng\Errors;

use App\Http\Controllers\Front\Heng\BaseController;

class NotFoundController extends BaseController
{
	public function render()
	{
		return $this->output( 'errors.404', [], 404 );
	}
}
<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Slider;
use App\Models\Brand;
use App\Models\LuckyDraw;
use App\Models\Promotion;

class HomeController extends BaseController
{
	public function index()
	{
		//https://github.com/artesaos/seotools
		// $this->seo()->setTitle('Home');
  //       $this->seo()->setDescription('This is my page description');
  //       $this->seo()->opengraph()->setUrl('http://current.url.com');
  //       $this->seo()->opengraph()->addProperty('type', 'articles');
  //       $this->seo()->twitter()->setSite('@LuizVinicius73');

		/*
			DB slider
		 */
		$db_slider = Slider::showed()->whereCategory( 'slider' )->take(10)->get();

		$slider_img 	= [];

		foreach( $db_slider as $ds )
		{
			$slider_img[ $ds->id ] 	= $ds->slider_img();
		}

		/*
			DB slider
		 */
		$db_thumb = Slider::showed()->whereCategory( 'thumbnail' )->orderBy( 'created_at', 'desc' )->take(2)->get();

		$thumb_img = [];

		foreach( $db_thumb as $ds )
		{
			$thumb_img[$ds->id] = $ds->slider_img();
		}

		/*
			DN Brands
		 */
		$db_brand = Brand::published()->featured()->orderBy( 'view_counts', 'desc' )->take(14)->get();

		foreach( $db_brand as $f_brand )
		{
			$f_brand[ 'image' ] = $f_brand->file_img();
		}

		/*
			featured lucky draws
		 */
		$db_featured_lucky = LuckyDraw::published()->featured()->orderBy( 'created_at', 'desc' )->take(6)->get();

		foreach( $db_featured_lucky as $f_featured )
		{
			$f_featured[ 'image' ] = $f_featured->file_img();
		}

		/*
			lucky draws
		 */
		$db_lucky = LuckyDraw::published()->notFeatured()->orderBy( 'created_at', 'desc' )->take(6)->get();

		foreach( $db_lucky as $f_lucky )
		{
			$f_lucky[ 'image' ] = $f_lucky->file_img();
		}

		/*
			promotions
		 */
		$db_promotions = Promotion::published()->orderBy( 'created_at', 'desc' )->take(6)->get();

		foreach( $db_promotions as $f_promo )
		{
			$f_promo[ 'image' ] = $f_promo->file_img();
		}

		/*
			compact data
		 */
		$data = compact(
				'db_slider',
				'slider_img',
				'db_thumb',
				'thumb_img',
				'db_lucky',
				'db_featured_lucky',
				'db_promotions',
				'db_brand'
			 );

		return $this->output( 'f.heng.home.home', $data );
	}
}

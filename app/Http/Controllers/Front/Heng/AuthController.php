<?php

namespace App\Http\Controllers\Front\Heng;

use Auth;
use Hash;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Verification;

use App\Http\Controllers\Traits\AuthTrait;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends BaseController
{
	use AuthTrait,
		ThrottlesLogins;

    private function getReferer()
    {
        return $this->r->get( 'l' );
    }

	public function register()
	{
        $referer = $this->getReferer();
		$data = compact( 'referer' );

        $this->seo()->setTitle( 'Register For Shop&Win Account' );
        $this->seo()->setDescription(
'Register for Shop&Win account and start participating in our exclusive lucky draw.' );

		return $this->output( 'f.heng.auth.register', $data );
	}


	public function login()
	{
        $referer = $this->getReferer();
		$data = compact( 'referer' );

        $this->seo()->setTitle( 'Log in to Shop&Win' );
        $this->seo()->setDescription(
'Log in to Shop&Win to start participating in our exclusive lucky draw.' );

		return $this->output( 'f.heng.auth.login', $data );
	}

	public function logout()
	{
		Auth::logout();

		return $this->redirectToURL( route('f.heng.home') );
	}

	public function forget_password()
	{
		$data = [];

        $this->seo()->setTitle( 'Forgot Password' );
        $this->seo()->setDescription(
'Forgot your Shop&Win Password? Please enter your email to get help logged in into your account.' );

		return $this->output( 'f.heng.auth.forget-password', $data );
	}

	public function mail_forget_password()
	{
		$rules =
		[
			'email'  			=> 'required|email',

			// 'g-recaptcha-response' => 'required|recaptcha'
		];

		$this->validate( $this->r, $rules );

		extract( $this->r->all() );

		$db_user = User::whereEmail( $email )->first();

		if( !$db_user )
		{
			$status = false;
			$msg = 'Email not registered.';
		}
		else
		{
			$status = true;
			$msg = 'Your Email confirmation for change password has been sent.';

			$db_verification	= Verification::createRecord( $email, $db_user->id, 'forgot-password' );
			$db_verification->sendForgotPasswordEmail( $db_verification );
		}

		return $this->redirectBack( $msg, $status );
	}

	/**
	 * 	verify redirect link for forgot password
	 */
	public function verify_forgot_password( $token )
	{
		$db_verification = Verification::whereType( 'forgot-password' )->whereToken( $token )->orderBy( 'created_at', 'desc' )->first();

		if( !$db_verification )
		{
			return $this->output( 'f.heng.verify.failed' );
		}

		$data = compact(
			'token'
		);

		return $this->output( 'f.heng.auth.update-password', $data );
	}

	public function reset_password()
	{
		$rules =
		[
			'token' 	 => 'required',
			'password'   => 'required|min:6|confirmed'
		];

		$this->validate( $this->r, $rules );

		extract( $this->r->all() );

		/*
			check token
		 */
		$db_verification = Verification::whereType( 'forgot-password' )->whereToken( $token )->orderBy( 'created_at', 'desc' )->first();

		if( !$db_verification )
		{
			$msg = 'Token mismatch.';
			return $this->redirectBack( $msg, false );
		}

		/*
			change password into user database
	 	*/
	 	$db_user = User::find( $db_verification->user_id );
	 	$db_user->password = Hash::make( $password );
	 	$db_user->save();

	 	/*
	 		delete old token if success
	 	 */
		$db_verification->deleteToken();

		return $this->output( 'f.heng.auth.success-password' );
	}

	public function success_password()
	{
		$data = [];
		return $this->output( 'f.heng.auth.success-password', $data );
	}

	public function username()
	{
		return 'email';
	}

	/**
	 * request new verification for member
	 */
	public function request_verification( $mail )
	{
		$status = false;

		if( $mail )
		{
			$db_user = User::whereEmail( $mail )->first();

			if( !$db_user )
			{
				$msg = 'Wrong mail address. Please try again.';
			}
			else
			{
				/*
					create new record in mailsubscriber
				 */
				$db_verif = Verification::createRecord( $db_user->email, $db_user->id, 'member' );

				/*
					send email verification
				 */
				$db_verif->sendUserVerificationEmail( $db_verif );

				/**
				 * message
				 */
				$status = true;
				$msg 	= 'Verification Email has been sent. Please check your inbox or spam folder.';
			}


		}
		else
		{
			$msg = 'Verification process not found. Please try again.';
		}

		return $this->redirectBack( $msg, $status );
	}

	public function auth()
	{
		$remember = $this->r->get( 'remember_me' ) === "YES"? true : false;
		$r_email  = $this->r->get( 'email' );

		/**
		 * check if current email registered in database or not
		 */
		$check_email = User::whereEmail( $r_email )->first();
		if( !$check_email )
		{
			$msg = __('messages.errors.auth.invalid_email');

			return $this->redirectBack( $msg, false );
		}

		return $this->doAuthentication( function( $user )
    	{
    		if( !$user->isFrontendAllowed() )
			{
				$msg = trans( 'messages.errors.auth.not_front_allowed' );
			}
			else if( $user->isBanned() )
			{
				$msg = trans( 'messages.errors.auth.banned' );
			}
			else if( !$user->isVerified() )
			{
				// $user->resendVerificationEmail();
				// $msg = trans( 'messages.errors.auth.not_verified' );

				$url = route( 'f.heng.auth.request_verification', $user->email );

				$msg = sprintf( 'You have not verified your account. Check verification email in your inbox or <a href="%s" class="ct-href">Click Here</a> to resend verification mail.', $url );
			}
			else
			{
				/**
				 * Redirect Back to Referer if  Exists
				 */
				$referer = $this->r->get( 'l' );
                $redirect = route( 'f.heng.home' );

				if( $referer )
				{
					/**
					 * Validate referer
					 * to prevent malicious url
					 */
					$referer  = urldecode(base64_decode($referer));
					$base_url = route( 'f.heng.home' );

					if( strpos( $referer, $base_url ) == 0 )
					{
						$redirect = $referer;
					}

				}

				return $this->redirectToURL( $redirect );
			}

			Auth::logout();

			return $this->redirectBack( $msg, false );

    	}, null, $remember );
	}

	public function store_register( Request $r )
	{
		$rules =
		[
			'email' 				=> 'required|email|unique:heng_db_members,email',
			'password'   			=> 'required|min:6|confirmed',
		];

		$this->validate( $this->r, $rules );

		extract( $this->r->all() );

		/**
		 * security measure for prevent spam
		 */
		if( $r->get( config('constants.security.md5' ) ))
		{
			$msg = 'Please try again.';

			return $this->redirectBack( $msg, false );
		}

		/**
		 * start process
		 */
        $referer = $r->get( 'l' );

		$check_agreement = $r->input( 'check_agreement' );
        // $check_sms       = $r->input( 'check_sms' );
        $check_email     = $r->input( 'check_email' );

		if( !$check_agreement )
		{
			$msg = 'You must agree to our Terms & Conditions in order to sign up.';
			return $this->redirectBack( $msg, false );
		}
		else
		{
			$r  			= new User;
			$r->email 		= $email;
			$r->password 	= Hash::make( $password );
            // $r->promo_sms   = $check_sms==='YES'? USER::PROMO_SMS_ENABLE : User::PROMO_SMS_DISABLE;
            $r->promo_email = $check_email==='YES'? USER::PROMO_EMAIL_ENABLE : User::PROMO_EMAIL_DISABLE;
			$r->save();


			/*
				create new record in mailsubscriber
			 */
			$db 	= Verification::createRecord( $email, $r->id, 'member' );

			/*
				send email verification
			 */
			$db->sendUserVerificationEmail( $db );


			return $this->doAuthentication( function( $user ) use ( $referer ){

                if( $referer )
                {
                    $referer = urldecode( base64_decode($referer));
                    return $this->redirectToURL( $referer );
                }
                else
                {
                    return $this->redirectToRoute( 'f.heng.users.my_account' );
                }
			});
		}
	}

	public function login_facebook( Request $r )
	{
		$r_email = $r->get( 'email' );
		$r_name  = $r->get( 'name' );

		$db_user = User::whereEmail( $r_email )->first();

		if( !$db_user )
		{
			$db_user  				= new User;
			$db_user->email 		= $r_email;
			$db_user->full_name 	= $r_name;
			$db_user->save();
		}

		return $this->doAuthentication( function( $user ){
			return $this->redirectToRoute( 'f.heng.users.my_account' );
		}, $db_user );
	}

}

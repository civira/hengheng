<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Information;

class InformationController extends BaseController
{

	 public function index( $h_slug=null )
	 {
	 	$db_info = Information::published()
	 						->showArticle()
	 						->orderBy( 'sort_order', 'asc' )
	 						->get();

	 	/*
	 		if slug article found
	 	 */
	 	if( $h_slug )
	 	{
	 		$db_article = Information::whereSlug( $h_slug )->first();

	 		/*
	 			abort if slug not found return 404 page not found
	 		 */
	 		if( !$db_article )
	 		{
	 			return abort( 404 );
	 		}

            $this->seo()->setTitle( sprintf( '%s', $db_article->title ));
            $this->seo()->setDescription( sprintf( 'Learn more and get help with %s on Heng Heng QR.', $db_article->title, $db_article->title ));

	 	}
	 	else
	 	{
	 		$db_article = Information::first();
	 	}

	 	$data = compact( 'db_info', 'db_article' );

	 	return $this->output( 'f.heng.information.index', $data );
	 }

}

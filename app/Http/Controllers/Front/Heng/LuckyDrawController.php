<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Receipt;
use App\Models\LuckyDraw;
use App\Models\LuckyDrawRetailer;
use App\Models\MemberLuckyDrawMap;

use App\Models\Counterable;
use App\Models\Winner;
use App\Models\User;

use DB;
use Auth;

use Illuminate\Http\Request;

class LuckyDrawController extends BaseController
{
	public function index()
	{
		$this->seo()->setTitle('Lucky Draw Win Prizes');
        $this->seo()->setDescription('Shop&Win LUCKY DRAW. Dont miss out our exclusive lucky draw, you could be one of our many exclusive lucky draw winner.');

		/*
			LUCKY DRAWS
		 */
		$db_query = LuckyDraw::published()->orderBy( 'created_at', 'desc' );

		$db_lucky = $db_query->paginate(12);

		$db_brand = [];

		foreach( $db_lucky as $db )
		{
			$db[ 'brand_data' ] = $db->detailBrand();
		}

		/*
			total result
		 */
		$total_lucky = $db_query->count();

		$data = compact(
				'db_lucky',
				'total_lucky'
				 );

		return $this->output( 'f.heng.lucky-draws.index', $data );
	}

    public function detailOld( $id )
    {
        $db = LuckyDraw::find( $id );

        if( !$db )
        {
            return abort( 404 );
        }

        return redirect( $db->detailUrl(), 301 );
    }

	public function detail( $brand_name, $slug )
	{
		/*
			current contest
		 */
		$db = LuckyDraw::whereSlug( $slug )->first();

		if( !$slug || !$db )
		{
			return abort(404);
		}

		/*
			SEO
		 */
		$this->seo()->setTitle( seo_meta_helper( 'title', $db->seo, sprintf( '%s Lucky Draw', $db->name ) ));
        $this->seo()->setDescription( 
        	seo_meta_helper(
        		'description',
        		$db->seo,
	        	sprintf(
				'Participate and WIN %s LUCKY DRAW at Shop&Win. Get a chance to win various exclusive reward from our limited exclusive lucky draw.'
		        , strtoupper($db->name))
        	)	
        );

		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'lucky-draw', $db->id, LuckyDraw::class );

		/*
			increase total view
		 */
		$db->total_view += 1;
		$db->save();

		/*
			variable for db image
		 */
		$db->image = $db->file_img();

		/**
		 * brand data
		 * @var [type]
		 */
		$db_brand 	= $db->detailBrand();
		/*
			logged member
		 */

		$log_user = Auth::user();

		/*
			emmpty variables
		 */

		$db_receipts 		= null;
		$db_subscribed 		= null;
		$db_participation 	= null;
		$img_receipts 	= [];
		$img_name 		= [];


		if( $log_user )
		{

			$db_participation = MemberLuckyDrawMap::whereMemberId( $log_user->id )->whereLuckyDrawId( $db->id )->first();

			$db_subscribed = Receipt::whereMemberId( $log_user->id )->whereLuckyDrawId( $db->id )->first();

			/*
				if user subscribed check if any uploaded file
			 */

			if( $db_subscribed )
			{

				$db_receipts = Receipt::whereMemberId( $log_user->id )->whereLuckyDrawId( $db->id )->get();

				/*
					get all receipt data image
				 */
				if( $db_receipts )
				{
					foreach( $db_receipts as $data_receipt )
					{
						$data_receipt[ 'all_files' ] = $data_receipt->all_file_receipts();
					}
				}
			}

		}

		/*
			other contest
		 */
		$db_other = LuckyDraw::where( 'id', '!=', $db->id )
					->published()
					->orderBy( 'created_at', 'desc' )
					->get();

		foreach( $db_other  as $dbo )
		{
			$dbo->brand_data = $dbo->detailBrand();
			$dbo->image		 = $dbo->file_img();
		}

        /**
         * Check if user participate and win
         */
        $user_participated = false;
        if( $db_receipts )
        {
            $user_participated = true;
        }

            $user_won = false;
            $winner   = null;
            if( $log_user && $db->isWinnerAnnounced() )
            {
                $winner = Winner::whereLuckyDrawId( $db->id )
                                ->whereMemberId( $log_user->id )
                                ->first();
                if( $winner )
                {
                    $user_won = true;
                }
            }

        /**
         * RETAILER LOGO
         */
		$db_associate_retailer = $db->lucky_draw_retailer;

		if( !$db_associate_retailer->isEmpty() )
		{
			foreach( $db_associate_retailer as $x )
			{
				$x['retailer_data']	= $x->detailRetailer;
				$x['retailer_logo'] = $x->detailRetailer->file_img();
			}
		}

		/*
			array data
		 */
		$data = compact(
			'db',
			'db_brand',
			'log_user',
			'db_receipts',
			'db_subscribed',
			'db_participation',
			'db_other',
            'user_won',
            'winner',
            'user_participated',
            'db_associate_retailer'
		);

		return $this->output( 'f.heng.lucky-draws.detail', $data );
	}

	public function participation( $id )
	{	
		/**
		 * login member data
		 */
		$log_user = Auth::user();

		$db = LuckyDraw::find( $id );

		if( !$id || !$db )
		{
			return abort(404);
		}

		/**
		 * check participation
		 */
		$check_participation = MemberLuckyDrawMap::whereLuckyDrawId( $id )
												->whereMemberId( $log_user->id )
												->first();

		if( $check_participation )
		{
			$msg = 'This lucky draw already is in your contest cart.';
		}
		else
		{
			/**
			 * create participation 
			 */
			$db_participate = new MemberLuckyDrawMap;
			$db_participate->member_id = $log_user->id;
			$db_participate->lucky_draw_id = $db->id;
			$db_participate->save();

			$msg = __('messages.lucky_draw.subscribed_success');
		}

		return $this->redirectBack( $msg, true );
	}

	public function subscribe_member( Request $r, $id )
	{
		$log_user = Auth::user();

		/*
			get agreement
		 */
		$is_status = false;
		$agreement_check = $r->input( 'check_agreement' );

		if( !$agreement_check )
		{
			$msg = __( 'messages.lucky_draw.no_agreement' );
		}
		else
		{

			if( empty( $log_user->full_name) || empty( $log_user->phone_number) )
			{

				$rules =
				[
					'name'   			=> 'required|min:6',
					'mobile_number' 	=> 'required|digits:8|unique:heng_db_members,phone_number,'.$log_user->id,
				];

				$this->validate( $this->r, $rules );

				extract( $this->r->all() );

				// $db_subscribed = MemberLuckyDrawMap::whereMemberId( $log_user->id )->whereLuckyDrawId( $id )->first();
				/*
					update member data
				 */
				$log_user->full_name 	= $name;
				$log_user->phone_number = $mobile_number;
				$log_user->save();
			}

			/*
				subscribe member data
			 */
			$db_subscribed 					= new MemberLuckyDrawMap;
			$db_subscribed->member_id  		= $log_user->id;
			$db_subscribed->lucky_draw_id  	= $id;
			$db_subscribed->save();

			$is_status = true;
			$msg = __('messages.lucky_draw.subscribed_success');
		}

		return $this->redirectBack( $msg, $is_status );

	}

	public function winner_list( $slug )
	{

		/*
			current contest
		 */
		$db = LuckyDraw::whereSlug( $slug )->first();

		if( !$slug || !$db )
		{
			return abort(404);
		}

        /**
         * SEO
         */
        $this->seo()->setTitle( sprintf( '%s Lucky Draw Winner List', $db->name ));
        $this->seo()->setDescription( sprintf(
		'%s LUCKY DRAW WINNER HAS BEEN ANNOUNCED at Shop&Win. Check out our current lucky winner and claim various exclusive reward.'
        , strtoupper($db->name)));

		/**
		 * BRNADS
		 * @var [type]
		 */
		$db_brand = $db->detailBrand();
		$db_brand[ 'image' ] = $db_brand->file_img();


		/*
			get all winner list from current lucky draw
		 */
		$db_winner = Winner::whereLuckyDrawId( $db->id )
							->whereIsDisqualify( 'NO' )
							->orderBy( 'position', 'asc' )
							->get();

		foreach( $db_winner as $dw )
		{
			if( !$dw->member_id )
			{
				$dw['member_data'] = new User;
				$dw['member_data']->full_name 	 = $dw->import_name;
				$dw['member_data']->phone_number = $dw->import_phone;
				$dw['member_data']->email 		 = $dw->import_email;
			}
			else
			{
				$dw['member_data'] = User::find( $dw->member_id );
			}
		}

		$data = compact(
				'db',
				'db_brand',
				'db_winner'
				 );

		return $this->output( 'f.heng.lucky-draws.winner-list', $data );
	}

    public function receipt_list( $lucky_draw_id )
    {
        $db = LuckyDraw::find( $lucky_draw_id );

        if( !$db )
        {
            return abort( 404 );
        }


        /**
         * SEO
         */
        $this->seo()->setTitle( sprintf( '%s Lucky Draw Receipt List', $db->name ));
        $this->seo()->setDescription( sprintf(
'See your uploaded receipt for %s LUCKY DRAW at Shop&Win. Check out your current lucky draw and receipts status.'
        , strtoupper($db->name)));

        $authed_user = \Auth::user();
        $receipts = Receipt::whereLuckyDrawId( $db->id )
                            ->whereMemberId( $authed_user->id )
                            ->orderBy( 'created_at', 'desc' )
                            ->get();

        $brand = $db->detailBrand();

        foreach( $receipts as $r )
        {
            $r->photos = $r->all_file_receipts();
        }

        $data = compact(
                    'db',
                    'receipts',
                    'brand'
            );

        return $this->output( 'f.heng.lucky-draws.receipt-list', $data );
    }

}

<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\User;
use App\Models\File;

use App\Models\Verification;
use App\Models\MailSubscriber;

use App\Models\Receipt;
use App\Models\Retailer;
use App\Models\Brand;
use App\Models\LuckyDraw;
use App\Models\Winner;
use App\Models\MemberLuckyDrawMap;
use App\Models\MemberAddress;

use App\Models\MemberPromotionMap;
use App\Models\MemberPromotionReceipt;

use App\Models\PromoCode;
use App\Models\MemberPromoCodeMap;
use App\Models\MemberPromoCodeReceipt;

use App\Models\FreeSample;
use App\Models\MemberFreeSampleRequest;

use App\Models\MemberProfileHistory;

use DB;
use Hash;
use Auth;
use Carbon\Carbon;

use App\Libraries\Simplifies\ImageUpload;
use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Http\Request;
use LVR\Phone\Phone;

class UserController extends BaseController
{
	public function dashboard( Request $request )
	{
		$this->seo()->setTitle('Contest Cart');

		/*
			set active menu
		 */
		$self_active_menu 	= 'dashboard';
		$self_active_child 	= 'lucky-draws';


		/*
			get member data
		 */
		$log_member = Auth::user();
		$self_url_upload 	= $log_member->routeToContestUpload();

		/*
			filtering lucky draw based on user uploading receipt or not
		 */
		// $db_check_receipt = Receipt::whereMemberId( $log_member->id )
		// 							->select( 'lucky_draw_id' )
		// 							->groupBy( 'lucky_draw_id' )
		// 							->get();

		// $to_array_lucky = $db_check_receipt->pluck( 'lucky_draw_id' )->toArray();

		/**
		 * 	get all lucky draw from database member lucky draw map where not in receipt lucky draw id
		 */
		$db_contests = MemberLuckyDrawMap::whereMemberId( $log_member->id )
										// ->whereNotIn( 'lucky_draw_id', $to_array_lucky )
										->whereHas('lucky_draw', function ($query){
                                            	$query->validContest();
											})
										->orderBy( 'created_at', 'desc' )
										->paginate(10);


        /**
         * foreach data
         * @var array
         */
        $data_group = [];
		foreach( $db_contests as $dc )
		{
			$curr_date = date_common( $dc->created_at );

			$receipts = Receipt::whereLuckyDrawId( $dc->lucky_draw_id )
								->whereMemberId( $log_member->id )
								->get();

			$data_group[ $curr_date ][$dc->id][ 'lucky_data' ] 	= $dc->detailLuckyDraw();
			$data_group[ $curr_date ][$dc->id][ 'brand_data' ] 	= $dc->detailLuckyDraw()->detailBrand();
			$data_group[ $curr_date ][$dc->id][ 'receipt_data' ] 	= $receipts;

			/**
			 * get all file receipts in system files 
			 */
			$db_all_receipts	= Receipt::whereMemberId( $log_member->id )->whereLuckyDrawId( $dc->lucky_draw_id )->get();
			$total_receipt  	= 0;
			foreach( $db_all_receipts as $dar )
			{
				$count_receipt = $dar->all_file_receipts()->count();
				$total_receipt = $total_receipt + $count_receipt;
			}

			$data_group[ $curr_date ][$dc->id][ 'total_receipt' ] = $total_receipt;

		}

		$data = compact(
			'self_active_menu',
			'self_active_child',
			'self_url_upload',
			'log_member',
			'db_contests',
			'data_group'
			 );
		return $this->output( 'f.heng.users.dashboard', $data );

	}

	public function redemption_list( Request $request )
	{
		$this->seo()->setTitle('Redemption Cart');

		/*
			set active menu
		 */
		$self_active_menu = 'dashboard';
		$self_active_child = 'redemptions';

		/*
			get member data
		 */
		$log_member 	= Auth::user();
		$self_url_upload 	= $log_member->routeToRedemptionUpload();

		/**
		 * 	get all promotion where participation is YES
		 */
		$db_contests = MemberPromotionMap::whereMemberId( $log_member->id )
                                        ->participated()
                                        ->whereHas('promotion', function ($query) {
                                            $query->validContest();
                                        })
										->orderBy( 'created_at', 'desc' )
										->paginate(10);
        /**
         * foreach data
         * @var array
         */
        $data_group = [];
		foreach( $db_contests as $dc )
		{
			$curr_date = date_common( $dc->created_at );

			/**
			 * get all file receipts in system files 
			 */
			$db_total_receipt	= MemberPromotionReceipt::whereMemberPromotionMapId( $dc->id )->count();

			$data_group[ $curr_date ][$dc->id][ 'redemption_data' ] = $dc->detailPromotion();
			$data_group[ $curr_date ][$dc->id][ 'brand_data' ] 		= $dc->detailPromotion()->detailBrand();
			$data_group[ $curr_date ][$dc->id][ 'receipt_count' ] 	= $db_total_receipt;
			$data_group[ $curr_date ][$dc->id][ 'receipt_url' ] 	= $dc->receiptListUrl();
		}

		$data = compact(
			'self_active_menu',
			'self_active_child',
			'self_url_upload',
			'log_member',
			'db_contests',
			'data_group'
			 );

		return $this->output( 'f.heng.users.redemption_list', $data );

	}

	public function promo_code_list( Request $request )
	{

		$this->seo()->setTitle('Promo Code Cart');

		/*
			set active menu
		 */
		$self_active_menu = 'dashboard';
		$self_active_child = 'promo-codes';

		/*
			get member data
		 */
		$log_member 	= Auth::user();
		$self_url_upload 	= $log_member->routeToPromoCodeUpload();

		/**
		 * 	get all lucky draw from database member lucky draw map where not in receipt lucky draw id
		 */
		$db_contests = MemberPromoCodeMap::whereMemberId( $log_member->id )
                                        ->participated()
                                        ->whereHas('promo_code', function ($query){
                                            $query->validContest();
                                        })
										->orderBy( 'created_at', 'desc' )
										->paginate(10);
        /**
         * foreach data
         * @var array
         */
        $data_group = [];
		foreach( $db_contests as $dc )
		{
			$curr_date = date_common( $dc->created_at );

			$db_total_receipt	= MemberPromoCodeReceipt::whereMemberPromoCodeMapId( $dc->id )->count();

			$data_group[ $curr_date ][$dc->id][ 'promo_data' ] 		= $dc->detailPromoCode();
			$data_group[ $curr_date ][$dc->id][ 'receipt_count' ] 	= $db_total_receipt;
			$data_group[ $curr_date ][$dc->id][ 'receipt_url' ] 	= $dc->receiptListUrl();

		}

		$data = compact(
			'self_active_menu',
			'self_active_child',
			'self_url_upload',
			'log_member',
			'db_contests',
			'data_group'
			 );
		return $this->output( 'f.heng.users.promo_code_list', $data );

	}

    public function promo_codes_receipt_list( $id )
    {
    	/**
    	 * [$authed_user description]
    	 */
        $authed_user = \Auth::user();

        /**
         * data map
         */
        $db = MemberPromoCodeMap::whereId( $id )->whereMemberId( $authed_user->id )->first();

        if( !$db )
        {
            return abort( 404 );
        }

        $db_promo_code = PromoCode::find( $db->promo_code_id );

        /**
         * SEO
         */
        $this->seo()->setTitle( sprintf( '%s Promo Code Receipt List', $db->name ));
        $this->seo()->setDescription( sprintf(
		'See your uploaded receipt for %s PROMO CODE at Shop&Win. Check out your current promo code files.'
        , strtoupper($db->name)));

        $receipts = MemberPromoCodeReceipt::whereMemberPromoCodeMapId( $id )
                            ->whereMemberId( $authed_user->id )
                            ->orderBy( 'created_at', 'desc' )
                            ->get();

        foreach( $receipts as $r )
        {
            $r->photos = $r->all_file_receipts();
        }

        $data = compact(
                    'db',
                    'db_promo_code',
                    'receipts'
            );

        return $this->output( 'f.heng.users.promo_codes_receipt_list', $data );
    }


	public function multi_delete_contest( Request $r )
	{
		$member_contest = $r->input( 'member_contest' );

		/*
			get member data
		 */
		$log_member = Auth::user();
		$is_status  = false;

		if( !$member_contest )
		{
			$msg =  'Select a lucky draw to cancel your participation.';
		}
		else
		{
			foreach( $member_contest as $mc )
			{
                if( !isset( $mc ))
                {
                    continue;
                }

				$curr = MemberLuckyDrawMap::whereMemberId( $log_member->id )->whereLuckyDrawId( $mc )->first();

                // $receipts = Receipt::whereMemberId( $log_member->id )
                //                     ->whereLuckyDrawId( $mc )
                //                     ->get();

				// if( $curr && $receipts->isEmpty() )
				if( $curr )
				{
					$curr->delete();
				}
			}


			$is_status  = true;
			$msg  		=  'Lucky draw participation has been successfully cancelled.';

		}

		return $this->redirectBack( $msg, $is_status );
	}

	/**
	 * delete redemption participation
	 */
	public function redemption_delete_participation( Request $r )
	{
		$member_contest = $r->input( 'member_contest' );

		/*
			get member data
		 */
		$log_member = Auth::user();
		$is_status  = false;

		if( !$member_contest )
		{
			$msg =  'Select a redemption to cancel your participation.';
		}
		else
		{
			foreach( $member_contest as $mc )
			{
                if( !isset( $mc ))
                {
                    continue;
                }

				$curr = MemberPromotionMap::find( $mc );
				
				if( $curr )
				{
					/**
					 * unlink image then delete the member promotion map
					 */
					$member_promotion_receipt = MemberPromotionReceipt::whereMemberPromotionMapId( $mc )->get();

					foreach( $member_promotion_receipt as $mpr )
					{
						$files_receipt = $mpr->all_file_receipts();

						/**
						 * foreach files from submitted 
						 */
						foreach( $files_receipt as $fr )
						{
							$db_file = File::whereDiskName( $fr->disk_name )
											->where( 'id', '!=', $fr->id )
											->first();

							if( !$db_file )
							{
								$a = new ImageUpload(
									[
										'old' => $fr->disk_name
									]);
								$a->deleteOldFile();
							}

							/**
							 * delete file system data
							 */
							$fr->delete();
						}
					}

					/**
					 * delete member promotion map
					 */
					$curr->delete();
				}
			}


			$is_status  = true;
			$msg  		=  'Redemption participation has been successfully cancelled.';

		}

		return $this->redirectBack( $msg, $is_status );
	}

	public function promo_code_delete_participation( Request $r )
	{
		$member_contest = $r->input( 'member_contest' );

		/*
			get member data
		 */
		$log_member = Auth::user();
		$is_status  = false;

		if( !$member_contest )
		{
			$msg =  'Select a promo code to cancel your participation.';
		}
		else
		{
			foreach( $member_contest as $mc )
			{
                if( !isset( $mc ))
                {
                    continue;
                }

				$curr = MemberPromoCodeMap::find( $mc );

				if( $curr )
				{
					/**
					 * unlink image then delete the member promotion map
					 */
					$member_promocode_receipt = MemberPromoCodeReceipt::whereMemberPromoCodeMapId( $mc )->get();

					foreach( $member_promocode_receipt as $mpr )
					{
						$files_receipt = $mpr->all_file_receipts();

						/**
						 * foreach files from submitted 
						 */
						foreach( $files_receipt as $fr )
						{
							$db_file = File::whereDiskName( $fr->disk_name )
											->where( 'id', '!=', $fr->id )
											->first();

							if( !$db_file )
							{
								$a = new ImageUpload(
									[
										'old' => $fr->disk_name
									]);
								$a->deleteOldFile();
							}

							/**
							 * delete file system data
							 */
							$fr->delete();
						}
					}

					/**
					 * delete member promotion map
					 */
					$curr->delete();
				}
			}


			$is_status  = true;
			$msg  		=  'Promo code participation has been successfully cancelled.';

		}

		return $this->redirectBack( $msg, $is_status );
	}


	public function delete_contest( $id )
	{
		/*
			get member data
		 */
		$log_member = Auth::user();
		$is_status  = false;

		$curr = MemberLuckyDrawMap::whereMemberId( $log_member->id )->whereId( $id )->first();

		if( !$curr )
		{
			$msg = 'No contest to cancel.';
		}
		else
		{
			/**
			 * 	check if user already uipload receipt then fail the cancelation
			 */
			$db_receipt = Receipt::whereMemberId( $log_member->id )->whereLuckyDrawId( $curr->lucky_draw_id )->first();

			if( $db_receipt )
			{

				$msg = 'Lucky Draw receipt cancellation failed, receipt already Submitted.';
			}
			else
			{
				$is_status = true;

				$curr->delete();

				$msg =  'Lucky draw participation has been successfully cancelled.';
			}

		}

		return $this->redirectBack( $msg, $is_status );
	}

	public function list_contests( $status )
	{
		/*
			get member data
		 */
		$log_member = Auth::user();
		/*
			get ongoing contest from member
		 */
		$all_contest = MemberLuckyDrawMap::whereMemberId( $log_member->id )->whereHas('lucky_draw', function ($query) use ( $status ) {
		    $query->where('status', '=', $status);
		})->orderBy( 'created_at', 'desc')->paginate(5);

		foreach( $all_contest as $oc )
		{
			$oc[ 'lucky_data' ] 	= $oc->detailLuckyDraw();
			$oc[ 'image_lucky' ] 	= $oc[ 'lucky_data' ]->file_img();
			$oc[ 'brand_data' ] 	= $oc[ 'lucky_data' ]->detailBrand();
			$oc[ 'member_receipt' ]	= Receipt::whereLuckyDrawId( $oc->lucky_draw_id )->whereMemberId( $log_member->id )->first();
		}

		if( $status == 'ongoing' )
		{
			$header_title = 'Ongoing Contest';
		}
		else
		{
			$header_title = 'Awaiting Winner Contest';
		}

		$data = compact(
			'header_title',
			'all_contest'
			 );

		return $this->output( 'f.heng.users.contest', $data );
	}


	public function my_account()
	{
        $this->seo()->setTitle('Update Profile');

		$self_active_menu 	= 'my-account';
		$self_active_child 	= null;
		$log_member 	  = true;

        $user = \Auth::user();

        if( $user->dob )
        {
            $user->dob = Date( 'd F Y', strtotime( $user->dob ));
        }

		$data = compact( 
			'self_active_menu', 
			'self_active_child',
			'log_member', 
			'user' );

		return $this->output( 'f.heng.users.my_account', $data );
	}

	public function multi_upload( $checked_id=null )
	{
		$self_active_menu = 'receipt-upload';

        $this->seo()->setTitle( 'Upload Receipt To Win!' );
        $this->seo()->setDescription(
		'Upload your purchase receipt to win exclusive prizes from your selected lucky draws.' );

		/*
			get member data
		 */
		$log_member = Auth::user();

		/*
			filtering lucky draw based on user uploading receipt or not
		 */
		$db_check_receipt = Receipt::whereMemberId( $log_member->id )
									->select( 'lucky_draw_id' )
									->groupBy( 'lucky_draw_id' )
									->get();

		$to_array_lucky = $db_check_receipt->pluck( 'lucky_draw_id' )->toArray();

		/*
			get subscribed contest
		 */
		$all_contest = MemberLuckyDrawMap::whereMemberId( $log_member->id )
											// ->whereNotIn( 'lucky_draw_id', $to_array_lucky )
											->whereHas('lucky_draw', function ($query) {
                                            	$query->validContest();
											})
										->get();

        /**
         * Default Selection
         */
        $selected_str = $this->r->get( 's' );
            $selecteds = explode( ",", $selected_str );

		foreach( $all_contest as $ac )
		{
			$ac[ 'lucky_draw' ] = LuckyDraw::published()->whereId( $ac->lucky_draw_id )->first();
			$ac[ 'brand' ] 		= Brand::find( $ac['lucky_draw']->brand_id );
            $ac[ 'checked' ]    = false;

            foreach( $selecteds as $s )
            {
                if( $s == $ac->lucky_draw_id )
                {
                    $ac[ 'checked' ] = true;
                    break;
                }
            }
		}

		$data = compact(
			'checked_id',
			'self_active_menu',
			'log_member',
			'all_contest',
            'selecteds'
		 );
		return $this->output( 'f.heng.users.receipt_upload', $data );
	}

	public function store_multi_upload( Request $r )
	{
		/*
			member data
		 */
		$log_user  		= Auth::user();

		/*
			get input data
		 */
		$msg 		= null;
		$is_status 	= false;
		$is_error 	= false;
		$old_uploader = null;
		/**
		 * variables
		 */
		$array_contest  = $r->input( 'contest_checked' );
		$image = $this->r->file( 'receipt_upload' );

		if( empty( $array_contest ))
		{
			$msg = 'At least 1 contest must be chosen.';
		}
		else if( empty( $image ) )
		{
			$msg = 'At least 1 photo receipt must be uploaded';
		}
		else
		{
			/**
			 * check each file in upload must be image
			 */
			$approved_file = [];

			DB::beginTransaction();

			/**
			 * 	save into db each file in filesystem
			 * 	for no duplicate image in storage we are only once uploaded the file and get the name
			 * 	file only can be uploaded once after that file is gone need to be uploaded, ERROR output.
			 */
			foreach( $image as $each_file )
			{
				$new_uploader = new ImageUpload(
				[
					'upload' => $each_file,
					'encode' => true
				]);


				if( $new_uploader->hasFile() )
				{
					/**
					 * Upload
					 */
					if( $fileinfo = $new_uploader->upload() ) {

						$approved_file[] = $fileinfo;

					}
					else
					{
						DB::rollback();		
						$msg    = $new_uploader->error();
						break;
					}
				}
				else
				{
					DB::rollback();		
					$msg  		= 'A receipt must be uploaded.';
					break;
				}
			}

			/**
			 *  if no message error then save into database current file uploaded
			 */
			if( empty($msg) )
			{
				/*
					set to data for each contest
				 */
				foreach( $array_contest as $ac )
				{

					/*
						save into db receipt current user upload
					 */
					$db_receipt = new Receipt;
					$db_receipt->lucky_draw_id 	= $ac;
					$db_receipt->member_id 		= $log_user->id;
					$db_receipt->is_validate 	= Receipt::$STATUS_PENDING;
					$db_receipt->save();


					/**
					 * save each file name into system files
					 */
					foreach( $approved_file as $ap )
					{
						$file = new File;
						$file->make( $db_receipt->id, (new Receipt())->getMorphClass(), $ap, 'user_receipt' );
						$file->save();
					}

					/**
					 *  Delete participation in lucky draw
					 */
					$db_lucky_map = MemberLuckyDrawMap::whereMemberId( $log_user->id )->whereLuckyDrawId($ac)->first();
					$db_lucky_map->delete();
				}

				DB::commit();

				$is_status = true;
				$msg = 'Upload receipt success.';

           		// return $this->redirectToRoute(['f.heng.lucky.etail', $redirect_id ], $msg );
	    	    return $this->redirectToRoute( 'f.heng.users.page_success_upload' );
			}
		}


		return $this->redirectBack( $msg, false );
	}

	public function page_success_upload()
	{
		$data = null;

		return $this->output( 'f.heng.users.success_upload', $data );
	}


	public function contest_summary( Request $request )
	{
        $this->seo()->setTitle('Contest Summary');

		$self_active_menu 		= 'contest-summary';
		$self_active_summary 	= 'lucky-draws';
		$self_active_child 		= 'summary-lucky';

		$log_member = Auth::user();

		/*
			request data filter
		 */
        $filter_status = $request->get( 'filter_status', 'all' );

        /*
        	query db
         */
        $query_member_contests = Receipt::select( 'lucky_draw_id' )
        							->whereMemberId( $log_member->id )
									->groupBy( 'lucky_draw_id' );

		if( $filter_status != 'all' )
		{
			$query_member_contests = $query_member_contests->whereHas('lucky_draw', function ($query) use ($filter_status){

											if( $filter_status == 1 )
											{
										    	$query->where('status', '!=', 'winner');
											}else
											{
										    	$query->where('status', '=', 'winner');
											}

										});
		}


		$db_member_contests = $query_member_contests->paginate(10);

		foreach( $db_member_contests as $dc )
		{
			$dc['lucky_data'] 	= LuckyDraw::find( $dc->lucky_draw_id );
			$dc['image_lucky'] 	= $dc['lucky_data']->file_img();
			$dc['brand_data'] 	= $dc['lucky_data']->detailBrand();

			/**
			 * get all file receipts in system files 
			 */
			$db_all_receipts	= Receipt::whereMemberId( $log_member->id )->whereLuckyDrawId( $dc->lucky_draw_id )->get();
			$total_receipt  	= 0;
			foreach( $db_all_receipts as $dar )
			{
				$count_receipt = $dar->all_file_receipts()->count();
				$total_receipt = $total_receipt + $count_receipt;
			}

			$dc[ 'total_receipt' ] = $total_receipt;
		}

		/*
			get all member receipt group it by receipt and then custom pagination
		 */
		// $db_receipt = Receipt::whereMemberId( $log_user->id )
		// 						->select( 'lucky_draw_id' )
		// 						->groupBy( 'lucky_draw_id' )
		// 						->get();

		// $array_lucky_id = $db_receipt->pluck( 'lucky_draw_id' )->toArray();

		// $db_lucky = LuckyDraw::whereIn( 'id', $array_lucky_id )->paginate(6);

		// $img_receipts 	= [];
		// $img_name 		= [];

		// foreach( $db_lucky as $lucky )
		// {
		// 	$lucky['image_file'] = $lucky->file_img();
		// 	$lucky['brand_data'] = $lucky->detailBrand();
		// 	$lucky['winner_data'] = Winner::whereLuckyDrawId( $lucky->id )->wherePosition( 1 )->first();
		// 	$lucky['member_receipt'] = Receipt::whereLuckyDrawId( $lucky->id )->whereMemberId( $log_user->id )->get();

		// 	if( $lucky['member_receipt'] )
		// 	{
		// 		foreach( $lucky['member_receipt'] as $r )
		// 		{
		// 			$img_receipts[ $r->id ] = $r->receipt_img();
		// 			$receipt = $r->files('user_receipt')->first();
		// 			$ext = 'jpg';
		// 			if( $receipt )
		// 			{
		// 				$ext = pathinfo( $receipt->disk_name, PATHINFO_EXTENSION );
		// 			}
		// 			$img_name[ $r->id ] 	= $r->file_rename() . ".$ext";
		// 		}
		// 	}

		// }

		/**
		 * 	append data to link pagination
		 */
		$db_member_contests->appends(request()->input())->links();

		$data = compact(
			'self_active_menu',
			'self_active_summary',
			'self_active_child',
			'db_member_contests',
			'filter_status'
		);

		return $this->output( 'f.heng.users.contest_history', $data );
	}


	public function redemptions_summary( Request $request )
	{
        $this->seo()->setTitle('Redemption Summary');

		$self_active_menu 		= 'contest-summary';
		$self_active_summary 	= 'redemptions';
		$self_active_child 		= 'summary-redemptions';
		$log_member 			= Auth::user();

		/*
			request data filter
		 */
        $filter_status = $request->get( 'filter_status', 'all' );

        /*
        	query db
         */
        $query_list = MemberPromotionMap::whereMemberId( $log_member->id )
        								->requestRedeem();

		if( $filter_status != 'all' )
		{
			$query_list = $query_list->whereStatus( $filter_status );
		}

		$db_list = $query_list->orderBy( 'created_at', 'desc')->paginate(10);

		/**
		 * group data based on crated at
		 */

		$data_group = [];
		foreach( $db_list as $dc )
		{
			$curr_date = date_common( $dc->created_at );

			$data_group[$curr_date][$dc->id]['data_information'] 	= $dc->promotion;
			$data_group[$curr_date][$dc->id]['info_image'] 			= $dc->promotion->file_img();
			$data_group[$curr_date][$dc->id]['brand_data'] 			= $dc->promotion->detailBrand();
			$data_group[$curr_date][$dc->id]['redemption_data'] 	= $dc;

			$data_group[$curr_date][$dc->id]['total_participation'] 	= MemberPromotionReceipt::whereMemberPromotionMapId( $dc->id )
																				->count();
		}

		/**
		 * 	append data to link pagination
		 */
		$db_list->appends(request()->input())->links();

		/**
		 * filter array data select
		 */
		$filter_option = MemberPromotionMap::getFilterSelection();

		$data = compact(
			'filter_option',
			'self_active_menu',
			'self_active_summary',
			'self_active_child',
			'db_list',
			'data_group',
			'filter_status'
		);

		return $this->output( 'f.heng.users.redemption_summary', $data );
	}


	public function promo_codes_summary( Request $request )
	{
        $this->seo()->setTitle('Promo Code Summary');

		$self_active_menu 		= 'contest-summary';
		$self_active_summary 	= 'promo-codes';
		$self_active_child 		= 'summary-promo';

		$log_member = Auth::user();

		/*
			request data filter
		 */
        $filter_status = $request->get( 'filter_status', 'all' );

        /*
        	query db
         */
        $query_list = MemberPromoCodeMap::requestRedeem()
        							->whereMemberId( $log_member->id );

		if( $filter_status != 'all' )
		{
			$query_list = $query_list->whereStatus( $filter_status );
		}

		$db_list = $query_list->orderBy( 'created_at', 'desc' )->paginate(10);

		/**
		 * group data based on date
		 */
		$data_group = [];
		foreach( $db_list as $dc )
		{
			$curr_date = date_common( $dc->created_at );

			$data_group[$curr_date][$dc->id]['data_information'] 	= $dc->promo_code;
			$data_group[$curr_date][$dc->id]['info_image'] 			= $dc->promo_code->file_img();
			$data_group[$curr_date][$dc->id]['promo_data'] 			= $dc;

			$data_group[$curr_date][$dc->id]['total_participation'] 	= MemberPromoCodeReceipt::whereMemberPromoCodeMapId( $dc->id )->count();
		}

		/**
		 * 	append data to link pagination
		 */
		$db_list->appends(request()->input())->links();

		/**
		 * filter option
		 */
		$filter_option = MemberPromoCodeMap::getFilterSelection();

		$data = compact(
			'filter_option',
			'self_active_menu',
			'self_active_summary',
			'self_active_child',
			'db_list',
			'data_group',
			'filter_status'
		);

		return $this->output( 'f.heng.users.promo_codes_summary', $data );
	}

	public function free_samples_summary( Request $request )
	{
        $this->seo()->setTitle('Free Sample Summary');

		$self_active_menu = 'contest-summary';
		$self_active_summary = 'free-samples';
		$self_active_child = 'summary-free';

		$log_member = Auth::user();

		/*
			request data filter
		 */
        $filter_status = $request->get( 'filter_status', 'all' );

        /*
        	query db
         */
        $query_list = MemberFreeSampleRequest::whereMemberId( $log_member->id );

		if( $filter_status != 'all' )
		{
			$query_list = $query_list->whereStatus( $filter_status );
		}

		$db_list = $query_list->orderBy( 'created_at', 'desc')->paginate(10);

		/**
		 * group data from date
		 */
		$data_group = [];
		foreach( $db_list as $dc )
		{
			$curr_date = date_common( $dc->created_at );

			$data_group[$curr_date][$dc->id]['data_information'] 	= $dc->free_sample;
			$data_group[$curr_date][$dc->id]['info_image']			= $dc->free_sample->file_img();
			$data_group[$curr_date][$dc->id]['brand_data'] 			= $dc->free_sample->detailBrand();
			$data_group[$curr_date][$dc->id]['freesample_data'] 	= $dc;

			$data_group[$curr_date][$dc->id]['total_participation'] = MemberFreeSampleRequest::whereMemberId( $log_member->id )
																->whereFreeSampleId( $dc->free_sample_id )
																->count();
		}

		/**
		 * 	append data to link pagination
		 */
		$db_list->appends(request()->input())->links();


		/**
		 * filter option
		 */
		$filter_option = MemberFreeSampleRequest::getFilterSelection();

		$data = compact(
			'filter_option',
			'self_active_menu',
			'self_active_summary',
			'self_active_child',
			'db_list',
			'data_group',
			'filter_status'
		);

		return $this->output( 'f.heng.users.free_sample_summary', $data );
	}

	public function change_password()
	{
        $this->seo()->setTitle('Change Pasword');

		$self_active_menu 	= 'change-password';
		$self_active_child 	= null;

		$log_member 	  = Auth::user();

		$data = compact( 
			'self_active_menu', 
			'self_active_child', 
			'log_member' );
		return $this->output( 'f.heng.users.change_password', $data );

	}

	public function store_password( Request $r )
	{
		$log_member 	= Auth::user();

		/*
			check if password null or not , null = login from facebook
		 */
		if( $log_member->password )
		{
			$current_password = $r->input( 'current_password' );

			$is_error = false;
			if( empty( $log_member->password ))
			{
				$is_error = true;
				$msg = 'Current password required.';
			}
			else if ( !Hash::check( $current_password, $log_member->password ))
			{
				$is_error = true;
				$msg = 'Current password does not match Old Password.';
			}

			/*
				if error
			 */
			if( $is_error )
			{
				return $this->redirectBack( $msg, false );
			}
		}

		/*
			standart rules
		 */
		$rules =
		[
			'password'   			=> 'required|min:6|confirmed'
		];

		$this->validate( $this->r, $rules );

		extract( $this->r->all() );

		/*
			save new password
		 */
		$log_member->password 	= Hash::make( $password );
		$log_member->save();

		$msg = 'Your password has been updated.';

		return $this->redirectBack( $msg, true );
	}


	public static function identification_nric($number)
    {

        if (strlen($number) !== 9) {
            return false;
        }
        $newNumber = strtoupper($number);
        $icArray = [];
        for ($i = 0; $i < 9; $i++) {
            $icArray[$i] = $newNumber{$i};
        }
        $icArray[1] = intval($icArray[1], 10) * 2;
        $icArray[2] = intval($icArray[2], 10) * 7;
        $icArray[3] = intval($icArray[3], 10) * 6;
        $icArray[4] = intval($icArray[4], 10) * 5;
        $icArray[5] = intval($icArray[5], 10) * 4;
        $icArray[6] = intval($icArray[6], 10) * 3;
        $icArray[7] = intval($icArray[7], 10) * 2;

        $weight = 0;
        for ($i = 1; $i < 8; $i++) {
            $weight += $icArray[$i];
        }
        $offset = ($icArray[0] === "T" || $icArray[0] == "G") ? 4 : 0;
        $temp = ($offset + $weight) % 11;

        $st = ["J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A"];
        $fg = ["X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K"];

        $theAlpha = "";
        if ($icArray[0] == "S" || $icArray[0] == "T") {
            $theAlpha = $st[$temp];
        } else if ($icArray[0] == "F" || $icArray[0] == "G") {
            $theAlpha = $fg[$temp];
        }
        return ($icArray[8] === $theAlpha);
    }


	public function store_profile()
	{
		$authed_user 	= \Auth::user();

		$rules =
		[
			'name'   			=> 'required|min:6',
			'mobile_number' 	=> 'required|digits:8|unique:heng_db_members,phone_number,'.$authed_user->id,
			'postal_code' 		=> 'required|digits:6',
			'gender' 			=> 'required',
			'address' 			=> 'required',
			'unit_no' 			=> 'required',
			// 'nric_number' 		=> 'required|unique:heng_db_members,identity_number,'.$authed_user->id,
            'date_of_birth'              => 'required|date_format:d F Y'

			// 'g-recaptcha-response' => 'required|recaptcha'
		];

		$this->validate( $this->r, $rules,
        [
            'date_of_birth.date_format' => 'Incorrect date format'
        ]);

        // $rules =
        // [
        //     'mobile_number' => new Phone
        // ];


        $this->validate( $this->r, $rules );

		extract( $this->r->all() );

		/**
		 * set default input value
		 */
		$promo_email = $this->r->get( 'promo_email', 0);

		/**
		 * begin db process
		 */
		DB::beginTransaction();

		/**
		 * save into database member address if not null
		 */
		if( $authed_user->member_address_id )
		{
			$db_address = MemberAddress::find( $authed_user->member_address_id );

			$db_address->address 	 		= nl2br(htmlentities($address));
			$db_address->postal_code    	= $postal_code;
			$db_address->unit_no    		= $unit_no;
			$db_address->is_default 		= 'YES';
			$db_address->save();
		}

		if( !$authed_user->member_address_id )
		{
			$db_address = new MemberAddress;
			$db_address->address_label 		= 'Home';
			$db_address->member_id 			= $authed_user->id;
			$db_address->address 	 		= nl2br(htmlentities($address));
			$db_address->postal_code    	= $postal_code;
			$db_address->unit_no    		= $unit_no;
			$db_address->is_default 		= 'YES';
			$db_address->save();
		}


		/**
		 * str to time date of birth
		 */
		$normalize_dob = Date( 'Y-m-d', strtotime($date_of_birth));

		/**
		 * save into profile history 
		 * everytime member update profile 
		 */
		
		if( $authed_user->full_name != $name || $authed_user->gender != $gender ||
			$authed_user->dob != $normalize_dob || $authed_user->phone_number != $mobile_number ||
			$authed_user->address != $address || $authed_user->postal_code != $postal_code || 
			$authed_user->unit_no != $unit_no || $authed_user->promo_email != $promo_email )
		{
			$db_history = new MemberProfileHistory;
			$db_history->member_id 			= $authed_user->id;
			$db_history->full_name    		= $name;
			$db_history->gender    			= $gender;
	        $db_history->dob                = $normalize_dob;
			$db_history->phone_number 		= $mobile_number;
			$db_history->address 	 		= nl2br(htmlentities($address));
			$db_history->postal_code    	= $postal_code;
			$db_history->unit_no    		= $unit_no;
	        $db_history->promo_email 	 	= $promo_email;
	        $db_history->save();
		}

		/**
		 * save into db member
		 */
		$r  					= User::find( $authed_user->id );
		$r->full_name    		= $name;
		$r->gender    			= $gender;
		$r->phone_number 		= $mobile_number;
		$r->address 	 		= nl2br(htmlentities($address));
		$r->postal_code    		= $postal_code;
		$r->unit_no    			= $unit_no;
        $r->dob                 = $normalize_dob;
        $r->member_address_id 	= $db_address->id;
        $r->promo_email 	 	= $promo_email;
		$r->save();




		DB::commit();

		$msg = 'Your profile has been Updated.';

		// }

		return $this->redirectBack( $msg, true );
	}

	public function photo_upload()
	{
		/*
			get current user login data
		 */
		$log_user = \Auth::user();

			/**
			 * IMAGE UPLOAD
			 * @var boolean
			 */
			$is_error = false;

			$image 	   = $this->r->file( 'photo_upload' );

			$old_uploader = null;
			$new_uploader = new ImageUpload(
				[
					'upload' => $image,
					'encode' => true
				]);

			if( $new_uploader->hasFile() )
			{
				$old_image = $log_user->file( 'user_photo' )->first();

				if( $old_image )
				{
					/**
					 * @todo DELETE PREVIOUS FILE
					 */

					$old_uploader = new ImageUpload(
						[
							'path' 	=> $old_image->path,
							'old'	=> $old_image->disk_name
						]);

					/**
					 * Delete record from database
					 */
					$old_image->delete();

				}

				/**
				 * Upload
				 */
				if( $fileinfo = $new_uploader->upload() ) {
					$file = new File;
					$file->make( $log_user->id, User::class, $fileinfo, 'user_photo' );
					$file->save();

				}
				else
				{
					$is_error = true;
					$error    = $new_uploader->error();
				}
			}


		if( $is_error )
		{
			$msg 		= $error;
			$is_status 	= false;
		}
		else
		{
			$msg  		= 'Profile updated.';
			$is_status 	= true;
		}

		return $this->redirectBack( $msg, $is_status );
	}


	public function receipt_upload( $id )
	{
		/*
			get current user login data
		 */
		$log_user = \Auth::user();

			/**
			 * IMAGE UPLOAD
			 * @var boolean
			 */
			$is_error = false;

			$image 	   = $this->r->file( 'receipt_upload' );

			$old_uploader = null;
			$new_uploader = new ImageUpload(
				[
					'upload' => $image,
					'encode' => true
				]);

			if( $new_uploader->hasFile() )
			{
				$old_image = null;

				if( $old_image )
				{
					/**
					 * @todo DELETE PREVIOUS FILE
					 */
				}

				/**
				 * Upload
				 */
				if( $fileinfo = $new_uploader->upload() ) {

					/*
						save into db receipt current user upload
					 */
					$db_receipt = new Receipt;
					$db_receipt->lucky_draw_id 	= $id;
					$db_receipt->member_id 		= $log_user->id;
					$db_receipt->save();

					/*
						save into file system
					 */
					$file = new File;
					$file->make( $db_receipt->id, Receipt::class, $fileinfo, 'user_receipt' );
					$file->save();

				}
				else
				{
					$is_error = true;
					$error    = $new_uploader->error();
				}
			}


		if( $is_error )
		{
			$msg = $error;
			$is_status = false;
		}
		else
		{
			$is_status = true;
			$msg = 'Upload receipt success.';
		}

		return $this->redirectBack( $msg, $is_status );

	}


	/**
	 * 	verify newsletter
	 */
	public function verify_subscriber( $token )
	{
		$db_verification = Verification::whereType( 'newsletter' )->whereToken( $token )->orderBy( 'created_at', 'desc' )->first();

 		$error_redirect = false;
		if( !$db_verification )
		{
			$error_redirect = true;
		}
		else if( $db_verification )
		{
			/*
				check if user trying to verify from older email token.
			 */
			$db_mail_check = MailSubscriber::whereEmail( $db_verification->packet )->first();

			if( $db_mail_check )
			{
				$error_redirect = true;
			}
		}

		/*
			if response error true
		 */
		if( $error_redirect )
		{
			return $this->output( 'f.heng.verify.newsletter_failed' );
		}

		/*
			prevent double submit into email.
		 */

		$db_mail  				= new MailSubscriber;
		$db_mail->email  		= $db_verification->packet;
		$db_mail->is_verified  = 1;
		$db_mail->save();

		/*
			delete row token
		 */
		$db_verification->deleteToken();

		return $this->output( 'f.heng.verify.newsletter_success' );
	}

	public function verify_member( $token )
	{
		$db_verification = Verification::whereType( 'member' )->whereToken( $token )->orderBy( 'created_at', 'desc' )->first();

		if( !$db_verification )
		{
			return $this->output( 'f.heng.verify.failed' );
		}

		/*
			prevent double submit into email.
		 */
		$db_user 	= User::find( $db_verification->user_id );
		$db_user->is_verified  = 1;
		$db_user->save();

		/*
			delete row token
		 */
		$db_verification->deleteToken();

		return $this->output( 'f.heng.verify.member_success' );
	}

	public function resend_verification()
	{
		$log_member = Auth::user();



		if( $log_member->is_verified != 1 )
		{

			/*
				create new record in mailsubscriber
			 */
			$db 	= Verification::createRecord( $log_member->email, $log_member->id, 'member' );

			/*
				send email verification
			 */
			$db->resendVerificationEmail( $db );

			$msg = 'Verification email has been sent.
					Please check your inbox or spam folder.';
		}
		else
		{
			$msg = 'Account is verified.';
		}


		return $this->redirectBack( $msg,  true);

	}

	/**
	 * USER = ADDRESS PAGE
	 */

    public function address_page()
    {
        /**
         * SEO
         */
        $this->seo()->setTitle( sprintf( 'Member Address List' ));

		$self_active_menu = 'dashboard';

        $authed_user = Auth::user();

        $db_address = MemberAddress::whereMemberId( $authed_user->id )
        							->orderBy( 'is_default', 'desc' )
        							->get();

        $data = compact(
                'db_address',
                'self_active_menu'
            );

        return $this->output( 'f.heng.users.address-list', $data );
    }

    public function address_edit_page( $id=null )
    {
        $this->seo()->setTitle( sprintf( 'Member Address Update' ));

    	$val_type 		= null;
    	$val_address 	= null;
    	$val_postal 	= null;
    	$val_unit 		= null;
    	$val_id 		= null;

    	if( $id )
    	{
    		$db = MemberAddress::find( $id );

    		$val_id 	 = $db->id;
    		$val_type 	 = $db->address_label;
    		$val_address = $db->address;
    		$val_postal  = $db->postal_code;
    		$val_unit 	 = $db->unit_no;
    	}

		$data = compact(
			'db',
			'val_id',
			'val_type',
			'val_address',
			'val_postal',
			'val_unit'
		);

    	return $this->output( 'f.heng.users.address-form', $data);
    }

    public function address_store( $id=null )
    {
        $log_user = Auth::user();

        /**
         * check if id
         */
        if( $id )
        {
	    	$db = MemberAddress::whereId( $id )
						->whereMemberId( $log_user->id )
						->first();

			if( !$db )
			{
           		return abort( 404 );
			}
        }

		$rules =
		[
			'address_label' 		=> 'required',
			'address' 			=> 'required',
			'unit_no' 			=> 'required',
			'postal_code' 		=> 'required|digits:6'
		];

        $this->validate( $this->r, $rules );

		extract( $this->r->all() );

		if( !$id )
		{
			$db = new MemberAddress;
		}

		$db->member_id 		= $log_user->id;
		$db->address_label 	= $address_label;
		$db->address 		= $address;
		$db->unit_no 		= $unit_no;
		$db->postal_code 	= $postal_code;

		$db->save();

		$msg = 'Updated Address data.';

		return $this->redirectBack( $msg, true, false );
    }

    public function address_default_store( $id )
    {
        $log_user = Auth::user();

    	$db = MemberAddress::whereId( $id )
    						->whereMemberId( $log_user->id )
    						->first();

    	if( !$id || !$db )
    	{
            return abort( 404 );
    	}

    	/**
    	 * change default address from member data
    	 */

    	$prev_default_db = MemberAddress::where( 'id', '!=', $id )
    						->whereMemberId( $log_user->id )
    						->whereIsDefault( 'YES' )
    						->first();

    	if( $prev_default_db )
    	{
    		$prev_default_db->is_default = 'NO';
    		$prev_default_db->save();
    	}


    	/**
    	 * change db member data
    	 */
    	$db->is_default = 'YES';
    	$db->save();

    	$log_user->member_address_id = $db->id;
    	$log_user->save();

    	/**
    	 * save database in user
    	 */
    	$log_user->address = $db->address;
    	$log_user->postal_code = $db->postal_code;
    	$log_user->unit_no 	= $db->unit_no;
    	$log_user->save();

    	$msg = 'Change default address success.';

		return $this->redirectBack( $msg, true );
    }

    public function free_sample_request_list ( $id )
    {
        $db = FreeSample::find( $id );

        if( !$db )
        {
            return abort( 404 );
        }


        /**
         * SEO
         */
        $this->seo()->setTitle( sprintf( '%s Free Sample Request List', $db->name ));
        $this->seo()->setDescription( sprintf(
		'See your uploaded receipt for %s FREE SAMPLE at Shop&Win. Check out your current free sample requests.'
        , strtoupper($db->name)));

        $authed_user = \Auth::user();

        $db_request = MemberFreeSampleRequest::whereFreeSampleId( $id )
                            ->whereMemberId( $authed_user->id )
                            ->orderBy( 'created_at', 'desc' )
                            ->get();

        foreach( $db_request as $dr )
        {
        	$dr->address_information = $dr->member_address;
        }


        $brand = $db->detailBrand();

        $data = compact(
                    'db',
                    'db_request',
                    'brand'
            );

        return $this->output( 'f.heng.users.free_samples_request_list', $data );
    }

    public function address_delete( $id )
    {

        $log_user = Auth::user();


    	$db = MemberAddress::whereMemberId( $log_user->id )
    									->whereId( $id )
    									->first();

    	/**
    	 * variables
    	 */
    	$is_status = false;

    	if( !$id || !$db )
    	{
            return abort( 404 );
    	}
    	else if( $db->isDefaultAddress() )
    	{
    		$msg = 'Can\'t delete default address!';
    	}
    	else
    	{
    		$db->delete();

    		$msg = 'Address deleted.';
    		$is_status = true;
    	}

    	return $this->redirectBack( $msg, $is_status );
    }

}

<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\PromoCode;
use App\Models\PromoCodeRetailer;
use App\Models\Counterable;
use App\Models\MemberPromoCodeMap;
use App\Models\MemberPromoCodeDownloadMap;
use App\Models\MemberPromoCodeReceipt;

use Auth;
use DB;

use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Models\File;
use App\Libraries\Simplifies\ImageUpload;

class PromoCodeController extends BaseController
{
	public function index()
	{
        $this->seo()->setTitle('Get Our Exclusive Promo Code');
        $this->seo()->setDescription('Shop&Win PROMO CODE. Dont miss out our exclusive promo code for discounts, gifts, and many more.');

		/*
			LUCKY DRAWS
		 */
		$db_query = PromoCode::published()->orderBy( 'created_at', 'desc' );

		$db_promo = $db_query->paginate(12);

		/*
			total result
		 */
		$total_promo = $db_query->count();

		$data = compact(
				'db_promo',
				'total_promo'
				 );

		return $this->output( 'f.heng.promo-codes.index', $data );
	}

    public function detailOld( $id )
    {
        $db = PromoCode::find( $id );

        if( !$db )
        {
            return abort( 404 );
        }

        return redirect( $db->detailUrl(), 301 );
    }

	public function detail( $id, $slug )
	{
		/*
			current contest
		 */
		$db = PromoCode::whereId( $id )
                        ->first();

		if( !$db )
		{
			return abort(404);
		}

        /*
            SEO INFO
         */
        $this->seo()->setTitle( seo_meta_helper( 'title', $db->seo, sprintf( '%s Promo Code', $db->name )) );
        $this->seo()->setDescription(
        	seo_meta_helper(
        		'description',
        		$db->seo,
	        	sprintf('GET %s PROMO CODE at Shop&Win. Get for free various exclusive promo code for discounts, gifts, and many more.', strtoupper($db->name))
        	)
    	);

		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'promo-code', $id, PromoCode::class );

		/*
			increase total view
		 */
		$db->total_view += 1;
		$db->save();

        /**
         * db promo code parameter
         */
        $db->image     = $db->file_img();

        /**
         * db brand empty for include
         */
        $db_brand   = null;

        /*
            other contest
         */
        $db_other = PromoCode::where( 'id', '!=', $id )
                    ->published()
                    ->orderBy( 'created_at', 'desc' )
                    ->get();

        foreach( $db_other as $each_other )
        {
            $each_other->image = $each_other->file_img();
        }

        /**
         * RETAILER LOGO
         */
        $db_associate_retailer = $db->promocode_retailer;

        if( !$db_associate_retailer->isEmpty() )
        {
            foreach( $db_associate_retailer as $x )
            {
                $x['retailer_data'] = $x->detailRetailer;
                $x['retailer_logo'] = $x->detailRetailer->file_img();
            }
        }

        /**
         * log user check
         */
        $log_user = Auth::user();

        if( $log_user )
        {
            $db_participation = MemberPromoCodeMap::whereMemberId( $log_user->id )
                                            ->wherePromoCodeId( $db->id )
                                            ->first();
        }
        else
        {
            $db_participation = null;
        }

        /*
            display page based on type data
         */
        if( $db->isTypeDownload() )
        {
            $db_promo_retailers = PromoCodeRetailer::wherePromoCodeId( $id )
                                                    ->get();

            foreach( $db_promo_retailers as $dpr )
            {
                $dpr['data_retailer'] = $dpr->retailer;
            }

            /*
                array data
             */
            $data = compact(
                'db',
                'db_brand',
                'db_participation',
                'db_promo_retailers',
                'db_other',
                'db_associate_retailer'
            );

            return $this->output( 'f.heng.promo-codes.page_download', $data );
            /*
                end display for type A - download view page
             */
        }
        else
        {
            /**
             * page with form type B - receipt view page
             */

            /**
             * if logged user
             */
            $db_exist_participation   = null;
            $receipt_count      = 0;

            if( $log_user )
            {
                /**
                 * exist previous participation
                 */
                $db_exist_participation = MemberPromoCodeMap::whereMemberId( $log_user->id )
                                                    ->wherePromoCodeId( $db->id )
                                                    ->first();

                /**
                 * get current active participation
                 */
                
                $db_active_participation = MemberPromoCodeMap::whereMemberId( $log_user->id )
                                                    ->wherePromoCodeId( $db->id )
                                                    ->participated()
                                                    ->first();
                if( $db_active_participation )
                {

                    $receipt_count = MemberPromoCodeReceipt::whereMemberPromoCodeMapId( $db_active_participation->id )->count();

                }

            }

            /**
             * TEXT INFORMATION DISPLAY
             */
            $txt_main_title = 'Participate to Win!';

            if( $db->isDateExpired() )
            {
                $txt_main_title = 'Promo code period has ended.';
            }
            else if( !$log_user )
            {
                $txt_main_title = 'Click to Redeem!';
            }
            else if( $db_exist_participation )
            {
                $txt_main_title = 'Upload Receipt To Redeem!';
            }
            else
            {
                $txt_main_title = 'Click to Redeem!';
            }

            $display_info['main_title'] = $txt_main_title;

            /*
                array data
             */
            $data = compact(
                'db',
                'db_brand',
                'db_exist_participation',
                'db_active_participation',
                'receipt_count',
                'display_info',
                'log_user',
                'db_other',
                'db_associate_retailer'
            );

            return $this->output( 'f.heng.promo-codes.page_receipt', $data );
        }
	}


    /**
     *  participation for redemption
     */
    public function participate( Request $r, $id )
    {
        $db_promocode = PromoCode::find( $id );

        if( !$id || !$db_promocode )
        {
            return abort(404);
        }


        $is_status = false;
        $log_user = Auth::user();

        /**
         * check if member already participate before or not
         */
        $db_participate = MemberPromoCodeMap::whereMemberId( $log_user->id )
                                            ->wherePromoCodeId( $id )
                                            ->first();

        if( $db_promocode->is_type == 'type-a' )
        {
            $db_participate                 = new MemberPromoCodeMap;
            $db_participate->member_id      = $log_user->id;
            $db_participate->promo_code_id  = $db_promocode->id;
            $db_participate->is_participate = 'YES';
            $db_participate->status         = 'awaiting';
            $db_participate->save();

            /*
                redirect back message
             */
            $msg = __('messages.promo_code.participation_success');
            $is_status = true;
        }
        else
        {
            /*
                get agreement
             */
            $agreement_check = $r->input( 'check_agreement' );

            /*
                conditions
             */
            if( !$agreement_check && !$db_participate )
            {
                $msg = __( 'messages.promo_code.no_agreement' );
            }
            else
            {

                if( empty( $log_user->full_name) || empty( $log_user->phone_number) )
                {

                    $rules =
                    [
                        'name'              => 'required|min:6',
                        'mobile_number'     => 'required|digits:8|unique:heng_db_members,phone_number,'.$log_user->id,
                    ];

                    $this->validate( $this->r, $rules );

                    extract( $this->r->all() );

                    /*
                        update member data
                     */
                    $log_user->full_name    = $name;
                    $log_user->phone_number = $mobile_number;
                    $log_user->save();
                }

                /*
                subscribe member data
                 */
                $db_participate                     = new MemberPromoCodeMap;
                $db_participate->member_id          = $log_user->id;
                $db_participate->promo_code_id      = $id;
                $db_participate->is_participate     = 'YES';
                $db_participate->status             = 'awaiting';
                $db_participate->save();

                $is_status = true;
                $msg = __('messages.promo_code.participation_success');
            }

        }

        return $this->redirectBack( $msg, $is_status );

    }

	public function download_file( $id )
	{
		/*
			current contest
		 */
		$db = PromoCode::find( $id );

		if( !$id || !$db )
		{
			return abort(404);
		}

		$db->download_counts += 1;;
		$db->save();

        $log_user = Auth::user();

        /**
         *  tracking member download
         */
        $db_map = MemberPromoCodeDownloadMap::whereMemberId( $log_user->id )
                                            ->wherePromoCodeId( $id )
                                            ->first();
        if( !$db_map )
        {
            $db_map = new MemberPromoCodeDownloadMap;
            $db_map->member_id      = $log_user->id;
            $db_map->promo_code_id  = $id;
            $db_map->save();
        }


		// $filename = 'temp-image.jpg';
		// $tempImage = tempnam(sys_get_temp_dir(), $filename);
		// copy('https://my-cdn.com/files/image.jpg', $tempImage);

		return response()->download( $db->file_data() );
	}


    /**
     * multi upload page
     */
    public function multi_upload( $checked_id=null )
    {
        $self_active_menu = 'receipt-upload';

        $this->seo()->setTitle( 'Upload Receipt To Redeem!' );
        $this->seo()->setDescription(
        'Upload your purchase receipt to win exclusive prizes from your selected promo code\'s.' );

        /*
            get member data
         */
        $log_user = Auth::user();

        /**
         * check participation
         */
        $db_participation = MemberPromoCodeMap::whereMemberId( $log_user->id )
                                            ->participated()
                                            ->whereHas('promo_code', function ($query){
                                                $query->validContest();
                                            })
                                            ->get();

        /**
         * Default Selection
         */
        $selected_str = $this->r->get( 's' );
            $selecteds = explode( ",", $selected_str );

        foreach( $db_participation as $ac )
        {
            $ac[ 'promotion' ]  = PromoCode::find( $ac->promo_code_id );
            $ac[ 'checked' ]    = false;

            foreach( $selecteds as $s )
            {
                if( $s == $ac->id )
                {
                    $ac[ 'checked' ] = true;
                    break;
                }
            }
        }

        $data = compact(
            'checked_id',
            'self_active_menu',
            'log_user',
            'db_participation',
            'selecteds'
         );
        return $this->output( 'f.heng.promo-codes.receipt_upload', $data );
    }

    /**
     * store multi upload
     */
    public function store_multi_upload( Request $r )
    {
        /*
            member data
         */
        $log_user       = Auth::user();

        /*
            get input data
         */
        $msg        = null;
        $is_status  = false;
        $is_error   = false;
        $old_uploader = null;
        /**
         * variables
         */
        $array_checked  = $r->input( 'checked_box' );
        $image = $this->r->file( 'receipt_upload' );

        if( empty( $array_checked ))
        {
            $msg = 'At least 1 promo code must be chosen.';
        }
        else if( empty( $image ) )
        {
            $msg = 'At least 1 photo receipt must be uploaded';
        }
        else
        {
            /**
             * check each file in upload must be image
             */
            $approved_file = [];

            DB::beginTransaction();

            /**
             *  save into db each file in filesystem
             *  for no duplicate image in storage we are only once uploaded the file and get the name
             *  file only can be uploaded once after that file is gone need to be uploaded, ERROR output.
             */
            foreach( $image as $each_file )
            {
                $new_uploader = new ImageUpload(
                [
                    'upload' => $each_file,
                    'encode' => true
                ]);


                if( $new_uploader->hasFile() )
                {
                    /**
                     * Upload
                     */
                    if( $fileinfo = $new_uploader->upload() ) {

                        $approved_file[] = $fileinfo;

                    }
                    else
                    {
                        DB::rollback();
                        $msg    = $new_uploader->error();
                        break;
                    }
                }
                else
                {
                    DB::rollback();
                    $msg        = 'A receipt must be uploaded.';
                    break;
                }
            }

            /**
             *  if no message error then save into database current file uploaded
             */
            if( empty($msg) )
            {
                /*
                    set to data for each contest
                 */
                foreach( $array_checked as $ac )
                {
                    /**
                     * data explode
                     * first array for member promo code id
                     * second array for promo code id
                     */
                    $data_id = explode( ',', $ac );
                    /*
                        save into db receipt current user upload
                     */
                    $db_receipt = new MemberPromoCodeReceipt;
                    $db_receipt->member_id                  = $log_user->id;
                    $db_receipt->member_promo_code_map_id   = $data_id[0];
                    $db_receipt->promo_code_id              = $data_id[1];
                    $db_receipt->is_valid                   = MemberPromoCodeReceipt::$STATUS_PENDING;
                    $db_receipt->save();

                    /**
                     * save each file name into system files
                     */
                    foreach( $approved_file as $ap )
                    {
                        $file = new File;
                        $file->make( $db_receipt->id, (new MemberPromoCodeReceipt())->getMorphClass(), $ap, 'user_promo_code_receipt' );
                        $file->save();
                    }
                }

                DB::commit();

                $is_status = true;
                $msg = 'Upload receipt success.';

                /**
                 * send mail to administrator for redemption attachment
                 */
                return $this->redirectToRoute( 'f.heng.promo_codes.success_upload' );
            }
        }


        return $this->redirectBack( $msg, false );
    }

    /**
     * page success upload receipt
     */
    public function success_upload()
    {
        $data = null;

        return $this->output( 'f.heng.promo-codes.page_success_upload', $data );
    }

    /**
     * page verify to admin
     */
    public function success_redeem()
    {
        $data = null;

        return $this->output( 'f.heng.promo-codes.page_success_redeem', $data );
    }

    /**
     * member request redeem data
     */
    public function store_redeem( $id )
    {
        $db_member_promo = MemberPromoCodeMap::find( $id );

        if( !$db_member_promo )
        {
            $msg = 'Member promo code data not found.';

            return $this->redirectBack( $msg, false );
        }
        else
        {
            $now = new Carbon;

            DB::beginTransaction();

            /**
             * data member promotion
             */
            $db_member_promo->is_participate = 'NO';
            $db_member_promo->status         = 'awaiting';
            $db_member_promo->request_date   = $now;
            $db_member_promo->save();

            $db_receipt = MemberPromoCodeReceipt::whereMemberPromoCodeMapId( $db_member_promo->id )->get();

            foreach( $db_receipt as $dr )
            {
                $dr->request_date    = $now;
                $dr->save();
            }

            /**
             * send mail to admin
             */
            DB::commit();

            $db_member_promo->sendPromoCodeAdminNotify( $db_member_promo );
        }

        return $this->redirectToRoute( 'f.heng.promo_codes.success_redeem' );
    }


}

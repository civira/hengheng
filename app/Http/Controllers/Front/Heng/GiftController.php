<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Brand;
use App\Models\Retailer;
use App\Models\Promotion;
use App\Models\Counterable;

use Illuminate\Http\Request;

class GiftController extends BaseController
{
	public function index( Request $r )
	{
        $this->seo()->setTitle('Get Exclusive Free Gift For Your Purchases');
        $this->seo()->setDescription('Shop&Win FREE GIFT. Dont miss out our exclusive free gift for your every purchases.');

        /**
         * filter
         */
		$f_brand 	= $r->get( 'filter_brand', 0 );
		$f_retailer = $r->get( 'filter_retailer', 0 );

		/*
			LUCKY DRAWS
		 */
		$db_query = Promotion::published()->categoryGift()->orderBy( 'created_at', 'desc' );

				/*
					query filter
				 */
				if( $f_brand != 0 )
				{
					$db_query = $db_query->where( 'brand_id', '=', $f_brand );
				}

				if( $f_retailer != 0 )
				{
					$db_query = $db_query->whereHas( 'promotion_retailer', function($query) use ( $f_retailer ) {
				        $query->where( 'retailer_id', '=', $f_retailer);
				    });
				}
				/*
					end filter
				 */

		$db_gifts = $db_query->paginate(12);

		foreach( $db_gifts as $dg )
		{
			$dg->brand_data = $dg->detailBrand();
		}

		/*
			total result
		 */
		$total_gifts = $db_query->count();

		/**
		 * vars
		 */
		$db_brands 		= Brand::orderBy( 'name', 'asc' )->get();
		$db_retailers 	= Retailer::orderBy( 'name', 'asc' )->get();

		$data = compact(
				'f_brand',
				'f_retailer',
				'db_gifts',
				'total_gifts',
				'db_brands',
				'db_retailers'
				 );

		return $this->output( 'f.heng.free-gifts.index', $data );
	}

    public function detailOld( $id )
    {
        $db = Promotion::find( $id );

        if( !$db )
        {
            return abort( 404 );
        }

        return redirect( $db->urlGift(), 301 );
    }

	public function detail( $brand, $slug )
	{
		/*
			current contest
		 */
		$db = Promotion::whereSlug( $slug )
                        ->first();

		if( !$db )
		{
			return abort(404);
		}
        $id = $db->id;

        $this->seo()->setTitle( seo_meta_helper( 'title', $db->seo, sprintf( '%s Free Gift', $db->name )) );
        $this->seo()->setDescription( 
        	seo_meta_helper(
        		'description',
        		$db->seo,
        		sprintf( 'GET %s FREE GIFT at Shop&Win. Get for free various exclusive gift from your every purchases.', strtoupper($db->name))
        	)
        );

		/*
			increase total view
		 */
		$db->total_view += 1;
		$db->save();

		$db->image 	= $db->file_img();

		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'free-gift', $id, Promotion::class );

		/*
			brand data
		 */
		$db_brand 	= $db->detailBrand();

		/*
			other contest
		 */
		$db_other = Promotion::where( 'id', '!=', $id )
					->published()
					->categoryGift()
					->orderBy( 'created_at', 'desc' )
					->get();

		foreach( $db_other as $do )
		{
			$do->brand_data = $do->detailBrand();
			$do->image 		= $do->file_img();
		}

        /**
         * RETAILER LOGO
         */
		$db_associate_retailer = $db->promotion_retailer;

		if( !$db_associate_retailer->isEmpty() )
		{
			foreach( $db_associate_retailer as $x )
			{
				$x['retailer_data']	= $x->detailRetailer;
				$x['retailer_logo'] = $x->detailRetailer->file_img();
			}
		}

		/*
			array data
		 */
		$data = compact(
			'db',
			'db_brand',
			'db_other',
			'db_associate_retailer'
		);

		return $this->output( 'f.heng.free-gifts.detail', $data );
	}

}

<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\Brand;
use App\Models\LuckyDraw;

use Illuminate\Http\Request;

use Endroid\QrCode\QrCode;

class ContestController extends BaseController
{
	public function index( Request $request )
	{

		$sort_val = $request->input( 'sort' );

		/*
			CONTEst
		 */
		$query = LuckyDraw::published()->orderBy( 'created_at', 'desc' );

		if( $sort_val == 'popular' )
		{
			$query = $query->orderBy( 'view_counts', 'desc' );
		}
		else
		{
			$query = $query->orderBy( 'created_at', 'desc' );
		}

		/*
			paginate
		 */
		$db_contest = $query->paginate(6);

		$total_contest = $query->count();

		$contest_img 	= [];
		$contest_brand  = [];

		foreach( $db_contest as $dc )
		{
			$contest_img[ $dc->id ] = $dc->file_img();
			$contest_brand[ $dc->id ] = Brand::find( $dc->brand_id );
		}

		/*
			BRAND
		 */

		$data = compact( 
				'db_contest',
				'contest_img',
				'sort_val',
				'total_contest',
				'contest_brand'
				 );

		return $this->output( 'f.heng.contests.index', $data );
	}

	public function detail( $id )
	{
		/*
			current contest
		 */
		$db = LuckyDraw::find( $id );

		if( !$id || !$db )
		{
			return abort(404);
		}

		$db_img = $db->file_img();

		/*
			BRAND
		 */
		$db_brand = Brand::find( $db->brand_id );

		/*
			other contest
		 */
		$db_other = LuckyDraw::where( 'id', '!=', $id )
					->whereBrandId( $db->brand_id )
					->published()
					->orderBy( 'created_at', 'desc' )
					->get();

		$db_other_img = [];

		foreach( $db_other as $do )
		{
			$db_other_img[ $do->id ] = $do->file_img();

		}

		/*
			array data
		 */
		$data = compact(
			'db',
			'db_brand',
			'db_img',
			'db_other',
			'db_other_img'
		);

		return $this->output( 'f.heng.contests.detail', $data );
	}

	public function qr()
	{

		$qrCode = new QrCode('Life is too short to be generating QR codes');

		header('Content-Type: '.$qrCode->getContentType());
		echo '<img="'.$qrCode->writeString().'" width="200" height="200" >"'; 
	}

}

<?php

namespace App\Http\Controllers\Front\Heng;

use App\Http\Controllers\BlankController;
use Illuminate\Http\Request;

use App\Models\Announcement;
use App\Models\Information;
use App\Models\Setting;
use App\Models\Brand;
use App\Models\Retailer;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class BaseController extends BlankController
{
	use SEOToolsTrait;

	public function __construct( Request $r )
	{
		parent::__construct( $r );
	}

    protected function generateMetaSEO()
    {
        $this->seo()->opengraph()->setUrl(url()->current());
    }

	protected function beforeOutput()
	{
        $this->generateMetaSEO();

		$st_setting 			= Setting::first();
		$st_footer_information 	= Information::published()->showFooter()->orderBy( 'sort_order', 'asc' )->take(8)->get();

		$st_footer_brands  		= Brand::published()->featured()->orderBy( 'sort_order', 'asc' )->take(8)->get();
		$st_footer_retailers 	= Retailer::published()->orderBy( 'created_at', 'desc' )->take(8)->get();

		$st_announcements 		= Announcement::published()->orderBy( 'created_at', 'desc' )->take(5)->get();

		return [
			'authed_user' 	 		=> \Auth::user(),
			'st_setting' 	 		=> $st_setting,
			'st_footer_brands' 		=> $st_footer_brands,
			'st_footer_retailers' 	=> $st_footer_retailers,
			'st_footer_info' 		=> $st_footer_information,
			'st_announcements' 		=> $st_announcements
		];
	}

}

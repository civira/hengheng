<?php

namespace App\Http\Controllers\Front\Heng;

use App\Models\FreeSample;
use App\Models\MemberFreeSampleMap;
use App\Models\MemberFreeSampleRequest;

use App\Models\Counterable;
use App\Models\User;
use App\Models\MemberAddress;

use DB;
use Auth;

use Illuminate\Http\Request;

class FreeSampleController extends BaseController
{
	public function index()
	{
		$this->seo()->setTitle('Free Sample Win Prizes');
        $this->seo()->setDescription('Shop&Win FREE SAMPLE. Dont miss out our exclusive free sample, you could be one of our many exclusive free sample winner.');

		/*
			LUCKY DRAWS
		 */
		$db_query = FreeSample::published()->orderBy( 'created_at', 'desc' );

		$db_sample = $db_query->paginate(12);

		$db_brand = [];

		foreach( $db_sample as $db )
		{
			$db[ 'brand_data' ] = $db->detailBrand();
		}

		/*
			total result
		 */
		$total_lucky = $db_query->count();

		$data = compact(
				'db_sample',
				'total_lucky'
				 );

		return $this->output( 'f.heng.free-samples.index', $data );
	}


	public function detail( $brand_slug, $slug )
	{
		/*
			current promotion
		 */
		$db = FreeSample::whereSlug( $slug )->first();

		if( !$db )
		{
			return abort(404);
		}

		/**
		 * 	SEO
		 */
        $this->seo()->setTitle( seo_meta_helper( 'title', $db->seo, sprintf( '%s Free Sample', $db->name )) );
        $this->seo()->setDescription(
        	seo_meta_helper(
        		'description',
        		$db->seo,
	        	sprintf( 'GET %s FREE SAMPLE at Shop&Win. Redeem various exclusive free sample for your every purchases.', strtoupper($db->name))
        	)
        );

		/*
			add counter data for daily view
		 */
		$counter_data = Counterable::counterable_views( 'free-sample', $db->id, Promotion::class );

		/*
			increase total view
		 */
		$db->total_view += 1;
		$db->save();

		/**
		 * 	data parameter
		 */
		$db->image 		= $db->file_img();
		$db_brand 		= $db->detailBrand();

		/**
		 * if logged user
		 */

		$log_user = Auth::user();

		$db_participation 	= null;
		$request_count 		= 0;

		if( $log_user )
		{
			$db_participation = MemberFreeSampleMap::whereMemberId( $log_user->id )
												->whereFreeSampleId( $db->id )
												->first();

			$db_request = MemberFreeSampleRequest::whereMemberId( $log_user->id )
											->whereFreeSampleId( $db->id )
											->get();

			$request_count = $db_request->count();
		}

		/*
			other contest
		 */
		$db_other = FreeSample::where( 'id', '!=', $db->id )
					->published()
					->orderBy( 'created_at', 'desc' )
					->get();

		foreach( $db_other as $do )
		{
			$do->image 			= $do->file_img();
			$do->brand_data 	= $do->detailBrand();
		}

		/**
		 * TEXT INFORMATION DISPLAY
		 */
		$txt_main_title = 'Participate to Win!';

		if( $db->isDateExpired() )
		{
			$txt_main_title = 'Free Sample period has ended.';
		}
		else if( !$log_user )
		{
			$txt_main_title = 'Participate to Redeem!';
		}
		else if( $db_participation )
		{
			$txt_main_title = 'Ready to Try?';
		}
		else
		{
			$txt_main_title = 'Participate to Redeem!';
		}

		$display_info['main_title'] = $txt_main_title;

		/*
			array data
		 */
		$data = compact(
			'db',
			'db_brand',
			'db_participation',
			'request_count',
			'db_other',
			'display_info',
			'log_user'
		);

		return $this->output( 'f.heng.free-samples.detail', $data );
	}


    /**
     * 	participation for redemption
     */
	public function participate( Request $r, $id )
	{
		$log_user = Auth::user();

		/*
			get agreement
		 */
		$is_status = false;
		$agreement_check = $r->input( 'check_agreement' );

		/**
		 * check if member already participate before or not
		 */

		$db_participate = MemberFreeSampleMap::whereMemberId( $log_user->id )
											->whereFreeSampleId( $id )
											->first();

		if( !$agreement_check && !$db_participate )
		{
			$msg = __( 'messages.free_sample.no_agreement' );
		}
		else
		{

			if( empty( $log_user->full_name) || empty( $log_user->phone_number) )
			{

				$rules =
				[
					'name'   			=> 'required|min:6',
					'mobile_number' 	=> 'required|digits:8|unique:heng_db_members,phone_number,'.$log_user->id,
				];

				$this->validate( $this->r, $rules );

				extract( $this->r->all() );

				// $db_subscribed = MemberLuckyDrawMap::whereMemberId( $log_user->id )->whereLuckyDrawId( $id )->first();
				/*
					update member data
				 */
				$log_user->full_name 	= $name;
				$log_user->phone_number = $mobile_number;
				$log_user->save();
			}

			/**
			 * check save participation
			 */
			if( $db_participate )
			{
				$db_participate->is_participate = 'YES';
				$db_participate->save();
			}
			else
			{
				/*
				subscribe member data
				 */
				$db_participate 					= new MemberFreeSampleMap;
				$db_participate->member_id  		= $log_user->id;
				$db_participate->free_sample_id  	= $id;
				$db_participate->is_participate 	= 'YES';
				$db_participate->save();
			}

			$is_status = true;
			$msg = __('messages.free_sample.participation_success');
		}

		return $this->redirectBack( $msg, $is_status );

	}

	public function request_page( $id )
	{

		$db = FreeSample::find( $id );

        $this->seo()->setTitle( sprintf( '%s Request Free Sample', $db->name ) );

		/**
		 * log user
		 */
		$log_user = Auth::user();
		
		/**
		 * db address
		 */
		$db_address = MemberAddress::whereMemberId( $log_user->id )->get();	
		$data = compact(
				'db',
				'db_address'
				 );

		return $this->output( 'f.heng.free-samples.request_form', $data );

	}

	public function request_select_store( $id )
	{
		if( !$id )
		{
			return abort(404);
		}

		/**
		 * 	get selected member address id
		 */
		$selected_address = $this->r->get( 'selected_address' );

		/**
		 * [$log_user description]
		 */
		$log_user = Auth::User();

		$db_request = new MemberFreeSampleRequest;

		$db_request->member_id 			= $log_user->id;
		$db_request->member_address_id 	= $selected_address;
		$db_request->free_sample_id 	= $id;
		$db_request->status 			= 'awaiting';
		$db_request->save();

		$msg = 'Free Sample redeem request success.';

		$db_request->sendMailRequest( $db_request );

	    return $this->redirectToRoute( 'f.heng.free_samples.request_success' );
	}

	public function request_new_store( $id )
	{
		if( !$id )
		{
			return abort(404);
		}


		$log_user = Auth::User();

		$rules =
		[
			'address_label' 	=> 'required',
			'address' 			=> 'required',
			'unit_no' 			=> 'required',
			'postal_code' 		=> 'required|digits:6'
		];

        $this->validate( $this->r, $rules );

		extract( $this->r->all() );

		$db_address = new MemberAddress;
		$db_address->member_id = $log_user->id;
		$db_address->postal_code = $postal_code;
		$db_address->address 	= $address;
		$db_address->unit_no 	= $unit_no;
		$db_address->address_label = $address_label;
		$db_address->save();

		/**
		 * create request map
		 */
		$db_request = new MemberFreeSampleRequest;
		$db_request->member_id 				= $log_user->id;
		$db_request->member_address_id 		= $db_address->id;
		$db_request->free_sample_id 		= $id;
		$db_request->status 				= 'awaiting';
		$db_request->save();

		$db_request->sendMailRequest( $db_request );

	    return $this->redirectToRoute( 'f.heng.free_samples.request_success' );
	}

	public function request_success_page()
	{
		return $this->output( 'f.heng.free-samples.page_success_request');
	}

}

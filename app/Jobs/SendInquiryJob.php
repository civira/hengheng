<?php

namespace App\Jobs;

use App\Models\Setting;

class SendInquiryJob extends BaseMailJob 
{
    /**
     * Variables
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $data )
    {
        parent::__construct();

        $this->data     = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $db_setting = Setting::first();

        $this->m->viewData = 
        [
            'email'         => $db_setting->admin_email,
            'data'          => $this->data
        ];

        /**
         * email subject based on subject chosed from inquiry
         */
        if( $this->data->subject == 'helps' )
        {
            $email_subject = 'emails.titles.inquiry_help';
        }
        else
        {
            $email_subject = 'emails.titles.inquiry_general';
        }

        $this->m->subject =  __( $email_subject, [ env( 'APP_NAME' ) ] );;
        $this->m->view    = 'emails.inquiry'; 

        $this->sendMail( array_get( $this->m->viewData, 'email' ) );
    }
}

<?php

namespace App\Jobs;

class SendForgotPasswordJob extends BaseMailJob 
{
    /**
     * Variables
     */
    private $subscribe;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $subscribe )
    {
        parent::__construct();

        $this->subscribe              = $subscribe;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //if status verification then send email for verification member
        $this->m->viewData = 
        [
            'email'         => $this->subscribe->packet,
            'link'          => $this->subscribe->verifyForgotPasswordUrl(),
            'admin_link'    => 'contact@shopnwin.sg'
        ];

        $this->m->subject =  __( 'emails.titles.forgot_password', [ env( 'APP_NAME' ) ] );;
        $this->m->view    = 'emails.forgot-password'; 

        $this->sendMail( array_get( $this->m->viewData, 'email' ) );
    }
}

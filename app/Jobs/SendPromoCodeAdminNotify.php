<?php

namespace App\Jobs;

use App\Models\Setting;
use App\Models\User;
use App\Models\PromoCode;

class SendPromoCodeAdminNotify extends BaseMailJob
{
    /**
     * Variables
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $data )
    {
        parent::__construct();

        $this->data     = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * data variable
         */
        $db_setting = Setting::first();
        $db_member  = User::find( $this->data->member_id );
        $db_promo   = PromoCode::find( $this->data->promo_code_id );
        $url_link   = config( 'constants.backend_url.promo_code_receipt' ).$this->data->id;

        $this->m->viewData =
        [
            'email'         => $db_setting->admin_email,
            'db_member'     => $db_member,
            'db_promo'      => $db_promo,
            'url_link'      => $url_link
        ];

        $this->m->subject =  __( 'emails.titles.promo_code_receipt', [ env( 'APP_NAME' ) ] );;
        $this->m->view    = 'emails.promo_code';

        $this->sendMail( array_get( $this->m->viewData, 'email' ) );
    }
}

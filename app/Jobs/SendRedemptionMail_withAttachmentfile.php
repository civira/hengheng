<?php

namespace App\Jobs;

use App\Models\Setting;

class SendRedemptionMailWithAttachment extends BaseMailJob 
{
    /**
     * Variables
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $data )
    {
        parent::__construct();

        $this->data     = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $db_setting = Setting::first();

        $this->m->viewData = 
        [
            'email'         => $db_setting->admin_email,
            'data'          => $this->data
        ];

        $this->m->subject =  __( 'emails.titles.redemption', [ env( 'APP_NAME' ) ] );;
        $this->m->view    = 'emails.redemption'; 


        foreach( $this->data->all_file_receipts() as $key=>$rp )
        {
            /**
             * [$file]
             */
            $file_ext = help_file_extension( $rp->content_type );
            $file_num = $key+1;
            $file_name = sprintf('attachment_%s%s', $file_num, $file_ext);

            $this->m->attach( $rp->original(), [ 'as'=> $file_name, 'mime'=> $rp->content_type ] );
        }

        $this->sendMail( array_get( $this->m->viewData, 'email' ) );
    }
}

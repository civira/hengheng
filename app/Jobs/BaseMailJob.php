<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Mail\Mailable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

/**
 * Tutorial:
 *
 * https://appdividend.com/2017/12/21/laravel-queues-tutorial-example-scratch/
 * https://laravel.com/docs/5.5/mail
 *
 * How to Kill nohup process:
 * https://stackoverflow.com/questions/17385794/how-to-get-the-process-id-to-kill-a-nohup-process
 *
 * How to Run nohup process:
 * https://stackoverflow.com/questions/28623001/how-to-keep-laravel-queue-system-running-on-server/28625847
 *
 * How to setup supervisorctl to autorun nohup:
 * https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-supervisor-on-ubuntu-and-debian-vps
 * https://stackoverflow.com/questions/37474439/run-php-artisan-without-change-directory
 * https://stackoverflow.com/questions/40909842/supervisor-fatal-exited-too-quickly-process-log-may-have-details
 */
class BaseMailJob
{
    protected $m;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->m = new Mailable;
        $this->m->from    =
        [
            [ 'address' => 'no-reply@shopnwin.sg', 'name'    => env( 'APP_NAME' ) ]
        ];
    }

    public function sendMail( $to )
    {
       return Mail::to( $to )->later( Carbon::now(), $this->m );
    }
}

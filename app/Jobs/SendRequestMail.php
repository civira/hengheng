<?php

namespace App\Jobs;

use App\Models\Setting;
use App\Models\User;
use App\Models\FreeSample;

class SendRequestMail extends BaseMailJob 
{
    /**
     * Variables
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $data )
    {
        parent::__construct();

        $this->data     = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * data variable
         */
        $db_setting = Setting::first();
        $db_member  = User::find( $this->data->member_id );
        $db_sample   = FreeSample::find( $this->data->free_sample_id );
        $url_link   = config( 'constants.backend_url.free_sample_request' ).$this->data->id;

        $this->m->viewData = 
        [
            'email'         => $db_setting->admin_email,
            'db_member'     => $db_member,
            'db_sample'      => $db_sample,
            'url_link'      => $url_link
        ];

        $this->m->subject =  __( 'emails.titles.free_sample_request', [ env( 'APP_NAME' ) ] );;
        $this->m->view    = 'emails.free_sample'; 

        $this->sendMail( array_get( $this->m->viewData, 'email' ) );
    }
}

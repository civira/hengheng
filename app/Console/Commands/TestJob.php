<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use DB;

use App\Models\Inquiry;

class TestJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'heng:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'testing Shop&Win cronjob';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $msg = 'hello cron job heng';

        $db = new Inquiry;
        $db->subject = 'test heng';
        $db->name       = 'test';
        $db->email      = 'hello';
        $db->phone_number = '123';
        $db->comment    = '123';
        $db->save();

        $this->info( $msg );
    }
}

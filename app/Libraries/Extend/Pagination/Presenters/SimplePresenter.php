<?php

namespace App\Libraries\Extend\Pagination\Presenters;

use Illuminate\Pagination\BootstrapThreePresenter;

class SimplePresenter extends BootstrapThreePresenter
{
	/**
	 * @return string
	 */
	public function render()
	{
		if( $this->hasPages() )
		{
			return sprintf('
<ul class="pagination">%s %s</ul>',
			$this->getPreviousButton(),
			$this->getNextButton());
		}

		return '';
	}

	/**
	 * @param  string $text
	 * @return string      
	 */
	public function getDisabledNextButton( $text = 'NEXT' )
	{
		return sprintf('
<li class="page next disabled">
	<div class="inner">
		<span class="label">%s</span>
		<span class="site-ico arrow stripe choc right"></span>
	</div>
</li>', $text );
	}

	/**
	 * @param  string $text
	 * @return string      
	 */
	public function getNextButton( $text = 'NEXT' )
	{
		if(! $this->paginator->hasMorePages() )
		{
			return $this->getDisabledNextButton( $text );
		}

		$url = $this->paginator->url(
				$this->paginator->currentPage() + 1
			);

		return sprintf('
<li class="page next">
	<a href="%s">
		<span class="label">%s</span>
		<span class="site-ico arrow stripe choc right"></span>
	</a>
</li>', $url, $text );
	}

	/**
	 * @param  string $text
	 * @return string      
	 */
	public function getPreviousButton( $text = 'PREV' )
	{
		if( $this->paginator->currentPage() <= 1 )
		{
			return $this->getDisabledPreviousButton( $text );
		}

		$url = $this->paginator->url(
				$this->paginator->currentPage() - 1
			);

		return sprintf('
<li class="page prev">
	<a href="%s">
		<span class="site-ico arrow stripe choc left"></span>
		<span class="label">%s</span>
	</a>
</li>', $url, $text );
	}

	/**
	 * @param  string $text
	 * @return string      
	 */
	public function getDisabledPreviousButton( $text = 'PREV' )
	{
		return sprintf('
<li class="page prev disabled">
	<div class="inner">
		<span class="site-ico arrow stripe choc left"></span>
		<span class="label">%s</span>
	</div>
</li>', $text );
	}
}
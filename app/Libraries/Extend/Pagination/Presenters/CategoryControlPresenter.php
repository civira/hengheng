<?php

namespace App\Libraries\Extend\Pagination\Presenters;

use Illuminate\Pagination\BootstrapThreePresenter;

class CategoryControlPresenter extends BootstrapThreePresenter
{
	/**
	 * @return string
	 */
	public function render()
	{
		return sprintf('
		%s %s',
		$this->getPreviousButton(),
		$this->getNextButton());
	}

	/**
	 * @param  string $text
	 * @return string      
	 */
	public function getDisabledNextButton( $text = 'NEXT' )
	{
		return sprintf('
<div class="page next disabled">
	<span class="inner">
		<span class="label">%s</span>
		<span class="site-ico arrow stripe white right"></span>
	</span>
</div>', $text );
	}

	/**
	 * @param  string $text
	 * @return string      
	 */
	public function getNextButton( $text = 'NEXT' )
	{
		if(! $this->paginator->hasMorePages() )
		{
			return $this->getDisabledNextButton( $text );
		}

		$url = $this->paginator->url(
				$this->paginator->currentPage() + 1
			);

		return sprintf('
<div class="page next">
	<a href="%s">
		<span class="label">%s</span>
		<span class="site-ico arrow stripe white right"></span>
	</a>
</div>', $url, $text );
	}

	/**
	 * @param  string $text
	 * @return string      
	 */
	public function getPreviousButton( $text = 'PREV' )
	{
		if( $this->paginator->currentPage() <= 1 )
		{
			return $this->getDisabledPreviousButton( $text );
		}

		$url = $this->paginator->url(
				$this->paginator->currentPage() - 1
			);

		return sprintf('
<div class="page prev">
	<a href="%s">
		<span class="site-ico arrow stripe white left"></span>
		<span class="label">%s</span>
	</a>
</div>', $url, $text );
	}

	/**
	 * @param  string $text
	 * @return string      
	 */
	public function getDisabledPreviousButton( $text = 'PREV' )
	{
		return sprintf('
<div class="page prev disabled">
	<span class="inner">
		<span class="site-ico arrow stripe white left"></span>
		<span class="label">%s</span>
	</span>
</div>', $text );
	}
}
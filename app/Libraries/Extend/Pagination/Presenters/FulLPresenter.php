<?php

namespace App\Libraries\Extend\Pagination\Presenters;

use Illuminate\Support\HtmlString;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\UrlWindow;
use App\Libraries\Extend\Pagination\UrlWindows\SimpleUrlWindow;

class FullPresenter extends SimplePresenter
{
	/**
	 * @param LengthAwarePaginator $paginator 
	 * @param SimpleUrlWindow|null $window   
	 */
	public function __construct( LengthAwarePaginator $paginator, SimpleUrlWindow $window = null )
	{
		$this->paginator = $paginator;
		$this->window    = is_null( $window )? SimpleUrlWindow::make( $paginator, 1 ) : $window->get();
	}

	/**
	 * @param  string $text 
	 * @return string       
	 */
	protected function getDisabledTextWrapper( $text )
	{
		return sprintf('
<li class="page disabled">
	<div class="inner">
		<span class="label">%s</span>
	</div>
</li>
', $text );
	}

	/**
	 * @param  string $url 
	 * @param  string $page
	 * @param  string $rel 
	 * @return string      
	 */
	protected function getAvailablePageWrapper( $url, $page, $rel = null )
	{
		$rel = is_null($rel) ? '' : ' rel="'.$rel.'"';

        return sprintf('
<li class="page">
	<a href="%s" rel="%s">
		<span class="label">%s</span>
	</a>
</li>', $url, $rel, $page );
	}

	/**
	 * @param  string $text 
	 * @return string       
	 */
	protected function getActivePageWrapper( $text )
	{
		return sprintf('
<li class="page disabled active">
	<div class="inner">
		<span class="label">%s</span>
	</div>
</li>', $text );
	}

	/**
     * Render the actual link slider.
     *
     * @return string
     */
    protected function getLinks()
    {
        $html = '';

        extract( $this->window );

        if ( $first ) {
            $html .= $this->getUrlLinks($this->window['first']);
        }

        if ( $slider ) {

        	end( $first );
        	$end = key( $first );

        	reset( $slider );
        	$start = key( $slider );

        	if( $end + 1 != $start )
        	{
	            $html .= $this->getDots();
            }

            $html .= $this->getUrlLinks($this->window['slider']);
        }

        if ( $last ) {

        	if( $slider )
        	{
	        	end( $slider );
	        	$end = key( $slider );
        	}
        	else
        	{
        		end( $first );
        		$end = key( $first );
        	}

        	reset( $last );
        	$start = key( $last );

        	if( $end + 1 != $start )
        	{
            	$html .= $this->getDots();
            }

            $html .= $this->getUrlLinks($this->window['last']);
        }

        return $html;
    }

    public function render( $with_next = true )
    {
    	if( $this->hasPages() )
    	{
    		if( $with_next )
    		{
    			$content = sprintf( 
	                '<ul class="pagination">%s %s %s</ul>',
	                $this->getPreviousButton(),
	                $this->getLinks(),
	                $this->getNextButton() 
		        );
    		}
    		else
    		{
    			$content = sprintf( 
	                '<ul class="pagination">%s</ul>',
	                $this->getLinks()
		        );
    		}

    		return new HtmlString( $content );
    	}

    	return '';
    }
}
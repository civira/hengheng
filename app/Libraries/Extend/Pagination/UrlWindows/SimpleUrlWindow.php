<?php

namespace App\Libraries\Extend\Pagination\UrlWindows;

use Illuminate\Pagination\UrlWindow;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class SimpleUrlWindow extends UrlWindow
{
	/**
     * Get the slider of URLs when a full slider can be made.
     *
     * @param  int  $on_each_side
     * @return array
     */
    protected function getFullSlider($on_each_side)
    {
        return [
            'first'  => $this->getStartUrls( $on_each_side ),
            'slider' => $this->getAdjacentUrlRange($on_each_side),
            'last'   => $this->getFinishUrls( $on_each_side ),
        ];
    }

    /**
     * @param  integer $on_each_side 
     * @return array               
     */
	public function getStartUrls( $on_each_side )
	{
		return $this->paginator->getUrlRange(1, $on_each_side );
	}

	/**
     * @param  integer $on_each_side 
     * @return array               
     */
	public function getFinishUrls( $on_each_side )
	{
		return $this->paginator->getUrlRange(
				$this->lastPage() - ( $on_each_side - 1 ),
				$this->lastPage()
			);
	}

	/**
     * Get the page range for the current page window.
     *
     * @param  int  $onEachSide
     * @return array
     */
    public function getAdjacentUrlRange($onEachSide)
    {
        return $this->paginator->getUrlRange(
            $this->currentPage() - $onEachSide,
            $this->currentPage() + $onEachSide
        );
    }
}
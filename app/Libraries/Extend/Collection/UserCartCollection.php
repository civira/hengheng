<?php

namespace App\Libraries\Extend\Collection;

use Illuminate\Support\Collection;
use App\Models\UserCart;

class UserCartCollection extends Collection
{
	public function __construct( $data )
	{
		$data = $this->prepareData( $data );
		parent::__construct( $data );
	}

	protected function prepareData( $data )
	{
		if( empty( $data )) return [];

		$tmp = [];
		foreach( $data as $d )
		{
			if( isset( $d['product_id'] ) )
			{
				$tmp[] = $d;
			}
			else
			{
				$tmp[] = $this->makeItem( $d[0], $d[1], $d[2] );
			}
		}

		return $tmp;
	}

	public function whereProduct( $product_id )
	{
		return $this->where( 'product_id', $product_id )
					->first();
	}

	protected function makeItem( $id, $qty, $desc )
	{
		$cart = new UserCart;
		$cart->product_id  = $id;
		$cart->qty 		   = $qty;
		$cart->description = $desc;

		return $cart;
	}

	public function addQty( $product_id, $qty, $desc = null )
	{
		$curr = $this->whereProduct( $product_id );
		if( empty( $curr ) )
		{
			$this->push( $this->makeItem( $product_id, $qty, $desc ));
		}
		else
		{
			$curr->qty += $qty;
		}
	}

	public function getValues()
	{
		$values = [];

		$this->each( function( $item, $key ) use ( &$values )
		{
			$values[] = 
			[
				$item->product_id,
				$item->qty,
				$item->description
			];
		});

		return $values;
	}

	public function remove( $product_id )
	{
		$this->each( function( $item, $key ) use ($product_id)
		{
			if( $item->product_id == $product_id )
			{
				unset( $this->items[ $key ] );
				return false; //break;
			}
		});
	}

	public function updateQty( $product_id, $qty )
	{
		$curr = $this->whereProduct( $product_id );
		$curr->qty = $qty;
	}

	public function getProductIds()
	{
		$returns = [];
		$this->each( function( $item, $key ) use ( &$returns )
		{
			$returns[] = $item->product_id;
		});

		return $returns;
	}

	public function count()
	{
		$total = 0;
		$this->each( function( $item, $key ) use ( &$total )
		{
			$total += $item->qty;
		});

		return $total;
	}

}
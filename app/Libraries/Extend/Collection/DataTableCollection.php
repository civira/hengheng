<?php

namespace App\Libraries\Extend\Collection;

use Illuminate\Database\Eloquent\Collection;

class DataTableCollection extends Collection
{
	public function filterByString( $str )
	{
		return $this->filter( function( $val, $key ) use ( $str )
		{
			return strpos( $val, $str );
		});
	}
}
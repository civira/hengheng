<?php

namespace App\Libraries\Extend\Collection;

use Illuminate\Support\Collection;

class RajaOngkirCollection extends Collection
{
	protected $allowed_services =
	[
		'CTC',
		'CTCOKE',
		'CTCYES',
		'CTCSPS',
		'OKE',
		'REG'
	];

	public function getAllowedServices()
	{
		return $this->allowed_services;
	}

	public function getFees()
	{
		if( !$this->isEmpty() )
		{
			$fees = [];
			$this->each( function( $o ) use ( &$fees )
			{
				if( in_array( $o->service, $this->getAllowedServices() ) )
				{
					$fees[] = $o->cost[0]->value;
				}
			});

			if( !empty( $fees ) )
			{
				asort( $fees );
				return $fees;
			}
		}

		return [0];
	}

	public function getFee()
	{
		if( !$this->isEmpty() )
		{
			$fees = $this->getFees();
			return $fees[0];
		}

		return 0;
	}
}
<?php

namespace App\Libraries\Simplifies;

use Exception;

class FileUpload
{
	protected 	$rename = false,
				$uploaded = false,
				$error  = '',
				$configs,
				$file,
				$prev_file,
				$fileinfo,
				$encoded_filename = null;

	protected $mimes =
	[
		'image' =>
		[
			'image/jpeg',
			'image/pict',
			'image/png',
			'image/x-rgb',
			'image/tiff',
			'image/x-tiff',
			'image/vnd.wap.wbmp',
			'image/bmp',
			'image/x-windows-bmp',
			'image/gif'
		],
		'audio' =>
		[
			'audio/mpeg',
			'audio/x-mpeg',
			'audio/midi',
			'audio/x-mid',
			'audio/x-midi',
			'audio/mpeg3',
			'audio/x-mpeg-3',
			'audio/voc',
			'audio/x-voc',
			'audio/wav',
			'audio/x-wav'
		],
		'video' =>
		[
			'video/avi',
			'video/msvideo',
			'video/x-msvideo',
			'video/gl',
			'video/x-gl',
			'video/mpeg',
			'video/x-motion-jpeg',
			'video/quicktime',
			'video/x-mpeg',
			'video/x-mpeq2a'
		]
	];

	/**
	 * Constructor
	 */
	public function __construct( $configs )
	{
		/**
		 * Config validation
		 */
		$configs = $this->validateConfig( $configs );

		$upload = isset( $configs[ 'upload' ] )? $configs[ 'upload' ] : null;
		$this->file = $upload;

		if( isset( $configs[ 'old' ] ) )
		{
			$this->prev_file = $configs[ 'old' ];
		}

		/**
		 * Set configuration default value
		 * Not override previous value
		 * @var Array
		 */
			$configs = $configs + [
				'mimes'    		=> null,
				'size'	   		=> 10 * 1024 * 1024, // 2 MB
				'replace'  		=> false,
				'encode'   		=> false,
				'retry'	   		=> 1000,
				'limit'	   		=> 50,
				'defer_remove'	=> false,
				'disk'			=> 'web'
			];

			$this->configs = $this->prepareConfigs( $configs );

		/**
		 * Set uploaded file information
		 */
			if( $this->hasFile() )
			{
				$this->fileinfo = [
					'mime'  		=> $upload->getMimeType(),
					'name'  		=> $upload->getClientOriginalName(),
					'original_name' => $upload->getClientOriginalName(),
					'raw'   		=> $this->rawName( $upload->getClientOriginalName() ),
					'ext'   		=> $upload->getClientOriginalExtension(),
					'size'  		=> $upload->getSize(),
					'path'			=> $this->configs[ 'path' ], //updated in prepareInfo()
					'type'			=> 'file'
				];
			}

			$this->fileinfo = $this->prepareInfo();
	}

	/**
	 * @return array
	 */
	protected function prepareInfo()
	{
		$fileinfo = $this->fileinfo;

		$mime = $fileinfo[ 'mime' ];
		if( $this->isMime( $mime, $this->mimes[ 'image' ]) )
			$fileinfo[ 'type' ] = 'image';

		elseif( $this->isMime( $mime, $this->mimes[ 'audio']) )
			$fileinfo[ 'type' ] = 'audio';

		elseif( $this->isMime( $mime, $this->mimes[ 'video']) )
			$fileinfo[ 'type' ] = 'video';

		/**
		 * Dimension
		 */
		if( $this->file )
		{
			$s = getimagesize( $this->file );
			$fileinfo[ 'width' ] = $s[ 0 ];
			$fileinfo[ 'height' ] = $s[ 1 ];
		}

		return $fileinfo;
	}

	/**
	 * @param  string $str
	 * @return string
	 */
	protected function splitEncoded( $str )
	{
		$sub_encoded  = substr( $str, 0, 9 );
		return implode( '/', str_split( $sub_encoded, '3' ) );
	}

	/**
	 * @return string
	 */
	protected function encodedPath( $name )
	{
		$name = $this->encoded( $name );
		return $this->splitEncoded( $name );
	}

	/**
	 * @param  array $configs
	 * @return array
	 */
	protected function prepareConfigs( $configs )
	{
		if( !isset( $configs[ 'root' ]) )
		{
			$disk_configs = $this->diskConfigs( $configs[ 'disk' ]);
			$configs[ 'root' ] = implode( '/',
				[
					$disk_configs[ 'root' ],
					$disk_configs[ 'upload_folder' ],
					$disk_configs[ 'visibility' ]
				]);
		}

		/**
		 * Get subdirectories
		 */
		if( !isset( $configs[ 'path' ]) && $this->hasFile() )
		{
			$configs[ 'path' ] = $this->encodedPath( $this->file->getClientOriginalName() );
		}

		return $configs;
	}

	/**
	 * @return array
	 */
	protected function diskConfigs( $disk )
	{
		return config( "filesystems.disks.$disk" );
	}

	/**
	 * Validate configuration variable
	 * (Error and Exception)
	 * @param  Array $configs - Config variable
	 * @return Array          - The prepared config
	 */
	protected function validateConfig( $configs )
	{
		/**
		 * Root can be empty ( "" )
		 */
		if( isset( $configs[ 'root' ]) )
		{
			$configs[ 'root' ] = $this->removeTrailingSlash( $configs[ 'root' ] );
		}

		/**
		 * If set, path can't be empty
		 */
		if( isset( $configs[ 'path' ]) )
		{
			if( empty( $configs[ 'path' ]) ) throw 'Path config is required.';
			$configs[ 'path' ] = $this->removeTrailingSlash( $configs[ 'path' ]);
		}

		return $configs;
	}

	/**
	 * Upload process
	 * @return Array - The new file information if success. False otherwise
	 */
	public function upload()
	{
		$this->uploaded = false;

		//sanitize filename
		$filename = str_slug( $this->fileinfo[ 'raw' ] );

		if( $this->configs[ 'encode' ] )
		{
			$filename =  $this->encoded( $this->fileinfo[ 'name' ] );
		}
		$filename = $this->limited( $filename );
		if( !$this->configs[ 'replace' ] )
		{
			$filename = $this->renamed( $filename );
		}

		if( $this->isAllowed() )
		{
			/**
			 * Create directory if not exists
			 */
			$path = $this->uploadPath();
			$this->makeDirectory( $path );
			$this->file->move( $path, $filename );
			$this->fileinfo[ 'name' ] = $filename;
			$this->fileinfo[ 'raw' ]  = $this->rawName( $filename );

			$this->uploaded = true;

			return $this->fileinfo;
		}

		return false;
	}

	/**
	 * @return string
	 */
	protected function uploadPath()
	{
		$root = $this->configs[ 'root' ];
		$path = $this->configs[ 'path' ];

		return implode( '/', [ $root, $path ]) . '/';
	}

	/**
	 * Create directory if not exists
	 * @param  String $dir - The directory path
	 * @return Void
	 */
	protected function makeDirectory( $dir )
	{
		if( !empty( $dir ) && !file_exists( $dir ) )
		{
			mkdir( $dir, 0777, true );
		}
	}

	/**
	 * Return whether file is already uploaded or not
	 * @return Boolean - True if uploaded, false otherwise
	 */
	public function uploaded()
	{
		return $this->uploaded;
	}

	/**
	 * @param  string  $dir
	 * @return boolean
	 */
	protected function isEmptyDir( $dir )
	{
		if ( !is_readable($dir) ) return false;

		$handle = opendir($dir);
		while ( false !== ( $entry = readdir($handle) ) )
		{
			if ( $entry != "." && $entry != ".." )
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Get renamed file if rename config is enabled
	 * @param  String $name - The file name
	 * @return String 		- The renamed file name
	 */
	protected function renamed( $name )
	{
		$ext 	  = $this->fileinfo[ 'ext' ];
		$raw 	  = $this->rawName( $name );

		$count 	  = 0;
		while( file_exists( $this->uploadFullpath( $name ) ) )
		{
			++$count;
			$name = "$raw$count.$ext";

			if( $count > $this->configs[ 'retry' ] )
			{
				break;
			}
		}

		return $name;
	}

	/**
	 * @return void
	 */
	public function deleteOldThumbnails()
	{
		$raw  = $this->rawName( $this->prev_file );
		$ext  = $this->extension( $this->prev_file );

		$this->deleteThumbnails( $raw, $ext );
	}

	/**
	 * Delete a file
	 * @param  String $filename - The file name
	 * @return Boolean          - True if delete success, false otherwise
	 */
	public function deleteFile( $filename )
	{
		if( empty( $filename ) )
		{
			return false;
		}

		$path      = $this->uploadPath();
		$full_path = $this->uploadFullpath( $filename );

		if( file_exists( $full_path ) )
		{
			unlink( $full_path );
		}

		if( $this->isEmptyDir( $path ) )
		{
			return rmdir( $path );
		}

		return false;
	}

	/**
	 * @param  string $rawname
	 * @param  string $ext
	 * @return void
	 */
	public function deleteThumbnails( $rawname, $ext )
	{
		/**
		 * Delete thumbnails
		 */
		$thumbs = sprintf( '%s/%s-t*.%s',
						$this->uploadPath(),
						$rawname,
						$ext );

		foreach( glob( $thumbs ) as $fullpath )
		{
			unlink( $fullpath );
		}
	}

	/**
	 * Delete the successfully uploaded file
	 * @return Boolean - True if delete is sucess, false otherwise
	 */
	public function rollback()
	{
		if( $this->uploaded )
		{
			$name = $this->fileinfo[ 'name' ];
			$raw  = $this->rawName( $name );
			$ext  = $this->extension( $name );

			$this->deleteThumbnails( $raw, $ext );
			return $this->deleteFile( $name );
		}

		return false;
	}

	/**
	 * Delete any file that is stored as previous data
	 * @return Boolean - true/false if success
	 */
	public function deleteOldFile()
	{
		if( !isset( $this->configs[ 'path' ] ))
		{
			$this->configs[ 'path' ] = $this->splitEncoded( $this->configs[ 'old' ] );
		}

		$this->deleteOldThumbnails();

		/**
		 * Delete current file
		 */
		return $this->deleteFile( $this->prev_file );
	}

	/**
	 * Limit file name only to certain length
	 * @param  String $filename - string which will be encoded
	 * @return string 			- the file name
	 */
	protected function limited( $filename )
	{
		$limit = $this->configs[ 'limit' ];
		$raw   = $this->rawName( $filename );
		return substr( $raw, 0, $limit ) . '.' . $this->fileinfo[ 'ext' ];
	}

	/**
	 * Encode file name
	 * @param  String $filename - string which will be encoded
	 * @return string 			- the file name
	 */
	protected function encoded( $filename )
	{
		if( !empty( $this->encoded_filename ))
		{
			return $this->encoded_filename;
		}

		/**
		 * October CMS Encoding
		 */
        $ext  = strtolower( $this->extension( $filename ) );
        $name = str_replace( '.', '', uniqid(null, true) );

        $filename = isset( $ext )? $name.'.'.$ext : $name;

        $this->encoded_filename = $filename;

        return $this->encoded_filename;
		// return sha1( $filename ) . '.' . $this->extension( $filename );
	}

	/**
	 * Get file name without extension
	 * @param  String $filename - string from which the extension will be stripped
	 * @return string 			- the file name
	 */
	protected function rawName( $filename )
	{
		return preg_replace( '/\\.[^.\\s]{3,4}$/', '', $filename );
	}

	/**
	 * @param  string $filename
	 * @return string
	 */
	protected function extension( $filename )
	{
		return pathinfo( $filename, PATHINFO_EXTENSION );
	}

	/**
	 * Get full upload path includes the filename
	 * @param  String $filename - The filename
	 * @return String           - Full upload path
	 */
	protected function uploadFullpath( $filename )
	{
		$path = $this->uploadPath();

		if( empty( $filename ) )
		{
			$filename = $this->fileinfo[ 'name' ];
		}

		return $path . $filename;
	}

	/**
	 * Getter to get upload file information detail
	 * @param  string $key - Optional. if specified only get key data
	 * @return Array 	   - information detail
	 */
	public function info( $key = null )
	{
		if( isset( $key ) )
		{
			return $this->fileinfo[ $key ];
		}

		return $this->fileinfo;
	}

	/**
	 * File validation base don many condition
	 * E.g: mime, file size, etc.
	 * @return Boolean - true if allowed, false otherwise. In case of false, error info will be stored and can be get through it's getter
	 */
	public function isAllowed()
	{
		$error = null;

		if( empty($this->file) )
		{
			$error = 'No file uploaded.';
		}
		else if( !$this->file->isValid() )
		{
			$error = 'Invalid file.';
		}
		else if( !$this->isCorrectMime() )
		{
			$error = 'Incorrect file type.';
		}
		else if( !$this->isAllowedSize() )
		{
			$error = 'Invalid file size.';
		}

		if( $error )
		{
			$this->error = $error;
			return false;
		}

		return true;
	}

	/**
	 * Check whether any file is uploaded
	 * @return Boolean - True if yes, false otherwise
	 */
	public function hasFile()
	{
		return ( $this->file && $this->file->isValid() );
	}

	/**
	 * Remove trailing slash ('/') from a string
	 * @param  String $str - The string
	 * @return String      - manipulated string
	 */
	protected function removeTrailingSlash( $str )
	{
		if( substr( $str, -1 ) == '/' )
		{
			return substr( $str, 0, -1 );
		}

		return $str;
	}

	/**
	 * @param  string  $mime
	 * @param  array   $mimes
	 * @return boolean
	 */
	public function isMime( $mime, $mimes )
	{
		return in_array( $mime, $mimes );
	}

	/**
	 * Check whether uploaded file is correct in mime type
	 * @return Boolean - true if correct, false otherwise
	 */
	protected function isCorrectMime()
	{
		if( empty( $this->configs[ 'mimes' ] ) )
		{
			return true;
		}

		return $this->isMime( $this->fileinfo[ 'mime' ], $this->configs[ 'mimes' ] );
	}

	/**
	 * Check whether uploaded file size is allowed
	 * @return Boolean - true if allowed, false otherwise
	 */
	protected function isAllowedSize()
	{
		if( empty( $this->configs[ 'size' ] ) )
		{
			return true;
		}

		return $this->fileinfo[ 'size' ] <= $this->configs[ 'size' ];
	}

	/**
	 * Get generated error string if exist.
	 * @return String - error message
	 */
	public function error()
	{
		return $this->error;
	}

	/**
	 * Setter/Getter for file size config
	 * @param  Integer $size - Optional. allowed size
	 * @return Integer       - Stored value
	 */
	public function filesize( $size = null)
	{
		if( isset( $size ) )
		{
			$this->configs[ 'size' ] = $size;
		}

		return $this->configs[ 'size' ];
	}

	/**
	 * Setter/Getter for pending remove flag
	 * @param  Boolean $val - Optional. Set pending remove flag
	 * @return Boolean      - Stored value
	 */
	public function deferRemove( $val = null )
	{
		if( isset( $val ) )
		{
			$this->configs[ 'defer_remove' ] = $val;
		}

		return $this->configs[ 'defer_remove' ];
	}

	/**
	 * Setter/Getter for allowed mimes. Set empty array to allow all
	 * @param  Array $allows - Optional. The allowed mime type in array
	 * @return Array         - Stored value
	 */
	public function mimes( $allows = null )
	{
		if( isset( $allows ) )
		{
			$this->configs[ 'mimes' ] = $allows;
		}

		return $this->configs[ 'mimes' ];
	}

	/**
	 * Setter/Getter for replace
	 * @param  Boolean $val - Optional. true/false
	 * @return Boolean      - stored value
	 */
	public function replace( $val = null )
	{
		if( isset( $val ) )
		{
			$this->configs[ 'replace' ] = $val;
		}

		return $this->configs[ 'replace' ];
	}

	/**
	 * Setter/Getter for name limit
	 * @param  Integer $val - Optional. Name limit
	 * @return Integer      - stored value
	 */
	public function limit( $val = null )
	{
		if( isset( $val ) )
		{
			$this->configs[ 'limit' ] = $val;
		}

		return $this->configs[ 'limit' ];
	}

	/**
	 * Setter/Getter for rename retry limit
	 * @param  Integer $val - Optional. retry limit
	 * @return Integer      - stored value
	 */
	public function retry( $val = null )
	{
		if( isset( $val ) )
		{
			$this->configs[ 'retry' ] = $val;
		}

		return $this->configs[ 'retry' ];
	}

	/**
	 * Setter/Getter for encode name
	 * @param  Boolean $val - Optional. true/false
	 * @return Boolean      - stored value
	 */
	public function encode( $val = null )
	{
		if( isset( $val ) )
		{
			$this->configs[ 'encode' ] = $val;
		}

		return $this->configs[ 'encode' ];
	}


}

<?php

namespace App\Libraries\Simplifies;

class ImageUpload extends FileUpload
{
	public function __construct( $configs )
	{
		$configs[ 'mimes' ] = $this->mimes[ 'image' ];

		parent::__construct( $configs );
	}
}
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'Heng\Db\Models\Slider' => 'App\Models\Slider',
            'Heng\Db\Models\Setting' => 'App\Models\Setting',
            'Heng\Db\Models\Brand' => 'App\Models\Brand',
            'Heng\Db\Models\BrandProduct' => 'App\Models\BrandProduct',
            'Heng\Db\Models\Retailer' => 'App\Models\Retailer',
            'Heng\Db\Models\FreeSample' => 'App\Models\FreeSample',
            'Heng\Db\Models\Promotion' => 'App\Models\Promotion',
            'Heng\Db\Models\LuckyDraw' => 'App\Models\LuckyDraw',
            'Heng\Db\Models\PromoCode' => 'App\Models\PromoCode',
            'Heng\Db\Models\Receipt' => 'App\Models\Receipt',
            'Heng\Db\Models\MemberPromotionReceipt' => 'App\Models\MemberPromotionReceipt',
            'Heng\Db\Models\MemberPromoCodeReceipt' => 'App\Models\MemberPromoCodeReceipt'
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        if ($this->app->environment() !== 'production') {
        }
    }
}

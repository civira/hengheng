<?php

if( !function_exists( 'form_open' ) )
{
    /**
     * @param  string  $url   
     * @param  array   $params
     * @param  boolean $files 
     * @return string
     */
    function form_open( $url, $params = [], $files = false )
    {
        $attrs = '';

        foreach( $params as $k=>$p )
        {
            $attrs .= sprintf( ' %s="%s"', $k, $p );
        }

        if( $files )
        {
            $attrs .= ' enctype="multipart/form-data"';
        }

        $token_html = '';
        if( isset( $params[ 'method' ] ) && $params[ 'method' ] == 'POST' )
        {
            $token_html = '<input type="hidden" name="_token" value="'.csrf_token().'"/>';
        }

        $form_html = sprintf( '<form action="%s" %s>', $url, $attrs );

        return $form_html . $token_html;
    }
}

if( !function_exists( 'form_close' ) )
{
    /**
     * @return string
     */
    function form_close()
    {
        return "</form>";
    }
}

if ( !function_exists( 'form_open_post' ) )
{
    /**
     * Shorthand for more cleaner Form::open with method post and additional id to be sent
     * @param  string  $link         - submitted route name
     * @param  integer $id           - Optional. record id to be appended if needed
     * @param  array   $params       - Optional. Laravel form parameter to be appended into
     * @param  boolean $files        - to include upload file or not
     * @return string                - HTML representation of form tag
     */
    function form_open_post( $link, $params = [], $files = false )
    {
        $url = $link;

        if( !is_array( $params ) ) $params = [];

        $params += [
            'method' => 'POST'
        ];

        return form_open( $url, $params, $files );
    }
    //-------------------------------------------------------
}

if ( !function_exists( 'form_set_value' ) )
{
    /**
     * Shorthand for more cleaner Form::open with method post and additional id to be sent
     * @param  string  $link         - submitted route name
     * @param  integer $id           - Optional. record id to be appended if needed
     * @param  array   $params       - Optional. Laravel form parameter to be appended into
     * @param  boolean $files        - to include upload file or not
     * @return string                - HTML representation of form tag
     */
    function form_set_value( $name, $value = "" )
    {
        if( !empty( old( $name )) ) {
            $value = old( $name );
        }

        return $value;
    }
    //-------------------------------------------------------
}
<?php

if( !function_exists( 'between_dates' ) )
{
    /**
     * @return string
     */
    function between_dates( $data )
    {
        // convert seconds into a specific format
        $start = date("d M y", strtotime($data->start_date));

        $end = date("d M y", strtotime($data->end_date));

        return $start.' - '.$end;
    }
}

if( !function_exists( 'string_dots_value' ) )
{
    /**
     * @return string
     */
    function string_dots_value( $string, $value )
    {
        $count = strlen( $string );

        if( $count > $value )
        {
            $string = mb_strimwidth( $string, 0, $value, '...');
        }

        return $string;
    }
}

if( !function_exists( 'title_dots' ) )
{
    /**
     * @return string
     */
    function title_dots( $string )
    {
        $count = strlen( $string );

        if( $count > 35 )
        {
            $string = mb_strimwidth( $string, 0, 35, '...');
        }

        return $string;
    }
}

if( !function_exists( 'string_dots' ) )
{
    /**
     * @return string
     */
    function string_dots( $string )
    {
        $count = strlen( $string );

        if( $count > 65 )
        {
            $string = mb_strimwidth( $string, 0, 65, '...');
        }

        return $string;
    }
}

if( !function_exists( 'encode_authenticate_redirect' ) )
{
    /**
     * @return string
     */
    function encode_authenticate_redirect( $string )
    {
        return base64_encode(urlencode( $string ));
    }
}

if( !function_exists( 'date_formatted' ) )
{
    /**
     * @return string
     */
    function date_formatted( $string )
    {
        $format =  date( 'jS F Y', strtotime( $string ));

        return $format;
    }
}

if( !function_exists( 'date_common' ) )
{
    /**
     * @return string
     */
    function date_common( $string )
    {
        $format =  date( 'Y-m-d', strtotime( $string ));

        return $format;
    }
}

if( !function_exists( 'ordinal_number' ) )
{
    /**
     * @return string
     */
    function ordinal_number( $number )
    {
       $ends = array('th','st','nd','rd','th','th','th','th','th','th');

        if ((($number % 100) >= 11) && (($number%100) <= 13))
        {
            return $number. 'th';
        }
        else
        {
            return $number. $ends[$number % 10];
        }
    }
}

if( !function_exists( 'get_url_not_authed' ))
{
    function get_url_not_authed()
    {
        $current_url = url()->current();
        return route( 'f.heng.auth.login',
                [
                        'l' => encode_authenticate_redirect( $current_url )
                ]);
    }
}

if( !function_exists( 'mask_string' ) )
{
    /**
     * @return string
     */
    function mask_string( $string, $type=null )
    {
        if( $type == 'email' )
        {
            $mail_segments = explode("@", $string);

            /**
             * sub string the name
             */
            $first_string       = substr( $mail_segments[0], 0, 4);
            $remaining_string   = substr( $mail_segments[0], 4);


            $mail_segments[0]   = str_repeat('*', strlen($remaining_string)+3);
            $implode_string     = implode('@', $mail_segments );

            $full_string = $first_string.$implode_string;


        }
        else
        {
            $substring          = substr( $string, 0, 4 );
            $remaining_string   = substr( $string, 4 );

            $asterisk_string    = str_repeat('*', strlen($remaining_string)+3 );

            $full_string        = $substring.$asterisk_string;
        }

        return $full_string;   
        
    }
}

if( !function_exists( 'seo_meta_helper' ))
{
    function seo_meta_helper( $type, $db, $txt )
    {
        if( $db )
        {
            if( $type == 'title' )
            {
                if( $db->meta_title )
                {
                    return  $db->meta_title;
                }
                else
                {
                    return $txt;
                }
            }
            else
            {
                if( $db->meta_description )
                {
                    return  $db->meta_description;
                }
                else
                {
                    return $txt;
                }
            }
        }
        else
        {
            return  $txt;
        }
    }
}

if( !function_exists( 'help_file_extension' ))
{
    function help_file_extension( $txt )
    {
        $output = '';

        if( $txt == 'image/png' )
        {
            $output = '.png';
        }
        else if( $txt == 'image/jpg' )
        {
            $output = '.jpg';
        }
        else if( $txt == 'image/jpeg' )
        {
            $output = '.jpeg';
        }

        return $output;
    }
}
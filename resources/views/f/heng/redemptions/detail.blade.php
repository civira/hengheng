
@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
	{!! html_css( 'heng/lucky-detail.css' ) !!}
	{!! html_css( 'heng/redemption-detail.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section class="st-container" id="ct_template_container">

    @if( html_message_is_valid() )
        <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class( 'txt-error', 'txt-valid' ) }}">
            {{ html_get_first_message() }}
        </div>
    @endif

	<section class="split-layout merged-mobile">

		<div class="sl-left">
			<div class="panel-article">
				<img src="{{ $db['file_img'] }}" width="765" height="490" alt="{{ $db->name }}" title="{{ $db->name }}" class="pa-main-image">
			</div>
			<section class="split-layout" id="ct_tabs_container">
				<div>
					<ul class="ct-tabs-menu">
						<li>
							<a href="#tabs_information" class="active-tab">Information</a>
						</li>
						@if( $db->agreement_description )
						<li>
							<a href="#tabs_terms_and_conditions">Terms & Conditions</a>
						</li>
						@endif
					</ul>
					<div class="panel-article terms" id="tabs_information">
						<div>
							<div class="pa-header">
								@if( $db_brand )
								<div class="slb-name">{{ $db_brand->name }}</div>
								@endif
								<div class="slb-date">
									<span class="ico-misc calender"></span>
									<span>{{ between_dates( $db ) }}</span>
								</div>
							</div>
							<div class="pa-context ct-page-header">
								<h1 class="slc-title">{{ $db->name }}</h1>
								<span class="cph-bar"></span>
							</div>
							<div class="pa-paragraph">
								{!! $db->description !!}
							</div>
						</div>
					</div>
					@if( $db->agreement_description )
					<div class="panel-article" id="tabs_terms_and_conditions">
						<div>
                            <div class="pa-header">
                                @if( $db_brand )
                                <div class="slb-name">{{ $db_brand->name }}</div>
                                @endif
                                <div class="slb-date">
                                    <span class="ico-misc calender"></span>
                                    <span>{{ between_dates( $db ) }}</span>
                                </div>
                            </div>
                            <div class="pa-context ct-page-header">
                                <div class="slc-title">{{ $db->name }}</div>
                                <span class="cph-bar"></span>
                            </div>
							<div class="pa-paragraph">
								{!! $db->agreement_description !!}
							</div>
						</div>
					</div>
					@endif
				</div>

                <div class="pa-sharing">
                    <div class="title">SHARE:</div>
                    <div class="addthis_inline_share_toolbox"></div>
                </div>
			</section>
		</div>

		<div class="sl-right">

			@if( $db_participation )
			<div class="panel-container info-box m-bottom">
				<div class="fd-main-title">
					More Redemptions
				</div>

				<div class="cf-agreement">
					You can upload receipts until you qualify for redemption. Browse our other redemptions.
				</div>

				<a href="{{ route( 'f.heng.redemptions.index' ) }}">
					<input type="submit" class="fd-button" value="Browse Redemptions">
				</a>
			</div>
			@endif

			<div class="panel-container">
				<!-- Main title text -->
				<div class="fd-main-title">
					{{ $display_info['main_title'] }}
				</div>

				@if( $log_user )

					@if( !html_message_is_valid() )
					<!-- error info -->
					<div class="ct-error-info {{ html_add_error_class() }}">
				    	{{ html_get_first_error_message() }}
					</div>
					@endif

					@if( $db_participation && $db_participation->isParticipateNo() && !$db->isDateExpired() )
						<a href="{{ route( 'f.heng.redemptions.participate',  $db->id ) }}" class="fd-button red">
							Participate Again
						</a>
					@elseif( $db_participation && $db_participation->isParticipateYes() && !$db->isDateExpired() )
						<a href="{{ route( 'f.heng.redemptions.multi_upload', [ null, 's' => $db->id ]) }}" class="fd-button red">
							Upload Receipt
						</a>
					@endif

					{!! form_open_post( route( 'f.heng.redemptions.participate', $db->id ), [ 'id'=>'form_receipt' ], true ) !!}
					@if( !$authed_user->isProfilePartialFilled() && !$db->isDateExpired() )
					<!-- form box data member -->
					<div class="cf-form-box">
						<div class="cf-form full">
							<input type="text" class="cf-input" placeholder="Name as per NRIC/FIN" name="name" value="{{ form_set_value( 'name', $authed_user->full_name ) }}">
							<input type="text" class="cf-input" placeholder="Fill your Email" name="email" value="{{ form_set_value( 'email', $authed_user->email ) }}" disabled="disabled">
							<input type="text" maxlength="8" class="prevent_chars cf-input no-spin" placeholder="8-Digit Mobile Number" name="mobile_number" value="{{ form_set_value( 'mobile_number', $authed_user->phone_number ) }}">
						</div>
					</div>
					@endif

					@if( !$db_participation && !$db->isDateExpired() )
					<!-- checkbox agreement -->
					<div class="cf-agreement">
						<input type="checkbox" name="check_agreement" id="check-agreement" class="css-checkbox" value="YES" @if( old('check_agreement') == 'YES')checked="checked"@endif />
						<label for="check-agreement" class="css-label">
							By participating, you are agreeing to this redemption’s terms & conditions.
						</label>
					</div>
					@endif

					<!-- note -->
					@if( $db->isDateExpired() )
						<div class="fd-guide">
							Sorry, our redemption period has ended.
						</div>
					@elseif( $db_participation )
						<div class="fd-guide">
							Note: <br/>
							You can choose multiple redemptions for a receipt<br/>
							JPG, JPEG, PNG file only
						</div>
					@elseif( !$db_participation && !$authed_user->isProfilePartialFilled() )
						<div class="fd-guide agreement">
							To participate in this redemption, you must fill all the information.
						</div>
					@endif

					@if( !$db_participation && !$db->isDateExpired() )
					<!-- submit button -->
						<div>
							<input type="submit" class="fd-button" value="Redeem">
						</div>
					@endif
					{!! form_close() !!}

					@if( $receipt_count > 0 )
						<!-- url file -->
						<div class="uploaded-receipt-files mobile receipt-count">
                            You have redeemed {{ $receipt_count }} {{ str_plural( 'time', $receipt_count )}}
                            <a href="{{ $db->receiptListUrl() }}" class="txt-link">view all receipts</a>
                        </div>
					@endif

				@else
					<!-- redirect login page -->
					<a href="{{ route( 'f.heng.auth.login', [ 'l' => encode_authenticate_redirect( $db->urlRedemption()) ] ) }}" class="fd-button">Login to Participate</a>
					<div class="fd-guide">
						Note: <br/>
						You can upload multiple receipts<br/>
						You can participate first and upload receipt afterwards<br/>
						JPG, JPEG, PNG file only
					</div>
				@endif

			</div>
			@if( $receipt_count > 0 )
				<!-- url file -->
				<div class="uploaded-receipt-files shift-right receipt-count">
                    You have redeemed {{ $receipt_count }} {{ str_plural( 'time', $receipt_count )}}
                    <a href="{{ $db->receiptListUrl() }}" class="txt-link">view all receipts</a>
				</div>
			@endif
		</div>
	</section>

	@if( !$db_other->isEmpty() )
    <!-- other redemptions data -->
	<section>
		<div class="ct-page-header">
			<h2>
				Other Redemptions
			</h2>
			<span class="cph-bar"></span>
		</div>
		<div id="contest_slider" class="ct-owl-carousel ct-galleries">
			<div class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false"
				 data-sizes= "[ [ 0, 1 ],
						[320, 1],
						[721, 2],
						[840, 3],
						[1040, 3],
						[1220, 3]
					]">
				@foreach( $db_other as $do )
				<div>
					<div class="ctg-relative">
						<a href="{{ $do->urlRedemption() }}">
							<img src="{{ $do->file_img() }}" alt="{{ $do->name }}" title="{{ $do->name }}">
						</a>
					</div>
					<div class="ctg-headings">
						<h4>{{ $do['brand_data']->name }}</h4>
						<h3>
							<a href="{{ $do->urlRedemption() }}" title="{{ $do->name }}">
								{{ string_dots( $do->name ) }}
							</a>
						</h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $do ) }}</span>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>
	@endif

</section>


@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b73f77d0c8e5f78" async="async"></script>
@stop

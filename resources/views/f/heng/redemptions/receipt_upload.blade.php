@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/receipt-upload.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container with-margin">

	<section id="page_receipt_upload">

		{!! form_open_post( route( 'f.heng.redemptions.store_multi_upload' ), [ 'id'=>'form_receipt' ], true ) !!}
        <input type="hidden" name="redirect_id" value="{{ $checked_id }}" />
		<div class="upload-panel">

            <h1 class="fd-main-title">
                 Upload Receipt To Redeem!
            </h1>

            <div class="ct-error-info {{ html_add_error_class() }}">
                {{ html_get_first_error_message() }}
            </div>

            <div id="receipt_dom_wrapper">
                @for( $i = 0; $i < 3; $i++ )
                <div class="upload-receipt-wrapper {{ $i == 0? 'first' : '' }}">
                    <div class="fd-img-preview-wrapper" class="fd-preview">
                        <img class="fd-img-preview" src="#" />
                        <div class="remove fd-remove-img">
                            <span class="i">X</span>
                        </div>
                    </div>

        			<div class="fd-upload-receipt fd-background-upload">
        				<input type="file" id="receipt_upload_{{ $i }}" name="receipt_upload[]" accept="image/*" class="receipt-upload ct-upload-button" />
        				<label for="receipt_upload_{{ $i }}">
            				<div class="upload-img">
            					<div class="ico-receipt-upload"></div>
            					<div>Shopping Receipt</div>
            					<div class="ico-receipt-question"></div>
            				</div>
                        </label>
        			</div>
                </div>
                @endfor
            </div>

			<div class="ct-form">

				<div class="title">
					Subscribed Redemptions
				</div>

                @if( $db_participation->isEmpty() )
                <div class="ct-no-data">
                    <div class="texts">{{ __('messages.redemption.no_participate') }}</div>
                </div>
                @else
				<ul class="cr-checkbox">
					@foreach( $db_participation as $ac )
					<li>
						<input type="checkbox" name="checked_box[]" id="{{ $ac->id }}" class="css-checkbox" 
                        value="{{ $ac->id }},{{ $ac->promotion->id }}"
							@if( $ac[ 'checked' ] ) checked="checked" @endif

							@if( is_array(old('checked_box')) && in_array( $ac->promotion_id, old('checked_box')) ) checked="checked" @endif
						  />
						<label for="{{ $ac->id }}" class="css-label" title="{{ $ac->promotion->name }}">
							<span class="brand">{{ $ac['brand']->name}}</br></span>
						</label>
						<label for="{{ $ac->id }}" class="label-name">
							{{ string_dots_value( $ac->promotion->name, 50) }}
						</label>
					</li>
					@endforeach
				</ul>
                @endif

				<input type="submit" name="Submit" value="Submit" class="cr-button">
			</div>

			<div class="guidance">
				Note: <br/>
				You can choose multiple redemptions for a receipt.<br/>
                You can upload up to 3 photos for a receipt.<br/>
				JPG, JPEG, PNG file only.
			</div>
		</div>
   		{!! form_close() !!}

	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


@section( 'foot_page_js' )
	@parent

	<script>
        function readURL(input, callback)
        {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = callback
                reader.readAsDataURL(input.files[0]);
            }
        }

		$(function()
		{
            var upload_visible = 0;

            var $uploads = $( ".receipt-upload" );
            for( var i = 0, len = $uploads.length; i < len; i++ )
            {
                var $upload = $uploads.eq( i );

                $upload.on( "change", function()
                {
                    let $this = $( this );

                    var file = this.files[0];
                    var fileType = file["type"];
                    var ValidImageTypes = [ "image/jpeg", "image/png", "image/jpg" ];
                    if ($.inArray(fileType, ValidImageTypes) < 0) {
                        alert( "uploaded receipt must be only in jpeg or png format." );
                        return false;
                    }


                    readURL( this, function( e )
                    {
                        let $wrapper = $this.parents( ".upload-receipt-wrapper" );

                        $wrapper.find( ".fd-img-preview-wrapper" ).show();
                        $wrapper.find( ".fd-upload-receipt" ).hide();
                        $wrapper.addClass( "uploaded" );

                        $wrapper.find( ".fd-img-preview" ).attr( "src", e.target.result );

                        $wrapper.siblings( ".upload-receipt-wrapper:hidden" ).first().show();
                    });
                    // else
                    // {
                    //  $( "#form_receipt" ).submit();
                    //  showLoadingOverlay();
                    // }
                });

                $( ".fd-remove-img" ).on( "click", function()
                {
                    let $wrapper = $(this).parents( ".upload-receipt-wrapper" );

                    $wrapper.find( ".fd-img-preview-wrapper" ).hide();
                    $wrapper.find( ".fd-upload-receipt" ).show();
                    $wrapper.find( ".receipt-upload" ).val( "" );
                    $wrapper.removeClass( "uploaded" );

                    /**
                     * Hide all input file except has file
                     * show the first hidden wrapper
                     */
                    $( ".upload-receipt-wrapper:not(.uploaded)" ).hide();
                    $( ".upload-receipt-wrapper:hidden:first" ).show();
                });
            }
		});
	</script>
@stop

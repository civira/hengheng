<ul class="ct-panel-nav">
	@foreach( $db_info as $di )
	<li @if( $db_article->id == $di->id ) 
			class="active-menu" 
		@endif>
		<a href="{{ route( 'f.heng.information.index', $di->slug ) }}" title="{{ $di->title }}">
			{{ string_dots_value( $di->title, 22 ) }}
		</a>
	</li>
	@endforeach
</ul>

<div class="ct-panel-mobile-nav">
	<div class="cpm-tabs">
		<div class="cpm-lines rows">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<div class="rows">
			Menu
		</div>
	</div>
	<ul class="cpm-routes">
		@foreach( $db_info as $di )
			<li @if( $db_article->id == $di->id ) class="active-menu" @endif>
				<a href="{{ route( 'f.heng.information.index', $di->slug ) }}" title="{{ $di->title }}">
					{{ $di->title }}
				</a>
			</li>
		@endforeach
	</ul>
</div>
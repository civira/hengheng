@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/information.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section id="information_content" class="st-container" >

	<section class="ct-panel">

		@include( 'f.heng.information.partials._nav' )

		<div id="page_information" class="ct-panel-contents">

			@if( $db_article )
				{!! $db_article->description !!}
			@endif

		</div>
	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


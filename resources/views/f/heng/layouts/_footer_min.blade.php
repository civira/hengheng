<section id="st_footer_min" class="st-container stm-navigation">
		<ul>
			<li class="stm-copy">
				Shop&Win&copy 2018
			</li>
			@foreach( $st_footer_info as $sf )
				@if( $sf->url_link )
					<li class="stm-link">
						<a href="{{ $sf->url_link }}">{{ $sf->title }}</a>
					</li>
				@else
					<li class="stm-link">
						<a href="{{ route( 'f.heng.information.index', $sf->slug ) }}">{{ $sf->title }}</a>
					</li>
				@endif
			@endforeach
		</ul>
</section>
<div id="st_logo_search">
	@if( $st_setting && $st_setting->logoHeader() )
	<a href="{{ route('f.heng.home') }}">
		<img src="{{ $st_setting->logoHeader() }}" id="st_logo" width="100" height="100" >
	</a>
	@endif

	<form id="st_search_form">
	    <div id="st_search_bar" class="typeahead__container">
			<label class="ico-magnify" for="st_search_input"></label>
	        <div class="typeahead__field">
	            <div class="typeahead__query">
	                <input id="st_search_input"
						   class="js-typeahead"
	                       name="q"
	                       type="search"
	                       autocomplete="off"
						   placeholder="Search on Shop&Win">
	            </div>
	            <div class="typeahead__button">
	                <button type="submit">
	                    <span class="typeahead__search-icon"></span>
	                </button>
	            </div>
	        </div>
	    </div>
	</form>
</div>

<div id="st_navigation">
	<ul>
		<li>
			<a href="{{ route( 'f.heng.lucky.index') }}">Lucky Draw</a>
			<!-- <span class="ico-dropdown down"></span> -->
		</li>
		<li>
			<a href="{{ route( 'f.heng.redemptions.index') }}">Redemption</a>
		</li>
		<li>
			<a href="{{ route( 'f.heng.gifts.index') }}">Free Gift</a>
		</li>
		<li>
			<a href="{{ route( 'f.heng.promo_codes.index') }}">Promo Code</a>
		</li>
		<li>
			<a href="{{ route( 'f.heng.free_samples.index') }}">Free Sample</a>
		</li>
		<li>
			<a href="{{ route( 'f.heng.brands.index') }}">Brands</a>
		</li>
		<li>
			<a href="{{ route( 'f.heng.retailers.index') }}">Retailers</a>
		</li>
		@if( empty( $authed_user ))
		<li>
			<a href="{{ route( 'f.heng.auth.login') }}" class="login-btn">
				Login / Sign Up
			</a>
		</li>
		@else
		<li class="st-navigation-member">
			{!! html_img_site( 'member-menu.png',
			[
				'class' => 'stm-icon',
				'w'  => 90,
				'h'  => 35,
			]) !!}
			<ul class="st-navigation-dropdown">
				<li>
					<a href="{{ route( 'f.heng.users.dashboard' ) }}">Cart</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.users.contest_summary' ) }}">Summary</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.users.my_account' ) }}">Profile</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.users.change_password' ) }}">Change Password</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.auth.logout' ) }}">Log out</a>
				</li>
			</ul>
		</li>
		@endif
	</ul>
</div>

<section id="st_navigation_mobile">
	<div class="st-mobile-menu-strip display" onclick="openMobileNav()">
	    <div class="line-bar"></div>
	    <div class="line-bar"></div>
	    <div class="line-bar"></div>
	</div>
</section>

<div id="st_mobile_menu" class="st-mobile-menu">
	<div id="st_mobile_top_container">
		<div href="javascript:void(0)" class="closebtn" onclick="closeMobileNav()">
			<div class="st-mobile-menu-strip inside">
				<div class="line-bar"></div>
				<div class="line-bar"></div>
				<div class="line-bar"></div>
			</div>
		</div>
	</div>
	<div class="smm-box">

		@if( empty( $authed_user) )
		<div id="st_mobile_menu_status" class="st-mobile-menu-status">
			<a href="{{ route( 'f.heng.auth.login') }}" class="sts-button">Login</a>
			<a href="{{ route( 'f.heng.auth.register') }}" class="sts-button">Sign Up</a>
		</div>
		@endif
		<div>
			<div class="title-menu">MENU</div>
            <ul>
    			<li><a href="{{ route( 'f.heng.lucky.index') }}">Lucky Draw</a></li>
    			<!-- <span class="ico-dropdown down"></span> -->
    			<li><a href="{{ route( 'f.heng.redemptions.index') }}">Redemption</a></li>
    			<li><a href="{{ route( 'f.heng.gifts.index') }}">Free Gift</a></li>
    			<li><a href="{{ route( 'f.heng.promo_codes.index') }}">Promo Code</a></li>
    			<li><a href="{{ route( 'f.heng.free_samples.index') }}">Free Sample</a></li>
    			<li><a href="{{ route( 'f.heng.brands.index') }}">Brands</a></li>
    			<li><a href="{{ route( 'f.heng.retailers.index') }}">Retailers</a></li>
                @if( !empty( $authed_user) )
                <li id="smmb_account">
                    <div id="smmba_trigger" class="account-trigger">
                        <span class="label">My Account</span>
                        <div class="at-arrow"></div>
                    </div>
                    <ul class="smmba-wrapper">
    					<li>
							<a href="{{ route( 'f.heng.users.dashboard' ) }}">Cart</a>
						</li>
                        <li><a href="{{ route( 'f.heng.users.contest_summary' ) }}">Summary</a></li>
                        <li><a href="{{ route( 'f.heng.users.my_account' ) }}">Profile</a></li>
                        <li><a href="{{ route( 'f.heng.users.change_password' ) }}">Change Password</a></li>
                        <li><a href="{{ route( 'f.heng.auth.logout' ) }}">Log out</a></li>
                    </ul>
                </li>
                @endif
            </ul>
		</div>
	</div>
</div>

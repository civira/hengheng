<section id="st_footer_full">

	<div class="st-container">

		<section id="footer_onmobile_head">
			@if( $st_setting )
				<div id="footer_brand">
					@if( !is_null( $st_setting->logoFooter()) )
						<img src="{{ $st_setting->logoFooter() }}" width="80" height="80" >
					@endif
					<div id="footer_copyright">&copy 2018 All Rights Reserved.</div>
				</div>

				<div id="footer_social_network">
					@if( $st_setting->facebook_link || $st_setting->instagram_link || $st_setting->linkedin_link )
					<div class="fsn-row connect">
						Connect
					</div>
					@endif
					@if( $st_setting->facebook_link )
					<a href="{{ $st_setting->facebook_link }}" target="_blank" class="fsn-row">
						{!! html_img_site( 'fb-icon.png',
						[
							'dir' => 'f/social',
							'w' => 11,
							'h' => 20,
						]) !!}
					</a>
					@endif
					@if( $st_setting->instagram_link )
					<a href="{{ $st_setting->instagram_link }}" target="_blank" class="fsn-row">
						{!! html_img_site( 'instagram-icon.png',
						[
							'dir' => 'f/social',
							'w' => 22,
							'h' => 22,
						]) !!}
					</a>
					@endif
					@if( $st_setting->linkedin_link )
					<a href="{{ $st_setting->linkedin_link }}" target="_blank" class="fsn-row">
						{!! html_img_site( 'linked-icon.png',
						[
							'dir' => 'f/social',
							'w' => 21,
							'h' => 19,
						]) !!}
					</a>
					@endif
				</div>
			@endif
		</section>

		<section id="footer_links">
			<div class="fl-columns col-2">
				<ul>
					@foreach( $st_footer_info  as $sf )
						@if( $sf->url_link )
							<li class="fl-txt"><a href="{{ $sf->url_link }}">{{ $sf->title }}</a></li>
						@else
							<li class="fl-txt"><a href="{{ route('f.heng.information.index', $sf->slug) }}">{{ $sf->title }}</a></li>
						@endif
					@endforeach
				</ul>
			</div>

			<div class="fl-columns">
				<ul>
					<li class="fl-txt">
						<a href="{{ route('f.heng.brands.index') }}">Brands</a>
					</li>
					<li class="fl-txt">
						<a href="{{ route('f.heng.retailers.index') }}">Retailers</a>
					</li>
				</ul>
			</div>
		</section>

	</div>

</section>

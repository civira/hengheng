@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/home.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section id="home_guides">
	<div class="st-container">
		<div class="hg-title">
			<div class="top-txt">How To</div>
			<div class="btm-txt">WIN</div>
		</div>
		<div class="hg-owl owl-carousel customized">
			<div class="hg-info">
				<div class="hi-numbering">1</div>
				<div class="hi-description">
					<div>
						<div class="ico-home-guides scan"></div>
						<div class="hid-title">SCAN QR</div>
					</div>
					<div>Login at Shop&Win</div>
				</div>
        		<div class="hgi-arrow"></div>
			</div>
			<div class="hg-info">
				<div class="hi-numbering">2</div>
				<div class="hi-description">
					<div>
						<div class="ico-home-guides upload"></div>
						<div class="hid-title">UPLOAD</div>
					</div>
					<div>Shopping Receipt</div>
				</div>
    			<div class="hgi-arrow"></div>
			</div>
			<div class="hg-info">
				<div class="hi-numbering">3</div>
				<div class="hi-description">
					<div>
						<div class="ico-home-guides hand"></div>
						<div class="hid-title">PARTICIPATE</div>
					</div>
					<div>at Brand Contest</div>
				</div>
    			<div class="hgi-arrow third"></div>
			</div>
		</div>
	</div>
</section>

<section id="home_slider" class="st-container">
	<div>
		<div class="hs-slider">
			<div class="owl-carousel" data-items="1" data-loop="false" data-dots="false" data-autoplay="true">
				@foreach( $db_slider as $ds )
				<a class="hs-img-container" href="@if($ds->redirect_url){{ $ds->redirect_url }}@else # @endif">
					<img src="{{ $slider_img[$ds->id] }}" alt='{{ $ds->name }}' title="{{ $ds->name }}" width="820" height="320" class="hs-slider-img">
				</a>
				@endforeach
			</div>
		</div>

		<div class="hs-thumbnails">
			@foreach( $db_thumb as $key=>$dt )
				<a href="@if($dt->redirect_url) {{ $dt->redirect_url }} @else # @endif">
					<img src="{{ $thumb_img[$dt->id] }}" alt="{{ $dt->name }}" title="{{ $dt->name }}" width="381" height="149" class="hst-image @if($key==0) top @else btm @endif">
				</a>
			@endforeach
		</div>
	</div>
</section>

<section class="st-container" id="favorite_contests">
	<div class="ct-page-header">
		<h2>
			Featured Lucky Draws
		</h2>
		<span class="cph-bar"></span>
	</div>
	<div class="ct-owl-carousel">
		<div class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false"
			 data-sizes= "[ [ 0, 1 ],
						[320, 1],
						[600, 2],
						[720, 2],
						[840, 2],
						[1040, 3],
						[1220, 3]
					]">
			@foreach( $db_featured_lucky as $dc )
			<div>
				<div class="ct-owl-overlay">
					<a href="{{ $dc->detailUrl() }}">
						<img src="{{ $dc['image'] }}" alt="{{ $dc->name }}" title="{{ $dc->name }}">
					</a>
					<a href="{{ $dc->detailUrl() }}" class="ct-owl-bg">
						<div class="ct-owl-btn">PARTICIPATE TO WIN</div>
					</a>
				</div>
				<div class="ct-owl-title" title="{{ $dc->name }}">
					{{ string_dots( $dc->name ) }}
				</div>
			</div>
			@endforeach
		</div>
	</div>

	<div id="other_contest">
		<ul>
			@foreach( $db_lucky as $dc )
			<li>
				<a href="{{ $dc->detailUrl() }}">
					<img src="{{ $dc['image'] }}" alt="{{ $dc->name }}" title="{{ $dc->name }}" width="130" height="88" class="oc-image">
				</a>
				<div class="oc-info">
					<h4 class="oc-title">
						<a href="{{ $dc->detailUrl() }}" title="{{ $dc->name }}">
							{{ string_dots_value( $dc->name, 50 ) }}
						</a>
					</h4>
					<a href="{{ $dc->detailUrl() }}" class="oc-link">PARTICIPATE TO WIN</a>
				</div>
			</li>
			@endforeach
		</ul>
	</div>

</section>

<section class="st-container">
	<div class="ct-page-header">
		<h2>
			Featured Promotions
		</h2>
		<span class="cph-bar"></span>
	</div>
	<div class="ct-owl-carousel">
		<div class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false"
 					data-sizes= "[ [ 0, 1 ],
						[320, 1],
						[600, 2],
						[720, 2],
						[840, 2],
						[1040, 3],
						[1220, 3]
					]">

			@foreach( $db_promotions as $db)
			<div>
				<div class="ct-owl-overlay">
					<a href="
						@if( $db->category == 'redemption' )
							{{ $db->urlRedemption() }}
						@else
							{{ $db->urlGift() }}
						@endif
						">
						<img src="{{ $db['image'] }}" alt="{{ $db->name }}" title="{{ $db->name }}">
					</a>
				</div>
				<div class="ct-owl-title" title="{{ $db->name }}">
					{{ string_dots( $db->name ) }}
				</div>
			</div>
			@endforeach

		</div>
	</div>

</section>

<section class="st-container" id="fm_brands">
	<div class="ct-page-header">
		<h2>
			Featured Brands
		</h2>
		<span class="cph-bar"></span>
	</div>
	<div class="ct-owl-carousel greyed">
		<div class="owl-carousel" data-items="6" data-dots="false" data-nav="true" data-margin="20" data-loop="false"
 					data-sizes= "[ [ 0, 1 ],
						[320, 1],
						[400, 2],
						[520, 3],
						[720, 4],
						[840, 5],
						[1040, 7],
						[1220, 7]
					]">
			@foreach( $db_brand as $db )
			<div>
				<a href="{{ $db->searchUrl() }}">
					<img src="{{ $db['image'] }}" title="{{ $db->name }}" alt="{{ $db->name }}">
				</a>
			</div>
			@endforeach

		</div>
	</div>

</section>

<div class="ct-error-info ct-ajax txt-error subscribe-msg"></div>
<div class="ct-error-info ct-ajax txt-success subscribe-msg"></div>
<section id="home_mail_subscribe">

    {!! form_open_post( route( 'f.heng.api.email.subscribe_newsletter' ),
    [
        'class' => 'st-ajax-form',
        'data-on-success' => 'afterSubscribe',
        'data-on-failed'  => 'afterSubscribeError',
        'data-before-sent' => 'beforeXHR'
    ]) !!}
	<div class="st-container hm-container">

		<div class="ico-home-subscribe"></div>
		<div class="hm-box">
			<div class="hm-row">
				<div class="txt-title">SUBSCRIBE NOW</div>
				<div class="txt-description">
					to get contest update and promotion info
				</div>
			</div>

			<div class="hm-row b-fields">
				<input type="text" class="inp-subs-email" name="email" placeholder="Enter your email address">
				<input type="submit" class="inp-btn" name="subscribe_mail" value="Subscribe">
			</div>
		</div>
	</div>
    {!! form_close() !!}
</section>

<label href="{{ route( 'f.heng.qr.index' ) }}" class="fab-btn" ripple for="input_qr">
	<div class="fab-btn" ripple title="Scan QR Code">
		{!! html_img_site( 'scan-float-icon.png',
			[
				'class' => 'fb-scan-ico',
				'w' => 35,
				'h' => 35,
			]) !!}
	</div>
</label>
<input type="file" accept="image/*" capture="filesystem" id="input_qr" class="d-none" />

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}

	{!! html_js( 'js/vendors/jsqrcode-master/src/grid.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/version.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/detector.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/formatinf.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/errorlevel.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/bitmat.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/datablock.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/bmparser.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/datamask.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/rsdecoder.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/gf256poly.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/gf256.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/decoder.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/qrcode.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/findpat.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/alignpat.js' ) !!}
	{!! html_js( 'js/vendors/jsqrcode-master/src/databr.js' ) !!}

	{!! html_js( 'js/vendors/gasparesganga-jquery-loading-overlay/dist/loadingoverlay.min.js' ) !!}

	<script>
		qrcode.callback = readQR

		function isValidURL(str) {
			var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
				  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
				  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
				  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
				  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
				  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

			if(!pattern.test(str))
			{
				alert("Invalid QR code. ");
				return false;
			}
			else
			{
				return true;
			}
		}

		function getLocation( href ) {
			var l = document.createElement("a");
		    l.href = href;
		    return l;
		}


		function trim( str, char ) {
			return str.replace( /\$/,  "" );
		}

		function readQR( result ){
			if( isValidURL( result )) {

				let url = getLocation( result );

				if( url.hostname == "dev.civira.com" ||
					url.hostname == "civira.com" ||
					url.hostname == "shopnwin.sg" ||
					url.hostname == "www.shopnwin.sg" )
				{
					document.location = result;
				}
				else
				{
					alert( "Incorrect QR code value. " );
				}
			}
		}

		function getBase64(file, callback, error )
		{
			var reader = new FileReader();

			reader.readAsDataURL(file);

			reader.onload = function ( e )
			{
				if ( typeof callback === "function" ) {
					callback( e.target.result );
				}
			};

			reader.onerror = function (error)
			{
				if( typeof callback === "function" ) {
					error( error );
				}
			};
		}

        var $subscribe_msg;
        function beforeXHR()
        {
            $.LoadingOverlaySetup({
                    text : ""
            });
        }
        function afterSubscribe( resp )
        {
            $( ".subscribe-msg" ).hide();
            $( ".subscribe-msg.txt-success" ).html( resp.data ).show();
            $( "#home_mail_subscribe" ).hide();
        }

        function afterSubscribeError( resp )
        {
            $( ".subscribe-msg" ).hide();
            $( ".subscribe-msg.txt-error" ).html( resp.data ).show();
        }

		$(function()
		{

			$( "#input_qr" ).on( "change", function()
			{
                $.LoadingOverlaySetup({
                    text : "Reading QR Code...",
                    imageColor : "#555555",
                    textResizeFactor : 0.2,
                    textColor : "#555555",
                    imageResizeFactor : 2.5
                });

				showLoadingOverlay();

				var file = this.files[ 0 ];
				var fileType = file["type"];
				var ValidImageTypes = [ "image/jpeg", "image/png"];
				if ($.inArray(fileType, ValidImageTypes) < 0) {
     				alert( "scanned qr code must be only in jpeg or png format." );
				}
				else
				{
					var base64 = getBase64( file,
					function( res ) {
						qrcode.decode( res );
						hideLoadingOverlay();
					},
					function( e ) {
						alert( "unexpected error occured." );
						console.log( e );
						hideLoadingOverlay();
					});
				}
			});
		});
	</script>
@stop

@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/verification.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container with-margin">
	<div id="forget_header" class="ct-page-header">
		<h1>
			Token
		</h1>
		<span class="cph-bar"></span>
	</div>

	<div id="curr_paragraph">
		<p>
			Token Mismatch.
		</p>
	</div>
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/verification.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container with-margin">
	<div id="forget_header" class="ct-page-header">
		<h1>
			Newsletter Email
		</h1>
		<span class="cph-bar"></span>
	</div>

	<div id="curr_paragraph">
		<p>
			Verify Newsletter failed, Token mismatch.<br/>
			Your are either trying to verify from older token or email already subscribed in newsletter.
		</p>
	</div>
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


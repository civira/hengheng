@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
	{!! html_css( 'heng/contacts.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container" id="contact_content">

	<div class="ct-page-header">
		<h1>
			Contact Us
		</h1>
		<span class="cph-bar"></span>
		<p>
			Feel free to contact us using the form below or simply email us at help@shopnwin.sg
		</p>
	</div>

    {!! form_open_post( route( 'f.heng.contact.store' ) ) !!} 

	<div class="ct-form large split-style">
		<div class="ct-error-info {{ html_add_error_class() }}">
	    	{{ html_get_first_error_message() }}
		</div>
		
		<input type="text" name="{{ config('constants.security.md5' ) }}" value="" class="ct-hidden">

		<div class="ctf-row f-left">
			<label for="ctf-name">Subject</label>
			<select class="cr-select" name="subject">
				<option value="general-question">General Question</option>
				<option value="helps">Helps</option>
			</select>
		</div>	
		<div class="ctf-row f-right">
			<label for="ctf-name">Name</label>
			<input type="text" name="name" id="ctf-name" class="cr-input" value="{{ old( 'name' ) }}">
		</div>
		<div class="ctf-row f-left">
			<label for="ctf-mobile-number">Mobile Number</label>
			<input type="text" maxlength="8" name="mobile_number" id="ctf-mobile-number" class="prevent_chars cr-input" value="{{ old( 'mobile_number' ) }}">
		</div>
		<div class="ctf-row f-right">
			<label for="ctf-email">Email</label>
			<input type="text" name="email" id="ctf-email" class="cr-input" value="{{ old( 'email' ) }}">
		</div>
		<div class="ctf-row f-full">
			<label for="ctf-message">Message</label>
			<textarea name="message" class="cr-input textarea">{{ old( 'message' ) }}</textarea>
		</div>
		<button type="submit" name="send" class="cr-button f-left">Send</button>
	</div>	
    {!! form_close() !!}

	{!! html_img_site( 'inquiry-bg.png', 
	[ 
		'id'=> 'inquiry_bg',
		'w' => 395, 
		'h' => 395,
	]) !!}
	
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


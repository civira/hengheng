@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
	{!! html_css( 'heng/register.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container" id="register_content">

	<div id="register_container_box">

		<div id="register_form_box">
			<h1>Member Login</h1>

	    	{!! form_open_post( route( 'f.heng.auth.auth', ['l' => $referer] ) ) !!}

			<div class="ct-error-info {{ html_add_error_class() }}" id="status_error">
		    	{!! html_get_first_error_message() !!}
			</div>

			<div id="rb_box">
				<div class="rb-row box">
					<span class="ico-register member"></span>
					<input type="text" name="email" class="rb-input" placeholder="Email" value="{{ old( 'email' ) }}">
				</div>
				<div class="rb-row box">
					<span class="ico-register lock"></span>
					<input type="password" name="password" class="rb-input" placeholder="Password">
				</div>
				<div class="rb-row align-left">
					<input type="checkbox" name="remember_me" id="remember_me" class="css-checkbox" value="YES" 
					checked="checked" />
					<label for="remember_me" class="css-label remember-me">
						Remember me
					</label>
				</div>
				<div class="rb-row">
					<input type="submit" name="" value="Login" class="rb-button">
				</div>
				<div class="rb-login">
					<a href="{{ route( 'f.heng.auth.forget_password' ) }}">Forget Password?</a>
				</div>

				{!! html_img_site( 'or-line.jpg',
				[
					'class' => 'rb-or-line',
					'w' => 241,
					'h' => 28,
				]) !!}

				<div class="ct-facebook-button">
					<div class="ico-facebook"></div>
					<div class="text">Log in With Facebook</div>
				</div>

				<div class="rb-login">
					Not a member? <a href="{{ route( 'f.heng.auth.register', [ 'l' => $referer ] ) }}">Sign up here</a>
				</div>
			</div>
	    	{!! form_close() !!}
		</div>
		<div class="with-background login">
			
		</div>
	</div>


</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/f/facebook-login.js' ) !!}
@stop



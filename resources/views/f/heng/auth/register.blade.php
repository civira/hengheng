@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
	{!! html_css( 'heng/register.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container" id="register_content">

	<div id="register_container_box">

    	{!! form_open_post( route( 'f.heng.auth.store_register', [ 'l' => $referer ] ) ) !!}
		

		<div id="register_form_box">
			<h1>Sign Up</h1>
			@php $error_msg = html_get_first_error_message() @endphp

			<input type="text" name="{{ config('constants.security.md5' ) }}" value="" class="ct-hidden">

			<div id="rb_box">
				<div class="rb-row ct-error-info {{ html_add_error_class() }}" id="status_error">
			    	{{ $error_msg }}
				</div>
				<div class="rb-row box">
					<span class="ico-register member"></span>
					<input type="text" name="email" class="rb-input" placeholder="Email" value="{{ old( 'email' ) }}" />
				</div>
				<div class="rb-row box">
					<span class="ico-register lock"></span>
					<input type="password" name="password" class="rb-input" placeholder="Password (Min. 6 Character)" />
				</div>
				<div class="rb-row box">
					<span class="ico-register lock"></span>
					<input type="password" name="password_confirmation" class="rb-input" placeholder="Password Confirmation" />
				</div>
				<div id="check_agree" class="rb-checkbox">
					<input type="checkbox" name="check_agreement" id="check-agreement" class="css-checkbox" value="YES" @if( old('check_agreement') == 'YES')checked="checked"@endif />
					<label for="check-agreement" class="css-label">
						By signing up, I agree to <a href="{{ route( 'f.heng.information.index', [ 'terms-conditions' ]) }}" target="_blank">Terms & Conditions</a> and 
						<a href="{{ route( 'f.heng.information.index', 'privacy-policy') }}" target="_blank">Privacy Policy</a> from Shop&Win
					</label>
				</div>

				<div class="rb-checkbox">
					<input type="checkbox" name="check_email" id="check_email" class="css-checkbox" value="YES" />
					<label for="check_email" class="css-label">
                        Yes! I would like to receive updates about lucky draws and promotions from Shop&Win via email
					</label>
				</div>

				<div class="rb-row">
					<input type="submit" name="" value="Sign Up" class="rb-button" />
				</div>

				{!! html_img_site( 'or-line.jpg',
				[
					'class' => 'rb-or-line',
					'w' => 241,
					'h' => 28,
				]) !!}

				<div class="ct-facebook-button">
					<div class="ico-facebook"></div>
					<div class="text">Sign up With Facebook</div>
				</div>


				<div id="status">
				</div>

				<div class="rb-login">
					Already a member? <a href="{{ route( 'f.heng.auth.login' ) }}">Login here</a>
				</div>
			</div>
		</div>

	    {!! form_close() !!}

	    <div class="with-background register">
	    	
	    </div>

	</div>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/f/facebook-login.js' ) !!}
@stop



@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/forget-pass.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container" id="forget_pass_content">
	<div id="forget_header" class="ct-page-header">
		<h2>
			Forget Your Password?
		</h2>
		<span class="cph-bar"></span>
	</div>

	<div id="forget_paragraph">
		<p>
			You may update your password any time. <br/>
			All new passwords must contain at least 6 characters. We also suggest having at least one capital and one lower-case letter (Aa-Zz).
		</p>

   		{!! form_open_post( route( 'f.heng.auth.reset_password' ) ) !!} 
		<div class="ct-form medium">
			<div class="ct-error-info {{ html_add_error_class() }}">
		    	{{ html_get_first_error_message() }}
			</div>
			<div class="ctf-row">
				<input type="hidden" name="token" id="ctf-password" class="cr-input" value="{{ $token }}">
			</div>
			<div class="ctf-row">
				<label for="ctf-password">New Password</label>
				<input type="password" name="password" id="ctf-password" class="cr-input" value="{{ old( 'password' ) }}">
			</div>
			<div class="ctf-row">
				<label for="ctf-conf-password">Confirm Password</label>
				<input type="password" name="password_confirmation" id="ctf-conf-password" class="cr-input">
			</div>
			<input type="submit" name="send" value="Send" class="cr-button f-right">
		</div>
    	{!! form_close() !!}
	</div>
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


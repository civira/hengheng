@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/forget-pass.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container" id="forget_pass_content">
	<div id="forget_header" class="ct-page-header">
		<h2>
			Forget Your Password?
		</h2>
		<span class="cph-bar"></span>
	</div>

	<div id="forget_paragraph">
		<p>
			PASSWORD UPDATED SUCCESSFULLY. <br/>
			Your new password has been set. You can login to your account with new password.
		</p>

		<div class="ct-form medium">
			<a href="{{ route( 'f.heng.auth.login' ) }}" class="cr-button f-left">Login</a>
		</div>
	</div>
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


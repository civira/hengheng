@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/forget-pass.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container with-margin">
	<div id="forget_header" class="ct-page-header">
		<h2>
			Forget Your Password?
		</h2>
		<span class="cph-bar"></span>
	</div>

	<div id="forget_paragraph">
		<p>
			Please enter your account email address.<br/>
			We will send you a link to reset your password.<br/><br/>

			Don't have an account? <a href="{{ route('f.heng.auth.register') }}">Sign up now.</a>
		</p>

    	{!! form_open_post( route( 'f.heng.auth.mail_forget_password' ) ) !!} 
		<div class="ct-form small">
			<div class="ct-error-info {{ html_add_error_class() }}">
		    	{{ html_get_first_error_message() }}
			</div>

			<div class="ctf-row">
				<label for="ctf-email">Email</label>
				<input type="text" name="email" id="ctf-email" class="cr-input" value="{{ old( 'email' ) }}">
			</div>
			<input type="submit" name="send" value="Send" class="cr-button f-right">
		</div>
    	{!! form_close() !!}
	</div>
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


@extends('f.heng.blank')

@section( 'head_end' )
    @include( 'f.heng._partials._ga' )
@stop

@section( 'head_page_meta' )
	<link rel="apple-touch-icon" sizes="128x128" href="{{ asset( 'apple-touch-icon-144x144.png' ) }}?m=<?php echo filemtime('apple-touch-icon-144x144.png'); ?>">
	<link rel="icon" sizes="192x192" href="{{ asset( 'apple-touch-icon-192x192.png' ) }}?m=<?php echo filemtime('apple-touch-icon-192x192.png'); ?>">
	{!! SEO::generate() !!}
@stop

@section( 'head_page_css' )
	{!! html_css( 'heng/shared.css' ) !!}
	{!! html_css( 'heng/main.css' ) !!}
@stop

@section( 'head_vendor_css' )
	@parent
	{!! html_css( 'jquery-typeahead/dist/jquery.typeahead.min.css', 'js/vendors' ) !!}
@stop

@section( 'site_content' )

	<header id="st_header" role="banner">
		<div id="st_wrapper_nav" class="st-container">
			@include( 'f.heng.layouts._header' )
		</div>
	</header>

	<div id="st_body" class="@yield( 'low_padding_body' )" role="main">
		@yield( 'page_content' )
	</div>

	<footer id="st_footer" role="contentinfo">
		@yield( 'footer_content' )
	</footer>

@stop

@section( 'foot_vendor_js' )
	@parent
	{!! html_js( 'js/vendors/jquery/dist/jquery.min.js' ) !!}
	{!! html_js( 'js/vendors/jquery-typeahead/dist/jquery.typeahead.min.js' ) !!}
@stop

@section( 'foot_page_js' )
	@parent
	<script>
		var st_base_url = "{{ asset('') }}";
	</script>
	{!! html_js( 'js/f/main.js' ) !!}
@stop

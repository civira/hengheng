
@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
	{!! html_css( 'heng/lucky-detail.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section class="st-container with-margin">

    @if( html_message_is_valid() )
        <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class( 'txt-error', 'txt-valid' ) }}">
            {{ html_get_first_message() }}
        </div>
    @endif

	<section class="split-layout">

        <div class="sl-left">

            <!-- information -->
            @include( 'f.heng._partials._section_tab_layout' )
            
            <!-- social network sharing -->
            @include( 'f.heng._partials._share_social_networks' )

        </div>

		<div class="sl-right">

			@if( $db_participation )

                @include( 'f.heng._partials._panel_more_data', 
                [ 
                'panel_more_title' => 'More Free Samples', 
                'panel_more_paragraph' => 'Browse our other free samples.',
                'panel_more_redirect' => route( 'f.heng.free_samples.index' ),
                'panel_more_btn' => 'Browse Free Samples'
                ] )

			@endif

			<div class="panel-container">
				<!-- Main title text -->
				<div class="fd-main-title">
					{{ $display_info['main_title'] }}
				</div>

				@if( $log_user )

					@if( !html_message_is_valid() )
					<!-- error info -->
					<div class="ct-error-info {{ html_add_error_class() }}">
				    	{{ html_get_first_error_message() }}
					</div>
					@endif

					@if( $db_participation && !$db->isDateExpired() )
						<a href="{{ route( 'f.heng.free_samples.request',  $db->id ) }}" class="fd-button red">
							Redeem Now
						</a>
					@endif

					{!! form_open_post( route( 'f.heng.free_samples.participate', $db->id ), [ 'id'=>'form_receipt' ], true ) !!}
                    @if( !$authed_user->isProfilePartialFilled() && !$db->isDateExpired() )

                        @include( 'f.heng._partials._form_member_data' )

                    @endif

					@if( !$db_participation && !$db->isDateExpired() )
					<!-- checkbox agreement -->
					<div class="cf-agreement">
						<input type="checkbox" name="check_agreement" id="check-agreement" class="css-checkbox" value="YES" @if( old('check_agreement') == 'YES')checked="checked"@endif />
						<label for="check-agreement" class="css-label">
							By participating, you are agreeing to this free sample terms & conditions.
						</label>
					</div>
					@endif

					<!-- note -->
					@if( $db->isDateExpired() )
						<div class="fd-guide">
							Sorry, our free sample period has ended.
						</div>
					@elseif( $db_participation )
						<div class="fd-guide">
							Note: <br/>
							Please fill the right mailing address to ensure you get your sample.
						</div>
					@elseif( !$db_participation && !$authed_user->isProfilePartialFilled() )
						<div class="fd-guide agreement">
							To participate in this free sample, you must fill all the information.
						</div>
					@endif

					@if( !$db_participation && !$db->isDateExpired() )
					<!-- submit button -->
						<div>
							<input type="submit" class="fd-button" value="Redeem">
						</div>
					@endif
					{!! form_close() !!}
				@else
					<!-- redirect login page -->
					<a href="{{ route( 'f.heng.auth.login', [ 'l' => encode_authenticate_redirect( $db->detailUrl()) ] ) }}" class="fd-button">Login to Participate</a>
					<div class="fd-guide">
						Login to participate free sample.
					</div>
				@endif
			</div>
		</div>
	</section>

    @include( 'f.heng._partials._section_owl_other_items', 
        [ 'owl_redirect_var'=> 'detail',
          'owl_other_title_name' => 'Other Free Samples', 
          'owl_overlayed' => false,
          'owl_other_webview'=> true ] )

</section>


@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b73f77d0c8e5f78" async="async"></script>
@stop

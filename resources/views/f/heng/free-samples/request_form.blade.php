@extends('f.heng.base')

@section( 'head_page_css' )
    @parent
    {!! html_css( 'heng/user-address-list.css' ) !!}
	{!! html_css( 'select2/dist/css/select2.min.css', 'js/vendors' ) !!}
@stop


@section( 'page_content' )

<div class="st-container with-margin">

    <section id="content_wrapper">

         @if( html_get_first_error_message() )
            <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class() }}">
                {{ html_get_first_error_message() }}
            </div>
        @endif

        <div class="db-form">
	        <section>
	            <div class="ct-page-header">
	                <h1>
	                	Delivery Address
	                </h1>
	                <span class="cph-bar"></span>
	                <p>
	                	Is this the address you want us to send the samples?
	                </p>
	            </div>

		        <div class="db-content">

		            <div class="list">
		                <div class="item no-margin no-pad-top">

			        		@if( !$db_address->isEmpty() )
	            				{!! form_open_post( route( 'f.heng.free_samples.request_select_store', $db->id ) ) !!}
	            				<input type="hidden" id="address_selected" name="selected_address" value="" />
				                <div id="address_controls" class="cs-row ss2-parent-wrapper">
				                    <select name="filter_sort" id="address_selection"
				                            class="control-retailer site-select2"
				                            data-placeholder="Select Category"
				                            data-class="ss2-wrapper"
				                            data-open-class="ss2-open-wrapper"
				                            data-no-search="true"
				                        >
				                        <option value="">Select Address</option>
				                        @foreach( $db_address as $val=>$faddress )
				                            <option value="{{ $faddress->getJSON() }}" @if( $authed_user->member_address_id == $faddress->id )selected="selected"@endif >{{ $faddress->address_label }}</option>
				                        @endforeach
				                    </select>
				                </div>

			        			<div id="address_detail" class="paragraph">
									<div class="address"></div>
									<div class="unit_no"></div>
									<div class="postal_code"></div>
			        			</div>

			                    <div class="name">
	            					<input type="submit" class="ct-button orange" value="Send to this address" />
			                    </div>
	            				{!! form_close() !!}
				            @else
					            <div class="name">
					            	There is no default address for delivery.
					            </div>
					            <a href="{{ route( 'f.heng.users.address_page' ) }}" class="name blue" target="_blank">Set up default Address.</a>
			            	@endif
		                </div>
		            </div>
		        </div>
	        </section>

	        <section>
	            <div class="ct-page-header margined">
	                <h1>
	                    Send to another address
	                </h1>
	                <span class="cph-bar"></span>
	            </div>

	            {!! form_open_post( route( 'f.heng.free_samples.request_new_store', $db->id ) ) !!}
	                <div class="ct-form medium">

	                    <div class="ctf-row">
	                        <label for="ctf-address">
	                            Address Label
	                            <span class="cr-asterisk">*</span>
	                        </label>
	                        <div class="cr-multi-info">
	                            <input type="text" name="address_label" id="ctf-unit" class="cr-input" value="{{ old('address_label' ) }}" placeholder="Fill address type">
	                            <div class="information">House, Office, Apartement, etc.</div>
	                        </div>
	                    </div>

	                    <div class="ctf-row">
	                        <label for="ctf-address">
	                            Address
	                            <span class="cr-asterisk">*</span>
	                        </label>
	                        <div class="cr-multi-info">
	                            <textarea name="address" class="cr-input textarea" placeholder="Block & Street Address">{{ old('address') }}</textarea>
	                        </div>
	                    </div>
	                    <div class="ctf-row">
	                        <label for="ctf-unit">
	                            Unit No
	                            <span class="cr-asterisk">*</span>
	                        </label>
	                        <div class="cr-multi-info">
	                            <input type="text" name="unit_no" id="ctf-unit" class="cr-input" value="{{ old('unit_no' ) }}" placeholder="Fill your unit no">
	                        </div>
	                    </div>

	                    <div class="ctf-row">
	                        <label for="ctf-postal">
	                            Postal Code
	                            <span class="cr-asterisk">*</span>
	                        </label>
	                        <div class="cr-multi-info">
	                            <input type="text" maxlength="6" name="postal_code" id="ctf-postal" class="prevent_chars cr-input" value="{{ old('postal_code') }}" placeholder="Key in postal code">
	                        </div>
	                    </div>

	                    <input type="submit" name="save" value="Send" class="cr-button f-right">
	                </div>
	            {!! form_close() !!}
            </section>

        </div>

    </section>

</div>

@stop


@section( 'footer_content' )
    @include( 'f.heng.layouts._footer_min' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/select2/dist/js/select2.full.min.js' ) !!}
	<script>
		$(function()
		{
			$( "#address_selection" ).on( "change", function()
			{
				let val = $(this).val(),
					obj = JSON.parse( val ),

				$detail = $( "#address_detail" );

				$detail.find( ".address" ).html( obj.address );
				$detail.find( ".unit_no" ).html( "Unit no: "+obj.unit_no );
				$detail.find( ".postal_code" ).html( "Postal code: "+obj.postal_code );

				$( "#address_selected" ).val( obj.id );

			}).change();
		});
	</script>
@stop

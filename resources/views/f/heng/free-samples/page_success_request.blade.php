@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/self-upload-success.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section class="st-container with-margin">

	<div id="content_paragraph">
		<p class="title">
			Well Done! 
		</p>
		<p class="text">
			Look out for the free sample details via email as soon as we verify your request.
		</p>
		<p class="text">
		</p>

		<a href="{{ route( 'f.heng.home' ) }}" class="button-redirect">OK</a>
	</div>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )
	<video id="scan_video"></video>
	<div id="scan_error"></div>
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/instascan-dev/dist/instascan.js' ) !!}
	<script>

		function isValidURL(str) {
			var pattern = new RegExp('^(https?:\/\/)?'+ // protocol
				'((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|'+ // domain name
				'((\d{1,3}\.){3}\d{1,3}))'+ // OR ip (v4) address
				'(\:\d+)?(\/[-a-z\d%_.~+]*)*'+ // port and path
				'(\?[;&a-z\d%_.~+=-]*)?'+ // query string
				'(\#[-a-z\d_]*)?$','i'); // fragment locater

			if(!pattern.test(str)) 
			{
				alert("Please enter a valid URL.");
				return false;
			} 
			else 
			{
				return true;
			}
		}

		function getLocation( href ) {
			var l = document.createElement("a");
		    l.href = href;
		    return l;
		}

		$(function()
		{
			let scanner = new Instascan.Scanner(
				{ 
					video  : document.getElementById('scan_video'),
					mirror : false
				});

			scanner.addListener('scan', function (content) {
				alert( content );
				if( isValidURL( content )) {
					if( getLocation( content ) == "dev.civira.com" || 
						getLocation( content ) == "civira.com" ) 
					{
						document.window.location = content;
					}
				}
				console.log(content);
			});

			Instascan.Camera.getCameras().then(function (cameras) 
			{
				if (cameras.length > 0) {
					let camera = cameras[ 0 ];
					if( cameras.length > 1 )
					{
						camera = cameras[ 1 ];
					}
				 	scanner.start(camera);
				} else {
					console.error('No cameras found.');
				}
			})

			.catch(function (e) {
				if( e.message = "Cannot access video stream (NotFoundError)." )
				{
					$( "#scan_video" ).hide();
					$( "#scan_error" ).html( "No camera found. Please refresh this page." );
				}
			});

		});
	</script>
@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop

@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/brand-list.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

	<link rel="stylsheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
@stop

@section( 'page_content' )

<section class="st-container with-margin">

	<section class="ct-logo-title">
		<div class="ctlt-logo">
			<img src="{{ $db_brand->file_img() }}" width="255" height="165" alt="{{ $db_brand->name }}" title="{{ $db_brand->name }}"
			>
		</div>
		<div class="ctlt-info">
			<h1 class="ctlt-title d-none">
				{{ $db_brand->name }}
			</h1>
			<form class="st-ajax-form"></form>
			<div class="ctlt-action-wrapper bot">
                @if( $authed_user )
				<a href="{{ route( 'f.heng.api.self.toggle_favorite') }}"
                    class="ctlt-button fav st-ajax-form {{ $faved? 'active' : '' }}"
                    data-on-success="afterFavorite"
                    data-on-failed="afterFavoriteError"
                >
					<span class="label">Add to Favorite</span>
                    <span class="label-fav">Remove from Favorite</span>
					<span class="icomoon-favorite-border"></span>
                    <span class="staf-data d-none" data-id="{{ $db_brand->id }}"></span>
				</a>
                @else
                <a href="{{ get_url_not_authed() }}"
                    class="ctlt-button fav"
                >
                    <span class="label">Add to Favorite</span>
                    <span class="label-fav">Remove from Favorite</span>
                    <span class="icomoon-favorite-border"></span>
                    <span class="staf-data d-none" data-id="{{ $db_brand->id }}"></span>
                </a>
                @endif
			</div>
		</div>
	</section>

	@if( !$db_products->isEmpty() )
	<section class="ct-list">
		<div class="ct-galleries informative">
			<ul>
				@foreach( $db_products as $keys=>$dc )
				<li>
					<div class="ctg-relative">
						<a href="{{ $dc->detailUrl() }}">
							<img src="{{ $dc->file_img() }}" alt="{{ $dc->name }}" title="{{ $dc->name }}" width="375" height="245">
						</a>
					</div>
					<div class="ctg-headings">
						<h3><a href="{{ $dc->detailUrl() }}" title="{{ $dc->name }}">{{ string_dots( $dc->name ) }}</a></h3>
					</div>
				</li>
				@endforeach
			</ul>
		</div>
	</section>
	@endif

	<section class="ct-list">
		<div class="ct-page-header margined">
			<h2>
				Brand Lucky Draw
			</h2>
			<span class="cph-bar"></span>
		</div>

		@if( !$db_lucky->isEmpty() )
		<ul class="ct-expand-list">
			@foreach( $db_lucky as $dm )
			<li>
				<div class="ce-image">
					<a href="{{ $dm->detailUrl() }}">
						<img src="{{ $lucky_img[$dm->id] }}" alt="{{ $dm->name }}" title="{{ $dm->name }}" width="320" height="210">
					</a>
				</div>
				<div class="ce-context">
					<h4 class="ctxt-brand">
						<a href="{{ $db_brand->searchUrl() }}">{{ $db_brand->name }}</a>
					</h4>
					<h3 class="ctxt-title">
						<a href="{{ $dm->detailUrl() }}" title="{{ $dm->name }}">{{ string_dots( $dm->name ) }}</a>
					</h3>
					<div class="ctxt-icons">
						<span class="ico-misc calender"></span>
						<div class="c-calender-date">{{ between_dates( $dm ) }}</div>
					</div>
					<a href="{{ $dm->detailUrl() }}" class="ctxt-button">
						Participate
					</a>
				</div>
			</li>
			@endforeach
		</ul>
		@else
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif
	</section>

	<section>
		<div class="ct-page-header">
			<h2>
				Brand Promotions
			</h2>
			<span class="cph-bar"></span>
		</div>
		@if( !$db_promotions->isEmpty() )
		<div id="contest_slider" class="ct-owl-carousel ct-galleries">
			<div class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false"
				 data-sizes= "[ [ 0, 1 ],
						[320, 1],
						[721, 2],
						[840, 3],
						[1040, 3],
						[1220, 3]
					]">
				@foreach( $db_promotions as $do )
				<div>
					<div class="ctg-relative">
						<a href="
							@if( $do->category == 'redemption' )
								{{ $do->urlRedemption() }}
							@else
								{{ $do->urlGift() }}
							@endif
							">
							<img src="{{ $promotion_img[$do->id] }}" alt="{{ $do->name }}" title="{{ $do->name }}">
						</a>
					</div>
					<div class="ctg-headings">
						<h4>{{ $db_brand->name }}</h4>
						<h3>
							<a href="
								@if( $do->category == 'redemption' )
									{{ $do->urlRedemption() }}
								@else
									{{ $do->urlGift() }}
								@endif
							"
							title="{{ $do->name }}"
							>
								{{ string_dots( $do->name ) }}
							</a>
						</h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $do ) }}</span>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		@else
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif
	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}
	{!! html_js( 'js/vendors/gasparesganga-jquery-loading-overlay/dist/loadingoverlay.min.js' ) !!}
    <script>
    function afterFavorite( resp )
    {
        if( resp.data.subscribed )
        {
            $( ".ctlt-button.fav" ).addClass( "active" );
        }
        else
        {
            $( ".ctlt-button.fav" ).removeClass( "active" );
        }
    }
    function afterFavoriteError(e)
    {

    }
    </script>
@stop

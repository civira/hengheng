@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/brands.css' ) !!}
	{!! html_css( 'select2/dist/css/select2.min.css', 'js/vendors' ) !!}
@stop

@section( 'page_content' )

<section id="brands_content" class="st-container" >

        @if( $authed_user && $favs->isNotEmpty() )
        <div id="fav_brands_header" class="ct-head-with-filter">
            <div class="ct-page-header">
                <h1>
                    My Preferred Brands
                </h1>
                <span class="cph-bar"></span>
            </div>
            <div class="ct-filter-simple">
                {!! form_open('',
                    [
                        'id' => 'ct_filter_form'
                    ])
                !!}
                <div class="controls cs-row ss2-parent-wrapper">
                    <select name="filter_sort"
                            class="control-retailer site-select2 control-sort"
                            data-placeholder="Select Category"
                            data-class="ss2-wrapper"
                            data-open-class="ss2-open-wrapper"
                        >
                        <option value="0">Select Category</option>
                        @foreach( $db_brand_categories as $val=>$db )
                            <option value="{{ $db->id }}" @if( $f_sort == $db->id )selected="selected"@endif >{{ $db->name }}</option>
                        @endforeach
                    </select>
                </div>
                {!! form_close() !!}
            </div>
        </div>
        <div id="fav_brands" class="fav-brands ct-gallery-thumbnail">
            <ul>
                @foreach( $favs as $key=>$f )
                @php $db = $f->brand @endphp
                <li>
                    <a href="@if($db->highlighted == 'NO' ) # @else {{ $db->searchUrl() }} @endif">
                        <img src="{{ $fav_img[$db->id] }}" alt="{{ $db->name }}" title="{{ $db->name }}" width="170" height="110" @if($db->highlighted == 'NO' )class="img-greyed" @endif>
                    </a>
                    <span class="icomoon-favorite"></span>
                </li>
                @endforeach
            </ul>
        </div>
        @endif

		<div class="ct-head-with-filter">
			<div class="ct-page-header">
				<h1>
					Brands
				</h1>
				<span class="cph-bar"></span>
			</div>
                    @if( !$authed_user || $favs->isEmpty() )
                    <div class="ct-filter-simple">
                        {!! form_open('',
                            [
                                'id' => 'ct_filter_form'
                            ])
                        !!}
                        <div class="cs-row ss2-parent-wrapper controls">
                            <select name="filter_sort"
                                    class="control-retailer site-select2 control-sort"
                                    data-placeholder="Select Category"
                                    data-class="ss2-wrapper"
                                    data-open-class="ss2-open-wrapper"
                                >
                                <option value="0">Select Category</option>
                                @foreach( $db_brand_categories as $val=>$db )
                                    <option value="{{ $db->id }}" @if( $f_sort == $db->id )selected="selected"@endif >{{ $db->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        {!! form_close() !!}
                    </div>
                    @endif
		</div>

		@if( $db_brand->isEmpty() )
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif

		<div class="ct-gallery-thumbnail">
			<ul>
				@foreach( $db_brand as $key=>$db )
				<li>
					<a href="@if($db->highlighted == 'NO' ) # @else {{ $db->searchUrl() }} @endif">
						<img src="{{ $brand_img[$db->id] }}" alt="{{ $db->name }}" title="{{ $db->name }}" width="170" height="110" @if($db->highlighted == 'NO' )class="img-greyed" @endif>
					</a>
				</li>
				@endforeach
			</ul>
		</div>

		<div class="ct-pagination">
			{{ $db_brand->links() }}
		</div>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/select2/dist/js/select2.full.min.js' ) !!}
	{!! html_js( 'js/vendors/gasparesganga-jquery-loading-overlay/dist/loadingoverlay.min.js' ) !!}
@stop

@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/brand-detail.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section id="brand_detail_content" class="st-container" >

	<section id="brand_media">

		<div class="hs_slider">
			<div class="owl-carousel" data-items="1" data-loop="true">
				<div>
					{!! html_img_site( 'stores/lock1.jpg', 
					[ 
						'w' => 535, 
						'h' => 350,
					]) !!}
				</div>
				<div>
					{!! html_img_site( 'stores/lock2.jpg', 
					[ 
						'w' => 535, 
						'h' => 350,
					]) !!}
				</div>
				<div>
					{!! html_img_site( 'stores/lock3.jpg', 
					[ 
						'w' => 535, 
						'h' => 350,
					]) !!}
				</div>
			</div>
		</div>

		<div id="brand_info">
			<div class="bi-header">
				{!! html_img_site( 'brands/lock.jpg', 
				[ 
					'class'=>'bi-logo',
					'w' => 170, 
					'h' => 110,
				]) !!}	
				<div class="bi-title">Lock & Lock</div>
			</div>

			<div class="bi-description">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac mauris lobortis, pretium diam ut, mollis mauris. Quisque congue justo sit amet sollicitudin finibus. Ut non porta massa. Donec est erat, lobortis nec nisi non, fermentum laoreet nibh. Ut id commodo sem. Quisque at tincidunt lacus. Sed iaculis augue non ligula hendrerit
			</div>

		</div>

	</section>

	<section class="informative-product">
		
			<div class="ct-page-header">
				<h2>
					Brand Contest
				</h2>
				<span class="cph-bar"></span>
			</div>

			<ul class="ct-expand-list">

				<li>
					<div class="ce-image">
						{!! html_img_site( 'contests/iphone.jpg', 
						[ 
							'w' => 320, 
							'h' => 210,
						]) !!}
						
					</div>
					<div class="ce-context">
						<h3 class="ctxt-title">
							<a href="#">Win A Iphone X 256GB!</a>
						</h3>
						<div class="ctxt-icons">
							<span class="ico-misc calender"></span>
							<div class="c-calender-date">23 may - 15 July</div>
						</div>
						<div class="ctxt-description">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac mauris lobortis, pretium diam ut, mollis mauris. Quisque congue justo sit amet sollicitudin finibus. 
						</div>
						<a href="#" class="ctxt-button">
							PARTICIPATE TO WIN
						</a>

					</div>	
				</li>

			</ul>


	</section>

	<section class="informative-product">
		
			<div class="ct-page-header">
				<h2>
					Brand Promotion
				</h2>
				<span class="cph-bar"></span>
			</div>

			<ul class="ct-expand-list">

				<li>
					<div class="ce-image">
						{!! html_img_site( 'contests/iphone.jpg', 
						[ 
							'w' => 320, 
							'h' => 210,
						]) !!}
						
					</div>
					<div class="ce-context">
						<h3 class="ctxt-title">
							<a href="#">Win A Iphone X 256GB!</a>
						</h3>
						<div class="ctxt-icons location">
							<span class="ico-misc location"></span>
							<div class="c-calender-date">Multiple Locations</div>
						</div>
						<div class="ctxt-icons">
							<span class="ico-misc calender"></span>
							<div class="c-calender-date">23 may - 15 July</div>
						</div>
						<div class="ctxt-description">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac mauris lobortis, pretium diam ut, mollis mauris. Quisque congue justo sit amet sollicitudin finibus. 
						</div>
						<a href="#" class="ctxt-button">
							SEE PROMOTION
						</a>
					</div>	
				</li>

			</ul>


	</section>



</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/jquery/dist/jquery.min.js' ) !!}
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	<script>
	$(function()
	{
		var $carousels = $( ".owl-carousel" ),
			i, len;
		for( i = 0, len = $carousels.length; i < len; i++ )
		{
			var $carousel = $carousels.eq( i ),
				data  	  = $carousel.data();

			data.items    = typeof data.items === "undefined"? 1 : data.items;
			data.autoplay = typeof data.autoplay === "undefined"? true : data.autoplay;

			$carousel.owlCarousel( data );
		}

		/**
		 * G.C
		 */
		i = len = $carousels = null;
	});
	</script>

@stop
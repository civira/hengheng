@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
@stop

@section( 'page_content' )

<section class="st-container with-margin" >

		<section class="ct-logo-title">
			<img src="{{ $retailer_logo }}" width="255" height="165" alt="{{ $db_retailer->name }}" title="{{ $db_retailer->name }}">
		</section>

		<div class="ct-page-header">
			<h1>
				Lucky Draw
			</h1>
			<span class="cph-bar"></span>
		</div>
		@if( $db_list->isEmpty() )
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif

		<div class="ct-galleries">
			<ul>
				@foreach( $db_list as $dl )
				<li>
					<div class="ctg-relative">
						<a href="{{ $dl->detailUrl() }}">
							<img src="{{ $dl->image_location }}" alt="{{ $dl->name }}" title="{{ $dl->name }}" width="375" height="245">
						</a>
						<a href="{{ $dl->detailUrl() }}" class="ctw-overlayed">
							<div class="ctw-subscribe">PARTICIPATE TO WIN</div>
						</a>
					</div>
					<div class="ctg-headings">
						<h4>{{ $dl->brand_data->name }}</h4>
						<h3>
							<a href="{{ $dl->detailUrl() }}" title="{{ $dl->name }}">
								{{ string_dots($dl->name) }}
							</a>
						</h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $dl ) }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

		<div class="ct-pagination">
			{{ $db_list->links() }}
		</div>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


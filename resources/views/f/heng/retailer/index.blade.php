@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/retailers.css' ) !!}
@stop

@section( 'page_content' )

<section id="retailers_content" class="st-container" >

		@if( $db_categories->isEmpty() )
		<div class="ct-page-header">
			<h1>
				Retailers
			</h1>
			<span class="cph-bar"></span>
		</div>
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif

		@foreach( $db_categories as $db_c )
		<section class="retailer-section">
			<div class="ct-head-with-filter">
				<div class="ct-page-header">
					<h2>
						{{ $db_c->name }}
					</h2>
					<span class="cph-bar"></span>
				</div>
			</div>

			<div class="ct-gallery-thumbnail">
				<ul>
					@foreach( $retailer_data[$db_c->id] as $key=>$rd )
					<li @if( $key%6 == 5 ) class="last-thumbnail greyed" @endif >
						<a href="{{ $rd->detailUrl() }}">
							<img src="{{ $rd->file_img() }}" alt="{{ $rd->name }}" title="{{ $rd->name }}" width="170" height="110">
						</a>
					</li>
					@endforeach
				</ul>
			</div>
		</section>
		@endforeach
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


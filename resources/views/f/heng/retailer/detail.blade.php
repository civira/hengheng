@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
	{!! html_css( 'heng/retailer-detail.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}
	{!! html_css( 'select2/dist/css/select2.min.css', 'js/vendors' ) !!}
@stop

@section( 'page_content' )

<section class="st-container with-margin" >

	<section class="ct-logo-title">
		<div class="ctlt-left">
			<img src="{{ $retailer_logo }}" width="255" height="165" alt="{{ $db_retailer->name }}" title="{{ $db_retailer->name }}">
		</div>
		@if( !empty( 'retailer_categories' ) )
		<div class="ctlt-right ss2-parent-wrapper">
			<select name="filter_retailer" id="control_retailer" class="control-retailer site-select2" data-placeholder="select retailer" data-class="ss2-wrapper" data-open-class="ss2-open-wrapper" data-no-search="true">
				@foreach( $retailer_categories as $rc )
				<optgroup label="{{ $rc->name }}">
					@foreach( $rc->retailers as $r )
						<option value="{{ $r->detailUrl() }}" @if($db_retailer->id==$r->id)selected="selected"@endif>{{ $r->name }}</option>
					@endforeach
				</optgroup>
				@endforeach
			</select>
		</div>
		@endif
	</section>

	<section>
		<div class="ct-head-with-filter">
			<div class="ct-page-header">
				<h2>
					Lucky Draw
				</h2>
				<span class="cph-bar"></span>
			</div>
		</div>

		@if( !$db_lucky->isEmpty() )
		<div class="ct-galleries">
			<ul>
				@foreach( $db_lucky as $dlucky )
				<li>
					<div class="ctg-relative">
						<a href="{{ $dlucky->detailUrl() }}">
							<img src="{{ $dlucky->image_location }}" alt="{{ $dlucky->name }}" title="{{ $dlucky->name }}" width="375" height="245">
						</a>
						<a href="{{ $dlucky->detailUrl() }}" class="ctw-overlayed">
							<div class="ctw-subscribe">PARTICIPATE TO WIN</div>
						</a>
					</div>
					<div class="ctg-headings">
						<h4>{{ $dlucky->brand_data->name }}</h4>
						<h3><a href="{{ $dlucky->detailUrl() }}" title="{{ $dlucky->name }}">{{ string_dots( $dlucky->name ) }}</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $dlucky ) }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

 		@if( $total_lucky > 3 )
		<div class="ct-show-more">
			@if( !$r_show )
			<a href="{{ $db_retailer->detailUrl().'?show=all' }}" class="button">
				View All
			</a>
			@endif
			<hr class="line">
		</div>
        @endif

		@else
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif

	</section>

	<section>
		<div class="ct-head-with-filter">

			<div class="ct-page-header">
				<h2>
					Redemption
				</h2>
				<span class="cph-bar"></span>
			</div>
		</div>
		@if( !$db_promo_redemptions->isEmpty() )
		<div class="ct-galleries">
			<ul>
				@foreach( $db_promo_redemptions as $dpr )
				<li>
					<div class="ctg-relative">
						<a href="{{ $dpr->urlRedemption() }}">
							<img src="{{ $dpr->image_location }}" alt="{{ $dpr->name }}" title="{{ $dpr->name }}" width="375" height="245" class="ctw-image">
						</a>
					</div>
					<div class="ctg-headings">
						<h4>{{ $dpr->brand_data->name }}</h4>
						<h3><a href="{{ $dpr->urlRedemption() }}" title="{{ $dpr->name }}">{{ string_dots( $dpr->name ) }}</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $dpr ) }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

 		@if( $total_redemption > 3 )
		<div class="ct-show-more">
			@if( !$r_show )
			<a href="{{ $db_retailer->detailUrl().'?show=all' }}" class="button">
				View All
			</a>
			@endif
			<hr class="line">
		</div>
        @endif

		@else
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif
	</section>

	<section>
		<div class="ct-head-with-filter">

			<div class="ct-page-header">
				<h2>
					Free Gift
				</h2>
				<span class="cph-bar"></span>
			</div>
		</div>
		@if( !$db_promo_gifts->isEmpty() )
		<div class="ct-galleries">
			<ul>
				@foreach( $db_promo_gifts as $dc )
				<li>
					<div class="ctg-relative">
						<a href="{{ $dc->urlGift() }}">
							<img src="{{ $dc->image_location }}" alt="{{ $dc->name }}" title="{{ $dc->name }}" width="375" height="245" class="ctw-image">
						</a>
					</div>
					<div class="ctg-headings">
						<h4>{{ $dc->brand_data->name }}</h4>
						<h3><a href="{{ $dc->urlGift() }}" title="{{ $dc->name }}">{{ string_dots( $dc->name ) }}</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $dc ) }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

		@if( $total_gift > 3 )
		<div class="ct-show-more">
			@if( !$r_show )
			<a href="{{ $db_retailer->detailUrl().'?show=all' }}" class="button">
				View All
			</a>
			<hr class="line">
			@endif
		</div>
        @endif

		@else
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif
	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/vendors/select2/dist/js/select2.full.min.js' ) !!}
	{!! html_js( 'js/vendors/gasparesganga-jquery-loading-overlay/dist/loadingoverlay.min.js' ) !!}
	<script>
	$(function()
	{
		var $carousels = $( ".owl-carousel" ),
			i, len;
		for( i = 0, len = $carousels.length; i < len; i++ )
		{
			var $carousel = $carousels.eq( i ),
				data  	  = $carousel.data();

			data.items    = typeof data.items === "undefined"? 1 : data.items;
			data.autoplay = typeof data.autoplay === "undefined"? true : data.autoplay;

			$carousel.owlCarousel( data );
		}

		$( "#control_retailer" ).on( "change", function()
		{
			document.location = $(this).val();
			showLoadingOverlay();
		});

		/**
		 * G.C
		 */
		i = len = $carousels = null;
	});
	</script>

@stop

<section class="ct-associate-retailer">
	@if( $db->is_islandwide == 0 )
		@if( !$db_associate_retailer->isEmpty() )
		    <div class="ct-page-header m-bottom">
		        <h3>
		            Retailers
		        </h3>
		        <span class="cph-bar"></span>
		    </div>
			<ul>
				@foreach( $db_associate_retailer as $da )
				<li>
					<a href="{{ $da->retailer_data->detailUrl() }}">
						<img src="{{ $da->retailer_logo }}" width="85" height="55" title="{{ $da->retailer_data->name }}" alt="{{ $da->retailer_data->name }}" class="">
					</a>
				</li>
				@endforeach
			</ul>
		@endif
	@else
	    <div class="ct-page-header m-bottom">
	        <h3>
	            Retailers
	        </h3>
	        <span class="cph-bar"></span>
	    </div>
	    <div class="ct-associate-retailer islandwide">
			{!! html_img_site( 'singapore-icon.png',
			[
				'w' => 65,
				'h' => 65,
			]) !!}
			<div class="title">
				Islandwide
			</div>
	    </div>
	@endif
</section>

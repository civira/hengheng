@if( !$db_other->isEmpty() )

    <div class="ct-page-header">
        <h2>
            {{ $other_sidebar_title }}
        </h2>
        <span class="cph-bar"></span>
    </div>

    <div id="other_items_bar">
    	@foreach( $db_other as $do )
            @if( $other_sidebar_var == 'redemption' )
                <?php $other_sidebar_url = $do->urlRedemption(); ?>
            @elseif( $other_sidebar_var == 'gift' )
                <?php $other_sidebar_url = $do->urlGift(); ?>
            @else
                <?php $other_sidebar_url = $do->detailUrl(); ?>
            @endif
    		<div class="items">
    			<a href="{{ $other_sidebar_url }}" class="it-col left">
                    <img src="{{ $do->image }}" alt="{{ $do->name }}" title="{{ $do->name }}" width="200" height="128">
    			</a>
    			<div class="it-col right">
    				<a href="{{ $do->brand_data->searchUrl() }}" title="{{ $do->brand_data->name }}" class="brand-name">
                    	{{ $do->brand_data->name }}
    				</a>
    				<a href="{{ $other_sidebar_url }}" title="{{ $do->name }}" class="title-name">
                   		{{ string_dots( $do->name ) }}
    				</a>
                    <div class="date-time">
                        <span class="ico-misc calender"></span>
                        <span>{{ between_dates( $do ) }}</span>
                    </div>
    			</div>
    		</div>
    	@endforeach
    </div>

@endif
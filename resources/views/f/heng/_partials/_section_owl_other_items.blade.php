@if( !$db_other->isEmpty() )
<section class="@if( !$owl_other_webview ) ct-onmobile @endif">
    <div class="ct-page-header">
        <h2>
            {{ $owl_other_title_name }}
        </h2>
        <span class="cph-bar"></span>
    </div>
    <div class="ct-owl-carousel ct-galleries">
        <div class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false"
             data-sizes= "[ [ 0, 1 ],
                    [320, 1],
                    [721, 2],
                    [840, 3],
                    [1040, 3],
                    [1220, 3]
                ]">

            @foreach( $db_other as $do )
                @if( $owl_redirect_var == 'detail' )
                    <?php $owl_redirect_url = $do->detailUrl(); ?>
                @elseif( $owl_redirect_var == 'redemption' )
                    <?php $owl_redirect_url = $do->urlRedemption(); ?>
                @elseif( $owl_redirect_var == 'gift' )
                    <?php $owl_redirect_url = $do->urlGift(); ?>
                @endif

            <div>
                @if( $owl_overlayed )
                    <div class="ctg-relative">
                        <a href="{{ $owl_redirect_url }}">
                            <img src="{{ $do->image }}" alt="{{ $do->name }}" title="{{ $do->name }}">
                        </a>
                    </div>
                @else
                    <div class="ct-owl-overlay">
                        <a href="{{ $owl_redirect_url }}">
                            <img src="{{ $do->image }}" alt="{{ $do->name }}" title="{{ $do->name }}">
                        </a>
                        <a class="ct-owl-bg" href="{{ $owl_redirect_url }}">
                            <div class="ct-owl-btn">PARTICIPATE TO WIN</div>
                        </a>
                    </div>
                @endif

                <div class="ctg-headings">
                    @if( $do->brand_data )
                    <h4>
                        <a href="{{ $do->brand_data->searchUrl() }}">
                            {{ $do->brand_data->name }}
                        </a>
                    </h4>
                    @endif
                    <h3>
                        <a href="{{ $owl_redirect_url }}" title="{{ $do->name }}">
                            {{ string_dots( $do->name ) }}
                        </a>
                    </h3>
                    <div class="ctg-info">
                        <div class="i-row center">
                            <span class="ico-misc calender"></span>
                            <span class="ir-txt">{{ between_dates( $do ) }}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
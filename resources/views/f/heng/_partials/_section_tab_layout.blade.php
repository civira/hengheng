<img src="{{ $db->image }}" width="765" height="490" alt="{{ $db->name }}" title="{{ $db->name }}" class="main-image">

<section id="ct_tabs_container">
	<div>
		<ul class="ct-tabs-menu">
			<li>
				<a href="#tabs_information" class="active-tab">Information</a>
			</li>
			@if( $db->agreement_description )
			<li>
				<a href="#tabs_terms_and_conditions">Terms & Conditions</a>
			</li>
			@endif
		</ul>

		<div class="panel-article terms" id="tabs_information">
			<div>
				<div class="pa-header">
					@if( $db_brand )
					<div class="slb-name">{{ $db_brand->name }}</div>
					@endif
					<div class="slb-date">
						<span class="ico-misc calender"></span>
						<span>{{ between_dates( $db ) }}</span>
					</div>
				</div>
				<div class="pa-context ct-page-header">
					<h1 class="slc-title">{{ $db->name }}</h1>
					<span class="cph-bar"></span>
				</div>
				<div class="pa-paragraph">
					{!! $db->description !!}
				</div>
			</div>
		</div>
		@if( $db->agreement_description )
		<div class="panel-article" id="tabs_terms_and_conditions">
			<div>
                <div class="pa-header">
                    @if( $db_brand )
                    <div class="slb-name">{{ $db_brand->name }}</div>
                    @endif
                    <div class="slb-date">
                        <span class="ico-misc calender"></span>
                        <span>{{ between_dates( $db ) }}</span>
                    </div>
                </div>
                <div class="pa-context ct-page-header">
                    <div class="slc-title">{{ $db->name }}</div>
                    <span class="cph-bar"></span>
                </div>
				<div class="pa-paragraph">
					{!! $db->agreement_description !!}
				</div>
			</div>
		</div>
		@endif
	</div>

</section>
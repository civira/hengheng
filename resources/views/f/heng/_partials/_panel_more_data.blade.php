<div class="panel-container m-bottom @if( !empty($panel_box_css)) {{ $panel_box_css }} @endif">
	<div class="fd-main-title">
		{{ $panel_more_title }}
	</div>

	<div class="fd-paragraph">
		{{ $panel_more_paragraph }}
	</div>

	<a href="{{ $panel_more_redirect }}">
		<input type="submit" class="fd-button" value="{{ $panel_more_btn }}">
	</a>
</div>
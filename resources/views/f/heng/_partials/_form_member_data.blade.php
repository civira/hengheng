<div class="cf-form">
	<input type="text" class="cf-input" placeholder="Name as per NRIC/FIN" name="name" value="{{ form_set_value( 'name', $authed_user->full_name ) }}">
	<input type="text" class="cf-input" placeholder="Fill your Email" name="email" value="{{ form_set_value( 'email', $authed_user->email ) }}" disabled="disabled">
	<input type="text" maxlength="8" class="prevent_chars cf-input no-spin" placeholder="8-Digit Mobile Number" name="mobile_number" value="{{ form_set_value( 'mobile_number', $authed_user->phone_number ) }}">
</div>
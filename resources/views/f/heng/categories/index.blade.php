@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/categories.css' ) !!}
@stop

@section( 'page_content' )

<section id="categories_content" class="st-container" >

	<section class="cc-section">
		<div class="ct-page-header">
			<h2>
				Food Contest
			</h2>
			<span class="cph-bar"></span>
		</div>

		<div class="ct-galleries">
			<ul>
				<li class="ctg-even">
					<div class="ctg-relative">
						<a href="#">
						{!! html_img_site( 'promotions/frap.jpg', 
						[ 
							'w' => 380, 
							'h' => 210,
						]) !!}
						</a>
						<a href="#" class="ctw-subscribe">PARTICIPATE TO WIN</a>
						<div class="ctw-title">
							CONTEST CATEGORY
						</div>
					</div>
					<div class="ctg-headings">
						<h4>KRISPY KREME</h4>
						<h3><a href="">Free Cappucino Ice Coffe Milk!</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">23 May - 10 July</span>
							</div>
						</div>
					</div>
				</li>
				<li  class="ctg-odd">
					<div class="ctg-relative">
						<a href="#">
						{!! html_img_site( 'promotions/donut.jpg', 
						[ 
							'w' => 380, 
							'h' => 210,
						]) !!}
						</a>
						<a href="#" class="ctw-subscribe">PARTICIPATE TO WIN</a>
						<div class="ctw-title">
							CONTEST CATEGORY
						</div>
					</div>
					<div class="ctg-headings">
						<h4>KRISPY KREME</h4>
						<h3><a href="">50% Discount for 1 Dozen Donuts</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">23 May - 10 July</span>
							</div>
						</div>
					</div>

				</li>
				<li class="ctg-no-margin ctg-even">
					<div class="ctg-relative">
						<a href="#">
						{!! html_img_site( 'promotions/din.jpg', 
						[ 
							'w' => 380, 
							'h' => 210,
						]) !!}
						</a>
						<a href="#" class="ctw-subscribe">PARTICIPATE TO WIN</a>
						<div class="ctw-title">
							CONTEST CATEGORY
						</div>
					</div>
					<div class="ctg-headings">
						<h4>KRISPY KREME</h4>
						<h3><a href="">Din Tai Fu Discount for Every Meal Purchase !</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">23 May - 10 July</span>
							</div>
						</div>
					</div>

				</li>
			</ul>
		</div>

	</section>

	<section class="cc-section">
		<div class="ct-page-header">
			<h2>
				Food Promotion
			</h2>
			<span class="cph-bar"></span>
		</div>

		<div class="ct-galleries">
			<ul>
				<li  class="ctg-even">
					<div class="ctg-relative">
						<a href="#">
						{!! html_img_site( 'promotions/frap.jpg', 
						[ 
							'w' => 380, 
							'h' => 210,
						]) !!}
						</a>
						<div class="ctw-title">
							FOOD & BEVERAGES
						</div>
					</div>
					<div class="ctg-headings">
						<h4>KRISPY KREME</h4>
						<h3><a href="">Free Cappucino Ice Coffe Milk!</a></h3>
						<div class="ctg-info">
							<div class="i-row left">
								<span class="ico-misc location"></span>
								<span class="ir-txt">Ion Mall</span>
							</div>
							<div class="i-row right">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">23 May - 10 July</span>
							</div>
						</div>

					</div>

				</li>
				<li  class="ctg-odd">
					<div class="ctg-relative">
						<a href="#">
						{!! html_img_site( 'promotions/donut.jpg', 
						[ 
							'w' => 380, 
							'h' => 210,
						]) !!}
						</a>
						<div class="ctw-title">
							FOOD & BEVERAGES
						</div>
					</div>
					<div class="ctg-headings">
						<h4>KRISPY KREME</h4>
						<h3><a href="">50% Discount for 1 Dozen Donuts</a></h3>
											<div class="ctg-info">
						<div class="i-row left">
							<span class="ico-misc location"></span>
							<span class="ir-txt">Ion Mall</span>
						</div>
						<div class="i-row right">
							<span class="ico-misc calender"></span>
							<span class="ir-txt">23 May - 10 July</span>
						</div>
					</div>
					</div>

				</li>
				<li class="ctg-no-margin ctg-even">
					<div class="ctg-relative">
						<a href="#">
						{!! html_img_site( 'promotions/din.jpg', 
						[ 
							'w' => 380, 
							'h' => 210,
						]) !!}
						</a>
						<div class="ctw-title">
							FOOD & BEVERAGES
						</div>
					</div>
					<div class="ctg-headings">
						<h4>KRISPY KREME</h4>
						<h3><a href="">Din Tai Fu Discount for Every Meal Purchase !</a></h3>
											<div class="ctg-info">
						<div class="i-row left">
							<span class="ico-misc location"></span>
							<span class="ir-txt">Ion Mall</span>
						</div>
						<div class="i-row right">
							<span class="ico-misc calender"></span>
							<span class="ir-txt">23 May - 10 July</span>
						</div>
					</div>
					</div>

				</li>
			</ul>
		</div>

	</section>

	<section class="cc-section">
		<div class="ct-page-header">
			<h2>
				Food Brand
			</h2>
			<span class="cph-bar"></span>
		</div>

		<div class="ct-gallery-thumbnail">
			<ul>
				<li>
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/tri.jpg', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/jack.jpg', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/harrods.jpg', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/clarks.jpg', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li >
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/samsung.png', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li class="last-thumbnail">
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/la.png', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/laira.png', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/roberto.jpg', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/tri.jpg', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.brands.detail', 1 ) }}">
					{!! html_img_site( 'brands/47.jpg', 
					[ 
						'w' => 170, 
						'h' => 110,
					]) !!}
					</a>
				</li>
				

			</ul>
		</div>

	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


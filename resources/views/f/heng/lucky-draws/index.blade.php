@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

@stop

@section( 'page_content' )

<section class="st-container with-margin">

		<div>
			<div class="ct-page-header">
				<h1>
					Lucky Draw
				</h1>
				<span class="cph-bar"></span>
			</div>
		</div>
		@if( $db_lucky->isEmpty() )
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif

		<div class="ct-galleries">
			<ul>
				@foreach( $db_lucky as $dc )
				<li>
					<div class="ctg-relative">
						<a href="{{ $dc->detailUrl() }}">
							<img src="{{ $dc->file_img() }}" alt="{{ $dc->name }}" width="375" height="245">
						</a>
						<a href="{{ $dc->detailUrl() }}" class="ctw-overlayed">
							<div class="ctw-subscribe">PARTICIPATE TO WIN</div>
						</a>
					</div>
					<div class="ctg-headings">
						@if( $dc[ 'brand_data'] )
						<h4>{{ $dc[ 'brand_data']->name }}</h4>
						@endif
						<h3>
							<a href="{{ $dc->detailUrl() }}" title="{{ $dc->name }}">
								{{ string_dots($dc->name) }}
							</a>
						</h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $dc ) }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>
 
		<div class="ct-pagination">
			{{ $db_lucky->links() }}
		</div>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


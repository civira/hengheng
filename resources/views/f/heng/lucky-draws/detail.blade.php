@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/lucky-detail.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section class="st-container with-margin">

    @if( html_message_is_valid() )
        <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class( 'txt-error', 'txt-valid' ) }}">
            {{ html_get_first_message() }}
        </div>
    @endif

	<section class="split-layout">
		<div class="sl-left">

			<!-- information -->
			@include( 'f.heng._partials._section_tab_layout' )

			<!-- retailer contest -->
			@include( 'f.heng._partials._retailer_associates' )

			<!-- social network sharing -->
			@include( 'f.heng._partials._share_social_networks' )

		</div>
		<div class="sl-right">

				@if( ( $db_participation && !$db->isDateExpired() ) || ( $db_subscribed && !$db->isDateExpired() ) ||  $db->statusAwaitingContest() ||  $db->statusWinnerContest() )

					@include( 'f.heng._partials._panel_more_data', 
					[ 
					'panel_more_title' => 'More Lucky Draws', 
					'panel_more_paragraph' => 'You can upload one receipt for multiple lucky draws. Browse and participate in our other lucky draws.',
					'panel_more_redirect' => route( 'f.heng.lucky.index' ),
					'panel_more_btn' => 'Browse Lucky Draws'
					] )

				@endif

			<div class="panel-container {{ $db->isWinnerAnnounced()? 'winner-announced' : '' }}">
				<div class="fd-main-title">
					@if( !$log_user )
						Participate to Win!
					@elseif( $db->statusAwaitingContest() )
						Awaiting Announcement!
					@elseif( $db->isWinnerAnnounced() )
                        @if( $user_participated )
                            {{ $user_won? 'Congratulations!' : 'Thanks For Participating' }}
                        @else
                            Winner Announced!
						@endif
					@elseif( $db->isDateExpired() )
						More Lucky Draws
					@elseif( $db_participation )
						Upload Receipt To Win!
					@else
						Participate to Win!
					@endif
				</div>
				@if( $log_user )
					@if( $db->statusAwaitingContest() )
						<a href="#" class="fd-button red">
							Pending
						</a>
                    @elseif( $user_participated && $db->isWinnerAnnounced() )
                        <div class="fd-guide">
                            {{ $user_won? __('messages.data.user_win', [ $winner->position ]) : __('messages.data.user_not_win') }}
                        </div>
						<a href="{{ $db->winnerUrl() }}" class="fd-button red">
							Winner List
						</a>
					@elseif( $db->isWinnerAnnounced() )
						<a href="{{ $db->winnerUrl() }}" class="fd-button red">
							Winner List
						</a>
					@elseif( $db->isDateExpired() )
						<a href="{{ route( 'f.heng.lucky.index') }}" class="fd-button">
							Browse Lucky Draws
						</a>
					@elseif( $db_subscribed && !$db_participation )
						<a href="{{ route( 'f.heng.lucky.participation', [ $db->id ]) }}" class="fd-button red">
							Participate Again
						</a>
					@elseif( $db_participation )
						<a href="{{ route( 'f.heng.users.multi_upload', [ null, 's' => $db->id ]) }}" class="fd-button red">
							Upload Receipt
						</a>
					@endif

					@if( !html_message_is_valid() )
					<div class="ct-error-info {{ html_add_error_class() }}">
				    	{{ html_get_first_error_message() }}
					</div>
					@endif

					{!! form_open_post( route( 'f.heng.lucky.subscribe', $db->id ), [ 'id'=>'form_receipt' ], true ) !!}
					@if( (empty( $log_user->full_name) || empty( $log_user->phone_number)) && $db->isOngoingContest())
						
						<!-- form -->
						@include( 'f.heng._partials._form_member_data' )

					@endif

					@if( !$db_subscribed && !$db_participation && $db->isOngoingContest() && !$db->isDateExpired() )
					<div class="cf-agreement">
						<input type="checkbox" name="check_agreement" id="check-agreement" class="css-checkbox" value="YES" @if( old('check_agreement') == 'YES')checked="checked"@endif />
						<label for="check-agreement" class="css-label">
							By participating, you are agreeing to this contest's terms & conditions.
						</label>
					</div>
					@endif
						@if( $db->statusAwaitingContest() )
							<div class="fd-guide">
								Please wait for further announcement.
							</div>
						@elseif( $db->statusWinnerContest() )
							<div class="fd-guide">
								Click on the button to check list of current winner.
							</div>
						<!-- note -->
						@elseif( $db->isDateExpired() )
							<div class="fd-guide">
								Sorry, our lucky draw period has ended. Browse and participate in our other lucky draws.
							</div>
						@elseif( $db_participation )
							<div class="fd-guide">
								Note: <br/>
								You can choose multiple lucky draws for a receipt<br/>
								JPG, JPEG, PNG file only
							</div>
						@elseif( !$db_participation && !$authed_user->isProfilePartialFilled() )
							<div class="fd-guide agreement">
								To participate in this lucky draw, you must fill all the information.
							</div>
						@elseif( $db_receipts )
							<div class="fd-guide">
								By participating, you are agreeing to this contest's terms & conditions.
							</div>
						@endif

					@if( $db_receipts )
						<div class="ct-uploaded-receipt-files mobile receipt-count">
                            @php $receipt_count = $db_receipts->count() @endphp
                            You have participated {{ $receipt_count }} {{ str_plural( 'time', $receipt_count )}}
                            <a href="{{ $db->receiptListUrl() }}" class="txt-link">view all receipts</a>
                        </div>
					@endif

					@if( !$db_subscribed && !$db_participation && $db->isOngoingContest() && !$db->isDateExpired() )
						<div>
							<input type="submit" class="fd-button" value="Participate">
						</div>
					@endif
					{!! form_close() !!}

				@else
					<a href="{{ route( 'f.heng.auth.login', [ 'l' => encode_authenticate_redirect( $db->detailUrl()) ] ) }}" class="fd-button">Login to Participate</a>
					<div class="fd-guide">
						Note: <br/>
						You can upload multiple receipts<br/>
						You can participate first and upload receipt afterwards<br/>
						JPG, JPEG, PNG file only
					</div>
				@endif

			</div>
				@if( $db_receipts )
					<div class="ct-uploaded-receipt-files shift-right receipt-count">
                        @php $receipt_count = $db_receipts->count() @endphp
                        You have participated {{ $receipt_count }} {{ str_plural( 'time', $receipt_count )}}
                        <a href="{{ $db->receiptListUrl() }}" class="txt-link">view all receipts</a>
					</div>
				@endif
		</div>
	</section>

	<!-- other items -->
    @include( 'f.heng._partials._section_owl_other_items', 
    	[ 'owl_redirect_var'=> 'detail',
    	  'owl_other_title_name' => 'Other Lucky Draws', 
    	  'owl_overlayed' => true,
    	  'owl_other_webview'=> true ] )

</section>


@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}
	{!! html_js( 'js/vendors/gasparesganga-jquery-loading-overlay/dist/loadingoverlay.min.js' ) !!}

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b73f77d0c8e5f78" async="async"></script>

@stop

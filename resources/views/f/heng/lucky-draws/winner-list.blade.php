@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
	{!! html_css( 'heng/lucky-winner-list.css' ) !!}
@stop

@section( 'page_content' )

<section class="st-container with-margin">

	<section id="page_headings">
		<div>
			<img src="{{ $db_brand->image }}" width="255" height="165" alt="{{ $db_brand->name }}" title="{{ $db_brand->name }}">
		</div>

		<h1 class="title">
			<a href="{{ $db->detailUrl() }}">
				{{ $db->name }}
			</a>
		</h1>

		<p class="paragraph">
			Current lucky draw list of winners:
		</p>
	</section>

	@if( $db->status == 'winner' )
	<section class="t-table">

		@foreach( $db_winner as $key=>$dw )
		<div class="row-data @if($key%2 == 0) greyed @endif">
			<div class="column numbering">
				{{ $key+1 }}
			</div>
			<div class="column">
				<div class="title">Name</div>
				{{ $dw->member_data->full_name }}
				<div class="mobile-data">
					<div class="title">
						Phone
					</div>
					{{ mask_string( $dw->member_data->phone_number ) }}
				</div>
			</div>
			<div class="column phone">
				<div class="title">Phone</div>
				{{ mask_string( $dw->member_data->phone_number ) }}
			</div>
			<div class="column">
				<div class="title">Email</div>
				{{ mask_string( $dw->member_data->email, 'email' ) }}
			</div>
		</div>
		@endforeach
	</section>
	@else
		<p class="description">
			Wait for Announcement!
		</p>
	@endif

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


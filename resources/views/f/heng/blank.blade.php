<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">

  @section( 'head_page_meta' )
    <title></title>
    <meta name="description" content="" />
  @show
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

  <link rel="manifest" href="/manifest.json" />
  <link rel="apple-touch-icon" href="apple-touch-icon.png?m=<?php echo filemtime('apple-touch-icon.png'); ?>" />
  <!-- Place favicon.ico in the root directory -->

  @yield( 'head_end' )
  @yield( 'head_vendor_css' )
  @yield( 'head_page_css' )
</head>

<body class="@yield( 'st_body' )">

  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <!-- Add your site or application content here -->
  <div id="st_outer">
  @yield( 'site_content' )


  @yield( 'foot_vendor_js' )
  @yield( 'foot_page_js' )
  </div>
</body>

</html>

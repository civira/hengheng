@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/contest-detail.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section class="st-container" id="contest_contents">

	<section id="contest_container">
		<div id="contest_information">
			<div class="ci-media">
				<img src="{{ $db_img }}" width="762" height="500" alt="{{ $db->name }}">
				<div class="ci-category-contest">CONTEST CATEGORY</div>
			</div>

			<div class="ci-description">
				<div class="ci-branding">
					<div class="cib-name">{{ $db_brand->name }}</div>
					<div class="cib-date">
						<span class="ico-misc calender"></span>
						<span>{{ $db->date }}</span>
					</div>
				</div>
				<div class="ci-context">
					<h1 class="cic-title">{{ $db->name }}</h1>
					<div class="cic-paragraph">
						{!! $db->description !!}
					</div>
				</div>

				<div class="ci-locations">
					LOCATION :
					<ul>
						<li><a href="">ANGUS CELL LOCATION 1</a></li>
						<li><a href="">ANGUS CELL LOCATION 2</a></li>
						<li><a href="">ANGUS CELL LOCATION 3</a></li>
						<li><a href="">ANGUS CELL LOCATION 4</a></li>
					</ul>
				</div>
				<div class="ci-sharing">
					<div class="cis-title">SHARE:</div>
					{!! html_img_site( 'bookmark.jpg', 
					[ 
						'w' => 264, 
						'h' => 28,
					]) !!}
				</div>
			</div>
		</div>
		<div id="contest_form">
			<div class="cf-main-title">Participate to Win!</div>

			{!! html_img_site( 'upload-receipt.jpg', 
			[ 
				'class'=> 'upload-img mobile',
				'w' => 140, 
				'h' => 140,
			]) !!}

			<div>
				<div class="cf-form">
					<input type="text" class="cf-input" placeholder="Name" name="name">
					<input type="text" class="cf-input" placeholder="Email" name="email">
					<input type="text" class="cf-input" placeholder="Mobile Number" name="mobile_number">
					<div class="cf-agreement">
						<input type="checkbox" name="checkboxG1" id="checkboxG1" class="css-checkbox" />
						<label for="checkboxG1" class="css-label">Agree with 
							<a href="#">terms & conditions</a>
						</label>
					</div>

				</div>
				{!! html_img_site( 'upload-receipt.jpg', 
				[ 
					'class'=> 'upload-img',
					'w' => 140, 
					'h' => 140,
				]) !!}

				<div>
					<input type="button" name="subscribe" value="Participate to this Contest" class="cf-button">
				</div>
			</div>
			<div class="cf-guide">
				Note: <br/>
				You can upload multiple receipt<br/>
				You can Participate first and upload receipt afterwards<br/>
				Jpg, JPEG, PDF file only
			</div>
		</div>
	</section>

	<section>
		<div class="ct-page-header">
			<h2>
				Other Contest
			</h2>
			<span class="cph-bar"></span>
		</div>
		<div id="contest_slider" class="ct-owl-carousel ct-galleries">
			<div class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="true"
				 data-sizes= "[ [ 0, 1 ], 
						[320, 1],
						[721, 2], 
						[840, 3], 
						[1040, 3], 
						[1220, 3]
					]">
				@foreach( $db_other as $do )
				<div>
					<div class="ctg-relative">
						<a href="{{ route( 'f.heng.contests.detail', $do->id )}}">
							<img src="{{ $db_other_img[$do->id] }}" alt="{{ $do->name }}" width="320" height="210">
						</a>
						<a href="{{ route( 'f.heng.contests.detail', $do->id )}}" class="ctw-subscribe">PARTICIPATE TO WIN</a>
						<div class="ctw-title">
							CONTEST CATEGORY
						</div>
					</div>
					<div class="ctg-headings">
						<h4>{{ $db_brand->name }}</h4>
						<h3>
							<a href="{{ route( 'f.heng.contests.detail', $do->id ) }}">
								{{ $do->name }}
							</a>
						</h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ $do->date }}</span>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>

</section>


@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}
@stop
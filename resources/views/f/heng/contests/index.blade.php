@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/contests.css' ) !!}
@stop

@section( 'page_content' )

<section class="st-container with-margin">

		<div class="ct-head-with-filter">

			<div class="ct-page-header">
				<h2>
					Contest
				</h2>
				<span class="cph-bar"></span>
			</div>

			<div class="ct-filter-simple">
				<div class="cs-row">{{ $total_contest }} Results</div>
				{!! form_open('',
				[
				'id' => 'ct_filter_form'
				]) !!}
				<div id="controls" class="cs-row">
					<select name="sort" id="control_sort">
						<option value="last-added">Sort by: Last added</option>
						<option value="most-popular">Sort by: Most popular</option>
					</select>
				</div>
				{!! form_close() !!}
			</div>
		</div>

		<div class="ct-galleries">
			<ul>
				@foreach( $db_contest as $dc )
				<li>
					<div class="ctg-relative">
						<a href="{{ route( 'f.heng.contests.detail', $dc->id ) }}">
							<img src="{{ $contest_img[ $dc->id ] }}" alt="{{ $dc->name }}" width="375" height="245">
						</a>
						<a href="{{ route( 'f.heng.contests.detail', $dc->id ) }}" class="ctw-subscribe">PARTICIPATE TO WIN</a>
						<div class="ctw-title c-white">
							CONTEST CATEGORY
						</div>
					</div>
					<div class="ctg-headings">
						<h4>{{ $contest_brand[$dc->id]->name }}</h4>
						<h3><a href="{{ route( 'f.heng.contests.detail', $dc->id ) }}">{{ $dc->name }}</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ $dc->date }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

		<div class="ct-pagination">
			{{ $db_contest->links() }}
		</div>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


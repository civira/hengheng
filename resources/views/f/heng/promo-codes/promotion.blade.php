@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/promotions.css' ) !!}
@stop

@section( 'page_content' )

<section id="promotions_content" class="st-container" >

		<div class="ct-head-with-filter">
			<div class="ct-page-header">
				<h2>
					Promotion
				</h2>
				<span class="cph-bar"></span>
			</div>
			<div class="ct-filter-simple">
				<div class="cs-row">{{ $total_promo }} Results</div>
				{!! form_open('',
					[
						'id' => 'ct_filter_form'
					]) 
				!!}
				<div id="controls" class="cs-row">
					<select name="filter_sort" id="control_sort">
						@foreach( $filters as $val=>$f)
						<option value="{{ $val }}" @if( $r_sort == $val) selected="selected"@endif >{{ $f }}</option>
						@endforeach
					</select>
				</div>
				{!! form_close() !!}
			</div>
		</div>

		<div class="ct-galleries">
			<ul>
				@foreach( $db_promo as $dp )
				<li>
					<div class="ctg-relative">
						<a href="#">
							<img src="{{ $promo_img[ $dp->id ] }}" width="380" height="210">
						</a>
						<div class="ctw-title">
							FOOD & BEVERAGES
						</div>
					</div>
					<div class="ctg-headings">
						<h4>KRISPY KREME</h4>
						<h3><a href="">{{ $dp->name }}</a></h3>
						<div class="ctg-info">
							<div class="i-row left">
								<span class="ico-misc location"></span>
								<span class="ir-txt">Ion Mall</span>
							</div>
							<div class="i-row right">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ $dp->date }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

		<div class="ct-pagination">
			{{ $db_promo->links() }}
		</div>
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


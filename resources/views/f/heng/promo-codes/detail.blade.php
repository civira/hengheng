
@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/promotion-detail.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section class="st-container with-margin">

	<section class="split-layout merged-mobile margin-half">
		<div class="sl-left">
			<div class="panel-article">
				<img src="{{ $db->file_img() }}" width="765" height="490" alt="{{ $db->name }}" class="pa-main-image">
			</div>
		</div>
		<div class="sl-right">
			<div class="panel-container full-mobile">
				<div class="fd-context">
					<h1 class="fd-title" title="{{ $db->name }}">
						{{ title_dots( $db->name ) }}
					</h1>
					<div class="fd-date">
						{{ between_dates( $db) }}
					</div>
					<div class="fd-description">
						{!! $db->description !!}
					</div>
					<div>
						@if( !$authed_user )
							<a href="{{ route( 'f.heng.auth.login', [ 'l' => encode_authenticate_redirect( $db->detailUrl()) ]) }}" class="fd-button">
								LOGIN TO DOWNLOAD
							</a>
						@else
							<a href="{{ route( 'f.heng.promo_codes.download', $db->id ) }}" class="fd-button">
								DOWNLOAD
							</a>
						@endif
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="ct-sharing">
		<div class="title">SHARE:</div>
		<div class="addthis_inline_share_toolbox"></div>
	</div>


	@if( $db_other->count() > 0 )
	<section>
		<div class="ct-page-header">
			<h2>
				Other Promotion Code
			</h2>
			<span class="cph-bar"></span>
		</div>
		<div id="contest_slider" class="ct-owl-carousel ct-galleries">
			<div class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false"
				 data-sizes= "[ [ 0, 1 ],
						[320, 1],
						[721, 2],
						[840, 3],
						[1040, 3],
						[1220, 3]
					]">
				@foreach( $db_other as $do )
				<div>
					<div class="ctg-relative">
						<a href="{{ $do->detailUrl() }}">
							<img src="{{ $do->file_img() }}" alt="{{ $do->name }}" title="{{ $do->name }}">
						</a>
					</div>
					<div class="ctg-headings">
						<h3>
							<a href="{{ $do->detailUrl() }}">
								{{ $do->name }}
							</a>
						</h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $do ) }}</span>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>
	@endif

</section>


@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b73f77d0c8e5f78" async="async"></script>
@stop

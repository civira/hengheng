@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/contests.css' ) !!}
@stop

@section( 'page_content' )

<section class="st-container with-margin">

		<div class="ct-head-with-filter">
			<div class="ct-page-header">
				<h1>
					Promotion Code
				</h1>
				<span class="cph-bar"></span>
			</div>
		</div>
		@if( $db_promo->isEmpty() )
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif

		@if( !$db_promo->isEmpty() )
		<div class="ct-galleries">
			<ul>
				@foreach( $db_promo as $dc )
				<li>
					<div class="ctg-relative">
						<a href="{{ $dc->detailUrl() }}">
							<img src="{{ $dc->file_img() }}" alt="{{ $dc->name }}" width="375" height="245" class="ctw-image">
						</a>
					</div>
					<div class="ctg-headings">
						<h3><a href="{{ $dc->detailUrl() }}" title="{{ $dc->name }}">{{ string_dots( $dc->name ) }}</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $dc ) }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

		<div class="ct-pagination">
			{{ $db_promo->links() }}
		</div>
		@endif
</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


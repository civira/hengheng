@extends('f.heng.base')

@section( 'head_page_css' )
    @parent

    {!! html_css( 'heng/promo-code-download.css' ) !!}

    {!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
    {!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section class="st-container with-margin">

    <section class="split-layout">


        <div class="sl-left">

            <!-- information -->
            @include( 'f.heng._partials._section_tab_layout' )

            <!-- retailer contest -->
            @include( 'f.heng._partials._retailer_associates' )
            
            <!-- social network sharing -->
            @include( 'f.heng._partials._share_social_networks' )

        </div>

        <div class="sl-right">
            @if( $authed_user )
            <div class="panel-container m-bottom">
                <div class="fd-main-title">
                    Buy Online
                </div>

                <div class="fd-guide">
                    Copy the code for use when you checkout your cart at any of the online stores shown below.
                </div>          

                @if( $db_participation )
                <div class="fd-dashed-box">
                    <div class="display-code-data">
                        {{ $db->display_code }}
                    </div>
                </div>
                @else
                    <a href="{{ route( 'f.heng.promo_codes.participate', $db->id ) }}" class="fd-button">
                        Click to redeem code
                    </a>
                @endif
            </div>

            <div class="panel-container">
                <div class="fd-main-title">
                    Buy at Store
                </div>

                <div class="fd-guide">
                    Download the code and save as an image to show to cashier upon payment.
                </div>          

                <a href="{{ route( 'f.heng.promo_codes.download', $db->id ) }}" class="fd-button red">
                    Click to download code
                </a>
            </div>
            @else
                <div class="panel-container">
                    <div class="fd-main-title">
                        Login to Redeem!
                    </div>

                    <div class="fd-guide">
                        Login to get this promo code for use when you checkout your cart at any of the online or buy at store.
                    </div>          

                    <a href="{{ route( 'f.heng.auth.login', [ 'l' => encode_authenticate_redirect( $db->detailUrl()) ]) }}" class="fd-button">
                        LOGIN TO DOWNLOAD
                    </a>
                </div>
            @endif
        </div>

    </section>

    @include( 'f.heng._partials._section_owl_other_items', 
        [ 'owl_redirect_var'=> 'detail',
          'owl_other_title_name' => 'Other Promotion Code', 
          'owl_overlayed' => false,
          'owl_other_webview'=> true ] )

</section>


@stop

@section( 'footer_content' )
    @include( 'f.heng.layouts._footer' )
@stop


@section( 'foot_page_js' )
    @parent
    {!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
    {!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}

    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b73f77d0c8e5f78" async="async"></script>
@stop

@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/users.css' ) !!}
	{!! html_css( 'heng/dashboard.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<div id="user_content" class="st-container" >

    @include( 'f.heng.users.partials._navigation' )

	<div class="ct-panel over-width">

		<section>

            @if( html_get_first_error_message() )
                <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class() }}">
                    {{ html_get_first_error_message() }}
                </div>
            @endif

            <div id="ct_user_childmenu_onmobile">
                @include( 'f.heng.users.partials._list' )
            </div>

            {!! form_open_post( route( 'f.heng.users.redemption_delete_participation' ) ) !!}
			<div class="ct-page-header margined">
                <div class="cph-left">
    				<h1>
    					Ongoing Redemptions
    				</h1>
    				<span class="cph-bar"></span>
                </div>
                <div class="cph-right">
                    @include( 'f.heng.users.partials._toolbar' )
                </div>
			</div>

				<ul class="ct-expand-list">

	                @if( $db_contests->isEmpty() )
	                <div class="ct-no-data slim-bottom">
	                    <div class="texts">{{ __('messages.redemption.no_participate') }}</div>
	                </div>
	                @else
                    	@foreach( $data_group as $key_date=>$contest )

                    	<div class="ct-listed-date">
                    		<div class="date">
                    			<span class="minor">Participated on</span>
                                <span class="major">{{ date_formatted( $key_date ) }}</span>
                    		</div>
                    	</div>
                                @php $array_number = 1; @endphp
        					@foreach( $contest as $key_number=>$data )

        						<li class="ctx-compact {{ count($contest) == $array_number ? 'last' : '' }}"">
                                    <div class="ctxt-wrapper-left">
                                        <div class="ctxt-img">
                							<input type="checkbox"
                                                    id="{{ $key_number }}"
                                                    name="member_contest[]"
                                                    class="css-checkbox"
                                                    value="{{ $key_number }}"
                                            />
                							<label for="{{ $key_number }}"
                                                    class="css-label contest-cart-checkbox">
                                            </label>
            								<label for="{{ $key_number }}" class="contest-cart-image-label">
                									<img src="{{ $data['redemption_data']->file_img() }}" alt="{{ $data['redemption_data']->name }}" title="{{ $data['redemption_data']->name }}" width="200" height="128">
            								</label>
                                        </div>
                                        <div class="ce-context">
                                            <div class="ctxt-metadata">
                                                <h4 class="ctxt-brand">
                                                    <a href="{{ $data['brand_data']->searchUrl() }}">
                                                        {{ $data['brand_data']->name }}
                                                    </a>
                                                </h4>
                                                <span class="sep">.</span>
                                                <div class="ctxt-icons">
                                                    <div class="c-calender-date">{{ between_dates( $data['redemption_data'] ) }}</div>
                                                </div>
                                            </div>
                                            <h3 class="ctxt-title">
                                                <a href="{{ $data['redemption_data']->urlRedemption() }}">{{ $data['redemption_data']->name }}</a>
                                            </h3>
                                            <div class="ctxt-wrapper-right mobile ctx-receipt">
                                                <a class="ctxtr-label" href="{{ $data['receipt_url'] }}">
                                                    @if( $data['receipt_count'] < 1 )
                                                        No receipt has been submitted
                                                    @else
                                                        You have submitted {{  $data['receipt_count'] }} {{ str_plural( 'receipt',  $data['receipt_count'] )}}
                                                    @endif
                                                </a>
                                            </div>
                                        </div>
                                    </div>
        						</li>
                                @php $array_number++; @endphp
        					@endforeach
                                @php $array_number = 1; @endphp
    					@endforeach

					@endif

				</ul>

    		{!! form_close() !!}

			<div class="ct-pagination">
				{{ $db_contests->links() }}
			</div>

		</section>

	</div>

</div>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop

@section( 'foot_page_js' )
	@parent
	<!-- Include jQuery - see http://jquery.com -->
	<script type="text/javascript">
        $(function()
        {

            $( "#btn_cancel" ).on( "click", function( e )
            {
                var $checkboxes = $( ".css-checkbox.nodel" );
                for( var i = 0, len = $checkboxes.length; i < len; i++ )
                {
                    let $checkbox = $checkboxes.eq( i );
                    if( $checkbox.is( ":checked" ) )
                    {
                        alert( "Participation for redemptions with a receipt can not be cancelled");
                        e.preventDefault();
                        return false;
                    }
                }

                return confirm('Are you sure you want to cancel all the selected redemptions participation?');
            });

            var ori_href = $( "#btn_upload" ).attr( "href" );
            $( "#btn_upload" ).on( "click", function( e )
            {
                var $checkboxes = $( ".css-checkbox" ),

                href= "",
                ids = [];

                for( var i = 0, len = $checkboxes.length; i < len; i++ )
                {
                    let $checkbox = $checkboxes.eq( i );
                    if( $checkbox.is( ":checked" ) )
                    {
                        ids.push($checkbox.val());
                    }
                }

                href = ori_href + "?s=" + ( ids.join(",") );

                $(this).attr( "href", href );
            });
        });
	</script>
@stop

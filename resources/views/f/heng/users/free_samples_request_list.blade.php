@extends('f.heng.base')

@section( 'head_page_css' )
    @parent
    {!! html_css( 'heng/lucky-receipt-list.css' ) !!}
@stop


@section( 'page_content' )

<div class="st-container with-margin" >

    <section id="content_wrapper">

        <div id="page_headings" class="db-detail">

            <h1 class="head-title">
                @php $count = $db_request->count() @endphp
                @if( $count < 1 )
                No receipt has been submitted
                @else
                <div>You have requested {{ $count }} free {{ str_plural( 'sample', $count ) }}:</div>
                @endif
            </h1>

            <div class="img">
                <a href="{{ $db->detailUrl() }}">
                    <img src="{{ $db->file_img() }}" width="210" height="135" alt="{{ $db->name }}" title="{{ $db->name }}">
                </a>
            </div>

            <div class="content">
                <div class="metadata">
                    <h4 class="brand">
                        <a href="{{ $brand->searchUrl() }}">
                            {{ $brand->name }}
                        </a>
                    </h4>
                    <span class="sep">.</span>
                    <div class="icons">
                        <div class="c-calender-date">{{ between_dates( $db ) }}</div>
                    </div>
                </div>
                <div class="title">
                    <a href="{{ $db->detailUrl() }}">
                        {{ $db->name }}
                    </a>
                </div>

            </div>
        </div>

        <div class="db-content">

            @if( $db_request->isEmpty() )
            <div class="ct-no-data">
                <div class="texts">No request has been submitted for this free sample.</div>
            </div>
            @else
            <ul class="list">
                @foreach( $db_request as $r )
                <li class="item">
                    <div class="date">Submitted at {{ Date( 'jS M Y H:i:s', strtotime( $r->created_at ) ) }}</div>
                    <div class="name">
                        <p>
                            <strong>{{ $r->address_information->address_label }}</strong>
                            <br/>
                            {{ $r->address_information->address }}
                            <br/>
                            <br/>
                            Unit no:{{ $r->address_information->postal_code }}
                            <br/>
                            Postal code: {{ $r->address_information->unit_no }}
                            <br/>
                            <br/>
                        </p>
                            @if( $r->status == 'delivered' )
                                <div class="cm-box orange">Status : Delivered</div>
                            @elseif( $r->status == 'rejected' )
                                <div class="cm-box black">Status : Rejected</div>
                            @else
                                <div class="cm-box blue">Status : Awaiting Delivery</div>
                            @endif
                    </div>
                </li>
                @endforeach
            </ul>
            @endif

        </div>

    </section>

</div>

@stop


@section( 'footer_content' )
    @include( 'f.heng.layouts._footer_min' )
@stop


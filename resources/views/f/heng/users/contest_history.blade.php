@extends('f.heng.base')

@section( 'head_page_css' )
	@parent
	{!! html_css( 'heng/users.css' ) !!}
	{!! html_css( 'heng/contest-history.css' ) !!}
    {!! html_css( 'select2/dist/css/select2.min.css', 'js/vendors' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section id="user_content" class="st-container" >

    @include( 'f.heng.users.partials._navigation' )

	<section class="ct-panel over-width">

		<div>

            <div id="ct_user_childmenu_onmobile">
                @include( 'f.heng.users.partials._summary_list' )
            </div>

			<ul class="ct-expand-list">

                <div class="ct-header">
                    <div class="ct-page-header margined">
                        <h1>
                            Contest Summary
                        </h1>
                        <span class="cph-bar"></span>
                    </div>
                    <div class="ct-filter-simple">
                        {!! form_open('',
                            [
                                'id' => 'ct_filter_form'
                            ])
                        !!}
                        <div id="controls" class="cs-row ss2-parent-wrapper">
                            <select name="filter_status" id="control_sort"
                                    class="control-retailer site-select2"
                                    data-placeholder="Select Category"
                                    data-class="ss2-wrapper"
                                    data-open-class="ss2-open-wrapper"
                                    data-no-search="true"
                                >
                                <option value="all">Select Status</option>
                                <option value="1" {{ $filter_status == '1'? 'selected="selected"' : '' }}>Awaiting Winner</option>
                                <option value="2" {{ $filter_status == '2'? 'selected="selected"' : '' }}>Winner Announced</option>
                            </select>
                        </div>
                        {!! form_close() !!}
                    </div>
                </div>

            @if( $db_member_contests->isEmpty() )
            <div class="ct-no-data slim-bottom">
                <div class="texts">{{ __('messages.data.no_history') }}</div>
            </div>
            @else

            	@foreach( $db_member_contests as $key=>$fdata )
					<li class="ctx-compact last">
						<a href="{{ $fdata->lucky_data->detailUrl() }}" class="ce-image">
							<img src="{{ $fdata->image_lucky }}" alt="{{ $fdata->lucky_data->name }}" title="{{ $fdata->lucky_data->name }}" width="200" height="128">
						</a>
						<div class="ce-context">
                            <div class="ctxt-metadata">
                                <h4 class="ctxt-brand">
                                    <a href="{{ $fdata->brand_data->searchUrl() }}">{{ $fdata->brand_data->name }}</a>
                                </h4>
                                <span class="sep">.</span>
                                <div class="ctxt-icons">
                                    <div class="c-calender-date">{{ between_dates( $fdata->lucky_data ) }}</div>
                                </div>
                            </div>
							<h3 class="ctxt-title">
								<a href="{{ $fdata->lucky_data->detailUrl() }}">{{ $fdata->lucky_data->name }}</a>
							</h3>

                            <div class="ctxt-wrapper-right mobile ctx-receipt">
                                <a class="ctxtr-label" href="{{ route( 'f.heng.lucky.receipts', $fdata->lucky_data->id ) }}">
                                    @php $receipt_count = $fdata->total_receipt @endphp
                                    @if( $receipt_count < 1 )
                                    No receipt has been submitted
                                    @else
                                    You have participated {{ $receipt_count }} {{ str_plural( 'time', $receipt_count )}}
                                    @endif
                                </a>
                            </div>

							<div class="ce-status mobile">
							@if( $fdata->lucky_data->status == 'winner' )
                                <a href="{{ $fdata->lucky_data->winnerUrl() }}" class="ces-info yellow">
                                    See Winner list
                                </a>
							@else
								<div class="ces-info blue">Status : Awaiting Winner</div>
							@endif
							</div>
						</div>
						<div class="ce-status">
                            @if( $fdata->lucky_data->status == 'winner' )
                                <a href="{{ $fdata->lucky_data->winnerUrl() }}" class="ces-info yellow">
                                    See Winner list
                                </a>
                            @else
                                <div class="ces-info blue">Status : Awaiting Winner</div>
                            @endif
						</div>

					</li>
				   @endforeach
                @endif
			</ul>

		</div>


		<div class="ct-pagination">
			{{ $db_member_contests->links() }}
		</div>

	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop

@section( 'foot_page_js' )
    @parent
    {!! html_js( 'js/vendors/select2/dist/js/select2.full.min.js' ) !!}
    {!! html_js( 'js/vendors/gasparesganga-jquery-loading-overlay/dist/loadingoverlay.min.js' ) !!}
@stop

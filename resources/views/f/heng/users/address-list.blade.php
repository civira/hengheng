@extends('f.heng.base')

@section( 'head_page_css' )
    @parent
    {!! html_css( 'heng/user-address-list.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<div class="st-container with-margin">

    <section id="content_wrapper">

        @if( html_message_is_valid() )
            <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class( 'txt-error', 'txt-valid' ) }}">
                {{ html_get_first_message() }}
            </div>
        @endif

        <div class="db-head">
            <a href="{{ route( 'f.heng.users.address_edit' ) }}" class="button">
                + Add Address
            </a>
            <a href="{{ route( 'f.heng.users.my_account' ) }}" class="button red">
                Back
            </a>
        </div>

        <div class="db-content">
            @if( $db_address->isEmpty() )
                <div class="ct-no-data slim-bottom no-border-top">
                    <div class="texts">{{ __('messages.data.no_address') }}</div>
                </div>
            @else

            <ul class="list">
                @foreach( $db_address as $r )
                <li class="item">
                    <div class="name bold">{{ $r->address_label }}</div>
                    <div class="name">
                        <p>
                            {{ $r->address }}<br/><br/>
                            Unit No: {{ $r->unit_no }}<br/>
                            Postal Code: {{ $r->postal_code}}
                        </p>
                    </div>
                    <div class="name">
                        @if( $r->isDefaultAddress() )
                            <span class="ct-txt-link orange no-padding-left nohoverunderline">Current Address</span>|
                            <a href="{{ route( 'f.heng.users.address_edit', $r->id ) }}" class="ct-txt-link blue">Edit</a>
                        @else
                            <a href="{{ route( 'f.heng.users.address_default_store', $r->id ) }}" class="ct-txt-link blue no-padding-left">Set Default</a>|
                            <a href="{{ route( 'f.heng.users.address_edit', $r->id ) }}" class="ct-txt-link blue">Edit</a>|
                            <a href="{{ route( 'f.heng.users.address_delete', $r->id ) }}" class="ct-txt-link red btn_cancel">Delete</a>
                        @endif
                    </div>
                </li>
                @endforeach
            </ul>
            @endif
        </div>

    </section>

</div>

@stop

@section( 'footer_content' )
    @include( 'f.heng.layouts._footer_min' )
@stop

@section( 'foot_page_js' )
    @parent
    <!-- Include jQuery - see http://jquery.com -->
    <script type="text/javascript">
        $(function()
        {

            $( ".btn_cancel" ).on( "click", function( e )
            {
                var $checkboxes = $( ".css-checkbox.nodel" );
                for( var i = 0, len = $checkboxes.length; i < len; i++ )
                {
                    let $checkbox = $checkboxes.eq( i );
                    if( $checkbox.is( ":checked" ) )
                    {
                        alert( "Participation for lucky draw with a receipt can not be cancelled");
                        e.preventDefault();
                        return false;
                    }
                }

                return confirm('Are you sure you want to delete address?');
            });

        });
    </script>
@stop

@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/users.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section id="user_content" class="st-container" >

	@include( 'f.heng.users.partials._navigation' )

	<section class="ct-panel over-width">

		<div>

            <div class="ct-page-header margined">
                <h1>
                    Change Password
                </h1>
                <span class="cph-bar"></span>
            </div>

			<p class="user-common-info">
				You may update your password any time. <br/>
				All new passwords must contain at least 6 characters.
				We also suggest having at least one capital and one lower-case letter (Aa-Zz).
			</p>

    	{!! form_open_post( route( 'f.heng.users.store_password' ) ) !!}

			<div class="ct-form medium">
				<div class="ctf-row ct-error-info {{ html_add_error_class() }}">
			    	{{ html_get_first_error_message() }}
				</div>
				@if( $log_member->password )
				<div class="ctf-row">
					<label for="ctf-current-password">
						Current Password
						<span class="cr-asterisk">*</span>
					</label>
					<input type="password" name="current_password" id="ctf-current-password" class="cr-input" value="{{ old( 'current_password' ) }}">
				</div>
				@endif
				<div class="ctf-row">
					<label for="ctf-password">
						New Password
						<span class="cr-asterisk">*</span>
					</label>
					<input type="password" name="password" id="ctf-password" class="cr-input" value="{{ old( 'password' ) }}">
				</div>
				<div class="ctf-row">
					<label for="ctf-conf-password">
						Confirm Password
						<span class="cr-asterisk">*</span>
					</label>
					<input type="password" name="password_confirmation" id="ctf-conf-password" class="cr-input" value="{{ old( 'password_confirmation' ) }}">
				</div>
				<input type="submit" name="send" value="Change Password" class="cr-button f-right">
			</div>

	    {!! form_close() !!}


		</div>
	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


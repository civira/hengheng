@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/users.css' ) !!}
@stop

@section( 'page_content' )

<section id="user_content" class="st-container" >

	<section class="ct-panel">

		@include( 'f.heng.users.partials._navigation' )

		<div id="page_contest_history" class="ct-panel-contents">

			<div class="ct-filter-simple">
				<div class="cs-row">3 Results</div>
				<div class="cs-row">
					<select name="filter_page">
						<option value="">Sort by: All Contest</option>
						<option value="">Sort by: Most popular</option>
					</select>
				</div>
			</div>

			<ul class="ct-expand-list">
				<li>
					<div class="ce-image">
						{!! html_img_site( 'contests/iphone.jpg', 
						[ 
							'w' => 320, 
							'h' => 210,
						]) !!}
						<span class="ce-sub-title">CONTEST CATEGORY</span>
					</div>
					<div class="ce-context">
						<h4 class="ctxt-brand">
							<a href="#">ERAFONE CELL</a>
						</h4>
						<h3 class="ctxt-title">
							<a href="#">Win A Iphone X 256GB!</a>
						</h3>
						<div class="ctxt-icons">
							<span class="ico-misc calender"></span>
							<div class="c-calender-date">23 may - 15 July</div>
						</div>
						<div class="ctxt-description">
							Contest Submitted on : 25 May 2018
						</div>
						<div class="ce-status mobile">
							<div class="ces-info">Status : Subscribed</div>
						</div>	
					</div>	
					<div class="ce-status">
						<div class="ces-info">Status : Subscribed</div>
					</div>	
				</li>
				<li>
					<div class="ce-image">
						{!! html_img_site( 'contests/lock.jpg', 
						[ 
							'w' => 320, 
							'h' => 210,
						]) !!}
						<span class="ce-sub-title">CONTEST CATEGORY</span>
					</div>
					<div class="ce-context">
						<h4 class="ctxt-brand">
							<a href="#">Lock & Lock</a>
						</h4>
						<h3 class="ctxt-title">
							<a href="#">Win Lock & Lock Oven Glass Gift Set!</a>
						</h3>
						<div class="ctxt-icons">
							<span class="ico-misc calender"></span>
							<div class="c-calender-date">23 may - 15 July</div>
						</div>
						<div class="ctxt-description">
							Contest Submitted on : 25 May 2018
						</div>
						<div class="ce-status mobile">
							<div class="ces-info">Status : Subscribed</div>
						</div>	
					</div>	
					<div class="ce-status">
						<div class="ces-info blue">Status : Awating Winner</div>
					</div>	
				</li>
				<li>
					<div class="ce-image">
						{!! html_img_site( 'contests/carefour.jpg', 
						[ 
							'w' => 320, 
							'h' => 210,
						]) !!}
						<span class="ce-sub-title">CONTEST CATEGORY</span>
					</div>
					<div class="ce-context">
						<h4 class="ctxt-brand">
							<a href="#">Carefour Departement Store</a>
						</h4>
						<h3 class="ctxt-title">
							<a href="#">Carefour Voucher $500 for each Products!</a>
						</h3>
						<div class="ctxt-icons">
							<span class="ico-misc calender"></span>
							<div class="c-calender-date">23 may - 15 July</div>
						</div>
						<div class="ctxt-description">
							Contest Submitted on : 25 May 2018
						</div>
						<div class="ce-status mobile">
							<div class="ces-info">Status : Subscribed</div>
						</div>	
					</div>	
					<div class="ce-status">
						<div class="ces-info yellow">Status : Winner Selected</div>
						<div class="ces-thropy">
							{!! html_img_site( 'winner-thropy.jpg', 
							[ 
								'class' => '',
								'w' => 100, 
								'h' => 102,
							]) !!}
						</div>
						<div class="ces-winner">
							<div class="cw-title">Winner :</div> 
							<div class="cw-name">Jane Joelyn</div>
						</div>
					</div>	
				</li>
			</ul>

		</div>
	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


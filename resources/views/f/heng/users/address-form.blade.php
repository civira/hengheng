@extends('f.heng.base')

@section( 'head_page_css' )
    @parent
    {!! html_css( 'heng/user-address-list.css' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<div class="st-container with-margin">

    <section id="content_wrapper">

        @if( html_get_first_error_message() )
            <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class() }}">
                {{ html_get_first_error_message() }}
            </div>
        @endif

        <div class="db-form">
            <div class="ct-page-header margined">
                <div class="cph-left">
                    <h1>
                        Update Address
                    </h1>
                    <span class="cph-bar"></span>
                </div>
                <div class="cph-right">
                    <a href="{{ route( 'f.heng.users.address_page' ) }}" class="ct-button small righted red">
                        Back
                    </a>
                </div>
            </div>

            {!! form_open_post( route( 'f.heng.users.address_store', $val_id ) ) !!}
                <div class="ct-form medium">

                    <div class="ctf-row">
                        <label for="ctf-address">
                            Address Label
                            <span class="cr-asterisk">*</span>
                        </label>
                        <div class="cr-multi-info">
                            <input type="text" name="address_label" id="ctf-unit" class="cr-input" value="{{ form_set_value( 'address_label', $val_type ) }}" placeholder="Fill address type">
                            <div class="information">House, Office, Apartement, etc.</div>
                        </div>
                    </div>

                    <div class="ctf-row">
                        <label for="ctf-address">
                            Address
                            <span class="cr-asterisk">*</span>
                        </label>
                        <div class="cr-multi-info">
                            <textarea name="address" class="cr-input textarea" placeholder="Block & Street Address">{{ form_set_value( 'address', $val_address ) }}</textarea>
                        </div>
                    </div>
                    <div class="ctf-row">
                        <label for="ctf-unit">
                            Unit No
                            <span class="cr-asterisk">*</span>
                        </label>
                        <div class="cr-multi-info">
                            <input type="text" name="unit_no" id="ctf-unit" class="cr-input" value="{{ form_set_value( 'unit_no', $val_unit ) }}" placeholder="Fill your unit no">
                        </div>
                    </div>

                    <div class="ctf-row">
                        <label for="ctf-postal">
                            Postal Code
                            <span class="cr-asterisk">*</span>
                        </label>
                        <div class="cr-multi-info">
                            <input type="text" maxlength="6" name="postal_code" id="ctf-postal" class="prevent_chars cr-input" value="{{ form_set_value( 'postal_code', $val_postal ) }}" placeholder="Key in postal code">
                        </div>
                    </div>

                    <input type="submit" name="save" value="Save" class="cr-button f-right">
                </div>
            {!! form_close() !!}

        </div>

    </section>

</div>

@stop


@section( 'footer_content' )
    @include( 'f.heng.layouts._footer_min' )
@stop


<ul class="ct-page-category">
    <li class="item @if( $self_active_summary == 'lucky-draws' ) active @endif">
        <a href="{{ route( 'f.heng.users.contest_summary' ) }}">Lucky Draws</a>
    </li>
    <li class="item @if( $self_active_summary == 'redemptions' ) active @endif">
        <a href="{{ route( 'f.heng.users.redemptions_summary') }}">Redemptions</a>
    </li>
    <li class="item @if( $self_active_summary == 'promo-codes' ) active @endif">
        <a href="{{ route( 'f.heng.users.promo_codes_summary') }}">Promo Codes</a>
    </li>
    <li class="item @if( $self_active_summary == 'free-samples' ) active @endif">
        <a href="{{ route( 'f.heng.users.free_samples_summary') }}">Free Samples</a>
    </li>
</ul>

@section( 'head_page_css' )
	@parent

    {!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
    {!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@if( !$st_announcements->isEmpty() )
<div class="ct-announcement">
	<div class="cta-megaphone">
		{!! html_img_site( 'megaphones.png',
		[
			'w' => 44,
			'h' => 44,
		]) !!}
	</div>
	<div class="owl-carousel" data-items="1" data-nav="true	" data-dots="false" data-margin="15" data-loop="true" data-autoplay="true">
		@foreach( $st_announcements as $announcement )
		<div class="ct-owl-overlay">
			{{ $announcement->description }}
		</div>
		@endforeach
	</div>
</div>
@endif

<div class="ct-navigation-menu">

	<div class="title">
		Dashboard
	</div>

	<ul class="list-item">
		<li class="with-child">
			<a href="{{ route( 'f.heng.users.dashboard' ) }}" 
				class="@if( $self_active_menu == 'dashboard' ) active-menu @endif title-parent"  
			>
				<span class="ico-user-navigation cart"></span>
				<span class="title">Cart</span>
			</a>
			<ul class="list-item-child">
				<li>
					<a href="{{ route('f.heng.users.dashboard') }}" class="@if( $self_active_child == 'lucky-draws' ) active-item-child @endif">Lucky Draws</a>
				</li>
				<li>
					<a href="{{ route('f.heng.users.redemption_list') }}" class="@if( $self_active_child == 'redemptions' ) active-item-child @endif">Redemptions</a>
				</li>
				<li>
					<a href="{{ route('f.heng.users.promo_code_list') }}" class="@if( $self_active_child == 'promo-codes' ) active-item-child @endif">Promo Codes</a>
				</li>
			</ul>
		</li>
		<li class="with-child">
			<a href="{{ route( 'f.heng.users.contest_summary' ) }}"
				class="@if( $self_active_menu == 'contest-summary' ) active-menu @endif title-parent" 
			>
				<span class="ico-user-navigation list"></span>
				<span class="title">Summary</span>
			</a>
			<ul class="list-item-child">
				<li>
					<a href="{{ route( 'f.heng.users.contest_summary' ) }}" class="@if( $self_active_child == 'summary-lucky' ) active-item-child @endif">Lucky Draws</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.users.redemptions_summary') }}" class="@if( $self_active_child == 'summary-redemptions' ) active-item-child @endif">Redemptions</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.users.promo_codes_summary') }}" class="@if( $self_active_child == 'summary-promo' ) active-item-child @endif">Promo Codes</a>
				</li>
				<li>
					<a href="{{ route( 'f.heng.users.free_samples_summary') }}" class="@if( $self_active_child == 'summary-free' ) active-item-child @endif">Free Samples</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="{{ route( 'f.heng.users.my_account' ) }}"
				class="@if( $self_active_menu == 'my-account' ) active-menu @endif title-parent" 
			>
				<span class="ico-user-navigation profile"></span>
				<span class="title">Profile</span>

			</a>
		</li>
		<li>
			<a href="{{ route( 'f.heng.users.address_page' ) }}"
				class="@if( $self_active_menu == 'my-address' ) active-menu @endif title-parent" 
			>
				<span class="ico-user-navigation location"></span>
				<span class="title">Address</span>
			
			</a>
		</li>
		<li>
			<a href="{{ route( 'f.heng.users.change_password' ) }}"
				class="@if( $self_active_menu == 'change-password' ) active-menu @endif title-parent" 
			>
				<span class="ico-user-navigation lock"></span>
				<span class="title">Change Password</span>
			</a>
		</li>
	</ul>

</div>


<div class="ct-panel-mobile-nav">
	<div class="cpm-tabs">
		<div class="cpm-lines rows">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<div class="rows">
			Menu
		</div>
	</div>
	<ul class="cpm-routes">
		<li @if( $self_active_menu == 'dashboard' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.dashboard' ) }}">
				Contest Cart
			</a>
		</li>
		<li @if( $self_active_menu == 'contest-summary' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.contest_summary' ) }}">
				Contest Summary
			</a>
		</li>
		<li @if( $self_active_menu == 'my-account' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.my_account' ) }}">
				Profile
			</a>
		</li>
		<li @if( $self_active_menu == 'my-address' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.address_page' ) }}">
				Address
			</a>
		</li>
		<li @if( $self_active_menu == 'change-password' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.change_password' ) }}">
				Change Password
			</a>
		</li>
	</ul>
</div>
@section( 'foot_page_js' )
	@parent
    {!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
    {!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}


@stop

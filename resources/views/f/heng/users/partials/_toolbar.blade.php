<a id="btn_upload" href="{{ $self_url_upload  }}" class="ct-button righted navy">
    <div class="ico-user-toolbar upload"></div>
    <div class="title">
        Upload Receipt
    </div>
</a>

<button id="btn_cancel" class="ct-button righted red confirm-cancel">
    <div class="ico-user-toolbar delete"></div>
    <span class="title">Cancel Participation</span>
</button>
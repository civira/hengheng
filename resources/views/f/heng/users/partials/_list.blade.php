<ul class="ct-page-category">
    <li class="item @if( $self_active_child == 'lucky-draws' ) active @endif">
        <a href="{{ route('f.heng.users.dashboard') }}">Lucky Draws</a>
    </li>
    <li class="item @if( $self_active_child == 'redemptions' ) active @endif">
        <a href="{{ route('f.heng.users.redemption_list') }}">Redemptions</a>
    </li>
    <li class="item @if( $self_active_child == 'promo-codes' ) active @endif">
        <a href="{{ route('f.heng.users.promo_code_list') }}">Promo Codes</a>
    </li>
</ul>


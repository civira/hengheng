
@section( 'head_page_css' )
	@parent

    {!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
    {!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@if( !$st_announcements->isEmpty() )
<div class="ct-announcement">
	<div class="cta-megaphone">
		{!! html_img_site( 'megaphones.png',
		[
			'w' => 44,
			'h' => 44,
		]) !!}
	</div>
	<div class="owl-carousel" data-items="1" data-nav="true	" data-dots="false" data-margin="15" data-loop="true" data-autoplay="true">
		@foreach( $st_announcements as $announcement )
		<div class="ct-owl-overlay">
			{{ $announcement->description }}
		</div>
		@endforeach
	</div>
</div>
@endif

<ul class="ct-panel-nav">
	<li @if( !empty( $self_active_menu ) && $self_active_menu == 'dashboard' ) class="active-menu" @endif>
		<a href="{{ route( 'f.heng.users.dashboard' ) }}">
			Cart
		</a>
	</li>
	<li @if( !empty( $self_active_menu ) && $self_active_menu == 'contest-summary' ) class="active-menu" @endif>
		<a href="{{ route( 'f.heng.users.contest_summary' ) }}">
			Summary
		</a>
	</li>
	<li @if( !empty( $self_active_menu ) && $self_active_menu == 'my-account' ) class="active-menu" @endif>
		<a href="{{ route( 'f.heng.users.my_account' ) }}">
			Profile
		</a>
	</li>
	<li @if( !empty( $self_active_menu ) && $self_active_menu == 'my-address' ) class="active-menu" @endif>
		<a href="{{ route( 'f.heng.users.address_page' ) }}">
			Address
		</a>
	</li>
	<li @if( !empty( $self_active_menu ) && $self_active_menu == 'change-password' ) class="active-menu" @endif>
		<a href="{{ route( 'f.heng.users.change_password' ) }}">
			Change Password
		</a>
	</li>
</ul>

<div class="ct-panel-mobile-nav">
	<div class="cpm-tabs">
		<div class="cpm-lines rows">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<div class="rows">
			Menu
		</div>
	</div>
	<ul class="cpm-routes">
		<li @if( !empty( $self_active_menu ) && $self_active_menu == 'dashboard' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.dashboard' ) }}">
				Contest Cart
			</a>
		</li>
		<li @if( !empty( $self_active_menu ) && $self_active_menu == 'contest-summary' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.contest_summary' ) }}">
				Contest Summary
			</a>
		</li>
		<li @if( !empty( $self_active_menu ) && $self_active_menu == 'my-account' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.my_account' ) }}">
				Profile
			</a>
		</li>
		<li @if( !empty( $self_active_menu ) && $self_active_menu == 'my-address' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.address_page' ) }}">
				Address
			</a>
		</li>
		<li @if( !empty( $self_active_menu ) && $self_active_menu == 'change-password' ) class="active-menu" @endif>
			<a href="{{ route( 'f.heng.users.change_password' ) }}">
				Change Password
			</a>
		</li>
	</ul>
</div>
@section( 'foot_page_js' )
	@parent
    {!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
    {!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}


@stop

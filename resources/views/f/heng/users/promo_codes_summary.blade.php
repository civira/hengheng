@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/users.css' ) !!}
	{!! html_css( 'heng/contest-history.css' ) !!}
    {!! html_css( 'select2/dist/css/select2.min.css', 'js/vendors' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section id="user_content" class="st-container" >

    @include( 'f.heng.users.partials._navigation' )

	<section class="ct-panel over-width">

		<div>
            <div id="ct_user_childmenu_onmobile">
                @include( 'f.heng.users.partials._summary_list' )
            </div>

			<ul class="ct-expand-list">

                <div class="ct-header">
                    <div class="ct-page-header margined">
                        <h1>
                            Promo Code Summary
                        </h1>
                        <span class="cph-bar"></span>
                    </div>
                    <!-- filter data -->
                    <div class="ct-filter-simple">
                        {!! form_open('',
                            [
                                'id' => 'ct_filter_form'
                            ])
                        !!}
                        <div id="controls" class="cs-row ss2-parent-wrapper">
                            <select name="filter_status" id="control_sort"
                                    class="control-retailer site-select2"
                                    data-placeholder="Select Category"
                                    data-class="ss2-wrapper"
                                    data-open-class="ss2-open-wrapper"
                                    data-no-search="true"
                                >
                                <option value="all">Select Status</option>

                                @foreach( $filter_option as $key=>$fo )
                                    <option value="{{$key}}" {{ $filter_status == $key? 'selected="selected"' : '' }}>{{ $fo }}</option>
                                @endforeach
                                
                            </select>
                        </div>
                        {!! form_close() !!}
                    </div>
                </div>

            @if( $db_list->isEmpty() )
            <div class="ct-no-data slim-bottom">
                <div class="texts">{{ __('messages.data.no_history') }}</div>
            </div>
            @else

                @foreach( $data_group as $key_date=>$group_data )

                    <div class="ct-listed-date">
                        <div class="date">
                            <span class="minor">Participated on</span>
                            <span class="major">{{ date_formatted( $key_date ) }}</span>
                        </div>
                    </div>
                	@foreach( $group_data as $key=>$fdata )
    					<li class="ctx-compact last">
    						<a href="{{ $fdata['data_information']->detailUrl() }}" class="ce-image">
    							<img src="{{ $fdata['info_image'] }}" alt="{{ $fdata['data_information']->name }}" title="{{ $fdata['data_information']->name }}" width="200" height="128">
    						</a>
    						<div class="ce-context">
                                <div class="ctxt-metadata">
                                    <div class="ctxt-icons">
                                        <div class="c-calender-date">{{ between_dates( $fdata['data_information'] ) }}</div>
                                    </div>
                                </div>
    							<h3 class="ctxt-title">
    								<a href="{{ $fdata['data_information']->detailUrl() }}">{{ $fdata['data_information']->name }}</a>
    							</h3>

                                <div class="ctxt-wrapper-right mobile ctx-receipt">
                                    <a class="ctxtr-label" href="{{ $fdata['promo_data']->receiptListUrl() }}">
                                        You have redeemed {{ $fdata['total_participation'] }} {{ str_plural( 'time', $fdata['total_participation'] )}}
                                    </a>
                                </div>

    							<div class="ce-status mobile">
                                    @if( $fdata['promo_data']->status == 'activate' )
                                        <div class="ces-info yellow">Activate Promo Code - check email for details</div>
                                    @elseif( $fdata['promo_data']->status == 'rejected' )
                                        <div class="ces-info black">Rejected</div>
                                    @else
                                        <div class="ces-info blue">Awaiting Verification</div>
                                    @endif
    							</div>
    						</div>
    						<div class="ce-status">
                                @if( $fdata['promo_data']->status == 'activate' )
                                    <div class="ces-info yellow">Activate Promo Code - check email for details</div>
                                @elseif( $fdata['promo_data']->status == 'rejected' )
                                    <div class="ces-info black">Rejected</div>
                                @else
                                    <div class="ces-info blue">Awaiting Verification</div>
                                @endif
    						</div>

    					</li>
    				   @endforeach

                   @endforeach

                @endif
			</ul>

		</div>


		<div class="ct-pagination">
			{{ $db_list->links() }}
		</div>

	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop

@section( 'foot_page_js' )
    @parent
    {!! html_js( 'js/vendors/select2/dist/js/select2.full.min.js' ) !!}
    {!! html_js( 'js/vendors/gasparesganga-jquery-loading-overlay/dist/loadingoverlay.min.js' ) !!}
@stop

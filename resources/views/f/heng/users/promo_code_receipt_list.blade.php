@extends('f.heng.base')

@section( 'head_page_css' )
    @parent
    {!! html_css( 'heng/lucky-receipt-list.css' ) !!}
@stop


@section( 'page_content' )

<div id="ct_template_container" class="st-container" >


    <section id="content_wrapper">

        <div id="page_headings" class="db-detail">

            <h1 class="head-title">
                @php $count = $receipts->count() @endphp
                @if( $count < 1 )
                No receipt has been submitted
                @else
                <div>You have submitted {{ $count }} {{ str_plural( 'receipt', $count ) }}:</div>
                @endif
            </h1>

            <div class="img">
                <a href="{{ $db->detailUrl() }}">
                    <img src="{{ $db->file_img() }}" width="210" height="135" alt="{{ $db->name }}" title="{{ $db->name }}">
                </a>
            </div>

            <div class="content">
                <div class="metadata">
                    <span class="sep">.</span>
                    <div class="icons">
                        <div class="c-calender-date">{{ between_dates( $db ) }}</div>
                    </div>
                </div>
                <div class="title">
                    <a href="{{ $db->detailUrl() }}">
                        {{ $db->name }}
                    </a>
                </div>

                 <div class="status">
                @if( $db->isDateExpired() )
                    <div class="info yellow">
                        Redemption has Ended
                    </div>
                @endif
            </div>


            </div>
        </div>

        <div class="db-content">

            @if( $receipts->isEmpty() )
            <div class="ct-no-data">
                <div class="texts">No receipt has been submitted for this lucky draw.</div>
            </div>
            @else
            <ul class="list">
                @foreach( $receipts as $r )
                <li class="item">
                    <div class="date">Submitted at {{ Date( 'jS M Y H:i:s', strtotime( $r->created_at ) ) }}</div>
                    @foreach( $r->photos as $i=>$p )
                    <div class="name">
                        <a href="{{ $p->original() }}" class="txt" target="_blank">{{ $r->file_rename( true, $i+1 ) }}</a>
                    </div>
                    @endforeach
                </li>
                @endforeach
            </ul>
            @endif

        </div>

    </section>

</div>

@stop


@section( 'footer_content' )
    @include( 'f.heng.layouts._footer_min' )
@stop


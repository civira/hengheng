@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/users.css' ) !!}
    {!! html_css( 'anypicker/dist/anypicker-all.min.css', 'js/vendors' ) !!}
@stop

@section( 'low_padding_body', 'st-lower' )

@section( 'page_content' )

<section id="user_content" class="st-container" >

	@include( 'f.heng.users.partials._navigation' )

	<section class="ct-panel over-width">
        @if( html_get_first_error_message() )
            <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class() }}">
                {{ html_get_first_error_message() }}
            </div>
        @endif
        
		<div>

            <div class="ct-page-header margined">
                <h1>
                    Update Profile
                </h1>
                <span class="cph-bar"></span>
            </div>

			<div id="my_account_box">
				{!! form_open_post( route( 'f.heng.users.photo_upload' ), [ 'id'=>'form_photo' ], true ) !!}
					<div class="mc-upload-photo">
						<input type="file" id="photo_upload" class="ct-upload-button" name="photo_upload" accept="image/*"/>
						<label for="photo_upload">
						@if( !$authed_user->profile_img() )
							{!! html_img_site( 'upload-photo.png',
							[
								'w' => 200,
								'h' => 200,
							]) !!}
						@else
							<img src="{{ $authed_user->profile_img() }}" alt="{{ $authed_user->full_name }}" title="{{ $authed_user->full_name }}" width="200" height="200" class="ct-image-rounded">
						@endif
						</label>
						<div class="upload-title">
							Upload Profile Picture
						</div>
 					</div>
				{!! form_close() !!}

    			{!! form_open_post( route( 'f.heng.users.my_account' ) ) !!}
					@php $error_msg = html_get_first_error_message() @endphp
					<div class="ct-form medium">

						<div class="ctf-row">
							<label for="ctf-name">
								Name
								<span class="cr-asterisk">*</span>
							</label>
							<div class="cr-multi-info">
								<input type="text" name="name" id="ctf-name" class="cr-input" value="{{ form_set_value( 'name', $authed_user->full_name ) }}" placeholder="Name as per NRIC/FIN">
								<div class="information">(For verification when you win)</div>
							</div>
						</div>
						<div class="ctf-row">
							<label for="ctf-email">Email</label>
							<div class="cr-multi-info">
								<input type="text" name="email" id="ctf-email" class="cr-input" value="{{ form_set_value( 'email', $authed_user->email ) }}" disabled="disabled" placeholder="Fill your email">
								@if( $authed_user->is_verified != 1)
									<a href="{{ route( 'f.heng.users.resend_verification' ) }}" class="information redirect">Resend verification email.</a>
								@endif
							</div>
						</div>
						<div class="ctf-row">
							<label for="ctf-mobile-number">
								Mobile Number
								<span class="cr-asterisk">*</span>
							</label>
							<div class="cr-multi-info">
								<input type="text" maxlength="8" name="mobile_number" id="ctf-mobile-number" class="prevent_chars cr-input no-spin" value="{{ form_set_value( 'mobile_number', $authed_user->phone_number ) }}" placeholder="8-digit Mobile No.">
								<div class="information">(Fill in Correct Mobile No to contact you when you win)</div>
							</div>
						</div>
                        <div class="ctf-row">
                            <label for="ctf-id-number">
                                Gender
                                <span class="cr-asterisk">*</span>
                            </label>
                            <div class="cr-multi-info">
                            	<div class="cr-radio top">
									<input type="radio" name="gender" value="male" id="male" class="bullet" 
									@if(old('gender') == 'male' ) checked @endif
									@if( $authed_user->gender == 'male' ) checked @endif
									>
									<label for="male">Male</label>
								</div>
								<div  class="cr-radio" >
									<input type="radio" name="gender" value="female" id="female" class="bullet"
									@if(old('gender') == 'female' ) checked @endif
									@if( $authed_user->gender == 'female' ) checked @endif
									>
									<label for="female">Female</label>
								</div>
                            </div>
                        </div>
                        <div class="ctf-row">
                            <label for="ctf-id-number">
                                Date of Birth
                                <span class="cr-asterisk">*</span>
                            </label>
                            <div class="cr-multi-info">
                                <input type="text" readonly="readonly" name="date_of_birth" id="ctf_dob" class="cr-input" value="{{ form_set_value( 'date_of_birth', $user->dob ) }}" placeholder="Fill your date of birth">
								<div class="information">(For verification when you win)</div>
                            </div>
                        </div>
						<div class="ctf-row">
							<label for="ctf-postal">
								Postal Code
								<span class="cr-asterisk">*</span>
							</label>
							<div class="cr-multi-info">
								<input type="text" maxlength="6" name="postal_code" id="ctf-postal" class="prevent_chars cr-input" value="{{ form_set_value( 'postal_code', $authed_user->postal_code ) }}" placeholder="Key in postal code">
							</div>
						</div>

						<div class="ctf-row">
							<label for="ctf-address">
								Address
								<span class="cr-asterisk">*</span>
							</label>
							<div class="cr-multi-info">
								<textarea name="address" class="cr-input textarea" placeholder="Block & Street Address">{{ form_set_value( 'address', $authed_user->address ) }}</textarea>
							</div>
						</div>
						<div class="ctf-row">
							<label for="ctf-unit">
								Unit No
								<span class="cr-asterisk">*</span>
							</label>
							<div class="cr-multi-info">
								<input type="text" name="unit_no" id="ctf-unit" class="cr-input" value="{{ form_set_value( 'unit_no', $authed_user->unit_no ) }}" placeholder="Fill your unit no">
							</div>
						</div>
                        <div class="ctf-row-checkbox">
							<input type="checkbox" name="promo_email" id="promo_email" class="css-checkbox" value="1" 
							@if( old('promo_email') == 1 || $authed_user->promo_email == 1 && empty( $error_msg) )checked="checked"@endif />
							<label for="promo_email" class="css-label">
		                        Yes! I would like to receive updates about lucky draws and promotions from Shop&Win via email.
							</label>
                        </div>
						<input type="submit" name="save" value="Save" class="cr-button f-right">
					</div>
   				{!! form_close() !!}

			</div>

		</div>
	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop

@section( 'foot_page_js' )
	@parent
    {!! html_js( 'js/vendors/anypicker/dist/anypicker.min.js' ) !!}
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}
	<script>
		$(function()
		{
			$( "#photo_upload" ).on( "change", function()
			{
				var file = this.files[0];
				var fileType = file["type"];
				var ValidImageTypes = [ "image/jpeg", "image/png"];
				if ($.inArray(fileType, ValidImageTypes) < 0) {
     				alert( "Uploaded photo must be only in jpeg or png format." );
				}
				else
				{
					$(this).parents( "form" ).first().submit();
				}
			});

            var $dates = $( "#ctf_dob" );
            for( var i = 0, len = $dates.length; i < len; i++ )
            {
                var $date = $dates.eq( i ),

                val = $date.val();

                $date.AnyPicker(
                {
                    dateTimeFormat      : "dd MMMM YYYY",
                    selectedDate        : val,
					minValue 			: new Date(1920, 01, 19),
                    animationDuration   : 300,
                    maxValue            : new Date()
                });	
            }
		});
	</script>
@stop

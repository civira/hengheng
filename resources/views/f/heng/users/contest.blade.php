@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/users.css' ) !!}
@stop

@section( 'page_content' )

<section id="user_content" class="st-container" >

	<section class="ct-panel">

		@include( 'f.heng.users.partials._navigation' )

		<section id="page_contest_history" class="ct-panel-contents">

		    @if( html_message_is_valid() )
		        <div class="ct-error-info show-panel pad-bottom {{ html_add_error_class( 'txt-error', 'txt-valid' ) }}">
		            {{ html_get_first_message() }}
		        </div>
		    @endif

			<div class="ct-page-header margined">
				<h2>
					{{ $header_title }}
				</h2>
				<span class="cph-bar"></span>
			</div>
			<ul class="ct-expand-list">

                @if( $all_contest->isEmpty() )
                <div class="ct-no-data slim-bottom">
                    <div class="texts">{{ __('messages.data.no_contest') }}</div>
                </div>
                @endif

				@foreach( $all_contest as $curr )
				<li>
					<a href="{{ $curr->lucky_data->detailUrl() ) }}" class="ce-image">
						<img src="{{ $curr->image_lucky }}" width="320" height="210">
					</a>
					<div class="ce-context">
						<h4 class="ctxt-brand">
							<a href="{{ $curr->brand_data->searchUrl() }}">{{ $curr->brand_data->name }}</a>
						</h4>
						<h3 class="ctxt-title">
							<a href="{{ $curr->lucky_data->detailUrl() ) }}">{{ string_dots( $curr->lucky_data->name ) }}</a>
						</h3>
						<div class="ctxt-icons">
							<span class="ico-misc calender"></span>
							<div class="c-calender-date">{{ between_dates( $curr->lucky_data ) }}</div>
						</div>
						@if( !$curr['member_receipt'])
						<a href="{{ route( 'f.heng.users.delete_contest', $curr->id ) }}" class="ctxt-button bg-red">
							Delete Subscribe
						</a>
						@endif
					</div>
				</li>
				@endforeach

			</ul>

			<div class="ct-pagination">
				{{ $all_contest->links() }}
			</div>

		</section>


	</section>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer_min' )
@stop


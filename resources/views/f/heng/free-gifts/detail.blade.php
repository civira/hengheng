
@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/redemption-detail.css' ) !!}

	{!! html_css( 'owl.carousel/dist/assets/owl.carousel.min.css', 'js/vendors' ) !!}
	{!! html_css( 'owl.carousel/dist/assets/owl.theme.default.min.css', 'js/vendors' ) !!}

@stop

@section( 'page_content' )

<section class="st-container with-margin">

	<section class="split-layout">

		<div class="sl-left">

            <!-- information -->
            @include( 'f.heng._partials._section_tab_layout' )

            <!-- retailer contest -->
            @include( 'f.heng._partials._retailer_associates' )
            
            <!-- social network sharing -->
            @include( 'f.heng._partials._share_social_networks' )

		</div>

	</section>

    @include( 'f.heng._partials._section_owl_other_items', 
        [ 'owl_redirect_var'=> 'gift',
          'owl_other_title_name' => 'Other Free Gifts', 
          'owl_overlayed' => false,
          'owl_other_webview'=> true ] )

</section>


@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop


@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/owl.carousel/dist/owl.carousel.min.js' ) !!}
	{!! html_js( 'js/f/mobile-owl-carousel.js' ) !!}

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b73f77d0c8e5f78" async="async"></script>
@stop

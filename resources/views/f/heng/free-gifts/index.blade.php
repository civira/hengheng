@extends('f.heng.base')

@section( 'head_page_css' )
	@parent

	{!! html_css( 'heng/contests.css' ) !!}
	{!! html_css( 'select2/dist/css/select2.min.css', 'js/vendors' ) !!}
@stop

@section( 'page_content' )

<section class="st-container with-margin" >

		<div class="ct-head-with-filter">
			<div class="ct-page-header">
				<h1>
					Free Gift
				</h1>
				<span class="cph-bar"></span>
			</div>

            <div class="ct-filter-simple">
                {!! form_open('',
                    [
                        'id' => 'ct_filter_form'
                    ])
                !!}
                <div class="controles cs-row ss2-parent-wrapper">
                    <select name="filter_brand"
                            class="control-retailer site-select2 control-sort"
                            data-placeholder="Select Brand"
                            data-class="ss2-wrapper"
                            data-open-class="ss2-open-wrapper"
                        >
                        <option value="0">Select Brand</option>
                        @foreach( $db_brands as $brand )
                            <option value="{{ $brand->id }}" @if( $f_brand == $brand->id )selected="selected"@endif >{{ $brand->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="controls cs-row ss2-parent-wrapper">
                    <select name="filter_retailer"
                            class="control-retailer site-select2 control-sort"
                            data-placeholder="Select Retailer"
                            data-class="ss2-wrapper"
                            data-open-class="ss2-open-wrapper"
                        >
                        <option value="0">Select Retailer</option>
                        @foreach( $db_retailers as $retailer )
                            <option value="{{ $retailer->id }}" @if( $f_retailer == $retailer->id )selected="selected"@endif >{{ $retailer->name }}</option>
                        @endforeach
                    </select>
                </div>
                {!! form_close() !!}
            </div>

		</div>
		@if( $db_gifts->isEmpty() )
		<div class="ct-no-data">
			<div class="texts">{{ __('messages.data.no_data') }}</div>
		</div>
		@endif

		<div class="ct-galleries">
			<ul>
				@foreach( $db_gifts as $dc )
				<li>
					<div class="ctg-relative">
						<a href="{{ $dc->urlGift() }}">
							<img src="{{ $dc->file_img() }}" alt="{{ $dc->name }}" width="375" height="245" class="ctw-image">
						</a>
					</div>
					<div class="ctg-headings">
						<h4>{{ $dc['brand_data']->name }}</h4>
						<h3><a href="{{ $dc->urlGift() }}" title="{{ $dc->name }}">{{ string_dots( $dc->name ) }}</a></h3>
						<div class="ctg-info">
							<div class="i-row center">
								<span class="ico-misc calender"></span>
								<span class="ir-txt">{{ between_dates( $dc ) }}</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>

		<div class="ct-pagination">
			{{ $db_gifts->links() }}
		</div>

</section>

@stop

@section( 'footer_content' )
	@include( 'f.heng.layouts._footer' )
@stop

@section( 'foot_page_js' )
	@parent
	{!! html_js( 'js/vendors/select2/dist/js/select2.full.min.js' ) !!}
	{!! html_js( 'js/vendors/gasparesganga-jquery-loading-overlay/dist/loadingoverlay.min.js' ) !!}
@stop

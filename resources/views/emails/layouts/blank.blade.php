<!doctype html>
<html>
<head>
		<meta name="viewport" content="width=device-width" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>
			{{ isset( $email_subject )? $email_subject : '' }} @yield( 'meta_title' )
		</title>

    	@yield( 'head_meta' )
		@yield( 'head_link' )
		    
	    @section( 'head_css' )
	        @yield( 'head_vendor_css' )
	        @section( 'head_page_css' )
				{!! html_embed_css( 'partials/embed/emails/reset.css' ) !!}
	        @show
	    @show

	    @section( 'head_js' )
	        @yield( 'head_vendor_js' )
	        @yield( 'head_page_js' )
	    @show

</head>
<body>
	<table border="0" cellpadding="0" cellspacing="0" class="body">
		<tr>
			<td class="container">
				<div class="content">
					@yield( 'blank_content' )
				</div>
			</td>
		</tr>
	</table>

@section( 'foot_js' )
	@yield( 'foot_vendor_js' )
	@yield( 'foot_page_js' )
@show

</body>
</html>
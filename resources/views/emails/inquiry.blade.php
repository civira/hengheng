<style type="text/css">
	p {
		margin : 0 0 20px;
	}
</style>

<p>
	From: {{ $data->name }} < {{ $data->email }} > <br/><br/>
	Date: {{ date( 'l, M d, Y,  h:i:sa' ) }}
</p>

<p>
	&nbsp;
</p>

<p>
	Content:
</p>

<p>
	{!! nl2br($data->comment) !!}
</p>

<p>
	&nbsp;
</p>

<p style="font-weight:bold;">
	*** This is an automatically sender email, please do not reply *** <br/>
</p>

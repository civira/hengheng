<style type="text/css">
	p {
		margin : 0 0 20px;
	}
</style>

<p>
	Thank you for subscribing to Shop&Win newsletter.
</p>
<p>
	Confirm your email before we send our latest news and story straight to your inbox.<br/>
	<a href="{{ $link }}">{{ $link }}</a>
</p>
<p>
	By subscribing to our newsletter, your agreee to our terms & conditions and privacy policy.
</p>

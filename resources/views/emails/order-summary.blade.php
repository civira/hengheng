@extends( 'f.emails.layouts.blank' )

@section( 'head_page_css' )
	@parent
	{!! html_embed_css( 'partials/embed/emails/order-summary.css' ) !!}
@stop

@section( 'blank_content' )
<p>
	<h2 class="title">Hi, {{ $trs->getRecipientName() }}</h2>
</p>
<p class="desc">
	Thank you for buying with us. 
	Below is a summary of your recent purchase. 
</p>
<p>
	<table id="order_header">
		<tr>
			<td class="label">Order ID:</td>
			<td class="value">{{ $trs->order_id }}</td>
		</tr>
		<tr>
			<td class="label">Purchase Date:</td>
			<td class="value">{{ $trs->getPurchaseDate() }}</td>
		</tr>
		<tr>
			<td class="label">Total Payment:</td>
			<td class="value">{{ $trs->getGrandTotal() }}</td>
		</tr>
	</table>
</p>
@if( $trs->isTransfer() )
<table id="payment_instruction">
	<tr>
		<td>
			<h3 class="title">Payment Instruction</h3>
		</td>
	</tr>
	<tr>
		<td>
			<table id="instruction_body">
				<tr>
					<td class="grandtotal">
						Total Amount: {{ $trs->getGrandTotal() }}
					</td>
				</tr>
				<tr>
					<td class="instruction">
						{!! $trs->payment_option->description !!}
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
@endif
<table id="order_detail_outer">
	<tr>
		<td id="order_detail_header">
			<h3 class="title">Your Order Detail:</h3>
		</td>
	</tr>
	<tr>
		<td id="order_detail_inner">
			<table id="order_details">
				@foreach( $trs->details as $d )
				<tr>
					<td class="order-item-outer">
						<table class="order-item">
							<tr>
								<td class="value name">
									{{ $d->product_name }}
								</td>
							</tr>
							<tr>
								<td class="label code">
									{{ $d->product_code }}
								</td>
							</tr>
							<tr>
								<td class="label price meta">
									<span clas="qty">{{ $d->qty }} Items at</span>
									<span clas="price">{{ $d->getUnitPrice() }}</span>
								</td>
							</tr>
							<tr>
								<td class="label subtotal meta">
									<span class="label">with subtotal of Rp. </span>
									<span class="subtotal">{{ $d->getSubtotal() }}</span>
								</td>
							</tr>
							<tr>
								<td class="label desc meta">
									{{ $d->description }}
								</td>
							</tr>
						</table>
					</td>
				</tr>
				@endforeach
			</table>
			<table id="order_detail_summary">
				<tr>
					<td class="filler"></td>
					<td class="label">Subtotal</td>
					<td class="value">{{ $trs->getSubtotal() }}</td>
				</tr>
				<tr>
					<td class="filler"></td>
					<td class="label">Delivery Fee</td>
					<td class="value">{{ $trs->getShippingFee() }}</td>
				</tr>
				<tr class="last grand">
					<td class="filler"></td>
					<td class="label">Total Payment</td>
					<td class="value">{{ $trs->getGrandTotal() }}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="footnote">
			<p>
				If you have any other inquiries, do not hesitate to contact us through our 
				<a href="{{ route( 'f.home' ) }}" target="_blank">Gallery Batik Tissa Store Website</a>.
			</p>
		</td>
	</tr>
</table>
@stop
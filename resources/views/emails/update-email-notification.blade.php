<style type="text/css">
	p {
		margin : 0 0 20px;
	}
</style>

<p>
	Hello {{ ucwords($fullname) }},
</p>
<p>
	Your email has been changed successfully. 
</p>

<p>
	If you haven't requested for this change, please send us an email at
	customerservice@ramayana.com.
</p>

<p>&nbsp;</p>
<p>
Please beware of any other party that uses Ramayana Lestari Sentosa name to distribute Ramayana voucher or asks for your personal data. <br/>
Ramayana Indonesia never requests your password or personal data through email, personal message or any other media.
</p>

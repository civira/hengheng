<style type="text/css">
	p {
		margin : 0 0 20px;
	}
</style>

<p>
	Hello {{ ucwords($fullname) }},
</p>
<p>
	Please verify your email by tapping this link below. 
	A verified email will make it easier for us to contact you when your phone is unreachable.
</p>
<p>
	Please follow the link below to Update your new Email.<br/>
	<a href="{{ $link }}">{{ $link }}</a>
</p>
<p>&nbsp;</p>
<p>
Please beware of any other party that uses Ramayana Lestari Sentosa name to distribute Ramayana voucher or asks for your personal data. <br/>
Ramayana Indonesia never requests your password or personal data through email, personal message or any other media.
</p>

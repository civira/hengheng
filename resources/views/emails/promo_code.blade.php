<p style="font-size:14px;">
    You have got a new promo code request, for <strong>{{ $db_promo->name }}</strong>.
</p>

<p>
    From: {{ $db_member->full_name }} < {{ $db_member->email }} > <br/>
    Date: {{ date( 'l, M d, Y,  h:i:sa' ) }} <br/><br/>
    Backend Link:
    {{ $url_link }}
</p>

<p>
</p>

<p style="font-weight:bold;">
    *** This is an automatically sender email, please do not reply *** <br/>
</p>

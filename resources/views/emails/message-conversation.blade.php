@extends( 'f.emails.layouts.blank' )

@section( 'blank_content' )
	<p class="head">
		Inquiry received on {{ Date( 'l, Y-m-d H:i:s' ) }}:
	</p>
	<div class="body">
		<div>
			Product : {{ $product->name }}
		</div>
		<div>
			Sender : {{ $get_from->name }}
		</div>
		<div class="focus" style="margin-top:25px;">
			<span class="label">Message : </span>
			<div style="margin-top:10px;">
			{!! $message_last->message !!}
			</div>
		</div>
	</div>
@stop

@section( 'head_page_css' )
	{!! html_embed_css('partials/embed/emails/message-conversations.css') !!}
@stop
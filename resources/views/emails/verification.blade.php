<style type="text/css">
	p {
		margin : 0 0 20px;
	}
</style>

<p>
	Your account with the e-mail address {{ $email }} has been created.
</p>
<p>
	Please follow the link below to activate your account.<br/>
	<a href="{{ $link }}">{{ $link }}</a>
</p>
<p>
	You will be able to change your settings (password, etc.) once your account is activated.
</p>
<p>&nbsp;</p>
<p>
	If you have not requested the creation of a Genymotion account or if you think this is an unauthorized use of your e-mail address, please forward this e-mail to 
	<b>{{ $admin_link }}</b>
	.
</p>

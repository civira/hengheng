@extends( 'f.emails.layouts.blank' )

@section( 'blank_content' )
	<p class="head">
		Inquiry received on {{ Date( 'l, Y-m-d H:i:s' ) }}:
	</p>
	<div class="body">
		<div>
			Name: <b>{{ $inquiry->sender }}</b> 
			<span class="em">({{ $inquiry->email }})</span>
		</div>
		<div>
			Inquiry Type : {{ $inquiry->type }}
		</div>
		<div>
			Phone : {{ $inquiry->phone }}
		</div>
		<div class="focus" style="margin-top:25px;">
			<span class="label">Message : </span>
			<div style="margin-top:10px;">
			{!! $inquiry->content !!}
			</div>
		</div>
	</div>
@stop
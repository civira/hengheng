<style type="text/css">
	p {
		margin : 0 0 20px;
	}
</style>

<p>
	There was a request to change your password.
</p>
<p>
	If you did not make this request, just ignore this email. 
	Otherwise, please follow the link below to change your password.<br/>
	<a href="{{ $link }}">{{ $link }}</a>
</p>
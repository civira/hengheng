<style type="text/css">
	p {
		margin : 0 0 20px;
	}
</style>

<p>
	This is your new verification Email.<br/>
	Your account with the e-mail address {{ $email }} is not verified.
</p>
<p>
	Please follow the link below to verify your account.<br/>
	<a href="{{ $link }}">{{ $link }}</a>
</p>

<p>
	If you have not requested this verification email of a Shop&Win account or if you think this is an unauthorized use of your e-mail address, please forward this e-mail to 
	<b>{{ $admin_link }}</b>
	.
</p>

@extends('f.heng.blank')

@section( 'head_page_css' )
	{!! html_css( 'heng/errors.css' ) !!}
@stop

@section( 'head_page_meta' )
	<title>Shop&Win - Page Not Found</title>
@stop

@section( 'site_content' )
	<div id="st_404">
		<div class="logo">
			<a href="{{ route( 'f.heng.home' ) }}">
				<img src="{{ $st_setting->logoHeader() }}" id="st_logo" width="200" height="200" >
			</a>
		</div>
		<div class="msg">
			Sorry, the page you are looking for could not be found.
		</div>
		<div class="btn">
			<a href="{{ route('f.heng.home') }}">Back to Shopnwin.sg</a>
		</div>
	</div>
@stop
<?php

return [

	'errors' => [
		'auth' => [
			'invalid_credential' => 'Username or password does not match.',
			'invalid_email' 	 => 'Email is not registered. Please sign up with new email.',
			'not_verified' 	 	 => 'Account is not verified. Please check your email.',
			'not_front_allowed'  => 'Account access is restricted.',
			'banned'			 => 'Account is banned. Please contact customer service for more informations.'
		]	
	],

	'data' => [
		'no_participate' => 'You have no uncomplete lucky draw, go ahead and participate in one right now.',
		'no_data' => 'No Items Available.',
        'no_history' => 'No History Available.',
        'no_contest' => 'No Contest Available.',
        'no_address' => 'No Address Available.',


        'user_not_win' => 'Thanks again for participating in our lucky draw. Unfortunately you didn\'t win this time around.',
        'user_win'     => 'You\'ve won the lucky draw in the number :0 position.'
	],

	'lucky_draw' =>
	[
		'subscribed_success' => 'You have participated in a lucky draw, please remember to upload your receipt.',
		'no_agreement' 		 => 'You must agree to the terms & conditions of this contest to participate.'
	],
	'redemption' =>
	[
		'no_agreement' 		 => 'You must agree to the terms & conditions of this redemption to participate.',
		'subscribed_success' => 'You have participated in a redemption, please remember to upload your receipt.',
        'no_participate'    => 'You have no uncomplete redemptions, go ahead and participate in one right now.'
	],
    'promo_code' =>
    [
        'no_agreement'       => 'You must agree to the terms & conditions of this promo code to participate.',
        'participation_success' => 'You have participated in a promo code, please remember to upload your receipt.',
        'no_participate'    => 'You have no uncomplete promo codes, go ahead and participate in one right now.'
    ],
    'free_sample' =>
    [
        'no_agreement'       => 'You must agree to the terms & conditions of this free sample to participate.',
        'participation_success' => 'You have successfully participated in a free sample.',
        'no_participate'    => 'You have no uncomplete promo codes, go ahead and participate in one right now.',
    ]
];

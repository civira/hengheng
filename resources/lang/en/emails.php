<?php

return
[
	'titles' =>
	[
		'inquiry_help' 		=> '[Shop&Win] Contact Inquiry - Helps',
		'inquiry_general' 	=> '[Shop&Win] Contact Inquiry - General Information',
		'newsletter' 	=> ':0 - Newsletter Verification',
		'forgot_password' => ':0 - Forgot Password',
		'user_verification' => ':0 - User Verification',
		'member_ramayana_id' => ':0 - Member Ramayana ID',
		'member_update_email' => ':0 - Member Update Email',
		'member_update_reminder' => ':0 - Member Email Changed Notification: Your Ramayana Email has been updated.',
		'member_inquiry' => ':0 - Member Inquiry Message.',
		'redemption_receipt'	=> ':0 - Redemption Request.',
        'promo_code_receipt'    => ':0 - Promo Code Request.',
        'free_sample_request'    => ':0 - Free Sample Request.',
	]
];

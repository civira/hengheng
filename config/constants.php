<?php

return
[
	'backend_url' =>
	[
		'redemption_receipt' => env('BACKEND_BASE_URL', 'http://shopnwin.sg/hengcms').'/backend/heng/db/memberpromotionmaps/update/',
        'promo_code_receipt' => env('BACKEND_BASE_URL', 'http://shopnwin.sg/hengcms').'/backend/heng/db/memberpromocodemaps/update/',
        'free_sample_request' => env('BACKEND_BASE_URL', 'http://shopnwin.sg/hengcms').'/backend/heng/db/memberfreesamplerequests/update/'
	],
	'security' =>
	[
		// keyword = shopnwin
		'md5' => '2F53DCF6CD93CA22A67E86EE3C31A241' 
	]
];

<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => "Shop&Win", // set false to total remove
            'description'  => 'Win great prizes with your Fairprice, Giant, ShengSiong, Guardian, Watson receipts. Sign up once and take part in all the contests, promo codes & redeem free gifts. The more you take part, the higher the chances of winning. It’s so easy with Shopnwin.sg. Sign up today', // set false to total remove
            'separator'    => ' - ',
            'keywords'     => [],
            'canonical'    => false, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Shop&Win', // set false to total remove
            'description' => 'Win great prizes with your Fairprice, Giant, ShengSiong, Guardian, Watson receipts. Sign up once and take part in all the contests, promo codes & redeem free gifts. The more you take part, the higher the chances of winning. It’s so easy with Shopnwin.sg. Sign up today', // set false to total remove
            'url'         => false, // Set null for using Url::current(), set false to total remove
            'type'        => false,
            'site_name'   => false,
            'images'      => [
              'https://www.shopnwin.sg//apple-touch-icon-180x180.png'
            ],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          //'card'        => 'summary',
          //'site'        => '@LuizVinicius73',
        ],
    ],
];
